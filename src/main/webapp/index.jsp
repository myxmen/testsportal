<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <link rel="icon"
          href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'
             width='16' height='16' fill='green' class='bi bi-check-circle-fill' viewBox='0 0 16 16'>
    <path d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
     7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z'/>
    </svg>"/>
    <title>${text['index.title']}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

</head>
<body>
<%--<jsp:include page="header.jsp" />--%>
<%--<jsp:include page="navbar.jsp" />--%>
<!--header-->
<div class="container-fluid fixed-top bg-light">
    <header class="d-flex flex-wrap justify-content-center py-3 ">
        <div class="d-flex bg-light mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <i class="bi bi-check2-circle h1 "></i>
            <h2 class="fs-2">&nbsp;${text['index.logo']}</h2>
        </div>
        <div class="col-md-3 text-end">
            <a class="btn btn-link " href="/signup.jsp" role="button">${text['login.createacc']}</a>
            <a class="btn btn-outline-primary me-2" href="/login.jsp" role="button">${text['index.login']}</a>
            <%--                        <button href="#" type="button" class="btn btn-outline-primary me-2"> ${text['index.login']}</button>--%>
        </div>
    </header>
    <!--header-->
    <!--navbar-->
    <nav class="navbar navbar-expand-md navbar-dark bg-primary ">
        <div class="container-fluid align-items-center">
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item ">
                        <%--                                                <a class="nav-link" href="/">Home</a>--%>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center">
                <div class="col-xs-2 text-end">
                    <form class="form-inline my-2 my-lg-0">
                        <input type="hidden" name="sort" value="${sort}">
                        <input type="hidden" name="sortcolumn" value="${sortcolumn}">
                        <input type="hidden" name="pagination" value="${pagination}">
                        <input type="hidden" name="page" value="${page}">
                        <select id="language" name="language" class="form-select form-select-md  "
                                aria-label=".form-select-md"
                                onchange="submit()">
                            <option value="EN" ${language == 'EN' ? 'selected' : ''}>English</option>
                            <option value="RU" ${language == 'RU' ? 'selected' : ''}>Русский</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
    </nav>
    <!--navbar-->
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="container ">
    <div class="row d-flex justify-content-center align-items-center ">
        <div class="col-lg-12 col-xl-11">
            <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">${text['index.welcomesubject']}</p>
            <c:if test="${error == 'error_page'}">
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                         class="bi flex-shrink-0 me-2" aria-label="Danger:" viewBox="0 0 16 16">
                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                    </svg>
                    <div>
                            ${text['login.err.dbproblem']}<br/>
                        <b> ${errormessage}</b>
                    </div>
                </div>
            </c:if>
            <div class="card text-black" style="border-radius: 25px;">
                <div class="card-body p-md-5">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">
                                <c:if test="${sort == 'desc'}">
                                    <a href="?sort=asc&sortcolumn=subject&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['index.subject']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'subject'}">--%>
                                            <%--                                            <i class="bi bi-caret-up-fill" color: rgb(237, 95, 30);></i>--%>
                                            <%--                                        </c:if>--%>
                                        <i class="bi bi-caret-up-fill"  ${sortcolumn == 'subject' ? '' : 'style="color: transparent;"'}></i>
                                    </a>
                                </c:if>
                                <c:if test="${sort == 'asc'}">
                                    <a href="?sort=desc&sortcolumn=subject&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['index.subject']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'subject'}">--%>
                                            <%--                                            <i class="bi bi-caret-down-fill"></i>--%>
                                            <%--                                        </c:if>--%>
                                        <i class="bi bi-caret-down-fill" ${sortcolumn == 'subject' ? '' : 'style="color: transparent;"'}></i>
                                    </a>
                                </c:if>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${subject}" var="subjectone">
                            <tr>
                                <td>${subjectone.subject}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <!-- Pagination-->
                    <nav class="navbar ">

                        <div class="d-flex align-items-center">

                            <div>${text['index.show']}&nbsp;</div>

                            <div class="col-xs-2 text-end">
                                <form class="form-inline my-2 my-lg-0">
                                    <input type="hidden" name="sort" value="${sort}">
                                    <input type="hidden" name="sortcolumn" value="${sortcolumn}">
                                    <select id="pagination" name="pagination" class="form-select form-select-md  "
                                            aria-label=".form-select-md"
                                            onchange="submit()">
                                        <option value=5 ${pagination== 5 ?
                                                'selected' : ''}>5
                                        </option>
                                        <option value=10 ${pagination== 10 ?
                                                'selected' : ''}>10
                                        </option>
                                        <option value=15 ${pagination== 15 ?
                                                'selected' : ''}>15
                                        </option>
                                        <option value=20 ${pagination== 20 ?
                                                'selected' : ''}>20
                                        </option>
                                    </select>
                                </form>
                            </div>
                            <div>&nbsp;${text['index.entries']}</div>
                        </div>

                        <ul class="pagination justify-content-end">

                            <%--                            test--%>
                            <%--                            // Show the Previous button only if you are on a page other than the first--%>
                            <c:if test="${page > 1}">
                                <li class="page-item"><a class="page-link"
                                                         href="?pagination=${pagination}&page=${page-1}&sort=${sort}&sortcolumn=${sortcolumn}">&lsaquo;
                                        ${text['index.previous']}</a>
                                </li>
                            </c:if>
                            <%--                            // Show all the pagination elements if there are less than 6 pages total--%>
                            <c:choose>
                            <c:when test="${noofpages < 6}">
                                <%--                            <c:if test="${noofpages < 6}">--%>
                                <c:forEach begin="1" end="${noofpages}" var="i">
                                    <c:choose>
                                        <c:when test="${page eq i}">
                                            <li class="page-item active"><a class="page-link"> ${i} </a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li class="page-item"><a class="page-link"
                                                                     href="?pagination=${pagination}&page=${i}&sort=${sort}&sortcolumn=${sortcolumn}">${i}</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <%--                            </c:if>--%>
                            </c:when>
                                <%--                            // Use "..." to collapse pages outside of a certain range--%>
                            <c:otherwise>
                                <%--                            // Show the very first page followed by a "..." at the beginning of the--%>
                                <%--                            // pagination section (after the Previous button)--%>
                            <c:if test="${page > 2}">
                            <li class="page-item"><a class="page-link"
                                                     href="?pagination=${pagination}&page=1&sort=${sort}&sortcolumn=${sortcolumn}">1</a>
                                <c:if test="${page > 3}">
                            <li class="page-item"><a class="page-link"
                                                     href="?pagination=${pagination}&page=${page-2}&sort=${sort}&sortcolumn=${sortcolumn}">...</a>
                                </c:if>
                                </c:if>
                                    <c:set var="pageCutLow" value="${page-1}"/>
                                    <c:set var="pageCutHigh" value="${page+1}"/>
                                    <%--                                // Determine how many pages to show after the current page index--%>
                                <c:choose>
                                <c:when test="${page eq 1}">
                                    <c:set var="pageCutHigh" value="${pageCutHigh+2}"/>
                                </c:when>
                                <c:otherwise>
                                <c:if test="${page eq 2}">
                                    <c:set var="pageCutHigh" value="${pageCutHigh+1}"/>
                                </c:if>
                                </c:otherwise>
                                </c:choose>
                                    <%--                                // Determine how many pages to show before the current page index--%>
                                <c:choose>
                                <c:when test="${page eq noofpages}">
                                    <c:set var="pageCutLow" value="${pageCutLow-2}"/>
                                </c:when>
                                <c:otherwise>
                                <c:if test="${page eq noofpages-1}">
                                    <c:set var="pageCutLow" value="${pageCutLow-1}"/>
                                </c:if>
                                </c:otherwise>
                                </c:choose>
                                    <%--                                // Output the indexes for pages that fall inside the range of pageCutLow--%>
                                    <%--                                // and pageCutHigh--%>
                                    <c:set var="flagContinue" value="${pageCutLow-1}"/>
                                <c:forEach begin="${pageCutLow}" end="${pageCutHigh}" var="p">
                                    <%--                                //set continue--%>
                                    <c:set var="continueExecuting" scope="request" value="true"/>
                                <c:if test="${flagContinue != p}">
                                <c:if test="${p eq 0}">
                                    <c:set var="flagContinue" value="${p+1}"/>
                                    <c:set var="p" value="${p+1}"/>
                                </c:if>
                                <c:if test="${p > noofpages}">
                                    <%--                                // continue--%>
                                    <c:set var="continueExecuting" scope="request" value="false"/>
                                </c:if>
                                <c:if test="${continueExecuting}">
                                <c:choose>
                                <c:when test="${page eq p}">
                            <li class="page-item active"><a class="page-link"> ${p} </a></li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item"><a class="page-link"
                                                         href="?pagination=${pagination}&page=${p}&sort=${sort}&sortcolumn=${sortcolumn}">${p}</a>
                                </li>
                            </c:otherwise>
                            </c:choose>
                            </c:if>
                            </c:if>
                            </c:forEach>
                                <%--                            // Show the very last page preceded by a "..." at the end of the pagination--%>
                                <%--                            // section (before the Next button)--%>
                            <c:if test="${page < noofpages-1}">
                            <c:if test="${page < noofpages-2}">
                            <li class="page-item"><a class="page-link"
                                                     href="?pagination=${pagination}&page=${page+2}&sort=${sort}&sortcolumn=${sortcolumn}">...</a>
                                </c:if>
                            <li class="page-item"><a class="page-link"
                                                     href="?pagination=${pagination}&page=${noofpages}&sort=${sort}&sortcolumn=${sortcolumn}">${noofpages}</a>
                                </c:if>
                                </c:otherwise>
                                </c:choose>
                                <%--                                // Show the Next button only if you are on a page other than the last--%>
                                <c:if test="${page lt noofpages}">
                            <li class="page-item"><a class="page-link"
                                                     href="?pagination=${pagination}&page=${page+1}&sort=${sort}&sortcolumn=${sortcolumn}">${text['index.next']}
                                &rsaquo;</a>
                            </li>
                            </c:if>
                            <%--                            test--%>


                            <%--                            &lt;%&ndash;                            <li class="page-item disabled"><a class="page-link"&ndash;%&gt;--%>
                            <%--                            &lt;%&ndash;                                                              href="?pagelist=previous">&lsaquo;Previous</a></li>&ndash;%&gt;--%>
                            <%--                            <c:if test="${page != 1}">--%>
                            <%--                                <li class="page-item"><a class="page-link"--%>
                            <%--                                                         href="?pagination=${pagination}&page=${page-1}&sort=${sort}&sortcolumn=${sortcolumn}">&lsaquo;--%>
                            <%--                                        ${text['index.previous']}</a>--%>
                            <%--                                </li>--%>
                            <%--                            </c:if>--%>
                            <%--                            <c:forEach begin="1" end="${noofpages}" var="i">--%>
                            <%--                                <c:choose>--%>
                            <%--                                    <c:when test="${page eq i}">--%>
                            <%--                                        <li class="page-item active"><a class="page-link"> ${i} </a></li>--%>
                            <%--                                    </c:when>--%>
                            <%--                                    <c:otherwise>--%>
                            <%--                                        <li class="page-item"><a class="page-link"--%>
                            <%--                                                                 href="?pagination=${pagination}&page=${i}&sort=${sort}&sortcolumn=${sortcolumn}">${i}</a>--%>
                            <%--                                        </li>--%>
                            <%--                                    </c:otherwise>--%>
                            <%--                                </c:choose>--%>
                            <%--                            </c:forEach>--%>
                            <%--                            &lt;%&ndash;    <li class="page-item ${page== 2 ?'active' : ''}"> <a class="page-link" href="?page=2">2</a></li>&ndash;%&gt;--%>
                            <%--                            &lt;%&ndash;    <li class="page-item ${page== 3 ?'active' : ''}"><a class="page-link" href="?page=3">3</a></li>&ndash;%&gt;--%>
                            <%--                            &lt;%&ndash;                            <li class="page-item "><a class="page-link" href="?pagelist=next">Next &rsaquo;</a></li>&ndash;%&gt;--%>
                            <%--                            <c:if test="${page lt noofpages}">--%>
                            <%--                                <li class="page-item"><a class="page-link"--%>
                            <%--                                                         href="?pagination=${pagination}&page=${page+1}&sort=${sort}&sortcolumn=${sortcolumn}">${text['index.next']}--%>
                            <%--                                    &rsaquo;</a>--%>
                            <%--                                </li>--%>
                            <%--                            </c:if>--%>
                        </ul>
                    </nav>
                    <!-- Pagination-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="page-footer font-small ">

    <%--    <!-- Copyright -->--%>
    <%--    <div class="footer-copyright text-center py-3">© 2022 Copyright:--%>
    <%--        <a href="mailto:leonid.vasyliev2021@gmail.com"> Vasyliev Leonid</a>--%>
    <%--    </div>--%>
    <%--    <!-- Copyright -->--%>
    <jsp:include page="footer.jsp"/>
</footer>

<!-- Footer -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>
