<<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <link rel="icon"
          href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'
             width='16' height='16' fill='red' class='bi bi-check-circle-fill' viewBox='0 0 16 16'>
    <path d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
     7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z'/>
    </svg>"/>
    <title>Error page</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Cardo&family=Lemonada:wght@300&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            background-color: #fd7e14;
            font-family: 'Cardo', serif;
        }

        .row {
            margin: 0;
        }

        a {
            background-color: #ff0000;
            border: 0px;
            text-decoration: none;
            padding: 10px;
            color: #fff;
            font-family: 'Lemonada', cursive;
        }

        a:hover {
            text-decoration: none;
            color: #fff;
            background-color: #0c2461;
        }

        h1 {
            font-size: 180px;
        }

        h4 {
            padding-bottom: 20px;
            font-family: 'Lemonada', cursive;
        }

        p {
            font-family: 'Lemonada', cursive;
            text-align: center;
        }
    </style>
</head>

<body>
<div class="row">
    <div class="col-md-12 d-flex flex-column justify-content-center align-items-center text-white vh-100">
        <h1>404</h1>
        <h4>Page not found</h4>
        <p>Oops! The page you are looking for does not exist. It might have been moved or deleted.</p>
        <a href="/">Back To Home</a>
    </div>
</div>
</body>

</html>