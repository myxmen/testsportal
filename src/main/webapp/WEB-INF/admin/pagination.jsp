<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<nav class="navbar ">
    <div class="d-flex align-items-center">

        <div>${text['index.show']}&nbsp;</div>

        <div class="col-xs-2 text-end">
            <form class="form-inline my-2 my-lg-0">
                <input type="hidden" name="sort" value="${sort}">
                <input type="hidden" name="sortcolumn" value="${sortcolumn}">
                <%--                                <c:set var="testid" scope="session" value="${testid}" />--%>
                <%--                                <c:set var="subjectid" scope="session" value="${subjectid}" />--%>

                <c:if test="${not empty testid}">
                    <input type="hidden" name="testid" value="${testid}">
                </c:if>
                <c:if test="${not empty subjectid}">
                    <input type="hidden" name="subjectid" value="${subjectid}">
                </c:if>
                <select id="pagination" name="pagination" class="form-select form-select-md  "
                        aria-label=".form-select-md"
                        onchange="submit()">
                    <option value=5 ${pagination== 5 ?
                            'selected' : ''}>5
                    </option>
                    <option value=10 ${pagination== 10 ?
                            'selected' : ''}>10
                    </option>
                    <option value=15 ${pagination== 15 ?
                            'selected' : ''}>15
                    </option>
                    <option value=20 ${pagination== 20 ?
                            'selected' : ''}>20
                    </option>
                </select>
            </form>
        </div>
        <div>&nbsp;${text['index.entries']}</div>
    </div>

    <ul class="pagination justify-content-end">
        <c:if test="${not empty testid}">
            <c:set var="testidex" scope="page" value="&testid=${testid}"/>
        </c:if>
        <c:if test="${not empty subjectid}">
            <c:set var="subjectidex" scope="page" value="&subjectid=${subjectid}"/>
        </c:if>
        <%--                <input type="hidden" name="testidex" value="&testid=${testid}">--%>
        <%--        <input type="hidden" name="subjectid" value="${subjectid}">--%>
        <%--                            test--%>
        <%--                            // Show the Previous button only if you are on a page other than the first--%>
        <c:if test="${page > 1}">
            <li class="page-item"><a class="page-link"
                                     href="?pagination=${pagination}&page=${page-1}&sort=${sort}&sortcolumn=${sortcolumn}${subjectidex}${testidex}">&lsaquo;
                    ${text['index.previous']}</a>
            </li>
        </c:if>
        <%--                            // Show all the pagination elements if there are less than 6 pages total--%>
        <c:choose>
        <c:when test="${noofpages < 6}">
            <%--                            <c:if test="${noofpages < 6}">--%>
            <c:forEach begin="1" end="${noofpages}" var="i">
                <c:choose>
                    <c:when test="${page eq i}">
                        <%--                        <input type="hidden" name="testid" value="${testid}">--%>
                        <%--                        <input type="hidden" name="subjectid" value="${subjectid}">--%>
                        <li class="page-item active"><a class="page-link"> ${i} </a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-item"><a class="page-link"
                                                 href="?pagination=${pagination}&page=${i}&sort=${sort}&sortcolumn=${sortcolumn}${subjectidex}${testidex}">${i}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <%--                            </c:if>--%>
        </c:when>
            <%--                            // Use "..." to collapse pages outside of a certain range--%>
        <c:otherwise>
            <%--                            // Show the very first page followed by a "..." at the beginning of the--%>
            <%--                            // pagination section (after the Previous button)--%>
        <c:if test="${page > 2}">
        <li class="page-item"><a class="page-link"
                                 href="?pagination=${pagination}&page=1&sort=${sort}&sortcolumn=${sortcolumn}${subjectidex}${testidex}">1</a>
            <c:if test="${page > 3}">
        <li class="page-item"><a class="page-link"
                                 href="?pagination=${pagination}&page=${page-2}&sort=${sort}&sortcolumn=${sortcolumn}${subjectidex}${testidex}">...</a>
            </c:if>
            </c:if>
                <c:set var="pageCutLow" value="${page-1}"/>
                <c:set var="pageCutHigh" value="${page+1}"/>
                <%--                                // Determine how many pages to show after the current page index--%>
            <c:choose>
            <c:when test="${page eq 1}">
                <c:set var="pageCutHigh" value="${pageCutHigh+2}"/>
            </c:when>
            <c:otherwise>
            <c:if test="${page eq 2}">
                <c:set var="pageCutHigh" value="${pageCutHigh+1}"/>
            </c:if>
            </c:otherwise>
            </c:choose>
                <%--                                // Determine how many pages to show before the current page index--%>
            <c:choose>
            <c:when test="${page eq noofpages}">
                <c:set var="pageCutLow" value="${pageCutLow-2}"/>
            </c:when>
            <c:otherwise>
            <c:if test="${page eq noofpages-1}">
                <c:set var="pageCutLow" value="${pageCutLow-1}"/>
            </c:if>
            </c:otherwise>
            </c:choose>
                <%--                                // Output the indexes for pages that fall inside the range of pageCutLow--%>
                <%--                                // and pageCutHigh--%>
                <c:set var="flagContinue" value="${pageCutLow-1}"/>
            <c:forEach begin="${pageCutLow}" end="${pageCutHigh}" var="p">
                <%--                                //set continue--%>
                <c:set var="continueExecuting" scope="request" value="true"/>
            <c:if test="${flagContinue != p}">
            <c:if test="${p eq 0}">
                <c:set var="flagContinue" value="${p+1}"/>
                <c:set var="p" value="${p+1}"/>
            </c:if>
            <c:if test="${p > noofpages}">
                <%--                                // continue--%>
                <c:set var="continueExecuting" scope="request" value="false"/>
            </c:if>
            <c:if test="${continueExecuting}">
            <c:choose>
            <c:when test="${page eq p}">
        <li class="page-item active"><a class="page-link"> ${p} </a></li>
        </c:when>
        <c:otherwise>
            <li class="page-item"><a class="page-link"
                                     href="?pagination=${pagination}&page=${p}&sort=${sort}&sortcolumn=${sortcolumn}${subjectidex}${testidex}">${p}</a>
            </li>
        </c:otherwise>
        </c:choose>
        </c:if>
        </c:if>
        </c:forEach>
            <%--                            // Show the very last page preceded by a "..." at the end of the pagination--%>
            <%--                            // section (before the Next button)--%>
        <c:if test="${page < noofpages-1}">
        <c:if test="${page < noofpages-2}">
        <li class="page-item"><a class="page-link"
                                 href="?pagination=${pagination}&page=${page+2}&sort=${sort}&sortcolumn=${sortcolumn}${subjectidex}${testidex}">...</a>
            </c:if>
        <li class="page-item"><a class="page-link"
                                 href="?pagination=${pagination}&page=${noofpages}&sort=${sort}&sortcolumn=${sortcolumn}${subjectidex}${testidex}">${noofpages}</a>
            </c:if>
            </c:otherwise>
            </c:choose>
            <%--                                // Show the Next button only if you are on a page other than the last--%>
            <c:if test="${page lt noofpages}">
        <li class="page-item"><a class="page-link"
                                 href="?pagination=${pagination}&page=${page+1}&sort=${sort}&sortcolumn=${sortcolumn}${subjectidex}${testidex}">${text['index.next']}
            &rsaquo;</a>
        </li>
        </c:if>
        <%--                            test--%>

    </ul>
</nav>
</html>

