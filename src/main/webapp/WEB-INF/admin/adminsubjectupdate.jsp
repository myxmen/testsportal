<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="true" %>

<html>
<head>
    <link rel="icon"
          href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'
             width='16' height='16' fill='green' class='bi bi-check-circle-fill' viewBox='0 0 16 16'>
    <path d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
     7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z'/>
    </svg>"/>
    <title>${text['admin.title']}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

</head>
<body>
<!--header-->
<div class="container-fluid fixed-top bg-light">
    <header class="d-flex flex-wrap justify-content-center py-3 ">
        <div class="d-flex bg-light mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <i class="bi bi-check2-circle h1 "></i>
            <h2 class="fs-2">&nbsp;${text['index.logo']}</h2>
        </div>
        <div class="col-md-3 text-end">
            <a class="btn btn-link " href="/admin/edit" role="button"><b><c:out
                    value="${sessionScope.user.username}"/></b></a>
            <a class="btn btn-outline-primary me-2" href="/logout" role="button">${text['btn.logout']}</a>
            <%--                        <button href="#" type="button" class="btn btn-outline-primary me-2"> ${text['index.login']}</button>--%>
        </div>
    </header>

    <!--navbar-->
    <nav class="navbar navbar-expand-md navbar-dark bg-primary ">
        <div class="container-fluid align-items-center">
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="/admin/subject">${text['admin.back']}</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center">
                <div class="col-xs-2 text-end">
                    <form class="form-inline my-2 my-lg-0">
                        <%--                        <input type="hidden" name="sort" value="${sort}">--%>
                        <%--                        <input type="hidden" name="sortcolumn" value="${sortcolumn}">--%>
                        <%--                        <input type="hidden" name="pagination" value="${pagination}">--%>
                        <%--                        <input type="hidden" name="page" value="${page}">--%>
                        <input type="hidden" name="id" value="${id}">
                        <select id="language" name="language" class="form-select form-select-md  "
                                aria-label=".form-select-md"
                                onchange="submit()">
                            <option value="EN" ${language == 'EN' ? 'selected' : ''}>English</option>
                            <option value="RU" ${language == 'RU' ? 'selected' : ''}>Русский</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>

    </nav>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="container ">
    <div class="row d-flex justify-content-center align-items-center ">
        <div class="col-xl-12 col-xl-11">
            <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">${text['admin.subject.update.welcome']}</p>
            <c:if test="${error == 'error_page'}">
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                         class="bi flex-shrink-0 me-2" aria-label="Danger:" viewBox="0 0 16 16">
                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                    </svg>
                    <div>
                            ${text['user.subject.err.dbproblem']}<br/>
                        <b> ${errormessage}</b>
                    </div>
                </div>
            </c:if>
            <form class="mx-1 mx-md-4" method="post" action="/admin/subjectupdate">
                <input type="hidden" name="id" value="${id}">
                <div class="card text-black" style="border-radius: 25px;">
                    <div class="card-body p-md-5">

                        <%--                        <div class="mb-3">--%>
                        <label for="subject" class="form-label">${text['index.subject']}</label>
                        <input type="text" id="subject" name="subject" maxlength="127"
                               class="form-control ${error == 'invalid_subject' ? 'is-invalid' : ''}"
                               ria-describedby="validationServer01Feedback" required
                               value="${subjectone.subject}"/>
                        <div id="validationServer01Feedback" class="invalid-feedback">
                            <b> ${errormessage}</b>
                            <%--                            <c:if test="${error == 'error_page'}">--%>
                            <%--                            </c:if>--%>
                            <c:choose>
                                <c:when test="${val == 'validation'}">
                                    ${text['admin.subject.err.validate']}
                                </c:when>
<%--                                <c:when test="${validation != 'on'}">--%>
<%--                                    ${text['admin.subject.err.subpresent']}--%>
<%--                                </c:when>--%>
                                <c:otherwise>
                                    ${text['admin.subject.err.present']}
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <%--                        </div>--%>
                        <br/>

                        <p class="h4 lh-sm">${text['admin.subject.delete.tests']} ${tests}</p>
                        <%--                        <form class="mx-1 mx-md-4" method="post" action="/admin/subjectdelete">--%>
                        <%--                            <input type="hidden" name="id" value="${id}">--%>
                        <%--                            <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">--%>
                        <%--                                <input type="submit" class="btn btn-danger btn-lg" name="submit"--%>
                        <%--                                       value="${text['admin.subject.delete.delete']}">--%>
                        <%--                                &lt;%&ndash;                                        <button type="button" class="btn btn-primary btn-lg">Sign in</button>&ndash;%&gt;--%>
                        <%--                            </div>--%>
                        <%--                        </form>--%>
                        <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                            <input type="submit" class="btn btn-primary btn-lg" name="submit"
                                   value="${text['admin.edit.update']}">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Footer -->
<%--<footer class="page-footer font-small ">--%>

<%--    <!-- Copyright -->--%>
<%--    <div class="footer-copyright text-center py-3">© 2022 Copyright:--%>
<%--        <a href="mailto:leonid.vasyliev2021@gmail.com"> Vasyliev Leonid</a>--%>
<%--    </div>--%>
<%--    <!-- Copyright -->--%>

<%--</footer>--%>
<jsp:include page="footer.jsp"/>
<!-- Footer -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

</body>
</html>