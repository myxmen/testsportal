<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="true" %>

<html>
<head>
    <link rel="icon"
          href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'
             width='16' height='16' fill='green' class='bi bi-check-circle-fill' viewBox='0 0 16 16'>
    <path d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
     7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z'/>
    </svg>"/>
    <title>${text['admin.title']}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

</head>
<body>
<!--header-->
<div class="container-fluid fixed-top bg-light">
    <header class="d-flex flex-wrap justify-content-center py-3 ">
        <div class="d-flex bg-light mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <i class="bi bi-check2-circle h1 "></i>
            <h2 class="fs-2">&nbsp;${text['index.logo']}</h2>
        </div>
        <div class="col-md-3 text-end">
            <a class="btn btn-link " href="/admin/edit" role="button"><b><c:out
                    value="${sessionScope.user.username}"/></b></a>
            <a class="btn btn-outline-primary me-2" href="/logout" role="button">${text['btn.logout']}</a>
            <%--                        <button href="#" type="button" class="btn btn-outline-primary me-2"> ${text['index.login']}</button>--%>
        </div>
    </header>

    <!--navbar-->
    <nav class="navbar navbar-expand-md navbar-dark bg-primary ">
        <div class="container-fluid align-items-center">
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="/admin/subject">${text['admin.back']}</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center">
                <div class="col-xs-2 text-end">
                    <form class="form-inline my-2 my-lg-0">
                        <input type="hidden" name="sort" value="${sort}">
                        <input type="hidden" name="sortcolumn" value="${sortcolumn}">
                        <input type="hidden" name="pagination" value="${pagination}">
                        <input type="hidden" name="page" value="${page}">
                        <input type="hidden" name="subjectid" value="${subjectid}">
                        <%--                        <input type="hidden" name="subject" value="${subject}">--%>
                        <select id="language" name="language" class="form-select form-select-md  "
                                aria-label=".form-select-md"
                                onchange="submit()">
                            <option value="EN" ${language == 'EN' ? 'selected' : ''}>English</option>
                            <option value="RU" ${language == 'RU' ? 'selected' : ''}>Русский</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>

    </nav>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="container ">
    <div class="row d-flex justify-content-center align-items-center ">
        <div class="col-xl-12 col-xl-11">
            <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">${text['admin.test.subject']} ${subject}</p>
            <%--            <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">${subject}</p>--%>
            <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">${text['admin.test.welcome']}</p>
            <c:if test="${error == 'error_page'}">
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                         class="bi flex-shrink-0 me-2" aria-label="Danger:" viewBox="0 0 16 16">
                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                    </svg>
                    <div>
                            ${text['user.subject.err.dbproblem']}<br/>
                        <b> ${errormessage}</b>
                    </div>
                </div>
            </c:if>
            <%--            <div class="card text-black" style="border-radius: 25px;">--%>
            <%--                <div class="card-body p-md-5">--%>
            <%--                <div class="card-body p-lg-5">--%>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">
                        <c:if test="${sort == 'desc'}">
                            <a href="?sort=asc&sortcolumn=test&pagination=${pagination}&subjectid=${subjectid}"
                               class="btn btn-xs btn-link p-0"><b>${text['user.column.test']}</b>
                                <i class="bi bi-caret-up-fill" ${sortcolumn == 'test' ? '' : 'style="color: transparent;"'}></i>
                            </a>
                        </c:if>
                        <c:if test="${sort == 'asc'}">
                            <a href="?sort=desc&sortcolumn=test&pagination=${pagination}&subjectid=${subjectid}"
                               class="btn btn-xs btn-link p-0"><b>${text['user.column.test']}</b>
                                <i class="bi bi-caret-down-fill" ${sortcolumn == 'test' ? '' : 'style="color: transparent;"'}></i>
                            </a>
                        </c:if>
                    </th>
                    <th scope="col">
                        <c:if test="${sort == 'desc'}">
                            <a href="?sort=asc&sortcolumn=difficulty&pagination=${pagination}&subjectid=${subjectid}"
                               class="btn btn-xs btn-link p-0"><b>${text['user.column.difficulty']}</b>
                                <i class="bi bi-caret-up-fill" ${sortcolumn == 'difficulty' ? '' : 'style="color: transparent;"'}></i>
                            </a>
                        </c:if>
                        <c:if test="${sort == 'asc'}">
                            <a href="?sort=desc&sortcolumn=difficulty&pagination=${pagination}&subjectid=${subjectid}"
                               class="btn btn-xs btn-link p-0"><b>${text['user.column.difficulty']}</b>
                                <i class="bi bi-caret-down-fill" ${sortcolumn == 'difficulty' ? '' : 'style="color: transparent;"'}></i>
                            </a>
                        </c:if>
                    </th>
                    <th scope="col">
                        <c:if test="${sort == 'desc'}">
                            <a href="?sort=asc&sortcolumn=requests&pagination=${pagination}&subjectid=${subjectid}"
                               class="btn btn-xs btn-link p-0"><b>${text['user.column.requests']}</b>
                                <i class="bi bi-caret-up-fill" ${sortcolumn == 'requests' ? '' : 'style="color: transparent;"'}></i>
                            </a>
                        </c:if>
                        <c:if test="${sort == 'asc'}">
                            <a href="?sort=desc&sortcolumn=requests&pagination=${pagination}&subjectid=${subjectid}"
                               class="btn btn-xs btn-link p-0"><b>${text['user.column.requests']}</b>
                                <i class="bi bi-caret-down-fill" ${sortcolumn == 'requests' ? '' : 'style="color: transparent;"'}></i>
                            </a>
                        </c:if>
                    </th>
                    <th scope="col">
                        <c:if test="${sort == 'desc'}">
                            <a href="?sort=asc&sortcolumn=time&pagination=${pagination}&subjectid=${subjectid}"
                               class="btn btn-xs btn-link p-0"><b>${text['user.column.time']}</b>
                                <i class="bi bi-caret-up-fill" ${sortcolumn == 'time' ? '' : 'style="color: transparent;"'}></i>
                            </a>
                        </c:if>
                        <c:if test="${sort == 'asc'}">
                            <a href="?sort=desc&sortcolumn=time&pagination=${pagination}&subjectid=${subjectid}"
                               class="btn btn-xs btn-link p-0"><b>${text['user.column.time']}</b>
                                <i class="bi bi-caret-down-fill" ${sortcolumn == 'time' ? '' : 'style="color: transparent;"'}></i>
                            </a>
                        </c:if>
                    </th>
                    <th scope="col">${text['admin.test.update']}</th>
                    <th scope="col">${text['admin.test.delete']}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${test}" var="testone">
                    <tr>
                        <td><a class="text-decoration-none" href="/admin/question?testid=${testone.id}">${testone.test}</a>
                        </td>
                        <td><a class="text-decoration-none"
                               href="/admin/question?testid=${testone.id}">${testone.difficulty}</a></td>
                        <td><a class="text-decoration-none"
                               href="/admin/question?testid=${testone.id}">${testone.requests}</a></td>
                        <td><a class="text-decoration-none"
                               href="/admin/question?testid=${testone.id}">${testone.time}</a></td>
                            <%--                                <td><a class="text-decoration-none"                                       --%>
                            <%--                                       href="/admin/test?id=${subjectone.id}">${subjectone.subject}</a></td>--%>
                        <td>
                                <%--                                    <c:if test="${userone.id != sessionScope.user.id}">--%>
                            <div class="form-button">
                                    <%--                                        <button type="button" class="btn btn-default btn-sm btn-primary"--%>
                                    <%--                                                onclick="editdata('#')">--%>
                                <a href="/admin/testupdate?testid=${testone.id}&subjectid=${subjectid}"
                                   class="btn btn-default btn-sm btn-primary" role="button"
                                   aria-pressed="true">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                         fill="currentColor" class="bi bi-pencil-square"
                                         viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd"
                                              d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                    <!--                                        <span class="glyphicon glyphicon-edit"></span>-->
                                </a>
                                    <%--                                        </button>--%>
                            </div>
                                <%--                                    </c:if>--%>
                        </td>
                        <td>
                                <%--                                    <c:if test="${userone.id != sessionScope.user.id}">--%>
                            <div class="form-button">
                                    <%--                                        <button type="button" class="btn btn-default btn-sm btn-danger"--%>
                                    <%--                                                onclick="deletedata('dfgfdghfgh');">--%>
                                <a href="/admin/testdelete?testid=${testone.id}&subjectid=${subjectid}"
                                   class="btn btn-default btn-sm btn-danger"
                                   role="button"
                                   aria-pressed="true">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                         fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                        <path fill-rule="evenodd"
                                              d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                    </svg>
                                    <!--                                    <span class="glyphicon glyphicon-trash"></span>-->
                                </a>
                                    <%--                                        </button>--%>
                            </div>
                                <%--                                    </c:if>--%>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <!-- Pagination-->
<%--            <nav class="navbar ">--%>
<%--                <div class="d-flex align-items-center">--%>
<%--                    <div>${text['index.show']}&nbsp;</div>--%>
<%--                    <div class="col-xs-2 text-end">--%>
<%--                        <form class="form-inline my-2 my-lg-0">--%>
<%--                            <input type="hidden" name="sort" value="${sort}">--%>
<%--                            <input type="hidden" name="sortcolumn" value="${sortcolumn}">--%>
<%--                            <input type="hidden" name="subjectid" value="${subjectid}">--%>
<%--                            <select id="pagination" name="pagination" class="form-select form-select-md  "--%>
<%--                                    aria-label=".form-select-md"--%>
<%--                                    onchange="submit()">--%>
<%--                                <option value=5 ${pagination== 5 ?--%>
<%--                                        'selected' : ''}>5--%>
<%--                                </option>--%>
<%--                                <option value=10 ${pagination== 10 ?--%>
<%--                                        'selected' : ''}>10--%>
<%--                                </option>--%>
<%--                                <option value=15 ${pagination== 15 ?--%>
<%--                                        'selected' : ''}>15--%>
<%--                                </option>--%>
<%--                                <option value=20 ${pagination== 20 ?--%>
<%--                                        'selected' : ''}>20--%>
<%--                                </option>--%>
<%--                            </select>--%>
<%--                        </form>--%>
<%--                    </div>--%>
<%--                    <div>&nbsp;${text['index.entries']}</div>--%>
<%--                </div>--%>

<%--                <ul class="pagination justify-content-end">--%>
<%--                    <c:if test="${page != 1}">--%>
<%--                        <li class="page-item"><a class="page-link"--%>
<%--                                                 href="?pagination=${pagination}&page=${page-1}&sort=${sort}&sortcolumn=${sortcolumn}">&lsaquo;--%>
<%--                                ${text['index.previous']}</a>--%>
<%--                        </li>--%>
<%--                    </c:if>--%>
<%--                    <c:forEach begin="1" end="${noofpages}" var="i">--%>
<%--                        <c:choose>--%>
<%--                            <c:when test="${page eq i}">--%>
<%--                                <li class="page-item active"><a class="page-link"> ${i} </a></li>--%>
<%--                            </c:when>--%>
<%--                            <c:otherwise>--%>
<%--                                <li class="page-item"><a class="page-link"--%>
<%--                                                         href="?pagination=${pagination}&page=${i}&sort=${sort}&sortcolumn=${sortcolumn}">${i}</a>--%>
<%--                                </li>--%>
<%--                            </c:otherwise>--%>
<%--                        </c:choose>--%>
<%--                    </c:forEach>--%>
<%--                    <c:if test="${page lt noofpages}">--%>
<%--                        <li class="page-item"><a class="page-link"--%>
<%--                                                 href="?pagination=${pagination}&page=${page+1}&sort=${sort}&sortcolumn=${sortcolumn}">${text['index.next']}--%>
<%--                            &rsaquo;</a>--%>
<%--                        </li>--%>
<%--                    </c:if>--%>
<%--                </ul>--%>
<%--            </nav>--%>
            <jsp:include page="pagination.jsp"/>
            <!-- Pagination-->
            <div class="form-button d-flex justify-content-center mx-4 mb-3 mb-lg-1">
                <a href="/admin/testadd?subjectid=${subjectid}"
                   class="btn btn-default btn-md btn-primary" role="button"
                   aria-pressed="true">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                         class="bi bi-plus-lg" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                              d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                    </svg>
                    ${text['admin.test.add']}
                </a>
            </div>
            <%--                </div>--%>
            <%--            </div>--%>
        </div>
    </div>
</div>
<!-- Footer -->
<%--<footer class="page-footer font-small ">--%>

<%--    <!-- Copyright -->--%>
<%--    <div class="footer-copyright text-center py-3">© 2022 Copyright:--%>
<%--        <a href="mailto:leonid.vasyliev2021@gmail.com"> Vasyliev Leonid</a>--%>
<%--    </div>--%>
<%--    <!-- Copyright -->--%>

<%--</footer>--%>
<jsp:include page="footer.jsp"/>
<!-- Footer -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

</body>
</html>