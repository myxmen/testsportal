<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="true" %>

<html>
<head>
    <link rel="icon"
          href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'
             width='16' height='16' fill='green' class='bi bi-check-circle-fill' viewBox='0 0 16 16'>
    <path d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
     7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z'/>
    </svg>"/>
    <title>${text['user.title']}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

</head>
<body>
<!--header-->
<div class="container-fluid fixed-top bg-light">
    <header class="d-flex flex-wrap justify-content-center py-3 ">
        <div class="d-flex bg-light mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <i class="bi bi-check2-circle h1 "></i>
            <h2 class="fs-2">&nbsp;${text['index.logo']}</h2>
        </div>
        <div class="col-md-3 text-end">
            <a class="btn btn-link " href="/user/edit" role="button"><b><c:out
                    value="${sessionScope.user.username}"/></b></a>
            <a class="btn btn-outline-primary me-2" href="/logout" role="button">${text['btn.logout']}</a>
            <%--                        <button href="#" type="button" class="btn btn-outline-primary me-2"> ${text['index.login']}</button>--%>
        </div>
    </header>

    <!--navbar-->
    <nav class="navbar navbar-expand-md navbar-dark bg-primary ">
        <div class="container-fluid align-items-center">
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="/user">${text['login.home']}</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="/user/subject">${text['user.test.subject']}</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center">
                <div class="col-xs-2 text-end">
                    <form class="form-inline my-2 my-lg-0">
                        <input type="hidden" name="sort" value="${sort}">
                        <input type="hidden" name="sortcolumn" value="${sortcolumn}">
                        <input type="hidden" name="pagination" value="${pagination}">
                        <input type="hidden" name="page" value="${page}">
                        <select id="language" name="language" class="form-select form-select-md  "
                                aria-label=".form-select-md"
                                onchange="submit()">
                            <option value="EN" ${language == 'EN' ? 'selected' : ''}>English</option>
                            <option value="RU" ${language == 'RU' ? 'selected' : ''}>Русский</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>

    </nav>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="container ">
    <div class="row d-flex justify-content-center align-items-center ">
        <div class="col-lg-12 col-xl-11">
            <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">${text['user.test.welcome']}
                <c:out value="${sessionScope.subject.subject}"/></p>
            <c:if test="${error == 'error_page'}">
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                         class="bi flex-shrink-0 me-2" aria-label="Danger:" viewBox="0 0 16 16">
                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                    </svg>
                    <div>
                            ${text['user.test.err.dbproblem']}<br/>
                        <b> ${errormessage}</b>
                    </div>
                </div>
            </c:if>
            <div class="card text-black" style="border-radius: 25px;">
                <div class="card-body p-md-5">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th scope="col">
                                <c:if test="${sort == 'desc'}">
                                    <a href="?sort=asc&sortcolumn=test&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['user.column.test']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'test'}">--%>
                                        <i class="bi bi-caret-up-fill" ${sortcolumn == 'test' ? '' : 'style="color: transparent;"'}></i>
                                            <%--                                        </c:if>--%>
                                    </a>
                                </c:if>
                                <c:if test="${sort == 'asc'}">
                                    <a href="?sort=desc&sortcolumn=test&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['user.column.test']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'test'}">--%>
                                        <i class="bi bi-caret-down-fill" ${sortcolumn == 'test' ? '' : 'style="color: transparent;"'}></i>
                                            <%--                                        </c:if>--%>
                                    </a>
                                </c:if>
                            </th>
                            <th scope="col">
                                <c:if test="${sort == 'desc'}">
                                    <a href="?sort=asc&sortcolumn=difficulty&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['user.column.difficulty']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'difficulty'}">--%>
                                        <i class="bi bi-caret-up-fill" ${sortcolumn == 'difficulty' ? '' : 'style="color: transparent;"'}></i>
                                            <%--                                        </c:if>--%>
                                    </a>
                                </c:if>
                                <c:if test="${sort == 'asc'}">
                                    <a href="?sort=desc&sortcolumn=difficulty&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['user.column.difficulty']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'difficulty'}">--%>
                                        <i class="bi bi-caret-down-fill" ${sortcolumn == 'difficulty' ? '' : 'style="color: transparent;"'}></i>
                                            <%--                                        </c:if>--%>
                                    </a>
                                </c:if>
                            </th>
                            <th scope="col">
                                <c:if test="${sort == 'desc'}">
                                    <a href="?sort=asc&sortcolumn=requests&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['user.column.requests']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'requests'}">--%>
                                        <i class="bi bi-caret-up-fill" ${sortcolumn == 'requests' ? '' : 'style="color: transparent;"'}></i>
                                            <%--                                        </c:if>--%>
                                    </a>
                                </c:if>
                                <c:if test="${sort == 'asc'}">
                                    <a href="?sort=desc&sortcolumn=requests&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['user.column.requests']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'requests'}">--%>
                                        <i class="bi bi-caret-down-fill" ${sortcolumn == 'requests' ? '' : 'style="color: transparent;"'}></i>
                                            <%--                                        </c:if>--%>
                                    </a>
                                </c:if>
                            </th>
                            <th scope="col">
                                <c:if test="${sort == 'desc'}">
                                    <a href="?sort=asc&sortcolumn=time&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['user.column.time']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'time'}">--%>
                                        <i class="bi bi-caret-up-fill" ${sortcolumn == 'time' ? '' : 'style="color: transparent;"'}></i>
                                            <%--                                        </c:if>--%>
                                    </a>
                                </c:if>
                                <c:if test="${sort == 'asc'}">
                                    <a href="?sort=desc&sortcolumn=time&pagination=${pagination}"
                                       class="btn btn-xs btn-link p-0"><b>${text['user.column.time']}</b>
                                            <%--                                        <c:if test="${sortcolumn == 'time'}">--%>
                                        <i class="bi bi-caret-down-fill" ${sortcolumn == 'time' ? '' : 'style="color: transparent;"'}></i>
                                            <%--                                        </c:if>--%>
                                    </a>
                                </c:if>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${test}" var="testone">
                            <tr>
                                <td><a class="nav-link" href="?id=${testone.id}">${testone.test}</a></td>
                                <td><a class="nav-link" href="?id=${testone.id}">${testone.difficulty}</a></td>
                                <td><a class="nav-link" href="?id=${testone.id}">${testone.requests}</a></td>
                                <td><a class="nav-link" href="?id=${testone.id}">${testone.time}</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <!-- Pagination-->
<%--                    <nav class="navbar ">--%>
<%--                        <div class="d-flex align-items-center">--%>
<%--                            <div>${text['index.show']}&nbsp;</div>--%>
<%--                            <div class="col-xs-2 text-end">--%>
<%--                                <form class="form-inline my-2 my-lg-0">--%>
<%--                                    <input type="hidden" name="sort" value="${sort}">--%>
<%--                                    <input type="hidden" name="sortcolumn" value="${sortcolumn}">--%>
<%--                                    <select id="pagination" name="pagination" class="form-select form-select-md  "--%>
<%--                                            aria-label=".form-select-md"--%>
<%--                                            onchange="submit()">--%>
<%--                                        <option value=5 ${pagination== 5 ?--%>
<%--                                                'selected' : ''}>5--%>
<%--                                        </option>--%>
<%--                                        <option value=10 ${pagination== 10 ?--%>
<%--                                                'selected' : ''}>10--%>
<%--                                        </option>--%>
<%--                                        <option value=15 ${pagination== 15 ?--%>
<%--                                                'selected' : ''}>15--%>
<%--                                        </option>--%>
<%--                                        <option value=20 ${pagination== 20 ?--%>
<%--                                                'selected' : ''}>20--%>
<%--                                        </option>--%>
<%--                                    </select>--%>
<%--                                </form>--%>
<%--                            </div>--%>
<%--                            <div>&nbsp;${text['index.entries']}</div>--%>
<%--                        </div>--%>

<%--                        <ul class="pagination justify-content-end">--%>
<%--                            <c:if test="${page != 1}">--%>
<%--                                <li class="page-item"><a class="page-link"--%>
<%--                                                         href="?pagination=${pagination}&page=${page-1}&sort=${sort}&sortcolumn=${sortcolumn}">&lsaquo;--%>
<%--                                        ${text['index.previous']}</a>--%>
<%--                                </li>--%>
<%--                            </c:if>--%>
<%--                            <c:forEach begin="1" end="${noofpages}" var="i">--%>
<%--                                <c:choose>--%>
<%--                                    <c:when test="${page eq i}">--%>
<%--                                        <li class="page-item active"><a class="page-link"> ${i} </a></li>--%>
<%--                                    </c:when>--%>
<%--                                    <c:otherwise>--%>
<%--                                        <li class="page-item"><a class="page-link"--%>
<%--                                                                 href="?pagination=${pagination}&page=${i}&sort=${sort}&sortcolumn=${sortcolumn}">${i}</a>--%>
<%--                                        </li>--%>
<%--                                    </c:otherwise>--%>
<%--                                </c:choose>--%>
<%--                            </c:forEach>--%>
<%--                            <c:if test="${page lt noofpages}">--%>
<%--                                <li class="page-item"><a class="page-link"--%>
<%--                                                         href="?pagination=${pagination}&page=${page+1}&sort=${sort}&sortcolumn=${sortcolumn}">${text['index.next']}--%>
<%--                                    &rsaquo;</a>--%>
<%--                                </li>--%>
<%--                            </c:if>--%>
<%--                        </ul>--%>
<%--                    </nav>--%>
                    <jsp:include page="pagination.jsp"/>
                    <!-- Pagination-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<%--<footer class="page-footer font-small ">--%>

<%--    <!-- Copyright -->--%>
<%--    <div class="footer-copyright text-center py-3">© 2022 Copyright:--%>
<%--        <a href="mailto:leonid.vasyliev2021@gmail.com"> Vasyliev Leonid</a>--%>
<%--    </div>--%>
<%--    <!-- Copyright -->--%>

<%--</footer>--%>
<jsp:include page="footer.jsp"/>
<!-- Footer -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

</body>
</html>

