<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <link rel="icon"
          href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'
             width='16' height='16' fill='green' class='bi bi-check-circle-fill' viewBox='0 0 16 16'>
    <path d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
     7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z'/>
    </svg>"/>
    <title>${text['index.registration']}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

</head>
<body>
<!--header-->
<div class="container-fluid fixed-top bg-light">
    <header class="d-flex flex-wrap justify-content-center py-3 ">
        <div class="d-flex bg-light mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <i class="bi bi-check2-circle h1 "></i>
            <h2 class="fs-2">&nbsp;${text['index.logo']}</h2>
        </div>
        <div class="col-md-3 text-end">
            <%--            <a class="btn btn-link " href="/login" role="button">${text['index.registration']}</a>--%>
            <a class="btn btn-outline-primary me-2" href="/logout" role="button">${text['btn.logout']}</a>
            <%--            <button href="#" type="button" class="btn btn-outline-primary me-2"> ${text['index.login']}</button>--%>
        </div>
    </header>

    <!--navbar-->
    <nav class="navbar navbar-expand-md navbar-dark bg-primary ">
        <div class="container-fluid align-items-center">
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="/user">${text['login.home']}</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center">
                <div class="col-xs-2 text-end">
                    <form class="form-inline my-2 my-lg-0">
                        <select id="language" name="language" class="form-select form-select-md  "
                                aria-label=".form-select-md"
                                onchange="submit()">
                            <option value="EN" ${language == 'EN' ? 'selected' : ''}>English</option>
                            <option value="RU" ${language == 'RU' ? 'selected' : ''}>Русский</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>

    </nav>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<%--<section class="vh-100" style="background-color: #eee;">--%>
<div class="container ">
    <div class="row d-flex justify-content-center align-items-center ">
        <div class="col-lg-12 col-xl-11">
            <div class="card text-black" style="border-radius: 25px;">
                <div class="card-body p-md-5">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">

                            <p class="text-center h3 fw-bold mb-5 mx-1 mx-md-4 mt-4">${text['user.edit.account']}</p>
                            <c:if test="${error == 'error_update'}">
                                <div class="alert alert-danger d-flex align-items-center" role="alert">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                                         class="bi flex-shrink-0 me-2" aria-label="Danger:" viewBox="0 0 16 16">
                                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                    </svg>
                                    <div>
                                        <b> ${errormessage}</b> ${text['user.err.wrongupd']}
                                    </div>
                                </div>
                            </c:if>
                            <form class="mx-1 mx-md-4" method="post" action="/user/edit">

                                <div class="d-flex flex-row align-items-center mb-4">
                                    <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                    <div class="form-outline flex-fill mb-0">
                                        <label class="form-label" for="login">${text['login.login']}</label>
                                        <input type="text" id="login" name="login" maxlength="35"
                                               class="form-control ${error == 'invalid_login' ? 'is-invalid' : ''}"
                                               ria-describedby="validationServer01Feedback" required
                                               value="${sessionScope.user.username}"/>
                                        <div id="validationServer01Feedback" class="invalid-feedback">
                                            <b> ${errormessage}</b> ${text['signup.err.accpresent']}
                                        </div>
                                    </div>
                                </div>

                                <%--                                    <div class="d-flex flex-row align-items-center mb-4">--%>
                                <%--                                        <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>--%>
                                <%--                                        <div class="form-outline flex-fill mb-0">--%>
                                <%--                                            <input type="email" id="form3Example3c" class="form-control" />--%>
                                <%--                                            <label class="form-label" for="form3Example3c">Your Email</label>--%>
                                <%--                                        </div>--%>
                                <%--                                    </div>--%>

                                <div class="d-flex flex-row align-items-center mb-4">
                                    <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                    <div class="form-outline flex-fill mb-0">
                                        <label class="form-label" for="password">${text['login.password']}</label>
                                        <input type="password" id="password" name="password" maxlength="35"
                                               class="form-control ${error == 'invalid_password' ? 'is-invalid' : ''}"
                                               ria-describedby="validationServer02Feedback" required
                                               value="${errordto.password}"/>
                                        <div id="validationServer02Feedback" class="invalid-feedback">
                                            <%--                                            <b> ${errormessage}</b> ${text['login.err.wrongpass']}--%>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex flex-row align-items-center mb-4">
                                    <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                    <div class="form-outline flex-fill mb-0">
                                        <label class="form-label" for="repeatpass">${text['login.repeatpass']}</label>
                                        <input type="password" id="repeatpass" name="repeatpass" maxlength="35"
                                               class="form-control ${error == 'invalid_password' ? 'is-invalid' : ''}"
                                               ria-describedby="validationServer03Feedback" required/>
                                        <div id="validationServer03Feedback" class="invalid-feedback">
                                            <b> ${errormessage}</b> ${text['signup.err.wrongpass']}
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex flex-row align-items-center mb-4">
                                    <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                    <div class="form-outline flex-fill mb-0">

                                        <label class="form-label" for="userlang">${text['login.defaultlang']}</label>
                                        <select id="userlang" name="userlang" class="form-select form-select-md  "
                                                aria-label=".form-select-md">
                                            <option value="EN"${language == 'EN' ? 'selected' : ''}>English</option>
                                            <option value="RU"${language == 'RU' ? 'selected' : ''}>Русский</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                    <input type="submit" class="btn btn-primary btn-lg" name="submit"
                                           value="${text['user.edit.update']}">
                                    <%--                                        <button type="button" class="btn btn-primary btn-lg">Sign in</button>--%>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--</section>--%>

    <!-- Footer -->
<%--    <footer class="page-footer font-small ">--%>

<%--        <!-- Copyright -->--%>
<%--        <div class="footer-copyright text-center py-3">© 2022 Copyright:--%>
<%--            <a href="mailto:leonid.vasyliev2021@gmail.com"> Vasyliev Leonid</a>--%>
<%--        </div>--%>
<%--        <!-- Copyright -->--%>

<%--    </footer>--%>
    <jsp:include page="footer.jsp"/>
    <!-- Footer -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
</body>
</html>
