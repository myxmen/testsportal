package com.vl.testing.dto;

import java.util.List;
import java.util.Objects;

@SuppressWarnings("PMD.ShortVariable")
public class TestDTO {
    private Long id;
    private Long subjectId;

    private String test;
    private Integer difficulty;
    private Long requests;
    private Long time;
    private List<QuestionDTO> questions;

    public TestDTO() {
    }

    public TestDTO(final Long id, final Long subjectId, final String test,
                   final Integer difficulty, final Long requests, final Long time,
                   final List<QuestionDTO> questions) {
        this.id = id;
        this.subjectId = subjectId;
        this.test = test;
        this.difficulty = difficulty;
        this.requests = requests;
        this.time = time;
        this.questions = questions;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(final Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getTest() {
        return test;
    }

    public void setTest(final String test) {
        this.test = test;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(final Integer difficulty) {
        this.difficulty = difficulty;
    }

    public Long getRequests() {
        return requests;
    }

    public void setRequests(final Long requests) {
        this.requests = requests;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(final Long time) {
        this.time = time;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(final List<QuestionDTO> questions) {
        this.questions = questions;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final TestDTO testDTO = (TestDTO) object;
        return Objects.equals(id, testDTO.id)
                && Objects.equals(subjectId, testDTO.subjectId)
                && Objects.equals(test, testDTO.test)
                && Objects.equals(difficulty, testDTO.difficulty)
                && Objects.equals(requests, testDTO.requests)
                && Objects.equals(time, testDTO.time)
                && Objects.equals(questions, testDTO.questions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, subjectId, test, difficulty, requests, time, questions);
    }

    @Override
    public String toString() {
        return "TestDTO{"
                + "id=" + id
                + ", subjectId=" + subjectId
                + ", test='" + test + '\''
                + ", difficulty=" + difficulty
                + ", requests=" + requests
                + ", time=" + time
                + ", questions=" + questions
                + '}';
    }

    /**
     * init Builder.
     *
     * @return DTO builder.
     */
    public static TestDTOBuilder builder() {
        return new TestDTOBuilder();
    }

    public static class TestDTOBuilder {
        private Long id;
        private Long subjectId;
        private String test;
        private Integer difficulty;
        private Long requests;
        private Long time;
        private List<QuestionDTO> questions;

        // CHECKSTYLE_OFF: JavadocMethod
        public TestDTOBuilder setId(final Long id) {
            this.id = id;
            return this;
        }

        public TestDTOBuilder setSubjectId(final Long subjectId) {
            this.subjectId = subjectId;
            return this;
        }

        public TestDTOBuilder setTest(final String test) {
            this.test = test;
            return this;
        }

        public TestDTOBuilder setDifficulty(final Integer difficulty) {
            this.difficulty = difficulty;
            return this;
        }

        public TestDTOBuilder setRequests(final Long requests) {
            this.requests = requests;
            return this;
        }

        public TestDTOBuilder setTime(final Long time) {
            this.time = time;
            return this;
        }

        public TestDTOBuilder setQuestions(final List<QuestionDTO> questions) {
            this.questions = questions;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built DTO.
         */
        public TestDTO build() {
            return new TestDTO(id, subjectId, test, difficulty, requests, time, questions);
        }
    }
}
