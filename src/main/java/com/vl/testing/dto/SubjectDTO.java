package com.vl.testing.dto;

import java.util.Objects;

@SuppressWarnings("PMD.ShortVariable")
public class SubjectDTO {
    private Long id;
    private String subject;
    private TestDTO test;

    public SubjectDTO() {
    }

    public SubjectDTO(final Long id, final String subject, final TestDTO test) {
        this.id = id;
        this.subject = subject;
        this.test = test;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public TestDTO getTest() {
        return test;
    }

    public void setTests(final TestDTO test) {
        this.test = test;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final SubjectDTO that = (SubjectDTO) object;
        return Objects.equals(id, that.id)
                && Objects.equals(subject, that.subject)
                && Objects.equals(test, that.test);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, subject, test);
    }

    @Override
    public String toString() {
        return "SubjectDTO{"
                + "id=" + id
                + ", subject='" + subject + '\''
                + ", tests=" + test
                + '}';
    }

    /**
     * init Builder.
     *
     * @return DTO builder.
     */
    public static SubjectDTOBuilder builder() {
        return new SubjectDTOBuilder();
    }

    public static class SubjectDTOBuilder {
        private Long id;
        private String subject;
        private TestDTO test;

        // CHECKSTYLE_OFF: JavadocMethod
        public SubjectDTOBuilder setId(final Long id) {
            this.id = id;
            return this;
        }

        public SubjectDTOBuilder setSubject(final String subject) {
            this.subject = subject;
            return this;
        }

        public SubjectDTOBuilder setTests(final TestDTO test) {
            this.test = test;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built DTO.
         */
        public SubjectDTO build() {
            return new SubjectDTO(id, subject, test);
        }
    }
}
