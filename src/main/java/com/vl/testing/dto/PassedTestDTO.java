package com.vl.testing.dto;

import java.util.Objects;

@SuppressWarnings("PMD.ShortVariable")
public class PassedTestDTO {
    private Long id;
    private TestDTO test;
    private Integer result;

    public PassedTestDTO() {
    }

    public PassedTestDTO(final Long id, final TestDTO test, final Integer result) {
        this.id = id;
        this.test = test;
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public TestDTO getTest() {
        return test;
    }

    public void setTest(final TestDTO test) {
        this.test = test;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(final Integer result) {
        this.result = result;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final PassedTestDTO that = (PassedTestDTO) object;
        return Objects.equals(id, that.id) && Objects.equals(test, that.test) && Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, test, result);
    }

    @Override
    public String toString() {
        return "PassedTestDTO{"
                + "id=" + id
                + ", test=" + test
                + ", result=" + result
                + '}';
    }

    /**
     * init Builder.
     *
     * @return DTO builder.
     */
    public static PassedTestDTOBuilder builder() {
        return new PassedTestDTOBuilder();
    }

    public static class PassedTestDTOBuilder {
        private Long id;
        private TestDTO test;
        private Integer result;

        // CHECKSTYLE_OFF: JavadocMethod
        public PassedTestDTOBuilder setId(final Long id) {
            this.id = id;
            return this;
        }

        public PassedTestDTOBuilder setTest(final TestDTO test) {
            this.test = test;
            return this;
        }

        public PassedTestDTOBuilder setResult(final Integer result) {
            this.result = result;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built DTO.
         */
        public PassedTestDTO build() {
            return new PassedTestDTO(id, test, result);
        }
    }
}
