package com.vl.testing.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@SuppressWarnings("PMD.ShortVariable")
public class UserDTO {
    private Long id;

    private String username;
    private String password;
    private Boolean admin;
    private Boolean ban;
    private Locale locale;
    private LocalDateTime loginTime;
    private List<PassedTestDTO> passedTests;

    public UserDTO() {
    }

    public UserDTO(final Long id, final String username, final String password,
                   final Boolean admin, final Boolean ban, final Locale locale,
                   final LocalDateTime time, final List<PassedTestDTO> passedTests) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.admin = admin;
        this.ban = ban;
        this.locale = locale;
        this.loginTime = time;
        this.passedTests = passedTests;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Boolean isAdmin() {
        return admin;
    }

    public void setAdmin(final Boolean admin) {
        this.admin = admin;
    }

    public Boolean isBan() {
        return ban;
    }

    public void setBan(final Boolean ban) {
        this.ban = ban;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(final Locale locale) {
        this.locale = locale;
    }

    public LocalDateTime getLoginTime() {
        return loginTime;
    }

    /**
     * For adminbasis.jsp.
     *
     * @return formatted time by text.
     */
    public String getFormatLoginTime() {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return loginTime.format(formatter);
    }

    public void setLoginDateTime(final LocalDateTime loginDateTime) {
        this.loginTime = loginDateTime;
    }

    public List<PassedTestDTO> getPassedTests() {
        return passedTests;
    }

    public void setPassedTests(final List<PassedTestDTO> passedTests) {
        this.passedTests = passedTests;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final UserDTO userDTO = (UserDTO) object;
        return Objects.equals(id, userDTO.id) && Objects.equals(username, userDTO.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, admin, ban);
    }

    @Override
    public String toString() {
        return "UserDTO{"
                + "id=" + id
                + ", username='" + username + '\''
                + ", password='" + password + '\''
                + ", admin=" + admin
                + ", ban=" + ban
                + ", locale=" + locale
                + ", loginDateTime=" + loginTime
                + ", passedTests=" + passedTests
                + '}';
    }

    /**
     * init Builder.
     *
     * @return DTO builder.
     */
    public static UserDTOBuilder builder() {
        return new UserDTOBuilder();
    }

    public static class UserDTOBuilder {
        private Long id;

        private String username;
        private String password;
        private Boolean admin;
        private Boolean ban;
        private Locale locale;
        private LocalDateTime loginTime;
        private List<PassedTestDTO> passedTests;

        // CHECKSTYLE_OFF: JavadocMethod
        public UserDTOBuilder setId(final Long id) {
            this.id = id;
            return this;
        }

        public UserDTOBuilder setUsername(final String username) {
            this.username = username;
            return this;
        }

        public UserDTOBuilder setPassword(final String password) {
            this.password = password;
            return this;
        }

        public UserDTOBuilder setAdmin(final Boolean admin) {
            this.admin = admin;
            return this;
        }

        public UserDTOBuilder setBan(final Boolean ban) {
            this.ban = ban;
            return this;
        }

        public UserDTOBuilder setLocale(final Locale locale) {
            this.locale = locale;
            return this;
        }

        public UserDTOBuilder setLoginTime(final LocalDateTime loginTime) {
            this.loginTime = loginTime;
            return this;
        }

        public UserDTOBuilder setPassedTests(final List<PassedTestDTO> passedTests) {
            this.passedTests = passedTests;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built DTO.
         */
        public UserDTO build() {
            return new UserDTO(id, username, password, admin, ban, locale, loginTime, passedTests);
        }
    }
}
