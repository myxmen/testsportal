package com.vl.testing.persistence;

public interface AutoClose extends AutoCloseable {

    /**
     * auto close connection.
     */
    void close();
}
