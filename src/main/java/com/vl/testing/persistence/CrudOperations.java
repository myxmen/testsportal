package com.vl.testing.persistence;

import java.util.Optional;

import com.vl.testing.persistence.exeptions.RepositoryException;

public interface CrudOperations<T, I> extends AutoClose {

    /**
     * create entity into database.
     *
     * @param entity Entity.
     * @return Entity.
     * @throws RepositoryException error.
     */
    T create(T entity) throws RepositoryException;

    /**
     * get entity from database.
     *
     * @param idEntity id of Entity.
     * @return Entity.
     * @throws RepositoryException error.
     */
    Optional<T> get(I idEntity) throws RepositoryException;

    /**
     * update entity from database.
     *
     * @param entity Entity.
     * @return Entity.
     * @throws RepositoryException error.
     */
    Optional<T> update(T entity) throws RepositoryException;

    /**
     * delete entity from database.
     *
     * @param idEntity id of Entity.
     * @return true if entity deleted.
     * @throws RepositoryException error.
     */
    boolean delete(I idEntity) throws RepositoryException;
}
