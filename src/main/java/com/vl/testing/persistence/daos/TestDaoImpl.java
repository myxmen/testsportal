package com.vl.testing.persistence.daos;

// CHECKSTYLE_OFF: AvoidStarImport
import java.sql.*;
// CHECKSTYLE_ON: AvoidStarImport
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.mapper.impl.TestMapperEntity;
import com.vl.testing.persistence.utils.SetPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestDaoImpl implements TestDao {
    private static final Logger LOGGER = LogManager.getLogger(TestDaoImpl.class);
    private final Connection connection;
    private static final int INDEX_1 = 1;
    private static final int INDEX_2 = 2;
    private static final int INDEX_3 = 3;
    private static final int INDEX_4 = 4;
    private static final int INDEX_5 = 5;
    private static final int INDEX_6 = 6;
    private static final Integer ZERO_OF_ROWS = 0;

    public TestDaoImpl(final Connection connection) {
        this.connection = connection;
    }

    @Override
    public TestEntity create(final TestEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO tests (test_subject_id,test,difficulty,requests,time) "
                        + "VALUES(?, ?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS)) {
            setStatementValuesForCreation(statement, entity);
            statement.executeUpdate();
            final ResultSet primaryKeys = statement.getGeneratedKeys();
            if (primaryKeys.next()) {
                entity.setIdTest(primaryKeys.getLong(INDEX_1));
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_CREATE_NEW_TEST.getMessage(), e);
        }
        return entity;
    }

    @Override
    public Optional<TestEntity> get(final Long idTest) throws RepositoryException {
        Objects.requireNonNull(idTest);
        try (PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM tests WHERE test_id = ?")) {
            statement.setLong(INDEX_1, idTest);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final TestEntity test = new TestMapperEntity().fillEntityWithValues(resultSet);
                return Optional.of(test);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage
                    .FAILED_TO_GET_TEST_BY_ID.getMessage() + " " + idTest, e);
        }
        return Optional.empty();
    }

    @Override
    public List<TestEntity> getAllBySubjectId(final SubjectEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity.getIdSubject());
        final List<TestEntity> all = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM tests WHERE test_subject_id = ?")) {
            statement.setLong(INDEX_1, entity.getIdSubject());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                all.add(new TestMapperEntity().fillEntityWithValues(resultSet));
            }
            return all;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_ALL_TEST_BY_SUBJECT_ID.getMessage(), e);
        }
    }

    @Override
    public Optional<TestEntity> update(final TestEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE tests SET test_subject_id=?,test=?,difficulty=?,requests=?,time=? WHERE test_id = ?")) {
            setStatementValuesForCreation(statement, entity);
            statement.setLong(INDEX_6, entity.getIdTest());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.info(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_UPDATE_TEST.getMessage());
        }
        return Optional.of(entity);
    }

    @Override
    public boolean delete(final Long idTest) throws RepositoryException {
        Objects.requireNonNull(idTest);
        try (PreparedStatement statement = connection
                .prepareStatement("DELETE FROM tests WHERE test_id = ?")) {
            statement.setLong(INDEX_1, idTest);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_DELETE_TEST.getMessage(), e);
        }
    }

    @Override
    public List<TestEntity> getPage(final SubjectEntity entity, final SetPage setPage) throws RepositoryException {
        Objects.requireNonNull(setPage);
        Objects.requireNonNull(entity.getIdSubject());
        final List<TestEntity> page = new ArrayList<>();
        final String query = "SELECT * FROM tests WHERE test_subject_id = ? ORDER BY "
                + setPage.getSortColumn() + " " + setPage.getSort() + " LIMIT ? OFFSET ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(INDEX_1, entity.getIdSubject());
            statement.setInt(INDEX_2, setPage.getRecordsPerPage());
            statement.setInt(INDEX_3, setPage.getStartRows());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                page.add(new TestMapperEntity().fillEntityWithValues(resultSet));
            }
            return page;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_PAGE_OF_TEST.getMessage(), e);
        }
    }

    @Override
    public Integer getNumberOfRows(final SubjectEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity.getIdSubject());
        Integer numOfRows = ZERO_OF_ROWS;
        try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM tests WHERE test_subject_id = ?")) {
            statement.setLong(INDEX_1, entity.getIdSubject());
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                numOfRows = resultSet.getInt(INDEX_1);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_TEST.getMessage(), e);
        }
        return numOfRows;
    }

    @Override
    public Boolean increaseRequestOfTest(final TestEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity.getIdTest());
        try (PreparedStatement statement = connection.prepareStatement("Update tests set requests=requests+1 where test_id=?")) {
            statement.setLong(INDEX_1, entity.getIdTest());
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_INCREASE_REQUEST_OF_TEST.getMessage(), e);
        }
    }

    @Override
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void setStatementValuesForCreation(final PreparedStatement statement, final TestEntity entity) throws SQLException {
        statement.setLong(INDEX_1, entity.getSubjectId());
        statement.setString(INDEX_2, entity.getTest());
        statement.setInt(INDEX_3, entity.getDifficulty());
        statement.setLong(INDEX_4, entity.getRequests());
        statement.setLong(INDEX_5, entity.getTime());
    }
}
