package com.vl.testing.persistence.daos;

import java.util.List;

import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.CrudOperations;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;

public interface QuestionDao extends CrudOperations<QuestionEntity, Long> {

    /**
     * Get All of QuestionEntity by TestEntity.
     *
     * @param entity TestEntity.
     * @return list of QuestionEntity.
     * @throws RepositoryException error.
     */
    List<QuestionEntity> getAllByTestId(TestEntity entity) throws RepositoryException;

    /**
     * Get Page of QuestionEntity.
     *
     * @param entity  TestEntity.
     * @param setPage SetPage.
     * @return list of QuestionEntity.
     * @throws RepositoryException error.
     */
    List<QuestionEntity> getPage(TestEntity entity, SetPage setPage) throws RepositoryException;

    /**
     * Get Number Of Rows.
     *
     * @param entity TestEntity.
     * @return Number Of Rows.
     * @throws RepositoryException error.
     */
    Integer getNumberOfRows(TestEntity entity) throws RepositoryException;

}
