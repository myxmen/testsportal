package com.vl.testing.persistence.daos;

import java.util.List;

import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.CrudOperations;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;

public interface TestDao extends CrudOperations<TestEntity, Long> {

    /**
     * Get All of TestEntity.
     *
     * @param entity SubjectEntity.
     * @return list of TestEntity.
     * @throws RepositoryException error.
     */
    List<TestEntity> getAllBySubjectId(SubjectEntity entity) throws RepositoryException;

    /**
     * Get Page of TestEntity.
     *
     * @param entity  SubjectEntity.
     * @param setPage SetPage.
     * @return list of TestEntity.
     * @throws RepositoryException error.
     */
    List<TestEntity> getPage(SubjectEntity entity, SetPage setPage) throws RepositoryException;

    /**
     * Get Number Of Rows.
     *
     * @param entity SubjectEntity.
     * @return Number Of Rows.
     * @throws RepositoryException error.
     */
    Integer getNumberOfRows(SubjectEntity entity) throws RepositoryException;

    /**
     * Increase Request Of Test.
     *
     * @param entity TestEntity.
     * @return true if increased RequestOfTest.
     * @throws RepositoryException error.
     */
    Boolean increaseRequestOfTest(TestEntity entity) throws RepositoryException;
}
