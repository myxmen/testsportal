package com.vl.testing.persistence.daos;

import java.util.List;

import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.persistence.CrudOperations;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;

public interface SubjectDao extends CrudOperations<SubjectEntity, Long> {

    /**
     * Get All of SubjectEntity.
     *
     * @return list of SubjectEntity.
     * @throws RepositoryException error.
     */
    List<SubjectEntity> getAll() throws RepositoryException;

    /**
     * Get Page of SubjectEntity.
     *
     * @param setPage SetPage.
     * @return list of SubjectEntity.
     * @throws RepositoryException error.
     */
    List<SubjectEntity> getPage(SetPage setPage) throws RepositoryException;

    /**
     * Get Number Of Rows.
     *
     * @return Number Of Rows.
     * @throws RepositoryException error.
     */
    Integer getNumberOfRows() throws RepositoryException;
}
