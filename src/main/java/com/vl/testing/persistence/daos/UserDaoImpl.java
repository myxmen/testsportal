package com.vl.testing.persistence.daos;

// CHECKSTYLE_OFF: AvoidStarImport

import java.sql.*;
// CHECKSTYLE_ON: AvoidStarImport
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.mapper.impl.UserMapperEntity;
import com.vl.testing.persistence.utils.SetPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserDaoImpl implements UserDao {
    private static final Logger LOGGER = LogManager.getLogger(UserDaoImpl.class);
    private final Connection connection;
    private static final int INDEX_1 = 1;
    private static final int INDEX_2 = 2;
    private static final int INDEX_3 = 3;
    private static final int INDEX_4 = 4;
    private static final int INDEX_5 = 5;
    private static final int INDEX_6 = 6;
    private static final Integer ZERO_OF_ROWS = 0;

    public UserDaoImpl(final Connection connection) {
        this.connection = connection;
    }

    @Override
    public UserEntity create(final UserEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO users (username, password, admin, ban, locale) "
                        + "VALUES(?, ?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS)) {
            setStatementValuesForCreation(statement, entity);
            statement.executeUpdate();
            final ResultSet primaryKeys = statement.getGeneratedKeys();
            if (primaryKeys.next()) {
                entity.setIdUser(primaryKeys.getLong(INDEX_1));
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_CREATE_NEW_USER.getMessage(), e);
        }
        return entity;
    }

    @Override
    public Optional<UserEntity> get(final Long idUser) throws RepositoryException {
        Objects.requireNonNull(idUser);
        try (PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM users WHERE user_id = ?")) {
            statement.setLong(INDEX_1, idUser);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final UserEntity user = new UserMapperEntity().fillEntityWithValues(resultSet);
                return Optional.of(user);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage
                    .FAILED_TO_GET_USER_BY_ID.getMessage() + " " + idUser, e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<UserEntity> getByName(final String name) throws RepositoryException {
        Objects.requireNonNull(name);
        try (PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM users WHERE username = ?")) {
            statement.setString(INDEX_1, name);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final UserEntity user = new UserMapperEntity().fillEntityWithValues(resultSet);
                return Optional.of(user);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage
                    .FAILED_TO_GET_USER_BY_NAME.getMessage() + " " + name, e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<UserEntity> update(final UserEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE users SET username= ?,password = ?,admin = ?,ban= ?, locale= ? WHERE user_id = ?")) {
            setStatementValuesForCreation(statement, entity);
            statement.setLong(INDEX_6, entity.getIdUser());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_UPDATE_USER.getMessage());
        }
        return Optional.of(entity);
    }

    @Override
    public Optional<UserEntity> updateAdminsFields(final UserEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE users SET admin = ?,ban= ? WHERE user_id = ?")) {
            statement.setBoolean(INDEX_1, entity.isAdmin());
            statement.setBoolean(INDEX_2, entity.isBan());
            statement.setLong(INDEX_3, entity.getIdUser());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_UPDATE_ADMIN_FIELDS.getMessage());
        }
        return Optional.of(entity);
    }

    @Override
    public Optional<UserEntity> updateUsersFields(final UserEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE users SET username= ?,password = ?, locale= ? WHERE user_id = ?")) {
            statement.setString(INDEX_1, entity.getUsername());
            statement.setString(INDEX_2, entity.getPassword());
            statement.setString(INDEX_3, entity.getLocale());
            statement.setLong(INDEX_4, entity.getIdUser());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_UPDATE_USER_FIELDS.getMessage());
        }
        return Optional.of(entity);
    }

    @Override
    public boolean delete(final Long idUser) throws RepositoryException {
        Objects.requireNonNull(idUser);
        try (PreparedStatement statement = connection
                .prepareStatement("DELETE FROM users WHERE user_id = ?")) {
            statement.setLong(INDEX_1, idUser);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_DELETE_USER.getMessage(), e);
        }
    }

    @Override
    public List<UserEntity> getAll() throws RepositoryException {
        final List<UserEntity> all = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM users")) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                all.add(new UserMapperEntity().fillEntityWithValues(resultSet));
            }
            return all;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_ALL_USER.getMessage(), e);
        }
    }

    @Override
    public List<UserEntity> getPage(final SetPage setPage) throws RepositoryException {
        Objects.requireNonNull(setPage);
        final List<UserEntity> page = new ArrayList<>();
        final String query = "SELECT * FROM users ORDER BY " + setPage.getSortColumn() + " " + setPage.getSort() + " LIMIT ? OFFSET ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(INDEX_1, setPage.getRecordsPerPage());
            statement.setInt(INDEX_2, setPage.getStartRows());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                page.add(new UserMapperEntity().fillEntityWithValues(resultSet));
            }
            return page;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_PAGE_OF_USER.getMessage(), e);
        }
    }

    @Override
    public Integer getNumberOfRows() throws RepositoryException {
        Integer numOfRows = ZERO_OF_ROWS;
        try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT (user_id) FROM users")) {
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                numOfRows = resultSet.getInt(INDEX_1);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_USER.getMessage(), e);
        }
        return numOfRows;
    }

    @Override
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void setStatementValuesForCreation(final PreparedStatement statement, final UserEntity entity) throws SQLException {
        statement.setString(INDEX_1, entity.getUsername());
        statement.setString(INDEX_2, entity.getPassword());
        statement.setBoolean(INDEX_3, entity.isAdmin());
        statement.setBoolean(INDEX_4, entity.isBan());
        statement.setString(INDEX_5, entity.getLocale());
    }
}
