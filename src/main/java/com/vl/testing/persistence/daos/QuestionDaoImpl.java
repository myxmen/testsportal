package com.vl.testing.persistence.daos;

// CHECKSTYLE_OFF: AvoidStarImport
import java.sql.*;
// CHECKSTYLE_ON: AvoidStarImport
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.mapper.impl.QuestionMapperEntity;
import com.vl.testing.persistence.utils.SetPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class QuestionDaoImpl implements QuestionDao {
    private static final Logger LOGGER = LogManager.getLogger(QuestionDaoImpl.class);
    private final Connection connection;
    private static final int INDEX_1 = 1;
    private static final int INDEX_2 = 2;
    private static final int INDEX_3 = 3;
    private static final int INDEX_4 = 4;
    private static final int INDEX_5 = 5;
    private static final int INDEX_6 = 6;
    private static final int INDEX_7 = 7;
    private static final int INDEX_8 = 8;
    private static final int INDEX_9 = 9;
    private static final int INDEX_10 = 10;
    private static final int INDEX_11 = 11;
    private static final int INDEX_12 = 12;
    private static final int INDEX_13 = 13;
    private static final int INDEX_14 = 14;
    private static final Integer ZERO_OF_ROWS = 0;

    public QuestionDaoImpl(final Connection connection) {
        this.connection = connection;
    }

    @Override
    public QuestionEntity create(final QuestionEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO questions (question_test_id,number_question,question,answer_1,answer_2,answer_3,answer_4,answer_5,"
                        + "correct_answer_1,correct_answer_2,correct_answer_3,correct_answer_4,correct_answer_5) "
                        + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS)) {
            setStatementValuesForCreation(statement, entity);
            statement.executeUpdate();
            final ResultSet primaryKeys = statement.getGeneratedKeys();
            if (primaryKeys.next()) {
                entity.setIdQuestion(primaryKeys.getLong(INDEX_1));
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_CREATE_NEW_QUESTION.getMessage(), e);
        }
        return entity;
    }

    @Override
    public Optional<QuestionEntity> get(final Long idQuestion) throws RepositoryException {
        Objects.requireNonNull(idQuestion);
        try (PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM questions WHERE question_id = ?")) {
            statement.setLong(INDEX_1, idQuestion);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final QuestionEntity test = new QuestionMapperEntity().fillEntityWithValues(resultSet);
                return Optional.of(test);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage
                    .FAILED_TO_GET_QUESTION_BY_ID.getMessage() + " " + idQuestion, e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<QuestionEntity> update(final QuestionEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE questions SET question_test_id=?,number_question=?,question=?,"
                        + "answer_1=?,answer_2=?,answer_3=?,answer_4=?,answer_5=?,"
                        + "correct_answer_1=?,correct_answer_2=?,correct_answer_3=?,correct_answer_4=?,"
                        + "correct_answer_5=? WHERE question_id = ?")) {
            setStatementValuesForCreation(statement, entity);
            statement.setLong(INDEX_14, entity.getIdQuestion());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.info(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_UPDATE_QUESTION.getMessage());
        }
        return Optional.of(entity);
    }

    @Override
    public boolean delete(final Long idQuestion) throws RepositoryException {
        Objects.requireNonNull(idQuestion);
        try (PreparedStatement statement = connection
                .prepareStatement("DELETE FROM questions WHERE question_id = ?")) {
            statement.setLong(INDEX_1, idQuestion);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_DELETE_QUESTION.getMessage(), e);
        }
    }

    @Override
    public List<QuestionEntity> getPage(final TestEntity entity, final SetPage setPage) throws RepositoryException {
        Objects.requireNonNull(setPage);
        Objects.requireNonNull(entity.getIdTest());
        final List<QuestionEntity> page = new ArrayList<>();
        final String query = "SELECT * FROM questions WHERE question_test_id = ? ORDER BY "
                + setPage.getSortColumn() + " " + setPage.getSort() + " LIMIT ? OFFSET ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(INDEX_1, entity.getIdTest());
            statement.setInt(INDEX_2, setPage.getRecordsPerPage());
            statement.setInt(INDEX_3, setPage.getStartRows());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                page.add(new QuestionMapperEntity().fillEntityWithValues(resultSet));
            }
            return page;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_PAGE_OF_QUESTIONS.getMessage(), e);
        }
    }

    @Override
    public Integer getNumberOfRows(final TestEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity.getIdTest());
        Integer numOfRows = ZERO_OF_ROWS;
        try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM questions WHERE question_test_id = ?")) {
            statement.setLong(INDEX_1, entity.getIdTest());
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                numOfRows = resultSet.getInt(INDEX_1);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_QUESTIONS.getMessage(), e);
        }
        return numOfRows;
    }

    @Override
    public List<QuestionEntity> getAllByTestId(final TestEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity.getIdTest());
        final List<QuestionEntity> all = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM questions WHERE question_test_id = ?")) {
            statement.setLong(INDEX_1, entity.getIdTest());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                all.add(new QuestionMapperEntity().fillEntityWithValues(resultSet));
            }
            return all;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_ALL_QUESTIONS_BY_TEST_ID.getMessage(), e);
        }
    }

    @Override
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void setStatementValuesForCreation(final PreparedStatement statement, final QuestionEntity entity) throws SQLException {
        statement.setLong(INDEX_1, entity.getTestId());
        statement.setInt(INDEX_2, entity.getNumberOfQuestion());
        statement.setString(INDEX_3, entity.getQuestion());
        statement.setString(INDEX_4, entity.getAnswer1());
        statement.setString(INDEX_5, entity.getAnswer2());
        statement.setString(INDEX_6, entity.getAnswer3());
        statement.setString(INDEX_7, entity.getAnswer4());
        statement.setString(INDEX_8, entity.getAnswer5());
        if (entity.isCorrectAnswer1() == null) {
            statement.setNull(INDEX_9, Types.BOOLEAN);
        } else {
            statement.setBoolean(INDEX_9, entity.isCorrectAnswer1());
        }
        if (entity.isCorrectAnswer2() == null) {
            statement.setNull(INDEX_10, Types.BOOLEAN);
        } else {
            statement.setBoolean(INDEX_10, entity.isCorrectAnswer2());
        }
        if (entity.isCorrectAnswer3() == null) {
            statement.setNull(INDEX_11, Types.BOOLEAN);
        } else {
            statement.setBoolean(INDEX_11, entity.isCorrectAnswer3());
        }
        if (entity.isCorrectAnswer4() == null) {
            statement.setNull(INDEX_12, Types.BOOLEAN);
        } else {
            statement.setBoolean(INDEX_12, entity.isCorrectAnswer4());
        }
        if (entity.isCorrectAnswer5() == null) {
            statement.setNull(INDEX_13, Types.BOOLEAN);
        } else {
            statement.setBoolean(INDEX_13, entity.isCorrectAnswer5());
        }
    }
}
