package com.vl.testing.persistence.daos;

// CHECKSTYLE_OFF: AvoidStarImport
import java.sql.*;
// CHECKSTYLE_ON: AvoidStarImport
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.mapper.impl.SubjectMapperEntity;
import com.vl.testing.persistence.utils.SetPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SubjectDaoImpl implements SubjectDao {
    private static final Logger LOGGER = LogManager.getLogger(SubjectDaoImpl.class);
    private final Connection connection;
    private static final int INDEX_1 = 1;
    private static final int INDEX_2 = 2;
    private static final Integer ZERO_OF_ROWS = 0;

    public SubjectDaoImpl(final Connection connection) {
        this.connection = connection;
    }

    @Override
    public SubjectEntity create(final SubjectEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO subjects (subject) VALUES(?)",
                Statement.RETURN_GENERATED_KEYS)) {
            setStatementValuesForCreation(statement, entity);
            statement.executeUpdate();
            final ResultSet primaryKeys = statement.getGeneratedKeys();
            if (primaryKeys.next()) {
                entity.setIdSubject(primaryKeys.getLong(INDEX_1));
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_CREATE_NEW_SUBJECT.getMessage(), e);
        }
        return entity;
    }

    @Override
    public Optional<SubjectEntity> get(final Long idSubject) throws RepositoryException {
        Objects.requireNonNull(idSubject);
        try (PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM subjects WHERE subject_id = ?")) {
            statement.setLong(INDEX_1, idSubject);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {

                final SubjectEntity subject = new SubjectMapperEntity().fillEntityWithValues(resultSet);
                return Optional.of(subject);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage
                    .FAILED_TO_GET_SUBJECT_BY_ID.getMessage() + " " + idSubject, e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<SubjectEntity> update(final SubjectEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE subjects SET subject= ? WHERE subject_id = ?")) {
            setStatementValuesForCreation(statement, entity);
            statement.setLong(INDEX_2, entity.getIdSubject());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.info(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_UPDATE_SUBJECT.getMessage());
        }
        return Optional.of(entity);
    }

    @Override
    public boolean delete(final Long idSubject) throws RepositoryException {
        Objects.requireNonNull(idSubject);
        try (PreparedStatement statement = connection
                .prepareStatement("DELETE FROM subjects WHERE subject_id = ?")) {
            statement.setLong(INDEX_1, idSubject);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_DELETE_SUBJECT.getMessage(), e);
        }
    }

    @Override
    public List<SubjectEntity> getAll() throws RepositoryException {
        final List<SubjectEntity> all = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM subjects")) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                all.add(new SubjectMapperEntity().fillEntityWithValues(resultSet));
            }
            return all;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_ALL_SUBJECT.getMessage(), e);
        }
    }

    @Override
    public List<SubjectEntity> getPage(final SetPage setPage) throws RepositoryException {
        Objects.requireNonNull(setPage);
        final List<SubjectEntity> page = new ArrayList<>();
        final String query = "SELECT * FROM subjects ORDER BY " + setPage.getSortColumn() + " " + setPage.getSort() + " LIMIT ? OFFSET ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(INDEX_1, setPage.getRecordsPerPage());
            statement.setInt(INDEX_2, setPage.getStartRows());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                page.add(new SubjectMapperEntity().fillEntityWithValues(resultSet));
            }
            return page;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_PAGE_OF_SUBJECT.getMessage(), e);
        }
    }

    @Override
    public Integer getNumberOfRows() throws RepositoryException {
        Integer numOfRows = ZERO_OF_ROWS;
        try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT (subject_id) FROM subjects")) {
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                numOfRows = resultSet.getInt(INDEX_1);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_SUBJECT.getMessage(), e);
        }
        return numOfRows;
    }

    @Override
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void setStatementValuesForCreation(final PreparedStatement statement, final SubjectEntity entity) throws SQLException {
        statement.setString(INDEX_1, entity.getSubject());
    }
}
