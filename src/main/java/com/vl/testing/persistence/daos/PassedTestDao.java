package com.vl.testing.persistence.daos;

import java.util.Map;
import java.util.Optional;

import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.AutoClose;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;

public interface PassedTestDao extends AutoClose {

    /**
     * Get All PassedTests fos UserEntity.
     *
     * @param entity UserEntity.
     * @return Map all PassedTestEntity, TestEntity.
     * @throws RepositoryException error.
     */
    Map<PassedTestEntity, TestEntity> getAllPassedTestFosUser(UserEntity entity) throws RepositoryException;

    /**
     * Create PassedTestEntity.
     *
     * @param entity PassedTestEntity.
     * @return PassedTestEntity.
     * @throws RepositoryException error.
     */
    PassedTestEntity create(PassedTestEntity entity) throws RepositoryException;

    /**
     * Update PassedTestEntity.
     *
     * @param entity PassedTestEntity.
     * @return Optional PassedTestEntity.
     * @throws RepositoryException error.
     */
    Optional<PassedTestEntity> update(PassedTestEntity entity) throws RepositoryException;

    /**
     * Get Page of PassedTestEntity & TestEntity.
     *
     * @param entity  UserEntity.
     * @param setPage SetPage.
     * @return Map of page PassedTestEntity, TestEntity.
     * @throws RepositoryException error.
     */
    Map<PassedTestEntity, TestEntity> getPage(UserEntity entity, SetPage setPage) throws RepositoryException;

    /**
     * Get Number Of Rows.
     *
     * @param entity UserEntity.
     * @return Number Of Rows.
     * @throws RepositoryException error.
     */
    Integer getNumberOfRows(UserEntity entity) throws RepositoryException;
}
