package com.vl.testing.persistence.daos;

import java.util.List;
import java.util.Optional;

import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.CrudOperations;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;

public interface UserDao extends CrudOperations<UserEntity, Long> {

    /**
     * Get All of UserEntity.
     *
     * @return list of UserEntity.
     * @throws RepositoryException error.
     */
    List<UserEntity> getAll() throws RepositoryException;

    /**
     * Get UserEntity by user`s name.
     *
     * @param name user`s name.
     * @return Optional UserEntity.
     * @throws RepositoryException error.
     */
    Optional<UserEntity> getByName(String name) throws RepositoryException;

    /**
     * Get Page of UserEntity.
     *
     * @param setPage SetPage.
     * @return list of UserEntity.
     * @throws RepositoryException error.
     */
    List<UserEntity> getPage(SetPage setPage) throws RepositoryException;

    /**
     * Update UserEntity admin fields admin, ban.
     *
     * @param entity UserEntity.
     * @return Optional UserEntity.
     * @throws RepositoryException error.
     */
    Optional<UserEntity> updateAdminsFields(UserEntity entity) throws RepositoryException;

    /**
     * Update UserEntity fields username, password, locale.
     *
     * @param entity UserEntity.
     * @return Optional UserEntity.
     * @throws RepositoryException error.
     */
    Optional<UserEntity> updateUsersFields(UserEntity entity) throws RepositoryException;

    /**
     * Get Number Of Rows.
     *
     * @return Number Of Rows.
     * @throws RepositoryException error.
     */
    Integer getNumberOfRows() throws RepositoryException;
}
