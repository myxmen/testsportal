package com.vl.testing.persistence.daos;

// CHECKSTYLE_OFF: AvoidStarImport
import java.sql.*;
// CHECKSTYLE_ON: AvoidStarImport
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.mapper.impl.PassedTestMapperEntity;
import com.vl.testing.persistence.mapper.impl.TestMapperEntity;
import com.vl.testing.persistence.utils.SetPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PassedTestDaoImpl implements PassedTestDao {
    private static final Logger LOGGER = LogManager.getLogger(PassedTestDaoImpl.class);
    private final Connection connection;
    private final PassedTestMapperEntity passedTestMapperDao = new PassedTestMapperEntity();
    private final TestMapperEntity testMapperEntity = new TestMapperEntity();
    private static final int INDEX_1 = 1;
    private static final int INDEX_2 = 2;
    private static final int INDEX_3 = 3;
    private static final Integer ZERO_OF_ROWS = 0;

    public PassedTestDaoImpl(final Connection connection) {
        this.connection = connection;
    }

    @Override
    public Map<PassedTestEntity, TestEntity> getAllPassedTestFosUser(final UserEntity userEntity) throws RepositoryException {
        Objects.requireNonNull(userEntity.getIdUser());
        final Map<PassedTestEntity, TestEntity> all = new LinkedHashMap<>();
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM passed_tests"
                        + " INNER JOIN tests ON passed_tests.passed_test_test_id=tests.test_id WHERE passed_tests.passed_test_user_id = ? "
                        + "ORDER BY passed_tests.passed_test_id ASC")) {
            statement.setLong(INDEX_1, userEntity.getIdUser());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                all.put(passedTestMapperDao.fillEntityWithValues(resultSet), testMapperEntity.fillEntityWithValues(resultSet));
            }
            return all;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_ALL_PASSEDTEST.getMessage());
        }
    }

    @Override
    public PassedTestEntity create(final PassedTestEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO passed_tests (passed_test_user_id, passed_test_test_id, result) "
                        + "VALUES(?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS)) {
            setStatementValuesForCreation(statement, entity);
            statement.executeUpdate();
            final ResultSet primaryKeys = statement.getGeneratedKeys();
            if (primaryKeys.next()) {
                entity.setIdPassedTest(primaryKeys.getLong(1));
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_CREATE_NEW_PASSEDTEST.getMessage(), e);
        }
        return entity;
    }

    @Override
    public Optional<PassedTestEntity> update(final PassedTestEntity entity) throws RepositoryException {
        Objects.requireNonNull(entity);
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE passed_tests SET result = ? WHERE passed_test_id = ?")) {
            statement.setInt(INDEX_1, entity.getResult());
            statement.setLong(INDEX_2, entity.getIdPassedTest());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_UPDATE_PASSEDTEST.getMessage());
        }
        return Optional.of(entity);
    }

    @Override
    public Map<PassedTestEntity, TestEntity> getPage(final UserEntity userEntity, final SetPage setPage) throws RepositoryException {
        Objects.requireNonNull(setPage);
        Objects.requireNonNull(userEntity.getIdUser());
        final Map<PassedTestEntity, TestEntity> all = new LinkedHashMap<>();
        final String query = "SELECT * FROM passed_tests INNER JOIN tests ON passed_tests.passed_test_test_id=tests.test_id"
                + " WHERE passed_tests.passed_test_user_id = ? ORDER BY " + setPage.getSortColumn()
                + " " + setPage.getSort() + " LIMIT ? OFFSET ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(INDEX_1, userEntity.getIdUser());
            statement.setInt(INDEX_2, setPage.getRecordsPerPage());
            statement.setInt(INDEX_3, setPage.getStartRows());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                all.put(passedTestMapperDao.fillEntityWithValues(resultSet), testMapperEntity.fillEntityWithValues(resultSet));
            }
            return all;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_PAGE_OF_PASSEDTEST.getMessage(), e);
        }
    }

    @Override
    public Integer getNumberOfRows(final UserEntity userEntity) throws RepositoryException {
        Objects.requireNonNull(userEntity.getIdUser());
        Integer numOfRows = ZERO_OF_ROWS;
        try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM passed_tests WHERE passed_test_user_id = ?")) {
            statement.setLong(INDEX_1, userEntity.getIdUser());
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                numOfRows = resultSet.getInt(INDEX_1);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RepositoryException(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_PASSEDTEST.getMessage(), e);
        }
        return numOfRows;
    }

    @Override
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void setStatementValuesForCreation(final PreparedStatement statement, final PassedTestEntity entity) throws SQLException {
        statement.setLong(INDEX_1, entity.getUserId());
        statement.setLong(INDEX_2, entity.getTestId());
        statement.setInt(INDEX_3, entity.getResult());
    }
}
