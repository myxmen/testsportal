package com.vl.testing.persistence;

import com.vl.testing.persistence.daofactory.JDBCDaoFactory;
// CHECKSTYLE_OFF: AvoidStarImport
import com.vl.testing.persistence.daos.*;
// CHECKSTYLE_ON: AvoidStarImport

@SuppressWarnings({"PMD.AbstractNaming", "PMD.AvoidFieldNameMatchingTypeName"})
public abstract class DaoFactory {

    /**
     * DaoFactory.
     */
    private static DaoFactory daoFactory;

    /**
     * UserDao.
     *
     * @return created UserDao.
     */
    public abstract UserDao createUserDao();

    /**
     * PassedTestDao.
     *
     * @return created PassedTestDao.
     */
    public abstract PassedTestDao createPassedTestDao();

    /**
     * SubjectDao.
     *
     * @return created SubjectDao.
     */
    public abstract SubjectDao createSubjectDao();

    /**
     * TestDao.
     *
     * @return created TestDao.
     */
    public abstract TestDao createTestDao();

    /**
     * QuestionDao.
     *
     * @return created QuestionDao.
     */
    public abstract QuestionDao createQuestionDao();

    /**
     * initialize & get singleton JDBCDaoFactory.
     *
     * @return JDBCDaoFactory.
     */
    @SuppressWarnings("PMD.DoubleCheckedLocking")
    public static DaoFactory getInstance() {
        if (daoFactory == null) {
            synchronized (DaoFactory.class) {
                if (daoFactory == null) {
                    daoFactory = new JDBCDaoFactory();
                }
            }
        }
        return daoFactory;
    }
}
