package com.vl.testing.persistence.daofactory;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

import com.vl.testing.persistence.DaoFactory;
// CHECKSTYLE_OFF: AvoidStarImport
import com.vl.testing.persistence.daos.*;
// CHECKSTYLE_ON: AvoidStarImport

public class JDBCDaoFactory extends DaoFactory {
    private final DataSource dataSource = ConnectionPoolHolder.getDataSource();

    @Override
    public UserDao createUserDao() {
        return new UserDaoImpl(getConnection());
    }

    @Override
    public PassedTestDao createPassedTestDao() {
        return new PassedTestDaoImpl(getConnection());
    }

    @Override
    public SubjectDao createSubjectDao() {
        return new SubjectDaoImpl(getConnection());
    }

    @Override
    public TestDao createTestDao() {
        return new TestDaoImpl(getConnection());
    }

    @Override
    public QuestionDao createQuestionDao() {
        return new QuestionDaoImpl(getConnection());
    }

    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
