package com.vl.testing.persistence.daofactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ConnectionPoolHolder {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPoolHolder.class);
    private static ConnectionPoolHolder connPool;
    private static volatile DataSource dataSource;
    private static Properties properties;
    private static final String PROP_FILE_NAME = "dbconfig.properties";

    private ConnectionPoolHolder() {
        properties = new Properties();
        loadProperties();
        LOGGER.info("ConnectionPoolHolder is initalized");
    }

    private Boolean loadProperties() {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResource(PROP_FILE_NAME).openStream()) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOGGER.error("Cannot read Data Base Properties " + e.getMessage());
        }
        return true;
    }

    /**
     * initialize & get singleton datasource.
     *
     * @return datasource.
     */
    public static DataSource getDataSource() {
        if (connPool == null) {
            synchronized (ConnectionPoolHolder.class) {
                if (connPool == null) {
                    connPool = new ConnectionPoolHolder();
                    final BasicDataSource dataSource = new BasicDataSource();
                    dataSource.setDriverClassName(properties.getProperty("DRIVER_CLASS"));
                    dataSource.setUrl(properties.getProperty("URL"));
                    dataSource.setUsername(properties.getProperty("USERNAME"));
                    dataSource.setPassword(properties.getProperty("PASSWORD"));
                    dataSource.setMinIdle(Integer.parseInt(properties.getProperty("MINIMUM_IDLE")));
                    dataSource.setMaxIdle(Integer.parseInt(properties.getProperty("MAXIMUM_IDLE")));
                    dataSource.setMaxOpenPreparedStatements(Integer.parseInt(properties.getProperty("MAX_OPEN_PREPARED_STATEMENT")));
                    ConnectionPoolHolder.dataSource = dataSource;
                    LOGGER.info("DataSource is initalized");
                }
            }
        }
        return dataSource;
    }
}
