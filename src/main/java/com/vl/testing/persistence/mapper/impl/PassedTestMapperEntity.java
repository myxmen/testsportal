package com.vl.testing.persistence.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.persistence.enums.ResultSetColumn;
import com.vl.testing.persistence.mapper.MapperEntity;

public class PassedTestMapperEntity implements MapperEntity<PassedTestEntity> {

    @Override
    public PassedTestEntity fillEntityWithValues(final ResultSet resultSet) throws SQLException {
        return PassedTestEntity.builder()
                .setIdPassedTest(resultSet.getLong(ResultSetColumn.PASSED_TEST_ID.getColumn()))//("passed_test_id"))
                .setUserId(resultSet.getLong(ResultSetColumn.PASSED_TEST_USER_ID.getColumn()))//("passed_test_user_id"))
                .setTestId(resultSet.getLong(ResultSetColumn.PASSED_TEST_TEST_ID.getColumn()))//("passed_test_test_id"))
                .setResult(resultSet.getInt(ResultSetColumn.RESULT.getColumn()))//("result"))
                .build();
    }
}
