package com.vl.testing.persistence.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.enums.ResultSetColumn;
import com.vl.testing.persistence.mapper.MapperEntity;

public class TestMapperEntity implements MapperEntity<TestEntity> {

    @Override
    public TestEntity fillEntityWithValues(final ResultSet resultSet) throws SQLException {
        return TestEntity.builder()
                .setIdTest(resultSet.getLong(ResultSetColumn.TEST_ID.getColumn()))//("test_id"))
                .setSubjectId(resultSet.getLong(ResultSetColumn.TEST_SUBJECT_ID.getColumn()))//("test_subject_id"))
                .setTest(resultSet.getString(ResultSetColumn.TEST.getColumn()))//("test"))
                .setDifficulty(resultSet.getInt(ResultSetColumn.DIFFICULTY.getColumn()))//("difficulty"))
                .setRequests(resultSet.getLong(ResultSetColumn.REQUESTS.getColumn()))//("requests"))
                .setTime(resultSet.getLong(ResultSetColumn.TIME.getColumn()))//("time"))
                .build();
    }
}
