package com.vl.testing.persistence.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.persistence.enums.ResultSetColumn;
import com.vl.testing.persistence.mapper.MapperEntity;

public class QuestionMapperEntity implements MapperEntity<QuestionEntity> {

    @Override
    public QuestionEntity fillEntityWithValues(final ResultSet resultSet) throws SQLException {
        return QuestionEntity.builder()
                .setIdQuestion(resultSet.getLong(ResultSetColumn.QUESTION_ID.getColumn()))//("question_id"))
                .setTestId(resultSet.getLong(ResultSetColumn.QUESTION_TEST_ID.getColumn()))//("question_test_id"))
                .setNumberOfQuestion(resultSet.getInt(ResultSetColumn.NUMBER_QUESTION.getColumn()))//("number_question"))
                .setQuestion(resultSet.getString(ResultSetColumn.QUESTION.getColumn()))//("question"))
                .setAnswer1(resultSet.getString(ResultSetColumn.ANSWER_1.getColumn()))//("answer_1"))
                .setAnswer2(resultSet.getString(ResultSetColumn.ANSWER_2.getColumn()))//("answer_2"))
                .setAnswer3(resultSet.getString(ResultSetColumn.ANSWER_3.getColumn()))//("answer_3"))
                .setAnswer4(resultSet.getString(ResultSetColumn.ANSWER_4.getColumn()))//("answer_4"))
                .setAnswer5(resultSet.getString(ResultSetColumn.ANSWER_5.getColumn()))//("answer_5"))
                .setCorrectAnswer1(resultSet.getBoolean(ResultSetColumn.CORRECT_ANSWER_1.getColumn()))//("correct_answer_1"))
                .setCorrectAnswer2(resultSet.getBoolean(ResultSetColumn.CORRECT_ANSWER_2.getColumn()))//("correct_answer_2"))
                .setCorrectAnswer3(resultSet.getBoolean(ResultSetColumn.CORRECT_ANSWER_3.getColumn()))//("correct_answer_3"))
                .setCorrectAnswer4(resultSet.getBoolean(ResultSetColumn.CORRECT_ANSWER_4.getColumn()))//("correct_answer_4"))
                .setCorrectAnswer5(resultSet.getBoolean(ResultSetColumn.CORRECT_ANSWER_5.getColumn()))//("correct_answer_5"))
                .build();
    }
}
