package com.vl.testing.persistence.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface MapperEntity<E> {

    /**
     * Mapping Entity fields from resultSet.
     * @param resultSet resultSet.
     * @return Entity.
     * @throws SQLException error.
     */
    E fillEntityWithValues(final ResultSet resultSet) throws SQLException;

}
