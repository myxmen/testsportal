package com.vl.testing.persistence.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.enums.ResultSetColumn;
import com.vl.testing.persistence.mapper.MapperEntity;

public class UserMapperEntity implements MapperEntity<UserEntity> {

    @Override
    public UserEntity fillEntityWithValues(final ResultSet resultSet) throws SQLException {
        return UserEntity.builder().setIdUser(resultSet.getLong(ResultSetColumn.USER_ID.getColumn()))//("user_id"))
                .setUsername(resultSet.getString(ResultSetColumn.USERNAME.getColumn())) //("username"))
                .setPassword(resultSet.getString(ResultSetColumn.PASSWORD.getColumn()))//("password"))
                .setAdmin(resultSet.getBoolean(ResultSetColumn.ADMIN.getColumn()))//("admin"))
                .setBan(resultSet.getBoolean(ResultSetColumn.BAN.getColumn()))//("ban"))
                .setLocale(resultSet.getString(ResultSetColumn.LOCALE.getColumn()))//("locale"))
                .build();
    }
}
