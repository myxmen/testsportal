package com.vl.testing.persistence.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.persistence.enums.ResultSetColumn;
import com.vl.testing.persistence.mapper.MapperEntity;

public class SubjectMapperEntity implements MapperEntity<SubjectEntity> {

    @Override
    public SubjectEntity fillEntityWithValues(final ResultSet resultSet) throws SQLException {
        return SubjectEntity.builder()
                .setIdSubject(resultSet.getLong(ResultSetColumn.SUBJECT_ID.getColumn()))//("subject_id"))
                .setSubject(resultSet.getString(ResultSetColumn.SUBJECT.getColumn()))//("subject"))
                .build();
    }
}
