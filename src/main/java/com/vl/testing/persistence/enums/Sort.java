package com.vl.testing.persistence.enums;

import java.util.Objects;

public enum Sort {
    ASC("ASC"),
    DESC("DESC");
    private final String sort;

    Sort(final String sort) {
        this.sort = Objects.requireNonNull(sort);
    }

    public String getSort() {
        return sort;
    }

    /**
     * Checking sort name.
     *
     * @param name name.
     * @return sort name.
     */
    public static String getSortByName(final String name) {
        final String str = name.trim().toUpperCase();
        for (final Sort sort : Sort.values()) {
            if (sort.name().equals(str)) {
                return sort.getSort();
            }
        }
        return ASC.getSort();
    }
}

