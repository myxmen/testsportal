package com.vl.testing.persistence.enums;

import java.util.Objects;

public enum SortColumn {
    /* FIRST-for jsp, ("second") - for DAO */
    TEST("tests.test"),
    RESULT("passed_tests.result"),
    SUBJECT("subject"),
    DIFFICULTY("difficulty"),
    REQUESTS("requests"),
    TIME("time"),
    USER("username"),
    ADMIN("admin"),
    BAN("ban"),
    NUMQUESTION("number_question"),
    QUESTION("question"),
    LOCALE("locale"),

    NONE("");

    private final String column;

    SortColumn(final String column) {
        this.column = Objects.requireNonNull(column);
    }

    public String getColumn() {
        return column;
    }

    /**
     * Get column by name.
     *
     * @param name name of column.
     * @return column.
     */
    public static String getColumnByName(final String name) {
        final String str = name.trim().toUpperCase();
        for (final SortColumn column : SortColumn.values()) {
            if (column.name().equals(str)) {
                return column.getColumn();
            }
        }
        return NONE.getColumn();
    }

    /**
     * Checking records.
     *
     * @param rec records.
     * @return true if records present.
     */
    public static boolean isColumn(final String rec) {
        final String str = rec.toUpperCase();
        for (final SortColumn val : SortColumn.values()) {
            if (val.name().equals(str)) {
                return true;
            }
        }
        return false;
    }
}
