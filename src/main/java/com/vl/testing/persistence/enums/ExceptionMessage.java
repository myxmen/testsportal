package com.vl.testing.persistence.enums;

public enum ExceptionMessage {

    FAILED_TO_CREATE_NEW_QUESTION("Failed to create new question"),
    FAILED_TO_UPDATE_QUESTION("Failed to update question"),
    FAILED_TO_DELETE_QUESTION("Failed to delete question"),
    FAILED_TO_GET_QUESTION_BY_ID("Failed to get the question by id"),
    FAILED_TO_GET_ALL_QUESTIONS_BY_TEST_ID("Failed to get all questions by test id"),
    FAILED_TO_GET_PAGE_OF_QUESTIONS("Failed to get page of questions"),
    FAILED_TO_GET_NUMBER_OF_ROWS_ALL_QUESTIONS("Failed to get number of rows all questions"),

    FAILED_TO_CREATE_NEW_PASSEDTEST("Failed to create new passedTest"),
    FAILED_TO_UPDATE_PASSEDTEST("Failed to update passedTest"),
    FAILED_TO_DELETE_PASSEDTEST("Failed to delete passedTest"),
    FAILED_TO_GET_PASSEDTEST_BY_ID("Failed to get the passedTest by id"),
    FAILED_TO_GET_ALL_PASSEDTEST("Failed to get all passedTest"),
    FAILED_TO_GET_PAGE_OF_PASSEDTEST("Failed to get page of passedTest"),
    FAILED_TO_GET_NUMBER_OF_ROWS_ALL_PASSEDTEST("Failed to get number of rows all passedTest"),

    FAILED_TO_CREATE_NEW_TEST("Failed to create new test"),
    FAILED_TO_UPDATE_TEST("Failed to update test"),
    FAILED_TO_DELETE_TEST("Failed to delete test"),
    FAILED_TO_GET_TEST_BY_ID("Failed to get the test by id"),
    FAILED_TO_GET_ALL_TEST_BY_SUBJECT_ID("Failed to get all test by subject id"),
    FAILED_TO_GET_PAGE_OF_TEST("Failed to get page of test"),
    FAILED_TO_GET_NUMBER_OF_ROWS_ALL_TEST("Failed to get number of rows all test"),
    FAILED_TO_INCREASE_REQUEST_OF_TEST("Failed to increase request of test"),

    FAILED_TO_CREATE_NEW_SUBJECT("Failed to create new subject"),
    FAILED_TO_UPDATE_SUBJECT("Failed to update subject"),
    FAILED_TO_DELETE_SUBJECT("Failed to delete subject"),
    FAILED_TO_GET_SUBJECT_BY_ID("Failed to get the subject by id"),
    FAILED_TO_GET_ALL_SUBJECT("Failed to get all subject"),
    FAILED_TO_GET_PAGE_OF_SUBJECT("Failed to get page of subject"),
    FAILED_TO_GET_NUMBER_OF_ROWS_ALL_SUBJECT("Failed to get number of rows all subject"),

    FAILED_TO_CREATE_NEW_USER("Failed to create new user"),
    FAILED_TO_UPDATE_USER("Failed to update user"),
    FAILED_TO_UPDATE_USER_FIELDS("Failed to update user fields"),
    FAILED_TO_UPDATE_ADMIN_FIELDS("Failed to update admin fields"),
    FAILED_TO_DELETE_USER("Failed to delete user"),
    FAILED_TO_GET_USER_BY_ID("Failed to get the user by id"),
    FAILED_TO_GET_USER_BY_NAME("Failed to get the user by name"),
    FAILED_TO_GET_ALL_USER("Failed to get all user"),
    FAILED_TO_GET_PAGE_OF_USER("Failed to get page of user"),
    FAILED_TO_GET_NUMBER_OF_ROWS_ALL_USER("Failed to get number of rows all user");

    private final String message;

    ExceptionMessage(final String message) {
        this.message = message;
    }

    /**
     * simple exception message.
     *
     * @return String of the message
     */
    public String getMessage() {
        return message;
    }
}
