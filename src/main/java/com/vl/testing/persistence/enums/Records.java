package com.vl.testing.persistence.enums;

public enum Records {
    FIRST(5),
    SECOND(10),
    THIRD(15),
    FOURTH(20);

    private final int value;

    Records(final int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    /**
     * Checking number.
     *
     * @param rec number.
     * @return value.
     */
    public static int isRecords(final Integer rec) {
        for (final Records val : Records.values()) {
            if (val.getValue() == rec) {
                return rec;
            }
        }
        return FIRST.value;
    }
}
