package com.vl.testing.persistence.enums;

import java.util.Objects;

public enum ResultSetColumn {
    /*PassedTest*/
    PASSED_TEST_ID("passed_test_id"),
    PASSED_TEST_USER_ID("passed_test_user_id"),
    PASSED_TEST_TEST_ID("passed_test_test_id"),
    RESULT("result"),
    /*Question*/
    QUESTION_ID("question_id"),
    QUESTION_TEST_ID("question_test_id"),
    NUMBER_QUESTION("number_question"),
    QUESTION("question"),
    ANSWER_1("answer_1"),
    ANSWER_2("answer_2"),
    ANSWER_3("answer_3"),
    ANSWER_4("answer_4"),
    ANSWER_5("answer_5"),
    CORRECT_ANSWER_1("correct_answer_1"),
    CORRECT_ANSWER_2("correct_answer_2"),
    CORRECT_ANSWER_3("correct_answer_3"),
    CORRECT_ANSWER_4("correct_answer_4"),
    CORRECT_ANSWER_5("correct_answer_5"),
    /*Subject*/
    SUBJECT_ID("subject_id"),
    SUBJECT("subject"),
    /*Test*/
    TEST_ID("test_id"),
    TEST_SUBJECT_ID("test_subject_id"),
    TEST("test"),
    DIFFICULTY("difficulty"),
    REQUESTS("requests"),
    TIME("time"),
    /*User*/
    USER_ID("user_id"),
    USERNAME("username"),
    PASSWORD("password"),
    ADMIN("admin"),
    BAN("ban"),
    LOCALE("locale"),
    NONE("");

    private final String column;

    ResultSetColumn(final String column) {
        this.column = Objects.requireNonNull(column);
    }

    public String getColumn() {
        return column;
    }
}
