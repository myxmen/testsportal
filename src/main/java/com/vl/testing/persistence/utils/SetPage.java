package com.vl.testing.persistence.utils;

import java.util.Objects;

import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.enums.SortColumn;

public class SetPage {
    private final Integer startRows;
    private final Integer recordsPerPage;
    private final String sort;
    private final String sortColumn;

    private SetPage(final Integer startRows, final Integer recordsPerPage, final String sort, final String sortColumn) {
        this.startRows = startRows;
        this.recordsPerPage = recordsPerPage;
        this.sort = sort;
        this.sortColumn = sortColumn;
    }

    public Integer getStartRows() {
        return startRows;
    }

    public Integer getRecordsPerPage() {
        return recordsPerPage;
    }

    public String getSort() {
        return sort;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    /**
     * init Builder.
     *
     * @return Page builder.
     */
    public static SetPageBuilder builder() {
        return new SetPageBuilder();
    }

    public static class SetPageBuilder {
        private Integer startRows;

        private Integer recordsPerPage;

        private String sort;
        private String sortColumn;

        // CHECKSTYLE_OFF: JavadocMethod
        public SetPageBuilder setStartRows(final Integer startRows) {
            Objects.requireNonNull(startRows);
            this.startRows = startRows;
            return this;
        }

        public SetPageBuilder setRecordsPerPage(final Integer recordsPerPage) {
            Objects.requireNonNull(recordsPerPage);
            this.recordsPerPage = recordsPerPage;
            return this;
        }

        public SetPageBuilder setSort(final String sort) {
            Objects.requireNonNull(sort);
            this.sort = sort;
            return this;
        }

        public SetPageBuilder setSortColumn(final String sortColumn) {
            Objects.requireNonNull(sortColumn);
            this.sortColumn = sortColumn;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built Page.
         */
        public SetPage build() {
            return new SetPage(startRows, recordsPerPage, Sort.getSortByName(sort), SortColumn.getColumnByName(sortColumn));
        }

    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final SetPage setPage = (SetPage) object;
        return Objects.equals(startRows, setPage.startRows)
                && Objects.equals(recordsPerPage, setPage.recordsPerPage)
                && Objects.equals(sort, setPage.sort)
                && Objects.equals(sortColumn, setPage.sortColumn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startRows, recordsPerPage, sort, sortColumn);
    }

    @Override
    public String toString() {
        return "SetPageBuilder{"
                + "startRows=" + startRows
                + ", recordsPerPage=" + recordsPerPage
                + ", sort='" + sort + '\''
                + ", sortColumn='" + sortColumn + '\''
                + '}';
    }
}
