package com.vl.testing.validation;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.enums.SortColumn;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;

@SuppressWarnings({"PMD.ModifiedCyclomaticComplexity", "PMD.StdCyclomaticComplexity"})
public class ValidatePagination {

    /**
     * Validate Pagination fields.
     *
     * @param pagination Pagination.
     * @return ValidatePagination.
     * @throws ValidateException Validate error.
     */
    @SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.NPathComplexity"})
    public ValidatePagination validate(final Pagination pagination) throws ValidateException {
        if (pagination == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE));
        }

        final Integer startRows = pagination.getStartRows();
        if (startRows == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATIONSTARTROWS_IS_EMPTY, Error.ERROR_PAGE));
        }
        if (startRows < 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATIONSTARTROWS_IS_NEGATIVE, Error.ERROR_PAGE));
        }

        final Integer recordsPerPage = pagination.getRecordsPerPage();
        if (recordsPerPage == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATIONRECORDSPERPAGE_IS_EMPTY, Error.ERROR_PAGE));
        }
        if (recordsPerPage < 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATIONRECORDSPERPAGE_IS_NEGATIVE, Error.ERROR_PAGE));
        }

        final String sort = pagination.getSort();
        if (sort == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATIONSORT_IS_EMPTY, Error.ERROR_PAGE));
        }
        if (!(sort.equalsIgnoreCase(Sort.ASC.getSort())
                || sort.equalsIgnoreCase(Sort.DESC.getSort()))) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATIONSORT_IS_INVALID, Error.ERROR_PAGE));
        }

        final String sortColumn = pagination.getSortColumn();
        if (sortColumn == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATIONSORTCOLUMN_IS_EMPTY, Error.ERROR_PAGE));
        }
        if (!SortColumn.isColumn(sortColumn)) {
            throw new ValidateException(getValidateException(ExceptionMessage.PAGINATIONSORTCOLUMN_IS_INVALID, Error.ERROR_PAGE));
        }
        return this;
    }
}
