package com.vl.testing.validation;

import com.vl.testing.validation.exeption.ValidateException;

public interface Validate<D> {

    /**
     * Validate DTO fields.
     *
     * @param dto DTO.
     * @return Validate.
     * @throws ValidateException Validate error.
     */
    Validate validate(D dto) throws ValidateException;

    /**
     * Validate DTO`s id field.
     *
     * @param idDTO id.
     * @return Validate.
     * @throws ValidateException Validate error.
     */
    Validate validateId(Long idDTO) throws ValidateException;
}
