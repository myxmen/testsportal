package com.vl.testing.validation.util;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.validation.enums.ExceptionMessage;

public final class ValidateUtil {

    private ValidateUtil() {
    }

    /**
     * Make and return text exception message: "text exception message {jsp`s error}".
     *
     * @param exceptionMessage text.
     * @param errorMessage     jsp`s error.
     * @return sum of text & error.
     */
    public static String getValidateException(final String exceptionMessage, final String errorMessage) {
        return exceptionMessage + "{" + errorMessage + "}";
    }

    /**
     * Make text exception message: "text exeption message {jsp`s error}".
     *
     * @param exception text.
     * @param error     jsp`s error.
     * @return sum of text & error.
     */
    public static String getValidateException(final ExceptionMessage exception, final Error error) {
        return exception.getMessage() + "{" + error.getMessage() + "}";
    }

    /**
     * Get message text for jsp from exception message.
     *
     * @param exception text of error.
     * @return message text for jsp.
     */
    public static String getExceptionMessage(final String exception) {
        return exception.substring(0, exception.indexOf('{'));
    }

    /**
     * Get error for jsp from exception message.
     *
     * @param error text of error.
     * @return error text for jsp.
     */
    public static String getError(final String error) {
        return error.substring(error.indexOf('{') + 1, error.indexOf('}'));
    }
}
