package com.vl.testing.validation.impl;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.validation.Validate;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;

public class ValidateSubjectDTO implements Validate<SubjectDTO> {
    private static final String REGEX = "\\s";

    @Override
    public Validate validate(final SubjectDTO subjectDTO) throws ValidateException {
        if (subjectDTO == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.SUBJECTDTO_IS_EMPTY, Error.ERROR_SUBJECT));
        }
        if (subjectDTO.getSubject() == null || subjectDTO.getSubject().replaceAll(REGEX, "").equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.SUBJECT_NAME_IS_EMPTY, Error.ERROR_SUBJECT));
        }
        return this;
    }

    @Override
    public Validate validateId(final Long subjectId) throws ValidateException {
        if (subjectId == null || subjectId <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.SUBJECT_ID_IS_EMPTY, Error.ERROR_PAGE));
        }
        return this;
    }
}
