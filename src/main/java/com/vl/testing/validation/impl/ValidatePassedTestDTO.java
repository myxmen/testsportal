package com.vl.testing.validation.impl;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.validation.Validate;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;

public class ValidatePassedTestDTO implements Validate<PassedTestDTO> {

    @Override
    public Validate validate(final PassedTestDTO passedTestDTO) throws ValidateException {
        if (passedTestDTO == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.PASSEDTESTDTO_IS_EMPTY, Error.ERROR_PAGE));
        }

        if (passedTestDTO.getTest().getId() == null || passedTestDTO.getTest().getId() <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.PASSEDTEST_TESTID_IS_EMPTY, Error.ERROR_PAGE));
        }

        if (passedTestDTO.getResult() == null || passedTestDTO.getResult() < 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.PASSEDTEST_RESULT_IS_EMPTY, Error.ERROR_PAGE));
        }
        return this;
    }

    @Override
    public Validate validateId(final Long passedTestId) throws ValidateException {
        if (passedTestId == null || passedTestId <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.PASSEDTEST_ID_IS_EMPTY, Error.ERROR_PAGE));
        }
        return this;
    }
}
