package com.vl.testing.validation.impl;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.validation.Validate;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;

@SuppressWarnings({"PMD.ModifiedCyclomaticComplexity", "PMD.StdCyclomaticComplexity"})
public class ValidateQuestionDTO implements Validate<QuestionDTO> {

    @Override
    @SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.NPathComplexity"})
    public Validate validate(final QuestionDTO questionDTO) throws ValidateException {
        if (questionDTO == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTIONDTO_IS_EMPTY, Error.ERROR_QUESTION));
        }

        if (questionDTO.getTestId() == null || questionDTO.getTestId() <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_TESTID_IS_EMPTY, Error.ERROR_QUESTION));
        }

        if (questionDTO.getNumberOfQuestion() == null || questionDTO.getNumberOfQuestion() <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_NUMDER_OF_QUESTION_IS_EMPTY, Error.ERROR_QUESTION));
        }

        if (questionDTO.getQuestion() == null || questionDTO.getQuestion().trim().equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_QUESTION_IS_EMPTY, Error.ERROR_QUESTION));
        }

        if (questionDTO.getAnswer1() == null || questionDTO.getAnswer1().trim().equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_ANSWER1_IS_EMPTY, Error.ERROR_ANSWER1));
        }

        if (questionDTO.getAnswer2() == null || questionDTO.getAnswer2().trim().equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_ANSWER2_IS_EMPTY, Error.ERROR_ANSWER2));
        }

        if (questionDTO.getAnswer3() != null && questionDTO.getAnswer3().trim().equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_ANSWER3_IS_EMPTY, Error.ERROR_QUESTION));
        }

        if (questionDTO.getAnswer4() != null && questionDTO.getAnswer4().trim().equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_ANSWER4_IS_EMPTY, Error.ERROR_QUESTION));
        }

        if (questionDTO.getAnswer5() != null && questionDTO.getAnswer5().trim().equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_ANSWER5_IS_EMPTY, Error.ERROR_QUESTION));
        }
        return this;
    }

    @Override
    public Validate validateId(final Long questionId) throws ValidateException {
        if (questionId == null || questionId <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.QUESTION_ID_IS_EMPTY, Error.ERROR_PAGE));
        }
        return this;
    }
}
