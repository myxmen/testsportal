package com.vl.testing.validation.impl;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.validation.Validate;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;

public class ValidateUserDTO implements Validate<UserDTO> {

    @Override
    public ValidateUserDTO validate(final UserDTO userDTO) throws ValidateException {
        validateNameAndPassword(userDTO);
        validateAdminsFields(userDTO);

        if (userDTO.getLocale() == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.USERFIELD_LOCALE_IS_EMPTY, Error.INVALID_LOGIN));
        }
        return this;
    }

    /**
     * Validate admin DTO`s fields.
     *
     * @param userDTO UserDTO.
     * @return ValidateUserDTO.
     * @throws ValidateException Validate error.
     */
    public ValidateUserDTO validateAdminsFields(final UserDTO userDTO) throws ValidateException {
        if (userDTO.isBan() == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.USERFIELD_BAN_IS_EMPTY, Error.ERROR_PAGE));
        }
        if (userDTO.isAdmin() == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.USERFIELD_ADMIN_IS_EMPTY, Error.ERROR_PAGE));
        }
        return this;
    }

    /**
     * Validate name & password DTO`s fields.
     *
     * @param userDTO UserDTO.
     * @return ValidateUserDTO.
     * @throws ValidateException Validate error.
     */
    public ValidateUserDTO validateNameAndPassword(final UserDTO userDTO) throws ValidateException {
        if (userDTO == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.USERDTO_IS_EMPTY, Error.INVALID_LOGIN));
        }

        if (userDTO.getUsername() == null || userDTO.getUsername().equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.USERFIELD_USERNAME_IS_EMPTY, Error.INVALID_LOGIN));
        }

        if (userDTO.getPassword() == null || userDTO.getPassword().equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.USERFIELD_PASSWORD_IS_EMPTY, Error.INVALID_PASSWORD));
        }
        return this;
    }

    @Override
    public ValidateUserDTO validateId(final Long userId) throws ValidateException {
        if (userId == null || userId <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.USER_ID_IS_EMPTY, Error.ERROR_PAGE));
        }
        return this;
    }
}
