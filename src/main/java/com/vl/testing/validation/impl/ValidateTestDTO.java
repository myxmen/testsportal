package com.vl.testing.validation.impl;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.validation.Validate;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;

public class ValidateTestDTO implements Validate<TestDTO> {
    private static final String REGEX = "\\s";

    @Override
    @SuppressWarnings("PMD.CyclomaticComplexity")
    public Validate validate(final TestDTO testDTO) throws ValidateException {
        if (testDTO == null) {
            throw new ValidateException(getValidateException(ExceptionMessage.TESTDTO_IS_EMPTY, Error.ERROR_TEST));
        }
        if (testDTO.getSubjectId() == null || testDTO.getSubjectId() <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.TEST_SUBJECTID_IS_EMPTY, Error.ERROR_TEST));
        }
        if (testDTO.getTest() == null || testDTO.getTest().replaceAll(REGEX, "").equals("")) {
            throw new ValidateException(getValidateException(ExceptionMessage.TEST_NAME_IS_EMPTY, Error.ERROR_TEST));
        }
        if (testDTO.getRequests() == null || testDTO.getRequests() < 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.TEST_REQUEST_IS_INVALID, Error.ERROR_TEST));
        }
        if (testDTO.getTime() == null || testDTO.getTime() <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.TEST_TIME_IS_INVALID, Error.ERROR_TEST));
        }
        return this;
    }

    @Override
    public Validate validateId(final Long testId) throws ValidateException {
        if (testId == null || testId <= 0) {
            throw new ValidateException(getValidateException(ExceptionMessage.TEST_ID_IS_EMPTY, Error.ERROR_PAGE));
        }
        return this;
    }
}
