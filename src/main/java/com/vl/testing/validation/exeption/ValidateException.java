package com.vl.testing.validation.exeption;

/**
 * Exception can be thrown if DTO fields is not valid.
 */
public class ValidateException extends Exception {
    private static final long serialVersionUID = 1234567890L;

    /**
     * Creates ValidateExeption instance, accepts exception message itself.
     *
     * @param message error message.
     */
    public ValidateException(final String message) {
        super(message);
    }
}
