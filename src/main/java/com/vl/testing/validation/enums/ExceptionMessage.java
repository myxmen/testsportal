package com.vl.testing.validation.enums;

public enum ExceptionMessage {

    /* UserDTO */
    //  USER_WAS_NOT_FOUND("User with this ID was not found"),
    USER_ID_IS_EMPTY("User ID is empty"),
    USERDTO_IS_EMPTY("UserDTO is empty"),
    USERFIELD_USERNAME_IS_EMPTY("Field Username is empty"),
    USERFIELD_PASSWORD_IS_EMPTY("Field Password is empty"),
    USERFIELD_ADMIN_IS_EMPTY("Field Admin is empty"),
    USERFIELD_BAN_IS_EMPTY("Field Ban is empty"),
    USERFIELD_LOCALE_IS_EMPTY("Field Locale is empty"),

    /* SubjectDTO */
    SUBJECTDTO_IS_EMPTY("SubjectDTO is empty"),
    SUBJECT_NAME_IS_EMPTY("Subject name is empty"),
    SUBJECT_ID_IS_EMPTY("Subject ID is empty"),

    /* TestDTO */
    TESTDTO_IS_EMPTY("TestDTO is empty"),
    TEST_SUBJECTID_IS_EMPTY("Test`s subject ID is empty"),
    TEST_NAME_IS_EMPTY("Test name is empty"),
    TEST_REQUEST_IS_INVALID("Invalid test`s requests"),
    TEST_TIME_IS_INVALID("Invalid test`s time"),
    TEST_ID_IS_EMPTY("Test ID is empty"),

    /* QuestionDTO */
    QUESTIONDTO_IS_EMPTY("QuestionDTO is empty"),
    QUESTION_ID_IS_EMPTY("Question ID is empty"),
    QUESTION_TESTID_IS_EMPTY("Question`s test ID is empty"),
    QUESTION_NUMDER_OF_QUESTION_IS_EMPTY("Invalid number of question"),
    QUESTION_QUESTION_IS_EMPTY("Question question is empty"),
    QUESTION_ANSWER1_IS_EMPTY("Question answer1 is empty"),
    QUESTION_ANSWER2_IS_EMPTY("Question answer2 is empty"),
    QUESTION_ANSWER3_IS_EMPTY("Question answer3 is empty"),
    QUESTION_ANSWER4_IS_EMPTY("Question answer4 is empty"),
    QUESTION_ANSWER5_IS_EMPTY("Question answer5 is empty"),

    PASSEDTESTDTO_IS_EMPTY("PassedTestDTO is empty"),
    PASSEDTEST_ID_IS_EMPTY("PassedTest ID is empty"),
    PASSEDTEST_TESTID_IS_EMPTY("PassedTest`s test ID is empty"),
    PASSEDTEST_RESULT_IS_EMPTY("PassedTest`s results is empty"),

    PAGINATION_IS_EMPTY("Pagination is empty"),
    PAGINATIONSTARTROWS_IS_EMPTY("Pagination`s Start Rows is empty"),
    PAGINATIONSTARTROWS_IS_NEGATIVE("Pagination`s Start Rows is negative"),
    PAGINATIONRECORDSPERPAGE_IS_EMPTY("Pagination`s Records per page is empty"),
    PAGINATIONRECORDSPERPAGE_IS_NEGATIVE("Pagination`s Records per page is negative"),
    PAGINATIONSORT_IS_EMPTY("Pagination`s Sort is empty"),
    PAGINATIONSORT_IS_INVALID("Invalid Pagination`s Sort"),
    PAGINATIONSORTCOLUMN_IS_EMPTY("Pagination`s Sort column is empty"),
    PAGINATIONSORTCOLUMN_IS_INVALID("Invalid Pagination`s Sort column"),

    END("");
    private final String message;

    ExceptionMessage(final String message) {
        this.message = message;
    }

    /**
     * simple exception message.
     *
     * @return String of the message
     */
    public String getMessage() {
        return message;
    }

//    @Override
//    public String toString() {
//        return message;
//    }
}
