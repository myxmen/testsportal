package com.vl.testing.service.exeptions;

/**
 * Exception can be thrown if Password is not valid.
 */
public class PasswordException extends Exception {
    private static final long serialVersionUID = 1234567891L;

    /**
     * Creates PasswordException instance, accepts exception message itself.
     *
     * @param message error message.
     */
    public PasswordException(final String message) {
        super(message);
    }
}
