package com.vl.testing.service.enums;

public enum ExceptionMessage {

    INVALID_PASSWORD("Wrong password!");

    private final String message;

    ExceptionMessage(final String message) {
        this.message = message;
    }

    /**
     * simple exception message.
     *
     * @return String of the message
     */
    public String getMessage() {
        return message;
    }
}
