package com.vl.testing.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daos.SubjectDao;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.mappers.PaginationMapperService;
import com.vl.testing.service.mappers.impl.SubjectEntityMapperService;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateSubjectDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SubjectServiceImpl implements SubjectService {
    private static final Logger LOGGER = LogManager.getLogger(SubjectServiceImpl.class);
    private final DaoFactory daoFactory;
    private final ValidateSubjectDTO validateSubjectDTO;
    private final ValidatePagination validatePagination;

    public SubjectServiceImpl() {
        this.daoFactory = DaoFactory.getInstance();
        this.validateSubjectDTO = new ValidateSubjectDTO();
        this.validatePagination = new ValidatePagination();
        LOGGER.info("Internal set Subject service");
    }

    public SubjectServiceImpl(final DaoFactory dao, final ValidateSubjectDTO validate, final ValidatePagination pagination) {
        this.daoFactory = dao;
        this.validateSubjectDTO = validate;
        this.validatePagination = pagination;
        LOGGER.info("External set Subject service");
    }

    @Override
    public SubjectDTO create(final SubjectDTO subjectDTO) throws RepositoryException, ValidateException {
        validateSubjectDTO.validate(subjectDTO);
        try (SubjectDao dao = daoFactory.createSubjectDao()) {
            return new SubjectEntityMapperService().entityToDTO(dao
                    .create(new SubjectEntityMapperService().dtoToEntity(subjectDTO)));
        }
    }

    @Override
    public Optional<SubjectDTO> get(final Long idSubject) throws RepositoryException, ValidateException {
        validateSubjectDTO.validateId(idSubject);
        try (SubjectDao dao = daoFactory.createSubjectDao()) {
            final Optional<SubjectEntity> entity = dao.get(idSubject);
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new SubjectEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public Optional<SubjectDTO> update(final SubjectDTO subjectDTO) throws RepositoryException, ValidateException {
        validateSubjectDTO.validateId(subjectDTO.getId()).validate(subjectDTO);
        try (SubjectDao dao = daoFactory.createSubjectDao()) {
            final Optional<SubjectEntity> entity = dao.update(new SubjectEntityMapperService().dtoToEntity(subjectDTO));
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new SubjectEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public boolean delete(final Long idSubject) throws RepositoryException, ValidateException {
        validateSubjectDTO.validateId(idSubject);
        try (SubjectDao dao = daoFactory.createSubjectDao()) {
            return dao.delete(idSubject);
        }

    }

    @Override
    public List<SubjectDTO> getAll() throws RepositoryException {
        try (SubjectDao dao = daoFactory.createSubjectDao()) {
            return dao.getAll().stream()
                    .map(x -> new SubjectEntityMapperService().entityToDTO(x))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public List<SubjectDTO> getPage(final Pagination pagination) throws RepositoryException, ValidateException {
        validatePagination.validate(pagination);
        try (SubjectDao dao = daoFactory.createSubjectDao()) {
            return dao.getPage(new PaginationMapperService().paginationToSetPage(pagination))
                    .stream()
                    .map(x -> new SubjectEntityMapperService().entityToDTO(x))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public Integer getNumberOfRows() throws RepositoryException {
        try (SubjectDao dao = daoFactory.createSubjectDao()) {
            return dao.getNumberOfRows();
        }
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final SubjectServiceImpl that = (SubjectServiceImpl) object;
        return Objects.equals(daoFactory, that.daoFactory)
                && Objects.equals(validateSubjectDTO, that.validateSubjectDTO)
                && Objects.equals(validatePagination, that.validatePagination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(daoFactory, validateSubjectDTO, validatePagination);
    }
}
