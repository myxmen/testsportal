package com.vl.testing.service.impl;

import java.util.List;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.Service;
import com.vl.testing.validation.exeption.ValidateException;

public interface SubjectService extends Service<SubjectDTO> {

    /**
     * Get All of SubjectDTOs.
     *
     * @return list of SubjectDTO.
     * @throws RepositoryException Repository error.
     */
    List<SubjectDTO> getAll() throws RepositoryException;

    /**
     * Get Page of SubjectDTOs by pagination.
     *
     * @param pagination Pagination.
     * @return list of SubjectDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException    Validate error.
     */
    List<SubjectDTO> getPage(Pagination pagination) throws RepositoryException, ValidateException;

    /**
     * Get Number Of Rows.
     *
     * @return Number Of Rows.
     * @throws RepositoryException Repository error.
     */
    Integer getNumberOfRows() throws RepositoryException;
}
