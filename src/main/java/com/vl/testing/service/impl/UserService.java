package com.vl.testing.service.impl;

import java.util.List;
import java.util.Optional;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.Service;
import com.vl.testing.service.exeptions.PasswordException;
import com.vl.testing.validation.exeption.ValidateException;

public interface UserService extends Service<UserDTO> {

    /**
     * Get All of UserDTO.
     *
     * @return list of users.
     * @throws RepositoryException Repository error.
     */
    List<UserDTO> getAll() throws RepositoryException;

    /**
     * Get user by name & password.
     *
     * @param userDTO UserDTO.
     * @return Optional UserDTO.
     * @throws RepositoryException Repository error.
     * @throws PasswordException   Password error.
     * @throws ValidateException   Validate error.
     */
    Optional<UserDTO> getByNameAndPassword(UserDTO userDTO) throws RepositoryException, PasswordException, ValidateException;

    /**
     * Get page of users.
     *
     * @param pagination pagination.
     * @return list of users.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    List<UserDTO> getPage(Pagination pagination) throws RepositoryException, ValidateException;

    /**
     * Get Number Of Rows.
     *
     * @return Number Of Rows.
     * @throws RepositoryException Repository error.
     */
    Integer getNumberOfRows() throws RepositoryException;

    /**
     * Get Page of logged users.
     *
     * @param list       list of users.
     * @param pagination pagination.
     * @return list of users.
     * @throws ValidateException Validate error.
     */
    List<UserDTO> getPageLoggedUsers(List<UserDTO> list, Pagination pagination) throws ValidateException;

    /**
     * Update admins users fields.
     *
     * @param userDTO UserDTO.
     * @return Optional UserDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    Optional<UserDTO> updateAdminsFields(UserDTO userDTO) throws RepositoryException, ValidateException;

    /**
     * Update users fields.
     *
     * @param userDTO UserDTO.
     * @return Optional UserDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    Optional<UserDTO> updateUsersFields(UserDTO userDTO) throws RepositoryException, ValidateException;

    /**
     * Get Number Of Rows logged users.
     *
     * @param list list of users.
     * @return Number Of Rows.
     */
    Integer getNumberOfRowsLoggedUsers(final List<UserDTO> list);
}
