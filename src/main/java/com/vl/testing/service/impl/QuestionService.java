package com.vl.testing.service.impl;

import java.util.List;
import java.util.Map;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.Service;
import com.vl.testing.validation.exeption.ValidateException;

public interface QuestionService extends Service<QuestionDTO> {

    /**
     * Get All of QuestionDTOs by TestDTO.
     *
     * @param test TestDTO.
     * @return list of QuestionDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    List<QuestionDTO> getAllByTest(TestDTO test) throws RepositoryException, ValidateException;

    /**
     * Calculate score.
     *
     * @param questionToAnswer map QuestionDTO & array of answers.
     * @param size             size test`s list of questions.
     * @return score.
     */
    Integer getScore(Map<QuestionDTO, String[]> questionToAnswer, Integer size);

    /**
     * Get Number Of Rows by TestDTO.
     *
     * @param testDTO TestDTO.
     * @return Number Of Rows.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    Integer getNumberOfRows(TestDTO testDTO) throws RepositoryException, ValidateException;

    /**
     * Get Page of QuestionDTOs by TestDTO & pagination.
     *
     * @param testDTO    TestDTO.
     * @param pagination pagination.
     * @return list of QuestionDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    List<QuestionDTO> getPage(TestDTO testDTO, Pagination pagination) throws RepositoryException, ValidateException;
}
