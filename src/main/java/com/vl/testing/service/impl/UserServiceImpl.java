package com.vl.testing.service.impl;

// CHECKSTYLE_OFF: AvoidStarImport
import java.util.*;
// CHECKSTYLE_ON: AvoidStarImport
import java.util.stream.Collectors;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daos.UserDao;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.enums.SortColumn;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.enums.ExceptionMessage;
import com.vl.testing.service.exeptions.PasswordException;
import com.vl.testing.service.mappers.PaginationMapperService;
import com.vl.testing.service.mappers.impl.UserEntityMapperService;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateUserDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);
    private final DaoFactory daoFactory;
    private final ValidateUserDTO validateUserDTO;
    private final ValidatePagination validatePagination;
    private static final int ROUNDS = 12;
    private static final String EMPTYLINE = "";

    public UserServiceImpl() {
        this.daoFactory = DaoFactory.getInstance();
        this.validateUserDTO = new ValidateUserDTO();
        this.validatePagination = new ValidatePagination();
        LOGGER.info("Internal set User service");
    }

    public UserServiceImpl(final DaoFactory dao, final ValidateUserDTO validateUserDTO, final ValidatePagination pagination) {
        this.daoFactory = dao;
        this.validateUserDTO = validateUserDTO;
        this.validatePagination = pagination;
        LOGGER.info("External set User service");
    }

    @Override
    public UserDTO create(final UserDTO userDTO) throws RepositoryException, ValidateException {
        validateUserDTO.validate(userDTO);
        userDTO.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt(ROUNDS)));
        try (UserDao dao = daoFactory.createUserDao()) {
            final UserEntity entity = dao.create(new UserEntityMapperService().dtoToEntity(userDTO));
            userDTO.setPassword(EMPTYLINE);
            return new UserEntityMapperService().entityToDTO(entity);
        }
    }

    @Override
    public Optional<UserDTO> get(final Long idUser) throws RepositoryException, ValidateException {
        validateUserDTO.validateId(idUser);
        try (UserDao dao = daoFactory.createUserDao()) {
            final Optional<UserEntity> entity = dao.get(idUser);
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new UserEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public Optional<UserDTO> getByNameAndPassword(final UserDTO userDTO) throws RepositoryException, PasswordException, ValidateException {
        validateUserDTO.validateNameAndPassword(userDTO);
        try (UserDao dao = daoFactory.createUserDao()) {
            final Optional<UserEntity> entity = dao.getByName(userDTO.getUsername());
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            if (BCrypt.checkpw(userDTO.getPassword(), entity.get().getPassword())) {
                userDTO.setPassword(EMPTYLINE);
                return Optional.of(new UserEntityMapperService().entityToDTO(entity.get()));
            }
            throw new PasswordException(ExceptionMessage.INVALID_PASSWORD.getMessage());
        }
    }

    // NOT USE!!!
    @Override
    public Optional<UserDTO> update(final UserDTO userDTO) throws RepositoryException, ValidateException {
        validateUserDTO.validateId(userDTO.getId()).validate(userDTO);
        userDTO.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt(ROUNDS)));
        try (UserDao dao = daoFactory.createUserDao()) {
            final Optional<UserEntity> entity = dao.update(new UserEntityMapperService().dtoToEntity(userDTO));
            userDTO.setPassword(EMPTYLINE);
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new UserEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public Optional<UserDTO> updateAdminsFields(final UserDTO userDTO) throws RepositoryException, ValidateException {
        validateUserDTO.validateId(userDTO.getId()).validateAdminsFields(userDTO);
        try (UserDao dao = daoFactory.createUserDao()) {
            final Optional<UserEntity> entity = dao.updateAdminsFields(new UserEntityMapperService().dtoToEntity(userDTO));
            userDTO.setPassword(EMPTYLINE);
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new UserEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public Optional<UserDTO> updateUsersFields(final UserDTO userDTO) throws RepositoryException, ValidateException {
        validateUserDTO.validateId(userDTO.getId()).validate(userDTO);
        userDTO.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt(ROUNDS)));
        try (UserDao dao = daoFactory.createUserDao()) {
            final Optional<UserEntity> entity = dao.updateUsersFields(new UserEntityMapperService().dtoToEntity(userDTO));
            userDTO.setPassword(EMPTYLINE);
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new UserEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public boolean delete(final Long idUser) throws RepositoryException, ValidateException {
        validateUserDTO.validateId(idUser);
        try (UserDao dao = daoFactory.createUserDao()) {
            return dao.delete(idUser);
        }
    }

    @Override
    public List<UserDTO> getAll() throws RepositoryException {
        try (UserDao dao = daoFactory.createUserDao()) {
            return dao.getAll().stream()
                    .map(x -> new UserEntityMapperService().entityToDTO(x))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public List<UserDTO> getPageLoggedUsers(final List<UserDTO> list, final Pagination pagination) throws ValidateException {
        validatePagination.validate(pagination);
        final String sortColumn = Sort.getSortByName(pagination.getSort()) + SortColumn.getColumnByName(pagination.getSortColumn());
        final Map<String, Comparator> sort = new HashMap();
        sort.put(Sort.ASC.getSort() + SortColumn.USER.getColumn(), Comparator.comparing(UserDTO::getUsername));
        sort.put(Sort.DESC.getSort() + SortColumn.USER.getColumn(), Comparator.comparing(UserDTO::getUsername).reversed());
        sort.put(Sort.ASC.getSort() + SortColumn.ADMIN.getColumn(), Comparator.comparing(UserDTO::isAdmin)
                .thenComparing(UserDTO::getUsername));
        sort.put(Sort.DESC.getSort() + SortColumn.ADMIN.getColumn(), Comparator.comparing(UserDTO::isAdmin)
                .thenComparing(UserDTO::getUsername).reversed());
        sort.put(Sort.ASC.getSort() + SortColumn.TIME.getColumn(), Comparator.comparing(UserDTO::getLoginTime));
        sort.put(Sort.DESC.getSort() + SortColumn.TIME.getColumn(), Comparator.comparing(UserDTO::getLoginTime).reversed());
        final Comparator<UserDTO> sortUser = sort.get(sortColumn);
        List<UserDTO> page;
        LOGGER.info(pagination.getStartRows());
        LOGGER.info(pagination.getRecordsPerPage());
        page = list.stream()
                .sorted(sortUser)
                .skip(pagination.getStartRows())
                .limit(pagination.getRecordsPerPage())
                .collect(Collectors.toList());
        return page;
    }

    @Override
    public Integer getNumberOfRowsLoggedUsers(final List<UserDTO> list) {
        Objects.requireNonNull(list);
        LOGGER.info(list.size());
        return list.size();
    }

    @Override
    public List<UserDTO> getPage(final Pagination pagination) throws RepositoryException, ValidateException {
        validatePagination.validate(pagination);
        try (UserDao dao = daoFactory.createUserDao()) {
            return dao.getPage(new PaginationMapperService().paginationToSetPage(pagination))
                    .stream()
                    .map(x -> new UserEntityMapperService().entityToDTO(x))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public Integer getNumberOfRows() throws RepositoryException {
        try (UserDao dao = daoFactory.createUserDao()) {
            return dao.getNumberOfRows();
        }
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final UserServiceImpl that = (UserServiceImpl) object;
        return ROUNDS == that.ROUNDS
                && Objects.equals(daoFactory, that.daoFactory)
                && Objects.equals(validateUserDTO, that.validateUserDTO)
                && Objects.equals(validatePagination, that.validatePagination)
                && Objects.equals(EMPTYLINE, that.EMPTYLINE);
    }

    @Override
    public int hashCode() {
        return Objects.hash(daoFactory, validateUserDTO, validatePagination, ROUNDS, EMPTYLINE);
    }
}
