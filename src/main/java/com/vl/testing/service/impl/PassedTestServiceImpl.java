package com.vl.testing.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daos.PassedTestDao;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.mappers.PaginationMapperService;
import com.vl.testing.service.mappers.impl.PassedTestEntityMapperService;
import com.vl.testing.service.mappers.impl.UserEntityMapperService;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidatePassedTestDTO;
import com.vl.testing.validation.impl.ValidateUserDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PassedTestServiceImpl implements PassedTestService<PassedTestDTO, UserDTO> {
    private static final Logger LOGGER = LogManager.getLogger(PassedTestServiceImpl.class);
    private final DaoFactory daoFactory;
    private final ValidatePassedTestDTO validatePassedTestDTO;
    private final ValidateUserDTO validateUserDTO;
    private final ValidatePagination validatePagination;

    public PassedTestServiceImpl() {
        this.daoFactory = DaoFactory.getInstance();
        this.validatePassedTestDTO = new ValidatePassedTestDTO();
        this.validateUserDTO = new ValidateUserDTO();
        this.validatePagination = new ValidatePagination();
        LOGGER.info("Internal set PassedTest service");
    }

    public PassedTestServiceImpl(final DaoFactory dao, final ValidatePassedTestDTO validatePassedTestDTO,
                                 final ValidateUserDTO validateUserDTO, final ValidatePagination pagination) {
        this.daoFactory = dao;
        this.validatePassedTestDTO = validatePassedTestDTO;
        this.validateUserDTO = validateUserDTO;
        this.validatePagination = pagination;
        LOGGER.info("External set PassedTest service");
    }

    @Override
    public List<PassedTestDTO> getAllPassedTestFosUser(final UserDTO userDTO) throws RepositoryException, ValidateException {
        validateUserDTO.validateId(userDTO.getId());
        Map<PassedTestEntity, TestEntity> all;
        try (PassedTestDao dao = daoFactory.createPassedTestDao()) {
            all = dao.getAllPassedTestFosUser(new UserEntityMapperService().dtoToEntity(userDTO));
        }
        return all.entrySet().stream().map(x -> new PassedTestEntityMapperService().entityToDTO(x.getKey(), x.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public PassedTestDTO create(final PassedTestDTO passedTestDTO, final UserDTO userDTO) throws RepositoryException, ValidateException {
        validatePassedTestDTO.validate(passedTestDTO);
        validateUserDTO.validateId(userDTO.getId());
        try (PassedTestDao dao = daoFactory.createPassedTestDao()) {
            final PassedTestEntity test = dao.create(new PassedTestEntityMapperService().dtoToEntity(passedTestDTO, userDTO.getId()));
            final PassedTestDTO result = new PassedTestEntityMapperService().entityToDTO(test);
            result.setTest(passedTestDTO.getTest());
            return result;
        }
    }

    @Override
    public Optional<PassedTestDTO> update(final PassedTestDTO passedTestDTO) throws RepositoryException, ValidateException {
        validatePassedTestDTO.validateId(passedTestDTO.getId()).validate(passedTestDTO);
        try (PassedTestDao dao = daoFactory.createPassedTestDao()) {
            final Optional<PassedTestEntity> entity = dao.update(new PassedTestEntityMapperService().dtoToEntity(passedTestDTO));
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new PassedTestEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public List<PassedTestDTO> getPage(final UserDTO userDTO, final Pagination pagination) throws RepositoryException, ValidateException {
        validateUserDTO.validateId(userDTO.getId());
        validatePagination.validate(pagination);
        Map<PassedTestEntity, TestEntity> page;
        try (PassedTestDao dao = daoFactory.createPassedTestDao()) {
            page = dao.getPage(new UserEntityMapperService().dtoToEntity(userDTO),
                    new PaginationMapperService().paginationToSetPage(pagination));
        }
        return page.entrySet().stream().map(x -> new PassedTestEntityMapperService().entityToDTO(x.getKey(), x.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public Integer getNumberOfRows(final UserDTO userDTO) throws RepositoryException, ValidateException {
        validateUserDTO.validateId(userDTO.getId());
        try (PassedTestDao dao = daoFactory.createPassedTestDao()) {
            return dao.getNumberOfRows(new UserEntityMapperService().dtoToEntity(userDTO));
        }
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final PassedTestServiceImpl that = (PassedTestServiceImpl) object;
        return Objects.equals(daoFactory, that.daoFactory)
                && Objects.equals(validatePassedTestDTO, that.validatePassedTestDTO)
                && Objects.equals(validateUserDTO, that.validateUserDTO)
                && Objects.equals(validatePagination, that.validatePagination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(daoFactory, validatePassedTestDTO, validateUserDTO, validatePagination);
    }
}
