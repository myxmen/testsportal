package com.vl.testing.service.impl;

import java.util.List;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.Service;
import com.vl.testing.validation.exeption.ValidateException;

public interface TestService extends Service<TestDTO> {

    /**
     * Get All of QuestionDTOs by SubjectDTO.
     *
     * @param subject SubjectDTO.
     * @return list of TestDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    List<TestDTO> getAllBySubject(SubjectDTO subject) throws RepositoryException, ValidateException;

    /**
     * Get Page of QuestionDTOs by SubjectDTO & pagination.
     *
     * @param subject    SubjectDTO.
     * @param pagination pagination.
     * @return list of TestDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    List<TestDTO> getPage(SubjectDTO subject, Pagination pagination) throws RepositoryException, ValidateException;

    /**
     * Get Number Of Rows by SubjectDTO.
     *
     * @param subject SubjectDTO.
     * @return Number Of Rows.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    Integer getNumberOfRows(SubjectDTO subject) throws RepositoryException, ValidateException;

    /**
     * Increase request of Test.
     *
     * @param testDTO TestDTO.
     * @return true if was increased.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    Boolean increaseRequestOfTest(TestDTO testDTO) throws RepositoryException, ValidateException;
}
