package com.vl.testing.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daos.TestDao;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.mappers.PaginationMapperService;
import com.vl.testing.service.mappers.impl.SubjectEntityMapperService;
import com.vl.testing.service.mappers.impl.TestEntityMapperService;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateSubjectDTO;
import com.vl.testing.validation.impl.ValidateTestDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestServiceImpl implements TestService {
    private static final Logger LOGGER = LogManager.getLogger(TestServiceImpl.class);
    private final DaoFactory daoFactory;
    private final ValidateTestDTO validateTestDTO;
    private final ValidateSubjectDTO validateSubjectDTO;
    private final ValidatePagination validatePagination;

    public TestServiceImpl() {
        this.daoFactory = DaoFactory.getInstance();
        this.validateTestDTO = new ValidateTestDTO();
        this.validateSubjectDTO = new ValidateSubjectDTO();
        this.validatePagination = new ValidatePagination();
        LOGGER.info("Internal set Test service");
    }

    public TestServiceImpl(final DaoFactory dao, final ValidateTestDTO validateTestDTO,
                           final ValidateSubjectDTO validateSubjectDTO, final ValidatePagination pagination) {
        this.daoFactory = dao;
        this.validateTestDTO = validateTestDTO;
        this.validateSubjectDTO = validateSubjectDTO;
        this.validatePagination = pagination;
        LOGGER.info("External set Test service");
    }

    @Override
    public TestDTO create(final TestDTO testDTO) throws RepositoryException, ValidateException {
        validateTestDTO.validate(testDTO);
        try (TestDao dao = daoFactory.createTestDao()) {
            return new TestEntityMapperService().entityToDTO(dao
                    .create(new TestEntityMapperService().dtoToEntity(testDTO)));
        }
    }

    @Override
    public Optional<TestDTO> get(final Long idTest) throws RepositoryException, ValidateException {
        validateTestDTO.validateId(idTest);
        try (TestDao dao = daoFactory.createTestDao()) {
            final Optional<TestEntity> entity = dao.get(idTest);
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new TestEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public Optional<TestDTO> update(final TestDTO testDTO) throws RepositoryException, ValidateException {
        validateTestDTO.validateId(testDTO.getId()).validate(testDTO);
        try (TestDao dao = daoFactory.createTestDao()) {
            final Optional<TestEntity> entity = dao.update(new TestEntityMapperService().dtoToEntity(testDTO));
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new TestEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public boolean delete(final Long idTest) throws RepositoryException, ValidateException {
        validateTestDTO.validateId(idTest);
        try (TestDao dao = daoFactory.createTestDao()) {
            return dao.delete(idTest);
        }
    }

    @Override
    public List<TestDTO> getAllBySubject(final SubjectDTO subjectDTO) throws RepositoryException, ValidateException {
        validateSubjectDTO.validateId(subjectDTO.getId());
        try (TestDao dao = daoFactory.createTestDao()) {
            return dao.getAllBySubjectId(new SubjectEntityMapperService().dtoToEntity(subjectDTO)).stream()
                    .map(x -> new TestEntityMapperService().entityToDTO(x))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public List<TestDTO> getPage(final SubjectDTO subjectDTO, final Pagination pagination) throws RepositoryException, ValidateException {
        validateSubjectDTO.validateId(subjectDTO.getId()).validate(subjectDTO);
        validatePagination.validate(pagination);
        try (TestDao dao = daoFactory.createTestDao()) {
            return dao.getPage(new SubjectEntityMapperService().dtoToEntity(subjectDTO),
                    new PaginationMapperService().paginationToSetPage(pagination))
                    .stream()
                    .map(x -> new TestEntityMapperService().entityToDTO(x))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public Integer getNumberOfRows(final SubjectDTO subjectDTO) throws RepositoryException, ValidateException {
        validateSubjectDTO.validateId(subjectDTO.getId());
        try (TestDao dao = daoFactory.createTestDao()) {
            return dao.getNumberOfRows(new SubjectEntityMapperService().dtoToEntity(subjectDTO));
        }
    }

    @Override
    public Boolean increaseRequestOfTest(final TestDTO testDTO) throws RepositoryException, ValidateException {
        validateTestDTO.validateId(testDTO.getId());
        try (TestDao dao = daoFactory.createTestDao()) {
            return dao.increaseRequestOfTest(new TestEntityMapperService().dtoToEntity(testDTO));
        }
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final TestServiceImpl that = (TestServiceImpl) object;
        return Objects.equals(daoFactory, that.daoFactory)
                && Objects.equals(validateTestDTO, that.validateTestDTO)
                && Objects.equals(validateSubjectDTO, that.validateSubjectDTO)
                && Objects.equals(validatePagination, that.validatePagination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(daoFactory, validateTestDTO, validateSubjectDTO, validatePagination);
    }
}