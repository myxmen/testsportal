package com.vl.testing.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daos.QuestionDao;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.mappers.PaginationMapperService;
import com.vl.testing.service.mappers.impl.QuestionEntityMapperService;
import com.vl.testing.service.mappers.impl.TestEntityMapperService;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateQuestionDTO;
import com.vl.testing.validation.impl.ValidateTestDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class QuestionServiceImpl implements QuestionService {
    private static final Logger LOGGER = LogManager.getLogger(QuestionServiceImpl.class);
    private final DaoFactory daoFactory;
    private final ValidateQuestionDTO validateQuestionDTO;
    private final ValidateTestDTO validateTestDTO;
    private final ValidatePagination validatePagination;
    private static final Integer PERCENT = 100;

    public QuestionServiceImpl() {
        this.daoFactory = DaoFactory.getInstance();
        this.validateQuestionDTO = new ValidateQuestionDTO();
        this.validateTestDTO = new ValidateTestDTO();
        this.validatePagination = new ValidatePagination();
        LOGGER.info("Internal set Question service");
    }

    public QuestionServiceImpl(final DaoFactory dao, final ValidateQuestionDTO validateQuestionDTO,
                               final ValidateTestDTO validateTestDTO, final ValidatePagination pagination) {
        this.daoFactory = dao;
        this.validateQuestionDTO = validateQuestionDTO;
        this.validateTestDTO = validateTestDTO;
        this.validatePagination = pagination;
        LOGGER.info("External set Question service");
    }

    @Override
    public QuestionDTO create(final QuestionDTO questionDTO) throws RepositoryException, ValidateException {
        validateQuestionDTO.validate(questionDTO);
        try (QuestionDao dao = daoFactory.createQuestionDao()) {
            return new QuestionEntityMapperService().entityToDTO(dao
                    .create(new QuestionEntityMapperService().dtoToEntity(questionDTO)));
        }
    }

    @Override
    public Optional<QuestionDTO> get(final Long idQuestion) throws RepositoryException, ValidateException {
        validateQuestionDTO.validateId(idQuestion);
        try (QuestionDao dao = daoFactory.createQuestionDao()) {
            final Optional<QuestionEntity> entity = dao.get(idQuestion);
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new QuestionEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public Optional<QuestionDTO> update(final QuestionDTO questionDTO) throws RepositoryException, ValidateException {
        validateQuestionDTO.validateId(questionDTO.getId()).validate(questionDTO);
        try (QuestionDao dao = daoFactory.createQuestionDao()) {
            final Optional<QuestionEntity> entity = dao.update(new QuestionEntityMapperService().dtoToEntity(questionDTO));
            if (entity.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new QuestionEntityMapperService().entityToDTO(entity.get()));
        }
    }

    @Override
    public boolean delete(final Long idQuestion) throws RepositoryException, ValidateException {
        validateQuestionDTO.validateId(idQuestion);
        try (QuestionDao dao = daoFactory.createQuestionDao()) {
            return dao.delete(idQuestion);
        }
    }

    @Override
    public Integer getNumberOfRows(final TestDTO testDTO) throws RepositoryException, ValidateException {
        validateTestDTO.validateId(testDTO.getId());
        try (QuestionDao dao = daoFactory.createQuestionDao()) {
            return dao.getNumberOfRows(new TestEntityMapperService().dtoToEntity(testDTO));
        }
    }

    @Override
    public List<QuestionDTO> getPage(final TestDTO testDTO, final Pagination pagination) throws RepositoryException, ValidateException {
        LOGGER.info(pagination);
        validateTestDTO.validate(testDTO).validateId(testDTO.getId());
        validatePagination.validate(pagination);
        try (QuestionDao dao = daoFactory.createQuestionDao()) {
            return dao.getPage(new TestEntityMapperService().dtoToEntity(testDTO),
                            new PaginationMapperService().paginationToSetPage(pagination))
                    .stream()
                    .map(x -> new QuestionEntityMapperService().entityToDTO(x))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public List<QuestionDTO> getAllByTest(final TestDTO testDTO) throws RepositoryException, ValidateException {
        validateTestDTO.validateId(testDTO.getId());
        try (QuestionDao dao = daoFactory.createQuestionDao()) {
            return dao.getAllByTestId(new TestEntityMapperService().dtoToEntity(testDTO)).stream()
                    .map(x -> new QuestionEntityMapperService().entityToDTO(x))
                    .collect(Collectors.toList());
        }
    }

    @Override
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public Integer getScore(final Map<QuestionDTO, String[]> questionToAnswer, final Integer size) {
        Objects.requireNonNull(questionToAnswer);
        Objects.requireNonNull(size);
        int countRightAnswersToQuestions = 0;
        for (final Map.Entry<QuestionDTO, String[]> entry : questionToAnswer.entrySet()) {
            final List<Boolean> rightAnswers = Stream.of(entry.getKey().isCorrectAnswer1(),
                            entry.getKey().isCorrectAnswer2(),
                            entry.getKey().isCorrectAnswer3(),
                            entry.getKey().isCorrectAnswer4(),
                            entry.getKey().isCorrectAnswer5())
                    .filter(x -> x != null)
                    .collect(Collectors.toList());
            boolean[] userAnswers = new boolean[rightAnswers.size()];
            for (final String str : entry.getValue()) {
                userAnswers[Integer.parseInt(str.trim()) - 1] = true;
            }
            boolean isRightAnswers = true;
            for (int i = 0; i < rightAnswers.size(); i++) {
                if (rightAnswers.get(i) != userAnswers[i]) {
                    isRightAnswers = false;
                    break;
                }
            }
            if (isRightAnswers) {
                countRightAnswersToQuestions++;
            }
        }
        return (int) ((double) countRightAnswersToQuestions / size * PERCENT);
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final QuestionServiceImpl that = (QuestionServiceImpl) object;
        return Objects.equals(daoFactory, that.daoFactory)
                && Objects.equals(validateQuestionDTO, that.validateQuestionDTO)
                && Objects.equals(validateTestDTO, that.validateTestDTO)
                && Objects.equals(validatePagination, that.validatePagination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(daoFactory, validateQuestionDTO, validateTestDTO, validatePagination);
    }
}
