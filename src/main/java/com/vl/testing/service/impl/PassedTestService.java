package com.vl.testing.service.impl;

import java.util.List;
import java.util.Optional;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.validation.exeption.ValidateException;

public interface PassedTestService<D, U> {

    /**
     * Get All of PassedTestDTOs by UserDTO.
     *
     * @param user UserDTO.
     * @return list of PassedTestDTOs.
     * @throws RepositoryException Repository error.
     * @throws ValidateException    Validate error.
     */
    List<D> getAllPassedTestFosUser(U user) throws RepositoryException, ValidateException;

    /**
     * Create PassedTestDTO.
     *
     * @param dto  PassedTestDTO.
     * @param user UserDTO.
     * @return PassedTestDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException    Validate error.
     */
    D create(D dto, U user) throws RepositoryException, ValidateException;

    /**
     * Update PassedTestDTO.
     *
     * @param dto PassedTestDTO.
     * @return Optional PassedTestDTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException    Validate error.
     */
    Optional<D> update(D dto) throws RepositoryException, ValidateException;

    /**
     * Get Page of PassedTestDTOs by UserDTO & pagination.
     *
     * @param user       UserDTO.
     * @param pagination pagination.
     * @return list of PassedTestDTOs.
     * @throws RepositoryException Repository error.
     * @throws ValidateException    Validate error.
     */
    List<D> getPage(U user, Pagination pagination) throws RepositoryException, ValidateException;

    /**
     * Get Number Of Rows by UserDTO.
     *
     * @param user UserDTO.
     * @return Number Of Rows.
     * @throws RepositoryException Repository error.
     * @throws ValidateException    Validate error.
     */
    Integer getNumberOfRows(U user) throws RepositoryException, ValidateException;
}
