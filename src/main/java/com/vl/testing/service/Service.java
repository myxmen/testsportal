package com.vl.testing.service;

import java.util.Optional;

import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.validation.exeption.ValidateException;

public interface Service<D> {

    /**
     * create DTO into database.
     *
     * @param dto DTO.
     * @return DTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    D create(D dto) throws RepositoryException, ValidateException;

    /**
     * get DTO from database.
     *
     * @param idDTO id of DTO.
     * @return DTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    Optional<D> get(Long idDTO) throws RepositoryException, ValidateException;

    /**
     * update DTO from database.
     *
     * @param dto DTO.
     * @return DTO.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    Optional<D> update(D dto) throws RepositoryException, ValidateException;

    /**
     * delete DTO from database.
     *
     * @param idDTO id of DTO.
     * @return true if DTO deleted.
     * @throws RepositoryException Repository error.
     * @throws ValidateException   Validate error.
     */
    boolean delete(Long idDTO) throws RepositoryException, ValidateException;

}
