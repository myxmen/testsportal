package com.vl.testing.service.mappers.impl;

import com.vl.testing.controller.enums.Localization;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.entities.UserEntity;
import com.vl.testing.service.mappers.MapperService;

public class UserEntityMapperService implements MapperService<UserDTO, UserEntity> {
    private static final String EMPTYLINE = "";

    @Override
    public UserDTO entityToDTO(final UserEntity userEntity) {
        return UserDTO.builder()
                .setId(userEntity.getIdUser())
                .setUsername(userEntity.getUsername())
                .setPassword(EMPTYLINE)
                .setAdmin(userEntity.isAdmin())
                .setBan(userEntity.isBan())
                .setLocale(Localization.getLocaleByName(userEntity.getLocale()))
                .build();
    }

    @Override
    public UserEntity dtoToEntity(final UserDTO userDTO) {
        return UserEntity.builder()
                .setIdUser(userDTO.getId())
                .setUsername(userDTO.getUsername())
                .setPassword(userDTO.getPassword())
                .setAdmin(userDTO.isAdmin())
                .setBan(userDTO.isBan())
                .setLocale(Localization.getNameByLocale(userDTO.getLocale()))
                .build();

    }
}
