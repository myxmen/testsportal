package com.vl.testing.service.mappers.impl;

import com.vl.testing.dto.TestDTO;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.service.mappers.MapperService;

public class TestEntityMapperService implements MapperService<TestDTO, TestEntity> {

    @Override
    public TestDTO entityToDTO(final TestEntity testEntity) {
        return TestDTO.builder()
                .setId(testEntity.getIdTest())
                .setSubjectId(testEntity.getSubjectId())
                .setTest(testEntity.getTest())
                .setDifficulty(testEntity.getDifficulty())
                .setRequests(testEntity.getRequests())
                .setTime(testEntity.getTime())
                .build();
    }

    @Override
    public TestEntity dtoToEntity(final TestDTO testDTO) {
        return TestEntity.builder()
                .setIdTest(testDTO.getId())
                .setSubjectId(testDTO.getSubjectId())
                .setTest(testDTO.getTest())
                .setDifficulty(testDTO.getDifficulty())
                .setRequests(testDTO.getRequests())
                .setTime(testDTO.getTime())
                .build();
    }
}
