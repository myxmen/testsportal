package com.vl.testing.service.mappers;

public interface MapperService<D, E> {

    /**
     * Map fields Entity to DTO.
     *
     * @param entity Entity.
     * @return DTO.
     */
    D entityToDTO(E entity);

    /**
     * Map fields DTO to Entity.
     *
     * @param dto DTO.
     * @return Entity.
     */
    E dtoToEntity(D dto);
}
