package com.vl.testing.service.mappers;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.persistence.utils.SetPage;

public class PaginationMapperService {

    /**
     * Mapping pagination fields to SetPage.
     *
     * @param pagination Pagination.
     * @return SetPage.
     */
    public SetPage paginationToSetPage(final Pagination pagination) {
        return SetPage.builder()
                .setStartRows(pagination.getStartRows())
                .setRecordsPerPage(pagination.getRecordsPerPage())
                .setSort(pagination.getSort())
                .setSortColumn(pagination.getSortColumn())
                .build();
    }
}
