package com.vl.testing.service.mappers.impl;

import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.service.mappers.MapperService;

public class QuestionEntityMapperService implements MapperService<QuestionDTO, QuestionEntity> {
    @Override
    public QuestionDTO entityToDTO(final QuestionEntity questionEntity) {
        return QuestionDTO.builder()
                .setId(questionEntity.getIdQuestion())
                .setTestId(questionEntity.getTestId())
                .setNumberOfQuestion(questionEntity.getNumberOfQuestion())
                .setQuestion(questionEntity.getQuestion())
                .setAnswer1(questionEntity.getAnswer1())
                .setAnswer2(questionEntity.getAnswer2())
                .setAnswer3(questionEntity.getAnswer3())
                .setAnswer4(questionEntity.getAnswer4())
                .setAnswer5(questionEntity.getAnswer5())
                .setCorrectAnswer1(questionEntity.isCorrectAnswer1())
                .setCorrectAnswer2(questionEntity.isCorrectAnswer2())
                .setCorrectAnswer3(questionEntity.isCorrectAnswer3())
                .setCorrectAnswer4(questionEntity.isCorrectAnswer4())
                .setCorrectAnswer5(questionEntity.isCorrectAnswer5())
                .build();
    }

    @Override
    public QuestionEntity dtoToEntity(final QuestionDTO questionDTO) {
        return QuestionEntity.builder()
                .setIdQuestion(questionDTO.getId())
                .setTestId(questionDTO.getTestId())
                .setNumberOfQuestion(questionDTO.getNumberOfQuestion())
                .setQuestion(questionDTO.getQuestion())
                .setAnswer1(questionDTO.getAnswer1())
                .setAnswer2(questionDTO.getAnswer2())
                .setAnswer3(questionDTO.getAnswer3())
                .setAnswer4(questionDTO.getAnswer4())
                .setAnswer5(questionDTO.getAnswer5())
                .setCorrectAnswer1(questionDTO.isCorrectAnswer1())
                .setCorrectAnswer2(questionDTO.isCorrectAnswer2())
                .setCorrectAnswer3(questionDTO.isCorrectAnswer3())
                .setCorrectAnswer4(questionDTO.isCorrectAnswer4())
                .setCorrectAnswer5(questionDTO.isCorrectAnswer5())
                .build();
    }
}
