package com.vl.testing.service.mappers.impl;

import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.service.mappers.MapperService;

public class PassedTestEntityMapperService implements MapperService<PassedTestDTO, PassedTestEntity> {

    /**
     * Map fields Entity to DTO & set Test field by TestEntity.
     *
     * @param passedTestEntity PassedTestEntity.
     * @param testEntity       TestEntity.
     * @return DTO.
     */
    public PassedTestDTO entityToDTO(final PassedTestEntity passedTestEntity, final TestEntity testEntity) {
        final TestEntityMapperService testEntityMapper = new TestEntityMapperService();
        final PassedTestDTO dto = entityToDTO(passedTestEntity);
        dto.setTest(testEntityMapper.entityToDTO(testEntity));
        return dto;
    }

    @Override
    public PassedTestDTO entityToDTO(final PassedTestEntity passedTestEntity) {
        return PassedTestDTO.builder()
                .setId(passedTestEntity.getIdPassedTest())
                .setResult(passedTestEntity.getResult())
                .build();
    }

    @Override
    public PassedTestEntity dtoToEntity(final PassedTestDTO passedTestDTO) {
        return PassedTestEntity.builder()
                .setIdPassedTest(passedTestDTO.getId())
                .setTestId(passedTestDTO.getTest().getId())
                .setResult(passedTestDTO.getResult())
                .build();
    }

    /**
     * Map fields DTO to Entity & add user`s id.
     *
     * @param passedTestDTO PassedTestDTO.
     * @param userId        user`s id.
     * @return entity.
     */
    public PassedTestEntity dtoToEntity(final PassedTestDTO passedTestDTO, final Long userId) {
        final PassedTestEntity entity = dtoToEntity(passedTestDTO);
        entity.setUserId(userId);
        return entity;
    }
}
