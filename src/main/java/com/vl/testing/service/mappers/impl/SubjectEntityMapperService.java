package com.vl.testing.service.mappers.impl;

import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.service.mappers.MapperService;

public class SubjectEntityMapperService implements MapperService<SubjectDTO, SubjectEntity> {
    @Override
    public SubjectDTO entityToDTO(final SubjectEntity subjectEntity) {
        return SubjectDTO.builder()
                .setId(subjectEntity.getIdSubject())
                .setSubject(subjectEntity.getSubject())
                .build();
    }

    @Override
    public SubjectEntity dtoToEntity(final SubjectDTO subjectDTO) {
        return SubjectEntity.builder()
                .setIdSubject(subjectDTO.getId())
                .setSubject(subjectDTO.getSubject())
                .build();
    }
}
