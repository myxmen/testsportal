package com.vl.testing.controller.util;

import javax.servlet.http.HttpServletRequest;

public final class SetErrorAtribute {

    private SetErrorAtribute() {
    }

    /**
     * Set Error Attributes.
     *
     * @param error        error.
     * @param val          validate.
     * @param errormessage error message.
     * @param request      request.
     * @return true.
     */
    public static boolean setRequestErrorAttributes(final String error, final String val,
                                                    final String errormessage, final HttpServletRequest request) {
        if (error != null) {
            request.setAttribute("error", error);
        }
        if (val != null) {
            request.setAttribute("val", val);
        }
        if (errormessage != null) {
            request.setAttribute("errormessage", errormessage);
        }
        return true;
    }

    /**
     * Set Error Attributes.
     *
     * @param error        error.
     * @param errormessage error message.
     * @param request      request.
     * @return true.
     */
    public static boolean setRequestErrorAttributes(final String error, final String errormessage, final HttpServletRequest request) {
        if (error != null) {
            request.setAttribute("error", error);
        }
        if (errormessage != null) {
            request.setAttribute("errormessage", errormessage);
        }
        return true;
    }
}
