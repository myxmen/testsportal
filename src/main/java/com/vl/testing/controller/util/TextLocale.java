package com.vl.testing.controller.util;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.enums.Localization;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class TextLocale extends ResourceBundle {
    private static final Logger LOGGER = LogManager.getLogger(TextLocale.class);

    private static final String TEXT_ATTRIBUTE_NAME = "text";
    private static final String TEXT_BASE_NAME = "messages";

    private TextLocale(final Locale locale) {
        setLocale(locale);
    }

    /**
     * Set up.
     * @param request request.
     */
    public static void setFor(final HttpServletRequest request) {
        if (request.getSession().getAttribute(TEXT_ATTRIBUTE_NAME) == null) {
            request.getSession().setAttribute(TEXT_ATTRIBUTE_NAME, new TextLocale(Localization.EN.getLocale()));
            LOGGER.info("TEXT_ATTRIBUTE_NAME - " + request.getSession().getAttribute(TEXT_ATTRIBUTE_NAME));
        }
    }

    /**
     * Get current text locale.
     * @param request request.
     * @return TextLocale.
     */
    public static TextLocale getCurrentInstance(final HttpServletRequest request) {
        return (TextLocale) request.getSession().getAttribute(TEXT_ATTRIBUTE_NAME);
    }

    /**
     * Set locale.
     * @param locale locale.
     */
    public void setLocale(final Locale locale) {
        if (parent == null || !parent.getLocale().equals(locale)) {
            setParent(getBundle(TEXT_BASE_NAME, locale));
            LOGGER.info("parent locale - " + parent.getLocale());
            LOGGER.info("parent key - " + parent.getKeys());
        }
    }

    @Override
    protected Object handleGetObject(final String key) {
        return parent.getObject(key);
    }

    @Override
    public Enumeration<String> getKeys() {
        return parent.getKeys();
    }
}
