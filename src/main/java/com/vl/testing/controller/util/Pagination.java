package com.vl.testing.controller.util;

import java.util.Objects;
import java.util.regex.Pattern;

import com.vl.testing.persistence.enums.Records;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.enums.SortColumn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Pagination {
    private static final Logger LOGGER = LogManager.getLogger(Pagination.class);

    private final String sort;
    private final String sortColumn;

    private final Integer currentPage;
    private final Integer recordsPerPage;
    private final Integer nOfPages;
    private final Integer startRows;

    private Pagination(final String sort, final String sortColumn, final Integer currentPage,
                       final Integer recordsPerPage, final Integer nOfPages, final Integer startRows) {
        this.sort = sort;
        this.sortColumn = sortColumn;
        this.currentPage = currentPage;
        this.recordsPerPage = recordsPerPage;
        this.nOfPages = nOfPages;
        this.startRows = startRows;
        LOGGER.info(this);
    }

    public String getSort() {
        return sort;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public Integer getRecordsPerPage() {
        return recordsPerPage;
    }

    public Integer getNOfPages() {
        return nOfPages;
    }

    public Integer getStartRows() {
        return startRows;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final Pagination that = (Pagination) object;
        return Objects.equals(sort, that.sort)
                && Objects.equals(sortColumn, that.sortColumn)
                && Objects.equals(currentPage, that.currentPage)
                && Objects.equals(recordsPerPage, that.recordsPerPage)
                && Objects.equals(nOfPages, that.nOfPages)
                && Objects.equals(startRows, that.startRows);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sort, sortColumn, currentPage, recordsPerPage, nOfPages, startRows);
    }

    @Override
    public String toString() {
        return "Pagination{"
                + "sort='" + sort + '\''
                + ", sortColumn='" + sortColumn + '\''
                + ", currentPage=" + currentPage
                + ", recordsPerPage=" + recordsPerPage
                + ", nOfPages=" + nOfPages
                + ", startRows=" + startRows
                + '}';
    }

    /**
     * init Builder.
     *
     * @return pagination builder.
     */
    public static PaginationBuilder builder() {
        return new PaginationBuilder();
    }

    public static class PaginationBuilder {
        private static final String DEFAULT_SORT = Sort.ASC.getSort().toLowerCase();
        private static final int DEFAULT_NUMBER_OF_PAGE = 1;
        private static final int DEFAULT_RECORDS_PER_PAGE = Records.FIRST.getValue();
        private static final String REGEX = "[0-9]+";
        private Integer rows;
        private String currPage;
        private String recPerPage;

        private String sort;
        private String sortColumn;
        private String defaultColumn;

        private Integer currentPage;
        private Integer recordsPerPage;
        private Integer nOfPages;
        private Integer startRows;

        // CHECKSTYLE_OFF: JavadocMethod
        public PaginationBuilder setRows(final Integer rows) {
            this.rows = rows;
            return this;
        }

        public PaginationBuilder setCurrentPage(final String currPage) {
            this.currPage = currPage;
            return this;
        }

        public PaginationBuilder setRecordsPerPage(final String recPerPage) {
            this.recPerPage = recPerPage;
            return this;
        }

        public PaginationBuilder setSort(final String sort) {
            this.sort = sort;
            return this;
        }

        public PaginationBuilder setSortColumn(final String sortColumn) {
            this.sortColumn = sortColumn;
            return this;
        }

        public PaginationBuilder setDefaultColumn(final String defaultColumn) {
            this.defaultColumn = defaultColumn;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built pagination.
         */
        public Pagination build() {
            this.sort = checkSort();

            this.sortColumn = checkSortColumn();

            this.recordsPerPage = checkRecPerPage();

            calculateNOfPages();

            calculateCurrentPage();

            this.startRows = calculateStartRows();

            return new Pagination(sort, sortColumn, currentPage, recordsPerPage, nOfPages, startRows);
        }

        private boolean isNumeric(final String string) {
            return Pattern.matches(REGEX, string);
        }

        private String checkSort() {
            if (this.sort == null) {
                return DEFAULT_SORT;
            } else {
                if (!(this.sort.equalsIgnoreCase(Sort.ASC.getSort())
                        || this.sort.equalsIgnoreCase(Sort.DESC.getSort()))) {
                    return DEFAULT_SORT;
                } else {
                    return this.sort.toLowerCase();
                }
            }
        }

        private String checkSortColumn() {
            LOGGER.info(this.sortColumn);
            if (this.sortColumn == null || !SortColumn.isColumn(this.sortColumn)) {
                return this.defaultColumn;
            }
            return this.sortColumn;
        }

        private Integer checkRecPerPage() {
            if (this.recPerPage == null || !isNumeric(this.recPerPage)) {
                return DEFAULT_RECORDS_PER_PAGE;
            } else {
                return Records.isRecords(Integer.parseInt(this.recPerPage));
            }
        }

        private Integer calculateNOfPages() {
            if ((double) this.rows / this.recordsPerPage <= 1) {
                this.nOfPages = 0;
            } else {
                this.nOfPages = this.rows / this.recordsPerPage;
                if (this.rows % this.recordsPerPage > 0) {
                    this.nOfPages++;
                }
            }
            return this.nOfPages;
        }

        private Integer checkCurrentPage() {
            if (this.currPage == null || !isNumeric(this.currPage)) {
                return DEFAULT_NUMBER_OF_PAGE;
            } else {
                return Integer.parseInt(this.currPage);
            }
        }

        private Integer calculateCurrentPage() {
            this.currentPage = checkCurrentPage();

            if (this.currentPage < 1) {
                this.currentPage = DEFAULT_NUMBER_OF_PAGE;
            }
            if (this.currentPage > this.nOfPages) {
                this.currentPage = this.nOfPages == 0 ? DEFAULT_NUMBER_OF_PAGE : this.nOfPages;
            }
            LOGGER.info(this.rows / this.recordsPerPage <= 1);
            LOGGER.info((double) this.rows / this.recordsPerPage <= 1);

            LOGGER.info(this.nOfPages);
            LOGGER.info(this.rows);
            return this.currentPage;
        }

        private Integer calculateStartRows() {
            return this.currentPage * this.recordsPerPage - this.recordsPerPage;
        }
    }
}
