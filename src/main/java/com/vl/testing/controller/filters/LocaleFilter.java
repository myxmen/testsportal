package com.vl.testing.controller.filters;

import java.io.IOException;
import java.util.Locale;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.util.TextLocale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LocaleFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(LocaleFilter.class);
    private static final String LANGUAGE = "language";
    private static final Locale DEFAULT_LOCALE = Localization.EN.getLocale();

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response,
                         final FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpSession session = req.getSession();
        LOGGER.info("Default locale " + DEFAULT_LOCALE);
        LOGGER.info("language from request " + req.getParameter(LANGUAGE));

        TextLocale.setFor((HttpServletRequest) request);

        if (session.getAttribute(LANGUAGE) == null && req.getParameter(LANGUAGE) == null) {
            session.setAttribute(LANGUAGE, DEFAULT_LOCALE);
        } else if (req.getParameter(LANGUAGE) != null) {
            final Locale currLocale = Localization.getLocaleByName(req.getParameter(LANGUAGE));
            session.setAttribute(LANGUAGE, currLocale);
            TextLocale.getCurrentInstance(req).setLocale(currLocale);
        }
        LOGGER.info("language in session " + session.getAttribute(LANGUAGE));
        LOGGER.info("Language in bundle " + session.getAttribute("text"));

        req.setAttribute(LANGUAGE, Localization.getNameByLocale((Locale) session.getAttribute(LANGUAGE)));
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
