package com.vl.testing.controller.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.dto.UserDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(AdminFilter.class);

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response,
                         final FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;
        final HttpSession session = req.getSession();
        LOGGER.info("Unput role " + session.getAttribute(SessionAttribute.USER.getAttribute()));
        LOGGER.info("Unput path " + req.getRequestURI());

        final UserDTO user = (UserDTO) session.getAttribute(SessionAttribute.USER.getAttribute());
        LOGGER.info(user);

        boolean adminFlag = false;
        if (user.isAdmin() != null) {
            adminFlag = user.isAdmin();
        }
        if (adminFlag) {
            LOGGER.info("Access is open into Admin folder");
            filterChain.doFilter(request, response);
        } else {
            LOGGER.info("Access denied " + user + " into Admin folder! Logout!");
            res.sendRedirect("/logout");
        }
    }

    @Override
    public void destroy() {
    }
}
