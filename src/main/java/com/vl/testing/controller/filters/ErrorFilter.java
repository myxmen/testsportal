package com.vl.testing.controller.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.enums.SessionAttribute;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ErrorFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(ErrorFilter.class);

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response,
                         final FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpSession session = req.getSession();
        final String error = (String) session.getAttribute(SessionAttribute.ERROR_PAGE.getAttribute());
        LOGGER.info("Get error from session: " + error);
        final String errormess = (String) session.getAttribute(SessionAttribute.ERROR_MESSAGE.getAttribute());
        LOGGER.info("Get errormessage from session: " + errormess);
        final Object errorDto = session.getAttribute(SessionAttribute.ERROR_DTO.getAttribute());
        LOGGER.info("Get errorDto from session: " + errorDto);
        if (error != null) {
            LOGGER.info("Set error in request: " + error);
            session.removeAttribute(SessionAttribute.ERROR_PAGE.getAttribute());
            request.setAttribute("error", error);
        }
        if (errormess != null) {
            LOGGER.info("Set errormessage in request: " + errormess);
            session.removeAttribute(SessionAttribute.ERROR_MESSAGE.getAttribute());
            request.setAttribute("errormessage", errormess);
        }
        if (errormess != null) {
            LOGGER.info("Set errorDto in request: " + errorDto);
            session.removeAttribute(SessionAttribute.ERROR_DTO.getAttribute());
            request.setAttribute("errordto", errorDto);
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
