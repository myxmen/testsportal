package com.vl.testing.controller.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.enums.ContextAttribute;
import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.dto.UserDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AuthFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(AuthFilter.class);
    private static final String PATH_LOGIN = "/login";
    private static final String PATH_LOGIN_JSP = "/login.jsp";
    private static final String PATH_SIGNUP_JSP = "/signup.jsp";
    private static final String PATH_SIGNUP = "/signup";
    private static final String PATH_INDEX = "/";
    private static final String PATH_INDEX_JSP = "/index.jsp";

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;
        final HttpSession session = req.getSession();
        final ServletContext context = request.getServletContext();
        if (session.getAttribute(SessionAttribute.USER.getAttribute()) == null) {
            final UserDTO guestUser = new UserDTO();
            guestUser.setUsername("Guest");
            session.setAttribute(SessionAttribute.USER.getAttribute(), guestUser);
        }

        LOGGER.info("User from session: " + session.getAttribute(SessionAttribute.USER.getAttribute()));
        LOGGER.info("Logged Users from context: " + context.getAttribute(ContextAttribute.LOGGED_USERS.getAttribute()));

        final String path = req.getRequestURI();
        LOGGER.info("Input path " + path);
        final UserDTO user = (UserDTO) session.getAttribute(SessionAttribute.USER.getAttribute());

        boolean loggedInUser = false;
        if (user.isAdmin() != null) {
            loggedInUser = !user.isAdmin();
        }

        boolean loggedInAdmin = false;
        if (user.isAdmin() != null) {
            loggedInAdmin = user.isAdmin();
        }

        final boolean basicPath = path.equals(PATH_LOGIN) || path.equals(PATH_LOGIN_JSP)
                || path.equals(PATH_INDEX) || path.equals(PATH_SIGNUP) || path.equals(PATH_SIGNUP_JSP) || path.equals(PATH_INDEX_JSP);
        LOGGER.info(loggedInAdmin + " " + loggedInUser + " " + basicPath);

        if (loggedInAdmin && basicPath || loggedInUser && basicPath) {
            LOGGER.info("Access denied " + user + " into root folder! Logout!");
            res.sendRedirect("/logout");
        } else {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
