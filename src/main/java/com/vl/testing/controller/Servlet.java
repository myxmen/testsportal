package com.vl.testing.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vl.testing.controller.commands.Command;
// CHECKSTYLE_OFF: AvoidStarImport
import com.vl.testing.controller.commands.impl.*;
// CHECKSTYLE_ON: AvoidStarImport
import com.vl.testing.controller.enums.ContextAttribute;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Servlet extends HttpServlet {
    private static final long serialVersionUID = 1234567895L;
    private static final Logger LOGGER = LogManager.getLogger(Servlet.class);

    private final Map<String, Command> commands = new HashMap<>();

    /**
     * init.
     *
     * @param servletConfig servlet config.
     */
    public void init(final ServletConfig servletConfig) {
        servletConfig.getServletContext()
                .setAttribute(ContextAttribute.LOGGED_USERS.getAttribute(), new HashSet<String>());

        commands.put("/logout",
                new LogOutCommand());
        commands.put("/login",
                new LoginCommand());
        commands.put("/admin",
                new AdminCommand());
        commands.put("/admin/edit",
                new AdminEditCommand());
        commands.put("/admin/userset",
                new AdminUserSetCommand());
        commands.put("/admin/userupdate",
                new AdminUserUpdateCommand());
        commands.put("/admin/userdelete",
                new AdminUserDeleteCommand());

        commands.put("/admin/subject",
                new AdminSubjectCommand());
        commands.put("/admin/subjectadd",
                new AdminSubjectAddCommand());
        commands.put("/admin/subjectupdate",
                new AdminSubjectUpdateCommand());
        commands.put("/admin/subjectdelete",
                new AdminSubjectDeleteCommand());

        commands.put("/admin/test",
                new AdminTestCommand());
        commands.put("/admin/testadd",
                new AdminTestAddCommand());
        commands.put("/admin/testupdate",
                new AdminTestUpdateCommand());
        commands.put("/admin/testdelete",
                new AdminTestDeleteCommand());

        commands.put("/admin/question",
                new AdminQuestionCommand());
        commands.put("/admin/questionadd",
                new AdminQuestionAddCommand());
        commands.put("/admin/questiondelete",
                new AdminQuestionDeleteCommand());
        commands.put("/admin/questionupdate",
                new AdminQuestionUpdateCommand());

        commands.put("/admin/next",
                new AdminNextCommand());

        commands.put("/user",
                new UserCommand());
        commands.put("/user/next",
                new UserNextCommand());
        commands.put("/",
                new IndexCommand());
        commands.put("/signup",
                new SignupCommand());
        commands.put("/user/edit",
                new UserEditCommand());
        commands.put("/user/subject",
                new UserSubjectCommand());
        commands.put("/user/test",
                new UserTestCommand());
        commands.put("/user/pretest",
                new UserPretestCommand());
        commands.put("/user/question",
                new UserQuestionCommand());
        commands.put("/user/endtest",
                new UserEndTestCommand());
    }

    /**
     * doGet.
     *
     * @param request  request.
     * @param response response.
     * @throws IOException      error.
     * @throws ServletException error.
     */
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        LOGGER.info("doGet servlet");
        processRequest(request, response, "Get");
    }

    /**
     * doPost.
     *
     * @param request  request.
     * @param response response.
     * @throws IOException      error.
     * @throws ServletException error.
     */
    public void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        LOGGER.info("doPost servlet");
        processRequest(request, response, "Post");
    }

    private void processRequest(final HttpServletRequest request, final HttpServletResponse response, final String method)
            throws ServletException, IOException {
        final String path = request.getRequestURI();
        LOGGER.info("Current path: " + path);

        final Command command = commands.getOrDefault(path,
                (r, x) -> "/logout");
        LOGGER.info(command.getClass().getName());

        final String page = command.execute(request, method);
        LOGGER.info(page);

        if (page.contains("redirect:")) {
            response.sendRedirect(page.replace("redirect:", ""));
        } else {
            request.getRequestDispatcher(page).forward(request, response);
        }
    }
}
