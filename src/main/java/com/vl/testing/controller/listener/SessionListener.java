package com.vl.testing.controller.listener;

import java.util.HashSet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.vl.testing.controller.enums.ContextAttribute;
import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.dto.UserDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SessionListener implements HttpSessionListener {
    private static final Logger LOGGER = LogManager.getLogger(SessionListener.class);
    private static final int SECONDS_IN_MINUTE = 60;
    private static final int MINUTE = 20;

    @Override
    public void sessionCreated(final HttpSessionEvent httpSessionEvent) {
        LOGGER.info("Session created");
        final HttpSession session = httpSessionEvent.getSession();
        session.setMaxInactiveInterval(MINUTE * SECONDS_IN_MINUTE); //seconds
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent httpSessionEvent) {
        LOGGER.info("Session destroyed");
        final HashSet<UserDTO> loggedUsers = (HashSet<UserDTO>) httpSessionEvent
                .getSession().getServletContext()
                .getAttribute(ContextAttribute.LOGGED_USERS.getAttribute());
        final UserDTO user = (UserDTO) httpSessionEvent.getSession()
                .getAttribute(SessionAttribute.USER.getAttribute());
        loggedUsers.remove(user);
        LOGGER.info("User removed:" + user);
        httpSessionEvent.getSession().getServletContext().setAttribute(ContextAttribute.LOGGED_USERS.getAttribute(), loggedUsers);
    }
}
