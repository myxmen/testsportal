package com.vl.testing.controller.commands.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserNextCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserNextCommand.class);

   @Override
    public String execute(final HttpServletRequest request, final String method) {
        final HttpSession session = request.getSession();
        LOGGER.info(session.getAttribute("subject"));
        return "/WEB-INF/user/usernext.jsp";
    }
}
