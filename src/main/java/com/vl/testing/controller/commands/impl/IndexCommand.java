package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IndexCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(IndexCommand.class);
    private final SubjectServiceImpl subjectService;

    public IndexCommand() {
        this.subjectService = new SubjectServiceImpl();
    }

    public IndexCommand(final SubjectServiceImpl subjectService) {
        this.subjectService = subjectService;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);

        LOGGER.info("Sort " + request.getParameter("sort"));
        LOGGER.info("Sort column " + request.getParameter("sortcolumn"));
        LOGGER.info("Pagination " + request.getParameter("pagination"));
        LOGGER.info("Page " + request.getParameter("page"));
        LOGGER.info("NoOfPages " + request.getParameter("noofpages"));

        try {
            final Integer rows = subjectService.getNumberOfRows();
            final Pagination pagination = Pagination.builder()
                    .setRows(rows)
                    .setCurrentPage(request.getParameter("page"))
                    .setRecordsPerPage(request.getParameter("pagination"))
                    .setSort(request.getParameter("sort"))
                    .setSortColumn(request.getParameter("sortcolumn"))
                    .setDefaultColumn(DefaultColumn.SUBJECT.getColumn())
                    .build();

            LOGGER.info("NoOfPages for jsp " + pagination.getNOfPages());
            LOGGER.info("sort for jsp " + pagination.getSort());
            LOGGER.info("sortColumn for jsp " + pagination.getSortColumn());
            LOGGER.info("page for jsp " + pagination.getCurrentPage());
            LOGGER.info("pagination for jsp " + pagination.getRecordsPerPage());

            request.setAttribute("page", pagination.getCurrentPage());
            request.setAttribute("pagination", pagination.getRecordsPerPage());
            request.setAttribute("noofpages", pagination.getNOfPages());
            request.setAttribute("sort", pagination.getSort());
            request.setAttribute("sortcolumn", pagination.getSortColumn());

            request.setAttribute("subject", subjectService.getPage(pagination));
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            request.setAttribute("error", Error.ERROR_PAGE.getMessage());
            request.setAttribute("errormessage", e.getMessage());
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            return "redirect:/";
        }

        return "/index.jsp";
    }
}
