package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.controller.util.TextLocale;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SignupCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(SignupCommand.class);
    private final UserServiceImpl userService;
    private static final String REDIRECT_SIGNUP_JSP = "redirect:/signup.jsp";
    private static final String REDIRECT_USER = "redirect:/user";
    private static final String SIGNUP_JSP = "/signup.jsp";

    public SignupCommand() {
        this.userService = new UserServiceImpl();
    }

    public SignupCommand(final UserServiceImpl service) {
        this.userService = service;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        final String name = request.getParameter("login");
        final String pass = request.getParameter("password");
        final String repeatPass = request.getParameter("repeatpass");
        final String userLang = request.getParameter("userlang");
        final HttpSession session = request.getSession();
        LOGGER.info(name);
        LOGGER.info(pass);
        LOGGER.info(repeatPass);
        LOGGER.info(userLang);

        final UserDTO errorUserDTO = UserDTO.builder()
                .setUsername(name)
                .setPassword(pass)
                .build();

        if (!pass.equals(repeatPass)) {
            LOGGER.info("Wrong password!");
            setErrorAttributes(Error.INVALID_PASSWORD.getMessage(), pass, errorUserDTO, session); //2
            return REDIRECT_SIGNUP_JSP;
        }

        final UserDTO userDTO = UserDTO.builder()
                .setUsername(name)
                .setPassword(pass)
                .setLocale(Localization.getLocaleByName(userLang))
                .setAdmin(false)
                .setBan(false)
                .build();

        UserDTO createdUser;
        LOGGER.info("NewUser " + userDTO);
        try {
            createdUser = userService.create(userDTO);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setErrorAttributes(getError(e.getMessage()), null, null, session);
            return REDIRECT_SIGNUP_JSP;
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setErrorAttributes(Error.INVALID_LOGIN.getMessage(), name, errorUserDTO, session); //1
            return REDIRECT_SIGNUP_JSP;
        }
        createdUser.setPassword("");
        LOGGER.info("Created User " + createdUser);
        if (CommandUtility.checkUserIsLogged(request, createdUser)) {
            setErrorAttributes(Error.LOGGED_USER.getMessage(), userDTO.getUsername(), errorUserDTO, session); //4
            return REDIRECT_SIGNUP_JSP;
        }
        if (createdUser.getId() != null) {
            CommandUtility.setUserRole(request, createdUser);
            session.setAttribute(SessionAttribute.LANGUAGE.getAttribute(), createdUser.getLocale());
            TextLocale.getCurrentInstance(request).setLocale(createdUser.getLocale());
            return REDIRECT_USER;
        }

        return SIGNUP_JSP;
    }

    private void setErrorAttributes(final String errorpage, final String errormessage, final UserDTO errordto, final HttpSession session) {
        if (errorpage != null) {
            session.setAttribute(SessionAttribute.ERROR_PAGE.getAttribute(), errorpage);
        }
        if (errormessage != null) {
            session.setAttribute(SessionAttribute.ERROR_MESSAGE.getAttribute(), errormessage);
        }
        if (errordto != null) {
            session.setAttribute(SessionAttribute.ERROR_DTO.getAttribute(), errordto);
        }
    }
}
