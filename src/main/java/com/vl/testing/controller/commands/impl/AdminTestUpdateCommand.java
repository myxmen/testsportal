package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminTestUpdateCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminTestUpdateCommand.class);
    private final TestServiceImpl testService;

    public AdminTestUpdateCommand(final TestServiceImpl testService) {
        this.testService = testService;
    }

    public AdminTestUpdateCommand() {
        this.testService = new TestServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        LOGGER.info("Subject ID = " + request.getParameter("subjectid"));
        LOGGER.info("Test ID = " + request.getParameter("testid"));
        LOGGER.info("test = " + request.getParameter("test"));
        LOGGER.info("difficulty = " + request.getParameter("difficulty"));
        LOGGER.info("time = " + request.getParameter("time"));

        Long subjectId;
        Long testId;

        try {
            subjectId = Long.parseLong(request.getParameter("subjectid"));
            testId = Long.parseLong(request.getParameter("testid"));
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }

        if (method.equals("Post")) {
            final String test = request.getParameter("test").trim();
            try {
                final Integer difficulty = Integer.parseInt(request.getParameter("difficulty"));
                final Long requests = Long.parseLong(request.getParameter("requests"));
                final Long time = Long.parseLong(request.getParameter("time"));
                testService.update(TestDTO.builder()
                        .setId(testId)
                        .setSubjectId(subjectId)
                        .setTest(test)
                        .setRequests(requests)
                        .setDifficulty(difficulty)
                        .setTime(time)
                        .build());
                LOGGER.info("Update test with id:" + testId);
                return "redirect:/admin/test?subjectid=" + subjectId;
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_TEST.getMessage(), test, request);
            } catch (ValidateException e) {
                LOGGER.error(getExceptionMessage(e.getMessage()));
                setRequestErrorAttributes(getError(e.getMessage()), "validation", test, request);
            } catch (NumberFormatException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_TEST.getMessage(), "validation", test, request);
            }
        }

        try {
            request.setAttribute("testone", testService.get(testId).get());
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setRequestErrorAttributes(getError(e.getMessage()), getExceptionMessage(e.getMessage()), request);
        }

        request.setAttribute("subjectid", subjectId);
        request.setAttribute("testid", testId);
        return "/WEB-INF/admin/admintestupdate.jsp";
    }
}
