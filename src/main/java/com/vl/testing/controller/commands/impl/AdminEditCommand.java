package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminEditCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminEditCommand.class);
    private final UserServiceImpl userService;
    private static final String REDIRECT_ADMIN_EDIT = "redirect:/admin/edit";
    private static final String REDIRECT_LOGOUT = "redirect:/logout";
    private static final String REDIRECT_ADMIN = "redirect:/admin";

    public AdminEditCommand() {
        this.userService = new UserServiceImpl();
    }

    public AdminEditCommand(final UserServiceImpl userService) {
        this.userService = userService;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        if (method.equals("Get")) {
            return "/WEB-INF/admin/adminedit.jsp";
        }
        final String name = request.getParameter("login");
        final String pass = request.getParameter("password");
        final String repeatPass = request.getParameter("repeatpass");
        final String userLang = request.getParameter("userlang");
        final HttpSession session = request.getSession();
        LOGGER.info(name);
        LOGGER.info(pass);
        LOGGER.info(repeatPass);
        LOGGER.info(userLang);
        final UserDTO errorUserDTO = UserDTO.builder()
                .setUsername(name)
                .setPassword(pass)
                .build();

        if (!pass.equals(repeatPass)) {
            LOGGER.info("Wrong password!");
            setErrorAttributes(Error.INVALID_PASSWORD.getMessage(), pass, errorUserDTO, session); //invalid password
            return REDIRECT_ADMIN_EDIT;
        }

        final UserDTO userDTO = CommandUtility.getNewSessionUserDTO(request, name, pass, userLang);
        Optional<UserDTO> createdUser;
        LOGGER.info("NewUser " + userDTO);

        try {
            createdUser = userService.updateUsersFields(userDTO);
            LOGGER.info("CreatedUser " + createdUser);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setErrorAttributes(getError(e.getMessage()), null, null, session);
            return REDIRECT_ADMIN_EDIT;
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setErrorAttributes(Error.INVALID_LOGIN.getMessage(), name, errorUserDTO, session); //invalid login 1
            return REDIRECT_ADMIN_EDIT;
        }

        if (createdUser.isEmpty() || createdUser.get().getId() == null) { //LOGGER.error(e.getMessage());
            setErrorAttributes(Error.ERROR_UPDATE.getMessage(), name, errorUserDTO, session); //Account failed to update 4
            return REDIRECT_ADMIN_EDIT;
        }

        LOGGER.info("NewUser after update" + userDTO);
        if (!CommandUtility.updateLoggedUser(request, createdUser)) {
            LOGGER.info("Current User is not found into context! Logout!");
            return REDIRECT_LOGOUT;
        }

        return REDIRECT_ADMIN;
    }

    private void setErrorAttributes(final String errorpage, final String errormessage, final UserDTO errordto, final HttpSession session) {
        if (errorpage != null) {
            session.setAttribute(SessionAttribute.ERROR_PAGE.getAttribute(), errorpage);
        }
        if (errormessage != null) {
            session.setAttribute(SessionAttribute.ERROR_MESSAGE.getAttribute(), errormessage);
        }
        if (errordto != null) {
            session.setAttribute(SessionAttribute.ERROR_DTO.getAttribute(), errordto);
        }
    }
}
