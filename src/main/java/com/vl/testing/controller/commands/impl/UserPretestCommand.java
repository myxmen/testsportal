package com.vl.testing.controller.commands.impl;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserPretestCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserPretestCommand.class);
    private final QuestionServiceImpl questionService;
    private final TestServiceImpl testService;
    private static final Integer COUNT = 0;
    private static final int SECONDS_IN_MINUTE = 60;

    public UserPretestCommand() {
        this.questionService = new QuestionServiceImpl();
        this.testService = new TestServiceImpl();
    }

    public UserPretestCommand(final QuestionServiceImpl questionService, final TestServiceImpl testService) {
        this.questionService = questionService;
        this.testService = testService;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        final HttpSession session = request.getSession();
        LOGGER.info(method);
        final SubjectDTO currentSubject = (SubjectDTO) session.getAttribute("subject");
        LOGGER.info(currentSubject);

        if (currentSubject == null) {
            return "redirect:/user/subject";
        }
        if (currentSubject.getTest() == null) {
            return "redirect:/user/test";
        }

        try {
            final TestDTO testDTO = currentSubject.getTest();
            testDTO.setQuestions(questionService.getAllByTest(testDTO)
                    .stream()
                    .sorted(Comparator.comparingInt(QuestionDTO::getNumberOfQuestion))
                    .collect(Collectors.toList()));
            final Boolean increaseRequestOfTest = testService.increaseRequestOfTest(testDTO);

            if (!increaseRequestOfTest) {
                setAttributes(Error.ERROR_PAGE.getMessage(), "Can not update the field of test's request", request);
                return "/WEB-INF/user/userpretest.jsp";
            }

            if (testDTO.getQuestions().isEmpty()) {
                setAttributes(Error.ERROR_PAGE.getMessage(), "List of questions for this test is empty", request);
                return "/WEB-INF/user/userpretest.jsp";
            }

            currentSubject.setTests(testDTO);
            session.setAttribute("subject", currentSubject);
            session.setAttribute("count", COUNT);
            session.setAttribute("questiontoanswer", new LinkedHashMap<QuestionDTO, String[]>());
            session.setAttribute("duration", testDTO.getTime() * SECONDS_IN_MINUTE);

        } catch (RepositoryException | ValidateException e) {
            LOGGER.error(e.getMessage());
            setAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        }

        LOGGER.info(currentSubject);
        return "/WEB-INF/user/userpretest.jsp";
    }

    private void setAttributes(final String error, final String errormessage, final HttpServletRequest request) {
        request.setAttribute("error", error);
        request.setAttribute("errormessage", errormessage);
    }
}
