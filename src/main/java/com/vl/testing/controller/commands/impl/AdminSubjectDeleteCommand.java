package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminSubjectDeleteCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminSubjectDeleteCommand.class);
    private final SubjectServiceImpl subjectService;
    private final TestServiceImpl testService;

    public AdminSubjectDeleteCommand(final SubjectServiceImpl subjectService, final TestServiceImpl testService) {
        this.subjectService = subjectService;
        this.testService = testService;
    }

    public AdminSubjectDeleteCommand() {
        this.subjectService = new SubjectServiceImpl();
        this.testService = new TestServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        LOGGER.info("ID " + request.getParameter("id"));
        Long subjectId;
        try {
            subjectId = Long.parseLong(request.getParameter("id"));
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }
        if (method.equals("Post")) {
            try {
                subjectService.delete(subjectId);
                LOGGER.info("Delete subject with id:" + subjectId);
                return "redirect:/admin/subject";
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
            } catch (ValidateException e) {
                LOGGER.error(getExceptionMessage(e.getMessage()));
                setRequestErrorAttributes(getError(e.getMessage()), getExceptionMessage(e.getMessage()), request);
            }
        }
        try {
            final SubjectDTO subjectDTO = subjectService.get(subjectId).get();
            request.setAttribute("subjectone", subjectDTO);
            request.setAttribute("tests", testService.getNumberOfRows(subjectDTO));
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setRequestErrorAttributes(getError(e.getMessage()), getExceptionMessage(e.getMessage()), request);
        }

        request.setAttribute("id", subjectId);
        return "/WEB-INF/admin/adminsubjectdelete.jsp";
    }
}
