package com.vl.testing.controller.commands.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserTestCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserTestCommand.class);
    private final TestServiceImpl testService;
    private static final int DEFAULT_ROWS = 0;

    public UserTestCommand() {
        this.testService = new TestServiceImpl();
    }

    public UserTestCommand(final TestServiceImpl service) {
        this.testService = service;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        final HttpSession session = request.getSession();
        LOGGER.info(method);

        final SubjectDTO currentSubject = (SubjectDTO) session.getAttribute("subject");
        LOGGER.info(currentSubject);
        if (currentSubject == null) {
            return "redirect:/user/subject";
        }
        LOGGER.info("Selected id " + request.getParameter("id"));
        final String testId = request.getParameter("id");
        if (testId != null && !testId.equals("")) {
            try {
                currentSubject.setTests(testService.get(Long.parseLong(testId)).get());
                session.setAttribute("subject", currentSubject);
                return "redirect:/user/pretest";
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
            } catch (ValidateException | NumberFormatException e) {
                LOGGER.error(e.getMessage());
                return "redirect:/user";
            }
        }

        Integer rows = DEFAULT_ROWS;
        try {
            rows = testService.getNumberOfRows(currentSubject);
        } catch (RepositoryException | ValidateException e) {
            LOGGER.error(e.getMessage());
            setAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        }

        final Pagination pagination = Pagination.builder()
                .setRows(rows)
                .setCurrentPage(request.getParameter("page"))
                .setRecordsPerPage(request.getParameter("pagination"))
                .setSort(request.getParameter("sort"))
                .setSortColumn(request.getParameter("sortcolumn"))
                .setDefaultColumn(DefaultColumn.TEST.getColumn())
                .build();

        try {
            request.setAttribute("test", testService.getPage(currentSubject, pagination));
        } catch (RepositoryException | ValidateException e) {
            LOGGER.error(e.getMessage());
            setAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        }

        request.setAttribute("page", pagination.getCurrentPage());
        request.setAttribute("pagination", pagination.getRecordsPerPage());
        request.setAttribute("noofpages", pagination.getNOfPages());
        request.setAttribute("sort", pagination.getSort());
        request.setAttribute("sortcolumn", pagination.getSortColumn());

        return "/WEB-INF/user/usertest.jsp";
    }

    private void setAttributes(final String error, final String errormessage, final HttpServletRequest request) {
        request.setAttribute("error", error);
        request.setAttribute("errormessage", errormessage);
    }
}
