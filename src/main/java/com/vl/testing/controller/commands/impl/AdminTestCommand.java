package com.vl.testing.controller.commands.impl;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminTestCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminTestCommand.class);
    private final SubjectServiceImpl subjectService;
    private final TestServiceImpl testService;

    public AdminTestCommand(final SubjectServiceImpl subjectService, final TestServiceImpl testService) {
        this.subjectService = subjectService;
        this.testService = testService;
    }

    public AdminTestCommand() {
        this.subjectService = new SubjectServiceImpl();
        this.testService = new TestServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        LOGGER.info("Subject ID = " + request.getParameter("subjectid"));
        try {
            final SubjectDTO currentSubject = subjectService.get(Long.parseLong(request.getParameter("subjectid"))).get();
            final Integer rows = testService.getNumberOfRows(currentSubject);

            final Pagination pagination = Pagination.builder()
                    .setRows(rows)
                    .setCurrentPage(request.getParameter("page"))
                    .setRecordsPerPage(request.getParameter("pagination"))
                    .setSort(request.getParameter("sort"))
                    .setSortColumn(request.getParameter("sortcolumn"))
                    .setDefaultColumn(DefaultColumn.TEST.getColumn())
                    .build();

            request.setAttribute("test", testService.getPage(currentSubject, pagination));
            request.setAttribute("page", pagination.getCurrentPage());
            request.setAttribute("pagination", pagination.getRecordsPerPage());
            request.setAttribute("noofpages", pagination.getNOfPages());
            request.setAttribute("sort", pagination.getSort());
            request.setAttribute("sortcolumn", pagination.getSortColumn());

            request.setAttribute("subjectid", currentSubject.getId());
            request.setAttribute("subject", currentSubject.getSubject());
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            request.setAttribute("error", Error.ERROR_PAGE.getMessage());
            request.setAttribute("errormessage", e.getMessage());
        } catch (ValidateException | NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }

        return "/WEB-INF/admin/admintest.jsp";
    }
}
