package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminQuestionDeleteCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminQuestionDeleteCommand.class);
    private final QuestionServiceImpl questionService;

    public AdminQuestionDeleteCommand(final QuestionServiceImpl questionService) {
        this.questionService = questionService;
    }

    public AdminQuestionDeleteCommand() {
        this.questionService = new QuestionServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        LOGGER.info("Subject ID = " + request.getParameter("subjectid"));
        LOGGER.info("Test ID = " + request.getParameter("testid"));
        LOGGER.info("Question ID = " + request.getParameter("questionid"));

        Long questionId;

        try {
            questionId = Long.parseLong(request.getParameter("questionid"));
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }

        if (method.equals("Post")) {
            try {
                final Long testId = Long.parseLong(request.getParameter("testid"));
                questionService.delete(questionId);
                LOGGER.info("Delete question with id: " + questionId);
                return "redirect:/admin/question?testid=" + testId;
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
            } catch (ValidateException e) {
                LOGGER.error(getExceptionMessage(e.getMessage()));
                setRequestErrorAttributes(getError(e.getMessage()), getExceptionMessage(e.getMessage()), request);
            } catch (NumberFormatException e) {
                LOGGER.error(e.getMessage());
                return "redirect:/admin";
            }
        }

        try {
            final QuestionDTO questionDTO = questionService.get(questionId).get();
            request.setAttribute("questionone", questionDTO);
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setRequestErrorAttributes(getError(e.getMessage()), getExceptionMessage(e.getMessage()), request);
        }

        request.setAttribute("questionid", questionId);
        return "/WEB-INF/admin/adminquestiondelete.jsp";
    }
}
