package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import java.util.LinkedHashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.PassedTestServiceImpl;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserEndTestCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserEndTestCommand.class);
    private final QuestionServiceImpl questionService;
    private final PassedTestServiceImpl passedTestService;

    public UserEndTestCommand() {
        this.questionService = new QuestionServiceImpl();
        this.passedTestService = new PassedTestServiceImpl();
    }

    public UserEndTestCommand(final QuestionServiceImpl questionService, final PassedTestServiceImpl passedTestService) {
        this.questionService = questionService;
        this.passedTestService = passedTestService;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        final HttpSession session = request.getSession();
        LOGGER.info(method);
        final LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap =
                (LinkedHashMap<QuestionDTO, String[]>) session.getAttribute("questiontoanswer");
        final TestDTO testDTO = ((SubjectDTO) session.getAttribute("subject")).getTest();
        if (questionToAnswerMap == null || testDTO == null) {
            return "redirect:/user";
        }
        final Integer result = questionService.getScore(questionToAnswerMap, testDTO.getQuestions().size());
        request.setAttribute("score", result);

        try {
            passedTestService.create(PassedTestDTO.builder()
                            .setResult(result)
                            .setTest(testDTO)
                            .build(),
                    (UserDTO) session.getAttribute("user"));
            session.removeAttribute("subject");
            session.removeAttribute("count");
            session.removeAttribute("timer");
            session.removeAttribute("duration");
            session.removeAttribute("questiontoanswer");
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            request.setAttribute("error", Error.ERROR_PAGE.getMessage());
            request.setAttribute("errormessage", e.getMessage());
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            request.setAttribute("error", getError(e.getMessage()));
            request.setAttribute("errormessage", getExceptionMessage(e.getMessage()));
        }

        return "/WEB-INF/user/userendtest.jsp";
    }
}
