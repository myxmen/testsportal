package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.ContextAttribute;
import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminCommand.class);
    private final transient UserServiceImpl userService;

    public AdminCommand() {
        this.userService = new UserServiceImpl();
    }

    public AdminCommand(final UserServiceImpl userService) {
        this.userService = userService;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        final HashSet<UserDTO> loggedUsers = (HashSet<UserDTO>) request.getSession().getServletContext()
                .getAttribute(ContextAttribute.LOGGED_USERS.getAttribute());
        final List<UserDTO> usersList = new ArrayList<>(loggedUsers);

        final Integer rows = userService.getNumberOfRowsLoggedUsers(usersList);
        final Pagination pagination = Pagination.builder()
                .setRows(rows)
                .setCurrentPage(request.getParameter("page"))
                .setRecordsPerPage(request.getParameter("pagination"))
                .setSort(request.getParameter("sort"))
                .setSortColumn(request.getParameter("sortcolumn"))
                .setDefaultColumn(DefaultColumn.USER.getColumn())
                .build();
        try {
            request.setAttribute("users", userService.getPageLoggedUsers(usersList, pagination));
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            return "redirect:/admin";
        }

        request.setAttribute("page", pagination.getCurrentPage());
        request.setAttribute("pagination", pagination.getRecordsPerPage());
        request.setAttribute("noofpages", pagination.getNOfPages());
        request.setAttribute("sort", pagination.getSort());
        request.setAttribute("sortcolumn", pagination.getSortColumn());

        return "/WEB-INF/admin/adminbasis.jsp";

    }
}
