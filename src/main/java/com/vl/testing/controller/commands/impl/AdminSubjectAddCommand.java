package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminSubjectAddCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminSubjectAddCommand.class);
    private final SubjectServiceImpl subjectService;

    public AdminSubjectAddCommand(final SubjectServiceImpl subjectService) {
        this.subjectService = subjectService;
    }

    public AdminSubjectAddCommand() {
        this.subjectService = new SubjectServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        if (method.equals("Post")) {
            final String subject = request.getParameter("subject").trim();
            try {
                final SubjectDTO createdSubject = subjectService
                        .create(SubjectDTO.builder()
                                .setSubject(subject)
                                .build());
                LOGGER.info("Create subject with id:" + createdSubject.getId());
                return "redirect:/admin/subject";
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_SUBJECT.getMessage(), subject, request);
            } catch (ValidateException e) {
                LOGGER.error(getExceptionMessage(e.getMessage()));
                setRequestErrorAttributes(getError(e.getMessage()), "validation", subject, request);
            }
        }

        return "/WEB-INF/admin/adminsubjectadd.jsp";
    }
}
