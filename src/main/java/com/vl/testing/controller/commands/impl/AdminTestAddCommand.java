package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminTestAddCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminTestAddCommand.class);
    private static final long DEFAULT_REQUESTS = 0;
    private final TestServiceImpl testService;

    public AdminTestAddCommand(final TestServiceImpl testService) {
        this.testService = testService;
    }

    public AdminTestAddCommand() {
        this.testService = new TestServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        Long subjectId;
        try {
            subjectId = Long.parseLong(request.getParameter("subjectid"));
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }
        if (subjectId <= 0) {
            LOGGER.error("Subject ID is empty");
            return "redirect:/admin";
        }
        LOGGER.info("Subject ID = " + subjectId);
        LOGGER.info("Test ID = " + request.getParameter("testid"));
        LOGGER.info("test = " + request.getParameter("test"));
        LOGGER.info("difficulty = " + request.getParameter("difficulty"));
        LOGGER.info("time = " + request.getParameter("time"));

        if (method.equals("Post")) {
            final String test = request.getParameter("test").trim();
            try {
                final Integer difficulty = Integer.parseInt(request.getParameter("difficulty"));
                final Long time = Long.parseLong(request.getParameter("time"));
                final TestDTO createdTest = testService
                        .create(TestDTO.builder()
                                .setSubjectId(subjectId)
                                .setTest(test)
                                .setRequests(DEFAULT_REQUESTS)
                                .setDifficulty(difficulty)
                                .setTime(time)
                                .build());
                LOGGER.info("Create test with id:" + createdTest.getId());
                return "redirect:/admin/test?subjectid=" + subjectId;
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_TEST.getMessage(), test, request);
            } catch (ValidateException e) {
                LOGGER.error(getExceptionMessage(e.getMessage()));
                setRequestErrorAttributes(getError(e.getMessage()), "validation", test, request);
            } catch (NumberFormatException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_TEST.getMessage(), "validation", test, request);
            }
        }

        request.setAttribute("subjectid", subjectId);
        return "/WEB-INF/admin/admintestadd.jsp";
    }
}
