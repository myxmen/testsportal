package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import java.util.NoSuchElementException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.PassedTestServiceImpl;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminUserDeleteCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminUserDeleteCommand.class);
    private final UserServiceImpl userService;
    private final PassedTestServiceImpl passedTestService;

    public AdminUserDeleteCommand(final UserServiceImpl userService, final PassedTestServiceImpl passedTestService) {
        this.userService = userService;
        this.passedTestService = passedTestService;
    }

    public AdminUserDeleteCommand() {
        this.userService = new UserServiceImpl();
        this.passedTestService = new PassedTestServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        final Long userId = Long.parseLong(request.getParameter("id"));
        LOGGER.info("Delete user page, id:" + userId);

        final HttpSession session = request.getSession();
        final UserDTO userCurrentAdmin = (UserDTO) session.getAttribute("user");
        if (userCurrentAdmin.getId() == userId) {
            LOGGER.error("Attempt to delete this session`s Admin: " + userCurrentAdmin.getUsername());
            return "redirect:/admin/userset";
        }

        if (method.equals("Post")) {
            try {
                userService.delete(userId);
                LOGGER.info("Delete user with id:" + userId);
                return "redirect:/admin/userset";
            } catch (ValidateException e) {
                final String message = getExceptionMessage(e.getMessage());
                LOGGER.error(message);
                setRequestErrorAttributes(getError(e.getMessage()), message, request);
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
            }
        }

        try {
            final UserDTO userDTO = userService.get(userId).get();
            request.setAttribute("userone", userDTO);
            request.setAttribute("tests", passedTestService.getNumberOfRows(userDTO));
        } catch (NoSuchElementException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin/userset";
        } catch (ValidateException e) {
            final String message = getExceptionMessage(e.getMessage());
            LOGGER.error(message);
            setRequestErrorAttributes(getError(e.getMessage()), message, request);
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        }

        request.setAttribute("id", userId);
        return "/WEB-INF/admin/adminuserdelete.jsp";
    }
}
