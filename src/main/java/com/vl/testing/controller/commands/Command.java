package com.vl.testing.controller.commands;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    /**
     * @param request HttpServletRequest.
     * @param method Http method.
     * @return url.
     */
    String execute(HttpServletRequest request, String method);
}
