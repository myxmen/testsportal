package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserSubjectCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserSubjectCommand.class);
    private final SubjectServiceImpl subjectService;
    private static final int DEFAULT_ROWS = 0;

    public UserSubjectCommand() {
        this.subjectService = new SubjectServiceImpl();
    }

    public UserSubjectCommand(final SubjectServiceImpl service) {
        this.subjectService = service;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        final HttpSession session = request.getSession();
        LOGGER.info(method);
        final String subId = request.getParameter("id");
        LOGGER.info("Selected id " + subId);
        if (subId != null && !subId.equals("")) {
            try {
                session.setAttribute("subject", subjectService.get(Long.parseLong(subId)).get()); //взять с предмет по ид с листа
                return "redirect:/user/test";
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                request.setAttribute("error", Error.ERROR_PAGE.getMessage());
                request.setAttribute("errormessage", e.getMessage());
                //  error = true;
            } catch (ValidateException e) {
                LOGGER.error(getExceptionMessage(e.getMessage()));
                return "redirect:/user";
            } catch (NumberFormatException e) {
                LOGGER.error(e.getMessage());
                return "redirect:/user";
            }
        }

        Integer rows = DEFAULT_ROWS;
        try {
            rows = subjectService.getNumberOfRows();
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            request.setAttribute("error", Error.ERROR_PAGE.getMessage());
            request.setAttribute("errormessage", e.getMessage());
        }

        final Pagination pagination = Pagination.builder()
                .setRows(rows)
                .setCurrentPage(request.getParameter("page"))
                .setRecordsPerPage(request.getParameter("pagination"))
                .setSort(request.getParameter("sort"))
                .setSortColumn(request.getParameter("sortcolumn"))
                .setDefaultColumn(DefaultColumn.SUBJECT.getColumn())
                .build();

        try {
            request.setAttribute("subject", subjectService.getPage(pagination));
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            request.setAttribute("error", Error.ERROR_PAGE.getMessage());
            request.setAttribute("errormessage", e.getMessage());
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            return "redirect:/user";
        }

        request.setAttribute("page", pagination.getCurrentPage());
        request.setAttribute("pagination", pagination.getRecordsPerPage());
        request.setAttribute("noofpages", pagination.getNOfPages());
        request.setAttribute("sort", pagination.getSort());
        request.setAttribute("sortcolumn", pagination.getSortColumn());

        return "/WEB-INF/user/usersubject.jsp";
    }
}
