package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.controller.util.TextLocale;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.exeptions.PasswordException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(LoginCommand.class);
    private final UserServiceImpl userService;
    private static final String REDIRECT_LOGIN_JSP = "redirect:/login.jsp";
    private static final String REDIRECT_USER = "redirect:/user";
    private static final String REDIRECT_ADMIN = "redirect:/admin";
    private static final String LOGIN = "/login";
    private static final String EMPTY = "XXXXXXXX";

    public LoginCommand() {
        this.userService = new UserServiceImpl();
    }

    public LoginCommand(final UserServiceImpl service) {
        this.userService = service;
    }

    @SuppressWarnings({"PMD.CyclomaticComplexity"})
    @Override
    public String execute(final HttpServletRequest request, final String method) {
        final HttpSession session = request.getSession();
        LOGGER.info("Get error from session " + session.getAttribute(SessionAttribute.ERROR_PAGE.getAttribute()));
        LOGGER.info("Get error from session " + session.getAttribute(SessionAttribute.ERROR_MESSAGE.getAttribute()));
        LOGGER.info("Get error from request " + request.getAttribute("error"));

        final String name = request.getParameter("name");
        final String pass = request.getParameter("pass");
        LOGGER.info(name + " " + pass);

        Optional<UserDTO> user = Optional.empty();
        final UserDTO errorUserDTO = UserDTO.builder()
                .setUsername(name)
                .setPassword(pass)
                .build();

        try {
            user = userService.getByNameAndPassword(errorUserDTO);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setErrorAttributes(getError(e.getMessage()), null, null, session);
            return REDIRECT_LOGIN_JSP;
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setErrorAttributes(Error.ERROR_DATABASE.getMessage(), e.getMessage(), null, session);
            return REDIRECT_LOGIN_JSP;
        } catch (PasswordException e) {
            LOGGER.error(e.getMessage());
            setErrorAttributes(Error.INVALID_PASSWORD.getMessage(), EMPTY, errorUserDTO, session);
            return REDIRECT_LOGIN_JSP;
        }

        if (user.isEmpty()) {
            setErrorAttributes(Error.INVALID_LOGIN.getMessage(), name, errorUserDTO, session);
            return REDIRECT_LOGIN_JSP;
        }

        final UserDTO userDTO = user.get();
        if (userDTO.isBan() != null && userDTO.isAdmin() != null) {
            if (userDTO.isBan()) {
                LOGGER.info("User is baned");
                setErrorAttributes(Error.BANNED_USER.getMessage(), userDTO.getUsername(), errorUserDTO, session); //banned user 3
                return REDIRECT_LOGIN_JSP;
            }

            if (CommandUtility.checkUserIsLogged(request, userDTO)) {
                LOGGER.info("User is logged");
                setErrorAttributes(Error.LOGGED_USER.getMessage(), userDTO.getUsername(), errorUserDTO, session); //logged_user 4
                return REDIRECT_LOGIN_JSP;
            }

            if (userDTO.isAdmin()) {
                CommandUtility.setUserRole(request, userDTO);
                session.setAttribute(SessionAttribute.LANGUAGE.getAttribute(), userDTO.getLocale());
                TextLocale.getCurrentInstance(request).setLocale(userDTO.getLocale());
                return REDIRECT_ADMIN;
            } else {
                CommandUtility.setUserRole(request, userDTO);
                session.setAttribute(SessionAttribute.LANGUAGE.getAttribute(), userDTO.getLocale());
                TextLocale.getCurrentInstance(request).setLocale(userDTO.getLocale());
                return REDIRECT_USER;
            }
        }

        return LOGIN;
    }

    private void setErrorAttributes(final String errorpage, final String errormessage, final UserDTO errordto, final HttpSession session) {
        if (errorpage != null) {
            session.setAttribute(SessionAttribute.ERROR_PAGE.getAttribute(), errorpage);
        }
        if (errormessage != null) {
            session.setAttribute(SessionAttribute.ERROR_MESSAGE.getAttribute(), errormessage);
        }
        if (errordto != null) {
            session.setAttribute(SessionAttribute.ERROR_DTO.getAttribute(), errordto);
        }
    }
}
