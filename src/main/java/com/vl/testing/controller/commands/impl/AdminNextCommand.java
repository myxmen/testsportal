package com.vl.testing.controller.commands.impl;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminNextCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminNextCommand.class);

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        LOGGER.info("ID = " + request.getParameter("id"));
        return "/WEB-INF/admin/adminnext.jsp";
    }
}
