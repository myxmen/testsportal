package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import java.util.Arrays;
import java.util.NoSuchElementException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminUserUpdateCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminUserUpdateCommand.class);
    private final UserServiceImpl userService;

    public AdminUserUpdateCommand(final UserServiceImpl userService) {
        this.userService = userService;
    }

    public AdminUserUpdateCommand() {
        this.userService = new UserServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        final Long userId = Long.parseLong(request.getParameter("id"));
        LOGGER.info("update " + userId);

        final HttpSession session = request.getSession();
        final UserDTO userCurrentAdmin = (UserDTO) session.getAttribute(SessionAttribute.USER.getAttribute());
        if (userCurrentAdmin.getId() == userId) {
            LOGGER.error("Attempt to update this session`s Admin: " + userCurrentAdmin.getUsername());
            return "redirect:/admin/userset";
        }

        try {
            final UserDTO userDTO = userService.get(userId).get();
            if (method.equals("Post")) {
                setAdminsAttributes(request, userDTO);
                userService.updateAdminsFields(userDTO);
                return "redirect:/admin/userset";
            }
            LOGGER.info("user: " + userDTO);
            request.setAttribute("userone", userDTO);
        } catch (NoSuchElementException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin/userset";
        } catch (ValidateException e) {
            final String message = getExceptionMessage(e.getMessage());
            LOGGER.error(message);
            request.setAttribute("error", getError(e.getMessage()));
            request.setAttribute("errormessage", message);
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            request.setAttribute("error", Error.ERROR_PAGE.getMessage());
            request.setAttribute("errormessage", e.getMessage());
        }
        request.setAttribute("id", userId);
        return "/WEB-INF/admin/adminuserupdate.jsp";
    }

    private void setAdminsAttributes(final HttpServletRequest request, final UserDTO userDTO) {
        final String[] choices = request.getParameterValues("choice");
        if (choices == null) {
            userDTO.setBan(false);
            userDTO.setAdmin(false);
        } else {
            Arrays.stream(choices).forEach(x -> LOGGER.info("Choice: " + x));
            if (choices.length == 2) {
                userDTO.setBan(true);
                userDTO.setAdmin(true);
            } else {
                if (Arrays.stream(choices).anyMatch(x -> x.equals("ban"))) {
                    userDTO.setBan(true);
                    userDTO.setAdmin(false);
                }
                if (Arrays.stream(choices).anyMatch(x -> x.equals("admin"))) {
                    userDTO.setBan(false);
                    userDTO.setAdmin(true);
                }
            }
        }
    }
}
