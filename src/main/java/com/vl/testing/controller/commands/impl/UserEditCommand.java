package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.commands.impl.CommandUtility.getNewSessionUserDTO;
import static com.vl.testing.controller.commands.impl.CommandUtility.updateLoggedUser;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserEditCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserEditCommand.class);
    private final UserServiceImpl userService;
    private static final String REDIRECT_USER_EDIT = "redirect:/user/edit";
    private static final String REDIRECT_LOGOUT = "redirect:/logout";
    private static final String REDIRECT_USER = "redirect:/user";

    public UserEditCommand() {
        this.userService = new UserServiceImpl();
    }

    public UserEditCommand(final UserServiceImpl userService) {
        this.userService = userService;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        if (method.equals("Get")) {
            return "/WEB-INF/user/useredit.jsp";
        }
        final String name = request.getParameter("login");
        final String pass = request.getParameter("password");
        final String repeatPass = request.getParameter("repeatpass");
        final String userLang = request.getParameter("userlang");
        final HttpSession session = request.getSession();
        LOGGER.info(name);
        LOGGER.info(pass);
        LOGGER.info(repeatPass);
        LOGGER.info(userLang);
        final UserDTO errorUserDTO = UserDTO.builder()
                .setUsername(name)
                .setPassword(pass)
                .build();

        if (!pass.equals(repeatPass)) {
            LOGGER.info("Wrong password!");
            setAttributes(Error.INVALID_PASSWORD.getMessage(), pass, errorUserDTO, session);
            return REDIRECT_USER_EDIT;
        }

        final UserDTO userDTO = getNewSessionUserDTO(request, name, pass, userLang);
        Optional<UserDTO> createdUser;
        LOGGER.info("NewUser: " + userDTO);

        try {
            createdUser = userService.updateUsersFields(userDTO);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            session.setAttribute(SessionAttribute.ERROR_PAGE.getAttribute(), getError(e.getMessage()));
            return REDIRECT_USER_EDIT;
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setAttributes(Error.INVALID_LOGIN.getMessage(), name, errorUserDTO, session);
            return REDIRECT_USER_EDIT;
        }

        if (createdUser.isEmpty() || createdUser.get().getId() == null) {
            setAttributes(Error.ERROR_UPDATE.getMessage(), name, errorUserDTO, session);
            return REDIRECT_USER_EDIT;
        }
        if (!updateLoggedUser(request, createdUser)) {
            LOGGER.info("Current User is not found into context! Logout!");
            return REDIRECT_LOGOUT;
        }

        return REDIRECT_USER;
    }

    private void setAttributes(final String errorpage, final String errormessage, final UserDTO errordto, final HttpSession session) {
        if (errorpage != null) {
            session.setAttribute(SessionAttribute.ERROR_PAGE.getAttribute(), errorpage);
        }
        if (errormessage != null) {
            session.setAttribute(SessionAttribute.ERROR_MESSAGE.getAttribute(), errormessage);
        }
        if (errordto != null) {
            session.setAttribute(SessionAttribute.ERROR_DTO.getAttribute(), errordto);
        }
    }
}
