package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminQuestionUpdateCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminQuestionUpdateCommand.class);
    private final QuestionServiceImpl questionService;
    private static final int INDEX_0 = 0;
    private static final int INDEX_1 = 1;
    private static final int INDEX_2 = 2;
    private static final int INDEX_3 = 3;
    private static final int INDEX_4 = 4;
    private static final int NUMBER_OF_QUESTIONS = 5;

    public AdminQuestionUpdateCommand(final QuestionServiceImpl questionService) {
        this.questionService = questionService;
    }

    public AdminQuestionUpdateCommand() {
        this.questionService = new QuestionServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        LOGGER.info("Test ID = " + request.getParameter("testid"));
        LOGGER.info("Question ID = " + request.getParameter("questionid"));
        LOGGER.info("Question Number = " + request.getParameter("qnumber"));
        LOGGER.info("Question = " + request.getParameter("question"));
        LOGGER.info("correctanswer1 = " + request.getParameter("correctanswer1"));
        LOGGER.info("answer1 = " + request.getParameter("answer1"));
        LOGGER.info("correctanswer2 = " + request.getParameter("correctanswer2"));
        LOGGER.info("answer2 = " + request.getParameter("answer2"));
        LOGGER.info("correctanswer3 = " + request.getParameter("correctanswer3"));
        LOGGER.info("answer3 = " + request.getParameter("answer3"));
        LOGGER.info("correctanswer4 = " + request.getParameter("correctanswer4"));
        LOGGER.info("answer4 = " + request.getParameter("answer4"));
        LOGGER.info("correctanswer5 = " + request.getParameter("correctanswer5"));
        LOGGER.info("answer5 = " + request.getParameter("answer5"));

        Long questionId = null;
        try {
            questionId = Long.parseLong(request.getParameter("questionid"));
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }

        if (method.equals("Post")) {
            Long testId = null;
            try {
                testId = Long.parseLong(request.getParameter("testid"));
            } catch (NumberFormatException e) {
                LOGGER.error(e.getMessage());
                return "redirect:/admin";
            }
            final String question = request.getParameter("question").trim();
            Integer numberOfQuestion = 0;
            try {
                numberOfQuestion = Integer.parseInt(request.getParameter("qnumber"));
                final String[] answer = getAnswers(request);
                questionService.update(QuestionDTO.builder()
                        .setId(questionId)
                        .setTestId(testId)
                        .setNumberOfQuestion(numberOfQuestion)
                        .setQuestion(question)
                        .setAnswer1(answer[INDEX_0])
                        .setAnswer2(answer[INDEX_1])
                        .setAnswer3(answer[INDEX_2])
                        .setAnswer4(answer[INDEX_3])
                        .setAnswer5(answer[INDEX_4])
                        .setCorrectAnswer1(getCorrectAnswer(answer[INDEX_0], request.getParameter("correctanswer1")))
                        .setCorrectAnswer2(getCorrectAnswer(answer[INDEX_1], request.getParameter("correctanswer2")))
                        .setCorrectAnswer3(getCorrectAnswer(answer[INDEX_2], request.getParameter("correctanswer3")))
                        .setCorrectAnswer4(getCorrectAnswer(answer[INDEX_3], request.getParameter("correctanswer4")))
                        .setCorrectAnswer5(getCorrectAnswer(answer[INDEX_4], request.getParameter("correctanswer5")))
                        .build());
                LOGGER.info("Update question with id:" + questionId);
                return "redirect:/admin/question?testid=" + testId;
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_QUESTION.getMessage(), String.valueOf(numberOfQuestion), request);
            } catch (NumberFormatException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_QUESTION.getMessage(), "validation", question, request);
            } catch (ValidateException e) {
                LOGGER.error(getExceptionMessage(e.getMessage()));
                setRequestErrorAttributes(getError(e.getMessage()), "validation", question, request);
            }
        }

        try {
            final QuestionDTO questionDTO = questionService.get(questionId).get();
            request.setAttribute("questionone", questionDTO);
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setRequestErrorAttributes(getError(e.getMessage()), getExceptionMessage(e.getMessage()), request);
        }

        request.setAttribute("questionid", questionId);
        return "/WEB-INF/admin/adminquestionupdate.jsp";
    }

    private String isStringEmpty(final String str) {
        if (str == null || str.replaceAll("\\s", "").equals("")) {
            return null;
        }
        return str;
    }

    private String[] getAnswers(final HttpServletRequest request) {
        String[] answer = new String[NUMBER_OF_QUESTIONS];
        answer[INDEX_0] = isStringEmpty(request.getParameter("answer1").trim());
        answer[INDEX_1] = isStringEmpty(request.getParameter("answer2").trim());
        answer[INDEX_2] = isStringEmpty(request.getParameter("answer3").trim());
        if (answer[INDEX_2] == null) {
            answer[INDEX_3] = null;
            answer[INDEX_4] = null;
        } else {
            answer[INDEX_3] = isStringEmpty(request.getParameter("answer4").trim());
            if (answer[INDEX_3] == null) {
                answer[INDEX_4] = null;
            } else {
                answer[INDEX_4] = isStringEmpty(request.getParameter("answer5").trim());
            }
        }
        return answer;
    }

    private Boolean getCorrectAnswer(final String answer, final String correctAnswer) {
        if (answer == null) {
            return null;
        }
        if (correctAnswer == null) {
            return false;
        }
        return true;
    }
}
