package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminQuestionCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminQuestionCommand.class);
    private final SubjectServiceImpl subjectService;
    private final TestServiceImpl testService;
    private final QuestionServiceImpl questionService;
    private static final int DEFAULT_ROWS = 0;

    public AdminQuestionCommand(final QuestionServiceImpl questionService,
                                final TestServiceImpl testService, final SubjectServiceImpl subjectService) {
        this.questionService = questionService;
        this.testService = testService;
        this.subjectService = subjectService;
    }

    public AdminQuestionCommand() {
        this.questionService = new QuestionServiceImpl();
        this.testService = new TestServiceImpl();
        this.subjectService = new SubjectServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        LOGGER.info("Subject ID = " + request.getParameter("subjectid"));
        LOGGER.info("Test ID = " + request.getParameter("testid"));
        TestDTO currentTest = null;
        Integer rows = DEFAULT_ROWS;

        try {
            currentTest = testService.get(Long.parseLong(request.getParameter("testid"))).get();
            request.setAttribute("testid", currentTest.getId());
            request.setAttribute("test", currentTest.getTest());
            final SubjectDTO currentSubject = subjectService.get(currentTest.getSubjectId()).get();
            currentSubject.setTests(currentTest);
            request.setAttribute("subjectid", currentSubject.getId());
            request.setAttribute("subject", currentSubject.getSubject());
            rows = questionService.getNumberOfRows(currentTest);
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            return "redirect:/admin";
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }

        final Pagination pagination = Pagination.builder()
                .setRows(rows)
                .setCurrentPage(request.getParameter("page"))
                .setRecordsPerPage(request.getParameter("pagination"))
                .setSort(request.getParameter("sort"))
                .setSortColumn(request.getParameter("sortcolumn"))
                .setDefaultColumn(DefaultColumn.NUMQUESTION.getColumn())
                .build();

        try {
            request.setAttribute("question", questionService.getPage(currentTest, pagination));
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), getExceptionMessage(e.getMessage()), request);
        }

        request.setAttribute("page", pagination.getCurrentPage());
        request.setAttribute("pagination", pagination.getRecordsPerPage());
        request.setAttribute("noofpages", pagination.getNOfPages());
        request.setAttribute("sort", pagination.getSort());
        request.setAttribute("sortcolumn", pagination.getSortColumn());

        return "/WEB-INF/admin/adminquestion.jsp";
    }
}
