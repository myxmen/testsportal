package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminSubjectCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminSubjectCommand.class);
    private final SubjectServiceImpl subjectService;

    public AdminSubjectCommand(final SubjectServiceImpl subjectService) {
        this.subjectService = subjectService;
    }

    public AdminSubjectCommand() {
        this.subjectService = new SubjectServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info("ID " + request.getParameter("id"));
        LOGGER.info(method);

        try {
            final Integer rows = subjectService.getNumberOfRows();

            final Pagination pagination = Pagination.builder()
                    .setRows(rows)
                    .setCurrentPage(request.getParameter("page"))
                    .setRecordsPerPage(request.getParameter("pagination"))
                    .setSort(request.getParameter("sort"))
                    .setSortColumn(request.getParameter("sortcolumn"))
                    .setDefaultColumn(DefaultColumn.SUBJECT.getColumn())
                    .build();

            request.setAttribute("page", pagination.getCurrentPage());
            request.setAttribute("pagination", pagination.getRecordsPerPage());
            request.setAttribute("noofpages", pagination.getNOfPages());
            request.setAttribute("sort", pagination.getSort());
            request.setAttribute("sortcolumn", pagination.getSortColumn());

            request.setAttribute("subject", subjectService.getPage(pagination));
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            return "redirect:/admin";
        }

        return "/WEB-INF/admin/adminsubject.jsp";
    }
}
