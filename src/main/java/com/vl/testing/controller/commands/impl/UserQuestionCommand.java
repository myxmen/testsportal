package com.vl.testing.controller.commands.impl;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.SubjectDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserQuestionCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserQuestionCommand.class);

    @SuppressWarnings({"PMD.ModifiedCyclomaticComplexity", "PMD.StdCyclomaticComplexity",
            "PMD.CyclomaticComplexity", "PMD.NPathComplexity"})
    @Override
    public String execute(final HttpServletRequest request, final String method) {
        final HttpSession session = request.getSession();
        LOGGER.info(method);

        final String[] answers = request.getParameterValues("answer");
        if (method.equals("Post") && answers == null) {
            request.setAttribute("error", Error.ERROR_ANSWER.getMessage());
        }

        final SubjectDTO currentSubject = (SubjectDTO) session.getAttribute("subject");
        LOGGER.info(currentSubject);
        Integer currentCount = (Integer) session.getAttribute("count");
        LOGGER.info(currentCount);

        if (currentSubject == null) {
            return "redirect:/user/subject";
        }

        if (currentSubject.getTest() == null || currentSubject.getTest().getQuestions() == null) {
            return "redirect:/user/test";
        }

        if (currentCount == null) {
            return "redirect:/user/pretest";
        }

        final List<QuestionDTO> allQuestions = currentSubject.getTest().getQuestions();
        LOGGER.info(allQuestions);

        if (answers != null) {
            Arrays.stream(answers).forEach(x -> LOGGER.info(x));
            final LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap =
                    (LinkedHashMap<QuestionDTO, String[]>) session.getAttribute("questiontoanswer");
            questionToAnswerMap.put(allQuestions.get(currentCount), answers);
            LOGGER.info(questionToAnswerMap);
            session.setAttribute("questiontoanswer", questionToAnswerMap);
            if (++currentCount < currentSubject.getTest().getQuestions().size()) {
                session.setAttribute("count", currentCount);
            } else {
                return "redirect:/user/endtest";
            }
        }

        final List<String> currentAnswers = Arrays.asList(allQuestions.get(currentCount).getAnswer1(),
                allQuestions.get(currentCount).getAnswer2(),
                allQuestions.get(currentCount).getAnswer3(),
                allQuestions.get(currentCount).getAnswer4(),
                allQuestions.get(currentCount).getAnswer5())
                .stream()
                .filter(x -> x != null)
                .collect(Collectors.toList());
        LOGGER.info(currentAnswers);

        if (currentCount == 0 && method.equals("Get")) {
            session.setAttribute("timer", Instant.now().toEpochMilli());
        }

        LOGGER.info(session.getAttribute("timer"));
        final Duration between = Duration.between(Instant.ofEpochMilli((long) session.getAttribute("timer")),
                Instant.ofEpochMilli(Instant.now().toEpochMilli()));
        LOGGER.info(between.getSeconds());
        final Duration remaining = Duration.ofSeconds(((long) session.getAttribute("duration")) - between.getSeconds());
        LOGGER.info(remaining.getSeconds());
        if (remaining.getSeconds() < 0) {
            return "redirect:/user/endtest";
        }

        request.setAttribute("quest", allQuestions.get(currentCount).getQuestion());
        request.setAttribute("questions", currentAnswers);
        request.setAttribute("count", currentCount + 1);
        request.setAttribute("timeleft", remaining.getSeconds());

        return "/WEB-INF/user/userquestion.jsp";
    }
}
