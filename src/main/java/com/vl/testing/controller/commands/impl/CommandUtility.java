package com.vl.testing.controller.commands.impl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.enums.ContextAttribute;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.enums.SessionAttribute;
import com.vl.testing.dto.UserDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class CommandUtility {
    private static final Logger LOGGER = LogManager.getLogger(CommandUtility.class);

    private CommandUtility() {
    }

    /**
     * Add User into session.
     * @param request request.
     * @param user UserDTO.
     */
    public static void setUserRole(final HttpServletRequest request, final UserDTO user) {
        final HttpSession session = request.getSession();
        user.setPassword("");
        session.setAttribute(SessionAttribute.USER.getAttribute(), user);
        LOGGER.info("This User set into this session");
    }

    /**
     * Check user if logged, & add new user into logged users.
     * @param request request.
     * @param user UserDTO.
     * @return true if user logged & false if user added.
     */
    public static boolean checkUserIsLogged(final HttpServletRequest request, final UserDTO user) {
        final HashSet<UserDTO> loggedUsers = (HashSet<UserDTO>) request.getSession().getServletContext()
                .getAttribute(ContextAttribute.LOGGED_USERS.getAttribute());

        if (loggedUsers.stream().anyMatch(user::equals)) {
            LOGGER.info("Context contain this User");
            return true;
        }
        user.setPassword("");
        user.setLoginDateTime(LocalDateTime.now());
        loggedUsers.add(user);
        request.getSession().getServletContext()
                .setAttribute(ContextAttribute.LOGGED_USERS.getAttribute(), loggedUsers);
        LOGGER.info("New User added into context");
        return false;
    }

    /**
     * Get user from session & add new name, password,language.
     * @param request request.
     * @param name users name.
     * @param pass users password.
     * @param userLang users language.
     * @return userDTO.
     */
    public static UserDTO getNewSessionUserDTO(final HttpServletRequest request,
                                               final String name, final String pass, final String userLang) {
        final HttpSession session = request.getSession();
        final UserDTO sessionUser = (UserDTO) session.getAttribute(SessionAttribute.USER.getAttribute());
        LOGGER.info("Return new User with updated fields from session User");
        return UserDTO.builder()
                .setId(sessionUser.getId())
                .setUsername(name)
                .setPassword(pass)
                .setLocale(Localization.getLocaleByName(userLang))
                .setAdmin(sessionUser.isAdmin())
                .setBan(sessionUser.isBan())
                .setLoginTime(sessionUser.getLoginTime())
                .build();
    }

    /**
     * Update logged user.
     * @param request request.
     * @param createdUser created user.
     * @return true if user updated.
     */
    public static boolean updateLoggedUser(final HttpServletRequest request, final Optional<UserDTO> createdUser) {
        final HttpSession session = request.getSession();
        final UserDTO sessionUser = (UserDTO) session.getAttribute(SessionAttribute.USER.getAttribute());
        final UserDTO createdUserDTO = createdUser.get();
        createdUserDTO.setPassword("");
        createdUserDTO.setPassedTests(sessionUser.getPassedTests());
        createdUserDTO.setLoginDateTime(sessionUser.getLoginTime());
        final ServletContext context = request.getServletContext();
        final HashSet<UserDTO> loggedUsers = (HashSet<UserDTO>) context.getAttribute(ContextAttribute.LOGGED_USERS.getAttribute());

        if (loggedUsers.stream().anyMatch(sessionUser::equals)) {
            loggedUsers.remove(sessionUser);
            loggedUsers.add(createdUserDTO);
        } else {
            LOGGER.error("Updated User is not replaced current User into context");
            return false;
        }
        request.getSession().getServletContext()
                .setAttribute(ContextAttribute.LOGGED_USERS.getAttribute(), loggedUsers);

        session.setAttribute(SessionAttribute.USER.getAttribute(), createdUserDTO);
        LOGGER.info("Updated User is replaced current User into context");
        return true;
    }

}
