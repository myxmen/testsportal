package com.vl.testing.controller.commands.impl;

import java.util.HashSet;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.ContextAttribute;
import com.vl.testing.dto.UserDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogOutCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(LogOutCommand.class);

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        // ToDo delete current user (context & session)
        final ServletContext context = request.getServletContext();
        final HttpSession session = request.getSession();
        final HashSet<UserDTO> loggedUsers = (HashSet<UserDTO>) context.getAttribute(ContextAttribute.LOGGED_USERS.getAttribute());
        LOGGER.info("LoggedUsers " + loggedUsers);
        final UserDTO user = (UserDTO) session.getAttribute("user");
        LOGGER.info(user);
        if (user != null && loggedUsers.stream().anyMatch(user::equals)) {
            loggedUsers.remove(user);
        }
        context.setAttribute(ContextAttribute.LOGGED_USERS.getAttribute(), loggedUsers);

        final UserDTO guestUser = new UserDTO();
        guestUser.setUsername("Guest");
        CommandUtility.setUserRole(request, guestUser);
        return "redirect:/";
    }
}
