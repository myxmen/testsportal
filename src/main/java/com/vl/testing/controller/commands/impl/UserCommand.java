package com.vl.testing.controller.commands.impl;

import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.PassedTestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserCommand.class);
    private static final int DEFAULT_ROWS = 0;
    private final PassedTestServiceImpl passedTestService;

    public UserCommand() {
        this.passedTestService = new PassedTestServiceImpl();
    }

    public UserCommand(final PassedTestServiceImpl service) {
        this.passedTestService = service;
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        final HttpSession session = request.getSession();
        final UserDTO user = (UserDTO) session.getAttribute("user");

        if (user == null) {
            return "redirect:/logout";
        }

        Integer rows = DEFAULT_ROWS;
        try {
            rows = passedTestService.getNumberOfRows(user);
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setAttributes(getError(e.getMessage()), getExceptionMessage(e.getMessage()), request);
        }

        final Pagination pagination = Pagination.builder()
                .setRows(rows)
                .setCurrentPage(request.getParameter("page"))
                .setRecordsPerPage(request.getParameter("pagination"))
                .setSort(request.getParameter("sort"))
                .setSortColumn(request.getParameter("sortcolumn"))
                .setDefaultColumn(DefaultColumn.TEST.getColumn())
                .build();

        List<PassedTestDTO> page = null;
        try {
            page = passedTestService.getPage(user, pagination);
            LOGGER.info(page);
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            LOGGER.error(getExceptionMessage(e.getMessage()));
            setAttributes(getError(e.getMessage()), getExceptionMessage(e.getMessage()), request);
        }

        request.setAttribute("passtests", page);
        request.setAttribute("page", pagination.getCurrentPage());
        request.setAttribute("pagination", pagination.getRecordsPerPage());
        request.setAttribute("noofpages", pagination.getNOfPages());
        request.setAttribute("sort", pagination.getSort());
        request.setAttribute("sortcolumn", pagination.getSortColumn());
        return "/WEB-INF/user/userbasis.jsp";
    }

    private void setAttributes(final String error, final String errormessage, final HttpServletRequest request) {
        request.setAttribute("error", error);
        request.setAttribute("errormessage", errormessage);
    }
}
