package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminTestDeleteCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminTestDeleteCommand.class);
    private final TestServiceImpl testService;
    private final QuestionServiceImpl questionService;

    public AdminTestDeleteCommand(final TestServiceImpl testService, final QuestionServiceImpl questionService) {
        this.testService = testService;
        this.questionService = questionService;
    }

    public AdminTestDeleteCommand() {
        this.testService = new TestServiceImpl();
        this.questionService = new QuestionServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);
        LOGGER.info("Subject ID = " + request.getParameter("subjectid"));
        LOGGER.info("Test ID = " + request.getParameter("testid"));

        Long testId;
        Long subId;

        try {
            subId = Long.parseLong(request.getParameter("subjectid"));
            testId = Long.parseLong(request.getParameter("testid"));
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }

        if (method.equals("Post")) {
            try {
                testService.delete(testId);
                LOGGER.info("Delete test with id:" + testId);
                return "redirect:/admin/test?subjectid=" + subId;
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
            } catch (ValidateException e) {
                final String message = getExceptionMessage(e.getMessage());
                LOGGER.error(message);
                setRequestErrorAttributes(getError(e.getMessage()), message, request);
            }
        }

        try {
            final TestDTO testDTO = testService.get(testId).get();
            request.setAttribute("testone", testDTO);
            request.setAttribute("questions", questionService.getNumberOfRows(testDTO));
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            setRequestErrorAttributes(Error.ERROR_PAGE.getMessage(), e.getMessage(), request);
        } catch (ValidateException e) {
            final String message = getExceptionMessage(e.getMessage());
            LOGGER.error(message);
            setRequestErrorAttributes(getError(e.getMessage()), message, request);
        }

        request.setAttribute("subjectid", subId);
        request.setAttribute("testid", testId);
        return "/WEB-INF/admin/admintestdelete.jsp";
    }
}
