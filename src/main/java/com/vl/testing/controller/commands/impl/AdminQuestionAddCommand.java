package com.vl.testing.controller.commands.impl;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static com.vl.testing.validation.util.ValidateUtil.getError;
import static com.vl.testing.validation.util.ValidateUtil.getExceptionMessage;

import javax.servlet.http.HttpServletRequest;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminQuestionAddCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AdminQuestionAddCommand.class);
    private final QuestionServiceImpl questionService;

    public AdminQuestionAddCommand(final QuestionServiceImpl questionService) {
        this.questionService = questionService;
    }

    public AdminQuestionAddCommand() {
        this.questionService = new QuestionServiceImpl();
    }

    @Override
    public String execute(final HttpServletRequest request, final String method) {
        LOGGER.info(method);

        LOGGER.info("Test ID = " + request.getParameter("testid"));
        LOGGER.info("Question Number = " + request.getParameter("qnumber"));
        LOGGER.info("Question = " + request.getParameter("question"));
        LOGGER.info("correctanswer1 = " + request.getParameter("correctanswer1"));
        LOGGER.info("answer1 = " + request.getParameter("answer1"));
        LOGGER.info("correctanswer2 = " + request.getParameter("correctanswer2"));
        LOGGER.info("answer2 = " + request.getParameter("answer2"));
        LOGGER.info("correctanswer3 = " + request.getParameter("correctanswer3"));
        LOGGER.info("answer3 = " + request.getParameter("answer3"));
        LOGGER.info("correctanswer4 = " + request.getParameter("correctanswer4"));
        LOGGER.info("answer4 = " + request.getParameter("answer4"));
        LOGGER.info("correctanswer5 = " + request.getParameter("correctanswer5"));
        LOGGER.info("answer5 = " + request.getParameter("answer5"));

        Long testId;

        try {
            testId = Long.parseLong(request.getParameter("testid"));
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            return "redirect:/admin";
        }

        if (method.equals("Post")) {
            final String question = request.getParameter("question").trim();
            Integer numberOfQuestion = 0;
            try {
                numberOfQuestion = Integer.parseInt(request.getParameter("qnumber"));
                final String answer1 = isStringEmpty(request.getParameter("answer1").trim());
                final String answer2 = isStringEmpty(request.getParameter("answer2").trim());
                final String answer3 = isStringEmpty(request.getParameter("answer3").trim());
                final String answer4 = isStringEmpty(request.getParameter("answer4").trim());
                final String answer5 = isStringEmpty(request.getParameter("answer5").trim());
                final String correctAnswer1 = request.getParameter("correctanswer1");
                final String correctAnswer2 = request.getParameter("correctanswer2");
                final String correctAnswer3 = request.getParameter("correctanswer3");
                final String correctAnswer4 = request.getParameter("correctanswer4");
                final String correctAnswer5 = request.getParameter("correctanswer5");
                final QuestionDTO createdQuestion = questionService
                        .create(QuestionDTO.builder()
                                .setTestId(testId)
                                .setNumberOfQuestion(numberOfQuestion)
                                .setQuestion(question)
                                .setAnswer1(answer1)
                                .setAnswer2(answer2)
                                .setAnswer3(answer3)
                                .setAnswer4(answer4)
                                .setAnswer5(answer5)
                                .setCorrectAnswer1(getCorrectAnswer(answer1, correctAnswer1))
                                .setCorrectAnswer2(getCorrectAnswer(answer2, correctAnswer2))
                                .setCorrectAnswer3(getCorrectAnswer(answer3, correctAnswer3))
                                .setCorrectAnswer4(getCorrectAnswer(answer4, correctAnswer4))
                                .setCorrectAnswer5(getCorrectAnswer(answer5, correctAnswer5))
                                .build());

                LOGGER.info("Create question with id:" + createdQuestion.getId());
                return "redirect:/admin/question?testid=" + testId;
            } catch (RepositoryException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_QUESTION.getMessage(), String.valueOf(numberOfQuestion), request);
            } catch (NumberFormatException e) {
                LOGGER.error(e.getMessage());
                setRequestErrorAttributes(Error.ERROR_QUESTION.getMessage(), "validation", question, request);
            } catch (ValidateException e) {
                LOGGER.error(getExceptionMessage(e.getMessage()));
                setRequestErrorAttributes(getError(e.getMessage()), "validation", question, request);
            }
        }

        request.setAttribute("testid", testId);
        return "/WEB-INF/admin/adminquestionadd.jsp";
    }

    private String isStringEmpty(final String str) {
        if (str == null || str.replaceAll("\\s", "").equals("")) {
            return null;
        }
        return str;
    }

    private Boolean getCorrectAnswer(final String answer, final String correctAnswer) {
        if (answer == null) {
            return null;
        }
        if (correctAnswer == null) {
            return false;
        }
        return true;
    }
}
