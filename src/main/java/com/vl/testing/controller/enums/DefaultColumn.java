package com.vl.testing.controller.enums;

public enum DefaultColumn {
    USER("user"),
    SUBJECT("subject"),
    TEST("test"),
    NUMQUESTION("numquestion");

    private final String column;

    DefaultColumn(final String column) {
        this.column = column;
    }

    public String getColumn() {
        return column;
    }
}
