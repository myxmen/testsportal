package com.vl.testing.controller.enums;

public enum Error {

    BANNED_USER("banned_user"),
    LOGGED_USER("logged_user"),
    ERROR_DATABASE("error_database"),
    INVALID_LOGIN("invalid_login"),
    INVALID_PASSWORD("invalid_password"),
    ERROR_UPDATE("error_update"),
    ERROR_ANSWER("error_answer"),
    ERROR_PAGE("error_page"),
    ERROR_SUBJECT("invalid_subject"),
    ERROR_TEST("invalid_test"),
    ERROR_QUESTION("invalid_question"),
    ERROR_ANSWER1("invalid_answer1"),
    ERROR_ANSWER2("invalid_answer2");

    private final String message;

    Error(final String message) {
        this.message = message;
    }

    /**
     * simple exception message.
     *
     * @return String of the message
     */
    public String getMessage() {
        return message;
    }
}
