package com.vl.testing.controller.enums;

import java.util.Locale;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum Localization {

    EN(new Locale("en", "US")),
    RU(new Locale("ru", "RU"));

    private final Locale locale;

    Localization(final Locale localeArg) {
        this.locale = Objects.requireNonNull(localeArg);
    }

    public Locale getLocale() {
        return locale;
    }

    private static final Logger LOGGER = LogManager.getLogger(Localization.class);

    /**
     * Get Locale by name locale.
     * @param name name of locale.
     * @return locale.
     */
    public static Locale getLocaleByName(final String name) {
        final String str = name.trim().toUpperCase();
        LOGGER.info(str);
        for (final Localization env : Localization.values()) {
            LOGGER.info("value " + Localization.valueOf(str));
            LOGGER.info(env);
            if (env.name().equals(str)) {
                LOGGER.info("return " + env.locale);
                return env.getLocale();
            }
        }
        LOGGER.info("return dafault");
        return EN.getLocale();
    }

    /**
     * Get name of locale.
     * @param local locale.
     * @return name of locale.
     */
    public static String getNameByLocale(final Locale local) {
        LOGGER.info(local);
        for (final Localization env : Localization.values()) {
            LOGGER.info(env);
            if (env.getLocale().equals(local)) {
                LOGGER.info("return " + env.locale);
                return env.name();
            }
        }
        LOGGER.info("return default");
        return EN.name();
    }

    @Override
    public String toString() {
        return "Localization{"
                + "locale=" + locale
                + '}';
    }
}
