package com.vl.testing.controller.enums;

public enum ContextAttribute {
    LOGGED_USERS("loggedUsers");

    private final String attribute;

    ContextAttribute(final String loggedUsers) {
        this.attribute = loggedUsers;
    }

    public String getAttribute() {
        return attribute;
    }
}
