package com.vl.testing.controller.enums;

public enum SessionAttribute {
    USER("user"),
    ERROR_PAGE("errorpage"),
    ERROR_MESSAGE("errormessage"),
    ERROR_DTO("errordto"),
    //  ERROR("error"),
    LANGUAGE("language"),
    NULL("");

    private final String attribute;

    SessionAttribute(final String attribute) {
        this.attribute = attribute;
    }

    public String getAttribute() {
        return attribute;
    }
}
