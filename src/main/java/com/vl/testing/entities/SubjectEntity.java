package com.vl.testing.entities;

import java.util.Objects;

public class SubjectEntity {
    private Long idSubject;

    private String subject;

    public SubjectEntity() {
    }

    public SubjectEntity(final Long idSubject, final String subject) {
        this.idSubject = idSubject;
        this.subject = subject;
    }

    public Long getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(final Long idSubject) {
        this.idSubject = idSubject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final SubjectEntity subject1 = (SubjectEntity) object;
        return Objects.equals(idSubject, subject1.idSubject)
                && Objects.equals(subject, subject1.subject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSubject, subject);
    }

    @Override
    public String toString() {
        return "Subject{"
                + "id=" + idSubject
                + ", subject='" + subject + '\''
                + '}';
    }

    /**
     * init Builder.
     *
     * @return Entity builder.
     */
    public static SubjectEntityBuilder builder() {
        return new SubjectEntityBuilder();
    }

    public static class SubjectEntityBuilder {
        private Long idSubject;

        private String subject;

        // CHECKSTYLE_OFF: JavadocMethod
        public SubjectEntityBuilder setIdSubject(final Long idSubject) {
            this.idSubject = idSubject;
            return this;
        }

        public SubjectEntityBuilder setSubject(final String subject) {
            this.subject = subject;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built Entity.
         */
        public SubjectEntity build() {
            return new SubjectEntity(idSubject, subject);
        }
    }
}
