package com.vl.testing.entities;

import java.util.Objects;

public class PassedTestEntity {
    private Long idPassedTest;
    private Long userId;

    private Long testId;
    private Integer result;

    public PassedTestEntity() {
    }

    public PassedTestEntity(final Long idPassedTest, final Long userId, final Long testId, final Integer result) {
        this.idPassedTest = idPassedTest;
        this.userId = userId;
        this.testId = testId;
        this.result = result;
    }

    public Long getIdPassedTest() {
        return idPassedTest;
    }

    public void setIdPassedTest(final Long idPassedTest) {
        this.idPassedTest = idPassedTest;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(final Long userId) {
        this.userId = userId;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(final Long testId) {
        this.testId = testId;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(final Integer result) {
        this.result = result;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final PassedTestEntity that = (PassedTestEntity) object;
        return Objects.equals(idPassedTest, that.idPassedTest)
                && Objects.equals(userId, that.userId)
                && Objects.equals(testId, that.testId)
                && Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPassedTest, userId, testId, result);
    }

    @Override
    public String toString() {
        return "PassedTestEntity{"
                + "id=" + idPassedTest
                + ", userId=" + userId
                + ", testId=" + testId
                + ", result=" + result
                + '}';
    }

    /**
     * init Builder.
     *
     * @return Entity builder.
     */
    public static PassedTestEntityBuilder builder() {
        return new PassedTestEntityBuilder();
    }

    public static class PassedTestEntityBuilder {
        private Long idPassedTest;
        private Long userId;

        private Long testId;
        private Integer result;

        // CHECKSTYLE_OFF: JavadocMethod
        public PassedTestEntityBuilder setIdPassedTest(final Long idPassedTest) {
            this.idPassedTest = idPassedTest;
            return this;
        }

        public PassedTestEntityBuilder setUserId(final Long userId) {
            this.userId = userId;
            return this;
        }

        public PassedTestEntityBuilder setTestId(final Long testId) {
            this.testId = testId;
            return this;
        }

        public PassedTestEntityBuilder setResult(final Integer result) {
            this.result = result;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built Entity.
         */
        public PassedTestEntity build() {
            return new PassedTestEntity(idPassedTest, userId, testId, result);
        }
    }
}
