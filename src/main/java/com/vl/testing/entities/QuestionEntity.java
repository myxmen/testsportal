package com.vl.testing.entities;

import java.util.Objects;

@SuppressWarnings("PMD.ExcessivePublicCount")
public class QuestionEntity {
    private Long idQuestion;
    private Long testId;

    private Integer numberOfQuestion;
    private String question;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String answer5;
    private Boolean correctAnswer1;
    private Boolean correctAnswer2;
    private Boolean correctAnswer3;
    private Boolean correctAnswer4;
    private Boolean correctAnswer5;

    public QuestionEntity() {
    }

    // CHECKSTYLE_OFF: ParameterNumber
    @SuppressWarnings("PMD.ExcessiveParameterList")
    public QuestionEntity(final Long idQuestion, final Long testId, final Integer numberOfQuestion,
                          final String question, final String answer1, final String answer2,
                          final String answer3, final String answer4, final String answer5,
                          final Boolean correctAnswer1, final Boolean correctAnswer2, final Boolean correctAnswer3,
                          final Boolean correctAnswer4, final Boolean correctAnswer5) {
        this.idQuestion = idQuestion;
        this.testId = testId;
        this.numberOfQuestion = numberOfQuestion;
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.answer5 = answer5;
        this.correctAnswer1 = correctAnswer1;
        this.correctAnswer2 = correctAnswer2;
        this.correctAnswer3 = correctAnswer3;
        this.correctAnswer4 = correctAnswer4;
        this.correctAnswer5 = correctAnswer5;
    }
    // CHECKSTYLE_ON: ParameterNumber

    public Long getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(final Long idQuestion) {
        this.idQuestion = idQuestion;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(final Long testId) {
        this.testId = testId;
    }

    public Integer getNumberOfQuestion() {
        return numberOfQuestion;
    }

    public void setNumberOfQuestion(final Integer numberOfQuestion) {
        this.numberOfQuestion = numberOfQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(final String question) {
        this.question = question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(final String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(final String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(final String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(final String answer4) {
        this.answer4 = answer4;
    }

    public String getAnswer5() {
        return answer5;
    }

    public void setAnswer5(final String answer5) {
        this.answer5 = answer5;
    }

    public Boolean isCorrectAnswer1() {
        return correctAnswer1;
    }

    public void setCorrectAnswer1(final Boolean correctAnswer1) {
        this.correctAnswer1 = correctAnswer1;
    }

    public Boolean isCorrectAnswer2() {
        return correctAnswer2;
    }

    public void setCorrectAnswer2(final Boolean correctAnswer2) {
        this.correctAnswer2 = correctAnswer2;
    }

    public Boolean isCorrectAnswer3() {
        return correctAnswer3;
    }

    public void setCorrectAnswer3(final Boolean correctAnswer3) {
        this.correctAnswer3 = correctAnswer3;
    }

    public Boolean isCorrectAnswer4() {
        return correctAnswer4;
    }

    public void setCorrectAnswer4(final Boolean correctAnswer4) {
        this.correctAnswer4 = correctAnswer4;
    }

    public Boolean isCorrectAnswer5() {
        return correctAnswer5;
    }

    public void setCorrectAnswer5(final Boolean correctAnswer5) {
        this.correctAnswer5 = correctAnswer5;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final QuestionEntity that = (QuestionEntity) object;
        return correctAnswer1 == that.correctAnswer1 && correctAnswer2 == that.correctAnswer2 && correctAnswer3
                == that.correctAnswer3 && correctAnswer4 == that.correctAnswer4 && correctAnswer5
                == that.correctAnswer5 && Objects.equals(idQuestion, that.idQuestion)
                && Objects.equals(testId, that.testId) && Objects.equals(numberOfQuestion, that.numberOfQuestion)
                && Objects.equals(question, that.question) && Objects.equals(answer1, that.answer1)
                && Objects.equals(answer2, that.answer2) && Objects.equals(answer3, that.answer3)
                && Objects.equals(answer4, that.answer4) && Objects.equals(answer5, that.answer5);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idQuestion, testId, numberOfQuestion, question,
                answer1, answer2, answer3, answer4, answer5,
                correctAnswer1, correctAnswer2, correctAnswer3, correctAnswer4, correctAnswer5);
    }

    @Override
    public String toString() {
        return "QuestionEntity{"
                + "id=" + idQuestion
                + ", testId=" + testId
                + ", numberOfQuestions=" + numberOfQuestion
                + ", question='" + question + '\''
                + ", answer1='" + answer1 + '\''
                + ", answer2='" + answer2 + '\''
                + ", answer3='" + answer3 + '\''
                + ", answer4='" + answer4 + '\''
                + ", answer5='" + answer5 + '\''
                + ", correctAnswer1=" + correctAnswer1
                + ", correctAnswer2=" + correctAnswer2
                + ", correctAnswer3=" + correctAnswer3
                + ", correctAnswer4=" + correctAnswer4
                + ", correctAnswer5=" + correctAnswer5
                + '}';
    }

    /**
     * init Builder.
     *
     * @return Entity builder.
     */
    public static QuestionEntityBuilder builder() {
        return new QuestionEntityBuilder();
    }

    public static class QuestionEntityBuilder {
        private Long idQuestion;
        private Long testId;

        private Integer numberOfQuestion;
        private String question;
        private String answer1;
        private String answer2;
        private String answer3;
        private String answer4;
        private String answer5;
        private Boolean correctAnswer1;
        private Boolean correctAnswer2;
        private Boolean correctAnswer3;
        private Boolean correctAnswer4;
        private Boolean correctAnswer5;

        // CHECKSTYLE_OFF: JavadocMethod
        public QuestionEntityBuilder setIdQuestion(final Long idQuestion) {
            this.idQuestion = idQuestion;
            return this;
        }

        public QuestionEntityBuilder setTestId(final Long testId) {
            this.testId = testId;
            return this;
        }

        public QuestionEntityBuilder setNumberOfQuestion(final Integer numberOfQuestion) {
            this.numberOfQuestion = numberOfQuestion;
            return this;
        }

        public QuestionEntityBuilder setQuestion(final String question) {
            this.question = question;
            return this;
        }

        public QuestionEntityBuilder setAnswer1(final String answer1) {
            this.answer1 = answer1;
            return this;
        }

        public QuestionEntityBuilder setAnswer2(final String answer2) {
            this.answer2 = answer2;
            return this;
        }

        public QuestionEntityBuilder setAnswer3(final String answer3) {
            this.answer3 = answer3;
            return this;
        }

        public QuestionEntityBuilder setAnswer4(final String answer4) {
            this.answer4 = answer4;
            return this;
        }

        public QuestionEntityBuilder setAnswer5(final String answer5) {
            this.answer5 = answer5;
            return this;
        }

        public QuestionEntityBuilder setCorrectAnswer1(final Boolean correctAnswer1) {
            this.correctAnswer1 = correctAnswer1;
            return this;
        }

        public QuestionEntityBuilder setCorrectAnswer2(final Boolean correctAnswer2) {
            this.correctAnswer2 = correctAnswer2;
            return this;
        }

        public QuestionEntityBuilder setCorrectAnswer3(final Boolean correctAnswer3) {
            this.correctAnswer3 = correctAnswer3;
            return this;
        }

        public QuestionEntityBuilder setCorrectAnswer4(final Boolean correctAnswer4) {
            this.correctAnswer4 = correctAnswer4;
            return this;
        }

        public QuestionEntityBuilder setCorrectAnswer5(final Boolean correctAnswer5) {
            this.correctAnswer5 = correctAnswer5;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built Entity.
         */
        public QuestionEntity build() {
            return new QuestionEntity(idQuestion, testId, numberOfQuestion, question, answer1, answer2, answer3, answer4, answer5,
                    correctAnswer1, correctAnswer2, correctAnswer3, correctAnswer4, correctAnswer5);
        }
    }
}
