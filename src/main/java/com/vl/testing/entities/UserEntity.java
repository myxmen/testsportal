package com.vl.testing.entities;

import java.util.Objects;

public class UserEntity {
    private Long idUser;

    private String username;
    private String password;
    private Boolean admin;
    private Boolean ban;
    private String locale;

    public UserEntity() {
    }

    public UserEntity(final Long idUser, final String username, final String password,
                      final Boolean admin, final Boolean ban, final String locale) {
        this.idUser = idUser;
        this.username = username;
        this.password = password;
        this.admin = admin;
        this.ban = ban;
        this.locale = locale;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(final Long idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Boolean isAdmin() {
        return admin;
    }

    public void setAdmin(final Boolean admin) {
        this.admin = admin;
    }

    public void setBan(final Boolean ban) {
        this.ban = ban;
    }

    public Boolean isBan() {
        return ban;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(final String locale) {
        this.locale = locale;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final UserEntity that = (UserEntity) object;
        return admin == that.admin && ban == that.ban && Objects.equals(idUser, that.idUser)
                && Objects.equals(username, that.username) && Objects.equals(password, that.password)
                && Objects.equals(locale, that.locale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUser, username, password, admin, ban, locale);
    }

    @Override
    public String toString() {
        return "UserEntity{"
                + "id=" + idUser
                + ", username='" + username + '\''
                + ", password='" + password + '\''
                + ", admin=" + admin
                + ", ban=" + ban
                + ", locale='" + locale + '\''
                + '}';
    }

    /**
     * init Builder.
     *
     * @return Entity builder.
     */
    public static UserEntityBuilder builder() {
        return new UserEntityBuilder();
    }

    public static class UserEntityBuilder {
        private Long idUser;
        private String username;
        private String password;
        private Boolean admin;
        private Boolean ban;
        private String locale;

        // CHECKSTYLE_OFF: JavadocMethod
        public UserEntityBuilder setIdUser(final Long idUser) {
            this.idUser = idUser;
            return this;
        }

        public UserEntityBuilder setUsername(final String username) {
            this.username = username;
            return this;
        }

        public UserEntityBuilder setPassword(final String password) {
            this.password = password;
            return this;
        }

        public UserEntityBuilder setAdmin(final Boolean admin) {
            this.admin = admin;
            return this;
        }

        public UserEntityBuilder setBan(final Boolean ban) {
            this.ban = ban;
            return this;
        }

        public UserEntityBuilder setLocale(final String locale) {
            this.locale = locale;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built Entity.
         */
        public UserEntity build() {
            return new UserEntity(idUser, username, password, admin, ban, locale);
        }
    }
}
