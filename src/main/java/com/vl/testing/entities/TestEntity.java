package com.vl.testing.entities;

import java.util.Objects;

public class TestEntity {
    private Long idTest;
    private Long subjectId;

    private String test;
    private Integer difficulty;
    private Long requests;
    private Long time;

    public TestEntity() {
    }

    public TestEntity(final Long idTest, final Long subjectId, final String test,
                      final Integer difficulty, final Long requests, final Long time) {
        this.idTest = idTest;
        this.subjectId = subjectId;
        this.test = test;
        this.difficulty = difficulty;
        this.requests = requests;
        this.time = time;
    }

    public Long getIdTest() {
        return idTest;
    }

    public void setIdTest(final Long idTest) {
        this.idTest = idTest;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(final Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getTest() {
        return test;
    }

    public void setTest(final String test) {
        this.test = test;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(final Integer difficulty) {
        this.difficulty = difficulty;
    }

    public Long getRequests() {
        return requests;
    }

    public void setRequests(final Long requests) {
        this.requests = requests;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(final Long time) {
        this.time = time;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final TestEntity that = (TestEntity) object;
        return Objects.equals(idTest, that.idTest)
                && Objects.equals(subjectId, that.subjectId)
                && Objects.equals(test, that.test)
                && Objects.equals(difficulty, that.difficulty)
                && Objects.equals(requests, that.requests)
                && Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTest, subjectId, test, difficulty, requests, time);
    }

    @Override
    public String toString() {
        return "TestEntity{"
                + "id=" + idTest
                + ", idSubject=" + subjectId
                + ", test='" + test + '\''
                + ", difficulty=" + difficulty
                + ", requests=" + requests
                + ", time=" + time
                + '}';
    }

    /**
     * init Builder.
     *
     * @return Entity builder.
     */
    public static TestEntityBuilder builder() {
        return new TestEntityBuilder();
    }

    public static class TestEntityBuilder {
        private Long idTest;
        private Long subjectId;

        private String test;
        private Integer difficulty;
        private Long requests;
        private Long time;

        // CHECKSTYLE_OFF: JavadocMethod
        public TestEntityBuilder setIdTest(final Long idTest) {
            this.idTest = idTest;
            return this;
        }

        public TestEntityBuilder setSubjectId(final Long subjectId) {
            this.subjectId = subjectId;
            return this;
        }

        public TestEntityBuilder setTest(final String test) {
            this.test = test;
            return this;
        }

        public TestEntityBuilder setDifficulty(final Integer difficulty) {
            this.difficulty = difficulty;
            return this;
        }

        public TestEntityBuilder setRequests(final Long requests) {
            this.requests = requests;
            return this;
        }

        public TestEntityBuilder setTime(final Long time) {
            this.time = time;
            return this;
        }
        // CHECKSTYLE_ON: JavadocMethod

        /**
         * finishing Builder.
         *
         * @return built Entity.
         */
        public TestEntity build() {
            return new TestEntity(idTest, subjectId, test, difficulty, requests, time);
        }
    }
}
