--subjects
-- INSERT INTO subjects VALUES (DEFAULT, 'subject1');
-- INSERT INTO subjects VALUES (DEFAULT, 'subject2');
-- INSERT INTO subjects VALUES (DEFAULT, 'subject3');
-- INSERT INTO subjects VALUES (DEFAULT, 'subject4');
-- INSERT INTO subjects VALUES (DEFAULT, 'subject5');

--users
--INSERT INTO users VALUES (DEFAULT, 'ivanov1','4321',true,true);
--INSERT INTO users VALUES (DEFAULT, 'ivanov2','1234',false,true);
--INSERT INTO users VALUES (DEFAULT, 'ivanov3','4321',false,false);
--INSERT INTO users VALUES (DEFAULT, 'ivanov4','4321',false,false);
--INSERT INTO users VALUES (DEFAULT, 'ivanov5','4321',false,false);

--tests
--INSERT INTO tests VALUES (DEFAULT, 1,'test1',1,10,100);
--INSERT INTO tests VALUES (DEFAULT, 2,'test2',1,10,100);
--INSERT INTO tests VALUES (DEFAULT, 3,'test3',1,100,1000);
--INSERT INTO tests VALUES (DEFAULT, 5,'test4',1,10,100);
--INSERT INTO tests VALUES (DEFAULT, 4,'test5',1,10,100);

--questions
--INSERT INTO questions VALUES (DEFAULT, 1, 1,'question1','answer1','answer2','answer3','answer4','answer5',false,true,true,true,false);
--INSERT INTO questions VALUES (DEFAULT, 4, 1,'question2','answer2','answer1',null,null,null,false,true,false,false,false);
--INSERT INTO questions VALUES (DEFAULT, 5, 1,'question1','answer1','answer2',null,null,null,true,false,false,false,false);
--INSERT INTO questions VALUES (DEFAULT, 2, 1,'question1','answer1','answer2','answer3','answer4','answer5',false,true,true,true,false);
--INSERT INTO questions VALUES (DEFAULT, 3, 1,'question1','answer1','answer2','answer3','answer4','answer5',false,true,false,true,false);

--passed_tests
--INSERT INTO passed_tests VALUES (DEFAULT, 2,1,100);
--INSERT INTO passed_tests VALUES (DEFAULT, 2,2,10);
--INSERT INTO passed_tests VALUES (DEFAULT, 3,2,100);
--INSERT INTO passed_tests VALUES (DEFAULT, 4,3,10);
--INSERT INTO passed_tests VALUES (DEFAULT, 5,5,100);

--get 100 users sorted by increase
--SELECT * FROM users ORDER BY id ASC LIMIT 100

