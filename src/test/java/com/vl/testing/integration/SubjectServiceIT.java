package com.vl.testing.integration;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.daofactory.ConnectionPoolHolder;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SubjectServiceIT {
    private final DataSource dataSource = ConnectionPoolHolder.getDataSource();
    private static Connection connection;
    private static final String CREATE_TABLES = "src/test/resources/create_table.sql";
    private static final String DROP_TABLES = "src/test/resources/drop_table.sql";
    private static final String INSERT_ENTITIES_INTO_TABLES = "src/test/resources/insert_into_table.sql";
    private static String createSQL;
    private static String dropSQL;
    private static String insertSQL;
    private static int count = 1;
    private static int subjectsInTable;
    private static SubjectServiceImpl service;

    @BeforeAll
    static void globalSetUp() throws IOException {
        createSQL = Files.readString(Path.of(CREATE_TABLES));
        System.out.println(createSQL);
        dropSQL = Files.readString(Path.of(DROP_TABLES));
        System.out.println(dropSQL);
        insertSQL = Files.readString(Path.of(INSERT_ENTITIES_INTO_TABLES));
        System.out.println(insertSQL);
        System.out.println("-- Test Subject --");
    }

    @BeforeEach
    void init() throws SQLException, RepositoryException {
        connection = dataSource.getConnection();
        connection.createStatement().executeUpdate(dropSQL);
        System.out.println("Drop");
        connection.createStatement().executeUpdate(createSQL);
        System.out.println("Create");
        connection.createStatement().executeUpdate(insertSQL);
        System.out.println("Insert");
        System.out.println("-- Test Subject -- " + count++);
        service = new SubjectServiceImpl();
        subjectsInTable = service.getAll().size();
    }

    @AfterEach
    void tearDown() throws FileNotFoundException, SQLException {
        connection.close();
        //   dropTable(connection);
    }

    @DisplayName("Create")
    @Test
    void addingSubjectShouldIncreaseListAndNotAffectExistingSubject() throws RepositoryException, ValidateException {
        SubjectDTO subjectDTO = createTestSubjectDto();
        // SubjectServiceImpl service = new SubjectServiceImpl();

        SubjectDTO created = service.create(subjectDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        subjectDTO.setId(created.getId());
        assertEquals(subjectDTO, created);
        List<SubjectDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(subjectsInTable + 1, all.size());
    }

    @DisplayName("Update")
    @Test
    void whenUpdatedSubjectShouldNotIncreaseDecreaseListAndAffectOnUpdatedSubject() throws RepositoryException, ValidateException {
        //  SubjectServiceImpl service = new SubjectServiceImpl();
        SubjectDTO created = service.create(createTestSubjectDto());
        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());

        SubjectDTO newSubjectDTO = SubjectDTO.builder()
                .setId(created.getId())
                .setSubject("anotherTestSubject")
                .build();

        SubjectDTO updated = service.update(newSubjectDTO).get();
        assertNotEquals(updated,created);
        assertEquals(newSubjectDTO, updated);
        assertEquals(updated, service.get(updated.getId()).get());
        List<SubjectDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(subjectsInTable + 1, all.size());
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedUserShouldDecreaseListAndNotAffectExistingUsersList() throws RepositoryException, ValidateException {
        SubjectDTO subjectDTO = createTestSubjectDto();
        //  SubjectServiceImpl service = new SubjectServiceImpl();
        List<SubjectDTO> subjectDTOList = service.getAll();
        SubjectDTO created = service.create(subjectDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        subjectDTO.setId(created.getId());
        assertEquals(subjectDTO, created);
        assertEquals(service.getAll().size(), subjectDTOList.size() + 1);
        System.out.println(created.getId());
        assertTrue(service.delete(created.getId()));
        List<SubjectDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertTrue(all.equals(subjectDTOList));

        assertTrue(service.delete(1l));
        List<SubjectDTO> allNext = service.getAll();
        allNext.forEach(x -> System.out.println(x));
        assertEquals(allNext.size(), all.size() - 1);
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllSubjectsReturnListOfExistingSubjects() throws RepositoryException {
        //  SubjectServiceImpl service = new SubjectServiceImpl();
        // UserServiceImpl serviceUser = new UserServiceImpl();
//        userDTO.setId(2l);
//        userDTO.setUsername("Smirnov");
//        userDTO.setPassword("dfgfghnghm");
//        userDTO.setRole(false);
//        userDTO.setBan(false);
        //  UserDTO userDTO = serviceUser.get(2l);
        System.out.println("-- Test --");
        List<SubjectDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), subjectsInTable);
    }

    @DisplayName("Get")
    @Test
    void whenGetSubjectsEqualsOneOfGetAllSubjects() throws RepositoryException, ValidateException {
        // SubjectDTO subjectDTO = createTestSubjectDto();
        //  SubjectServiceImpl service = new SubjectServiceImpl();

//        SubjectDTO created = service.create(subjectDTO);
//
//        assertNotNull(created);
//        System.out.println(created);
//        assertNotNull(created.getId());
//        subjectDTO.setId(created.getId());
//        assertEquals(subjectDTO, created);
//
//        SubjectDTO selected = service.get(created.getId()).get();
//        assertEquals(selected, subjectDTO);

        List<SubjectDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        //------------ delete \/
        SubjectDTO created = all.get(1);
        assertNotNull(created);
        assertNotNull(created.getId());
        SubjectDTO selected = service.get(created.getId()).get();
        assertEquals(selected, created);
        //------------- delete /\
        assertEquals(all.size(), subjectsInTable);
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageSubjectsReturnListOfExistingSubjects() throws RepositoryException, ValidateException {
        //  SubjectServiceImpl service = new SubjectServiceImpl();
        Pagination pagination = Pagination.builder()
                .setRows(service.getNumberOfRows())
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("subject")
                .setDefaultColumn("subject")
                .build();
        List<SubjectDTO> page = service.getPage(pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 5);

        pagination = Pagination.builder()
                .setRows(service.getNumberOfRows())
                .setCurrentPage("1")
                .setRecordsPerPage("10")
                .setSort(Sort.DESC.getSort())
                .setSortColumn("subject")
                .setDefaultColumn("subject")
                .build();
        page = service.getPage(pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 10);
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsSubjectsReturnNumberOfRowsOfExistingSubjects() throws RepositoryException {
        //   SubjectServiceImpl service = new SubjectServiceImpl();
        Integer rows = service.getNumberOfRows();
        System.out.println("rows" + rows);
        List<SubjectDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), rows);
    }

    private SubjectDTO createTestSubjectDto() {
        return SubjectDTO.builder()
                .setSubject("testSubject")
                .build();
    }

}
