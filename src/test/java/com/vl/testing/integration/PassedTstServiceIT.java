package com.vl.testing.integration;

import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.daofactory.ConnectionPoolHolder;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.PassedTestServiceImpl;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PassedTstServiceIT {
    private final DataSource dataSource = ConnectionPoolHolder.getDataSource();
    private static Connection connection;
    private static final String CREATE_TABLES = "src/test/resources/create_table.sql";
    private static final String DROP_TABLES = "src/test/resources/drop_table.sql";
    private static final String INSERT_ENTITIES_INTO_TABLES = "src/test/resources/insert_into_table.sql";
    private static String createSQL;
    private static String dropSQL;
    private static String insertSQL;
    private static int count = 1;
    private static PassedTestServiceImpl service;
    private static UserServiceImpl serviceUser;
    private static UserDTO userDTO;

    @BeforeAll
    static void globalSetUp() throws IOException {
        createSQL = Files.readString(Path.of(CREATE_TABLES));
        System.out.println(createSQL);
        dropSQL = Files.readString(Path.of(DROP_TABLES));
        System.out.println(dropSQL);
        insertSQL = Files.readString(Path.of(INSERT_ENTITIES_INTO_TABLES));
        System.out.println(insertSQL);
        System.out.println("-- Test PassedTest --");
    }

    @BeforeEach
    void init() throws SQLException, RepositoryException, ValidateException {
        connection = dataSource.getConnection();
        connection.createStatement().executeUpdate(dropSQL);
        System.out.println("Drop");
        connection.createStatement().executeUpdate(createSQL);
        System.out.println("Create");
        connection.createStatement().executeUpdate(insertSQL);
        System.out.println("Insert");
        System.out.println("-- Test PassedTest -- " + count++);
        service = new PassedTestServiceImpl();
        serviceUser = new UserServiceImpl();
        userDTO = serviceUser.get(2l).get();
    }

    @AfterEach
    void tearDown() throws FileNotFoundException, SQLException {
        connection.close();
        //   dropTable(connection);
    }

    @Test
    void addingPassedTestShouldIncreaseListAndNotAffectExistingPassedTest() throws RepositoryException, ValidateException {
//        UserDTO newUserDTO = new UserDTO();
//       // UserServiceImpl serviceUser = new UserServiceImpl();
//        newUserDTO.setUsername("Smirnov");
//        newUserDTO.setPassword("dfgfghnghm");
//        // userDTO.setRole(false);
//        newUserDTO.setAdmin(false);
//        newUserDTO.setBan(false);
//        newUserDTO.setLocale(Localization.EN.getLocale());
        UserDTO newUserDTO=UserDTO.builder()
                 .setUsername("Smirnov")
                 .setPassword("dfgfghnghm")
                 .setAdmin(false)
                 .setBan(false)
                 .setLocale(Localization.EN.getLocale())
                 .build();

        UserDTO createdUser = serviceUser.create(newUserDTO);

        assertNotNull(createdUser);
        System.out.println(createdUser);
        assertNotNull(createdUser.getId());
        newUserDTO.setId(createdUser.getId());
        assertEquals(newUserDTO, createdUser);
        TestDTO testDTO = new TestDTO();
//        testDTO.setId(1l);
//        testDTO.setTest("TestTest");
//        testDTO.setDifficulty(90);
//        testDTO.setRequests(10l);
//        testDTO.setTime(10000l);
        //PassedTestServiceImpl service = new PassedTestServiceImpl();
        List<PassedTestDTO> all = service.getAllPassedTestFosUser(userDTO);
        all.forEach(x -> System.out.println(x));
        PassedTestDTO passedTestDTO = all.get(0);
        passedTestDTO.setResult(30);
        passedTestDTO.setId(null);
        System.out.println(passedTestDTO);

        PassedTestDTO create = service.create(passedTestDTO, newUserDTO);
        assertNotNull(create);
        System.out.println(create);
        assertNotNull(create.getId());
        passedTestDTO.setId(create.getId());
        assertEquals(passedTestDTO, create);
        //passedTestDTO.setTest(testDTO);
        //passedTestDTO.setResult(100);
        // PassedTestDTO create = servicePassedTest.create(passedTestDTO);

        List<PassedTestDTO> result = service.getAllPassedTestFosUser(newUserDTO);
        result.forEach(x -> System.out.println(x));
        assertEquals(result.size(), 1);
    }

    @Test
    void whenUpdatedPassedTestShouldNotIncreaseDecreaseListAndAffectOnUpdatedPassedTest() throws RepositoryException, ValidateException {
      //  PassedTestDTO passedTestDTO = new PassedTestDTO();
      //  PassedTestServiceImpl service = new PassedTestServiceImpl();
       // UserServiceImpl serviceUser = new UserServiceImpl();
//        userDTO.setUsername("Smirnov");
//        userDTO.setPassword("dfgfghnghm");
//        userDTO.setRole(false);
//        userDTO.setBan(false);
      //  UserDTO userDTO = serviceUser.get(2l).get();
        assertNotNull(userDTO);
        System.out.println(userDTO);
        List<PassedTestDTO> all = service.getAllPassedTestFosUser(userDTO);
        all.forEach(x -> System.out.println(x));
        PassedTestDTO passedTestDTO = all.get(0);
        passedTestDTO.setResult(50);
        PassedTestDTO update = service.update(passedTestDTO).get();
        assertEquals(update.getId(), passedTestDTO.getId());
        assertEquals(update.getResult(), passedTestDTO.getResult());

        List<PassedTestDTO> result = service.getAllPassedTestFosUser(userDTO);
        result.forEach(x -> System.out.println(x));
        assertEquals(result.size(), 6);
    }

    @Test
    void whenGetAllPassedTestReturnListOfExistingPassedTests() throws RepositoryException, ValidateException {
     //   PassedTestServiceImpl service = new PassedTestServiceImpl();
     //   UserServiceImpl serviceUser = new UserServiceImpl();
//        userDTO.setId(2l);
//        userDTO.setUsername("Smirnov");
//        userDTO.setPassword("dfgfghnghm");
//        userDTO.setRole(false);
//        userDTO.setBan(false);
      //  UserDTO userDTO = serviceUser.get(2l).get();
        System.out.println("-- Test --");
        List<PassedTestDTO> all = service.getAllPassedTestFosUser(userDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), 6);
    }

    @Test
    void whenGetAllPassedTestReturnListsEmpty() throws RepositoryException, ValidateException {
     //   PassedTestServiceImpl service = new PassedTestServiceImpl();
     //   UserServiceImpl serviceUser = new UserServiceImpl();
//        userDTO.setId(2l);
//        userDTO.setUsername("Smirnov");
//        userDTO.setPassword("dfgfghnghm");
//        userDTO.setRole(false);
//        userDTO.setBan(false);
        UserDTO anoterUserDTO = serviceUser.get(1l).get();
        System.out.println("-- Test --");
        List<PassedTestDTO> all = service.getAllPassedTestFosUser(anoterUserDTO);
        System.out.println(all.size());
        System.out.println(all.isEmpty());
        System.out.println(all);
        all.forEach(x -> System.out.println(x));
        assertTrue(all.isEmpty());
        assertEquals(all.size(), 0);
    }

    @Test
    void whenGetPageReturnListOfExistingPassedTest() throws RepositoryException, ValidateException {
      //  PassedTestServiceImpl service = new PassedTestServiceImpl();
      //  UserServiceImpl serviceUser = new UserServiceImpl();
       // UserDTO userDTO = serviceUser.get(2l).get();
//        userDTO.setId(2l);
//        userDTO.setUsername("Smirnov");
//        userDTO.setPassword("dfgfghnghm");
//        userDTO.setRole(false);
//        userDTO.setBan(false);
        Pagination pagination = Pagination.builder()
                .setRows(service.getNumberOfRows(userDTO))
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("test")
                .setDefaultColumn("result")
                .build();
        System.out.println("-- Test --");
        List<PassedTestDTO> page = service.getPage(userDTO,pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 1);

       pagination = Pagination.builder()
                .setRows(service.getNumberOfRows(userDTO))
                .setCurrentPage("1")
                .setRecordsPerPage("10")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("test")
                .setDefaultColumn("test")
                .build();
        System.out.println("-- Test --");
        page = service.getPage(userDTO,pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 6);
    }

    @Test
    void whenGetNumberOfRowsPassedTestReturnNumberOfRowsOfExistingPassedTest() throws RepositoryException, ValidateException {
     //  PassedTestServiceImpl service = new PassedTestServiceImpl();
     //   UserServiceImpl serviceUser = new UserServiceImpl();
      //  UserDTO userDTO = serviceUser.get(2l).get();
        Integer rows = service.getNumberOfRows(userDTO);
        System.out.println("rows user: " + userDTO.getId() + " - " + rows);
        List<PassedTestDTO> all = service.getAllPassedTestFosUser(userDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), rows);
    }
}
