package com.vl.testing.integration;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.*;
import com.vl.testing.persistence.daofactory.ConnectionPoolHolder;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.*;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class QuestionServiceIT {
    private final DataSource dataSource = ConnectionPoolHolder.getDataSource();
    private static Connection connection;
    private static final String CREATE_TABLES = "src/test/resources/create_table.sql";
    private static final String DROP_TABLES = "src/test/resources/drop_table.sql";
    private static final String INSERT_ENTITIES_INTO_TABLES = "src/test/resources/insert_into_table.sql";
    private static String createSQL;
    private static String dropSQL;
    private static String insertSQL;
    private static int count = 1;
    private static final Long testTestId = 2L;
    private static final Integer numberOfQuestions = 2;
    private static QuestionServiceImpl service;
    private static TestServiceImpl serviceTest;
    private static TestDTO testDTO;
    private static List<QuestionDTO> before;

    @BeforeAll
    static void globalSetUp() throws IOException {
        createSQL = Files.readString(Path.of(CREATE_TABLES));
        System.out.println(createSQL);
        dropSQL = Files.readString(Path.of(DROP_TABLES));
        System.out.println(dropSQL);
        insertSQL = Files.readString(Path.of(INSERT_ENTITIES_INTO_TABLES));
        System.out.println(insertSQL);
        System.out.println("-- Test PassedTest --");
    }

    @BeforeEach
    void init() throws SQLException, RepositoryException, ValidateException {
        connection = dataSource.getConnection();
        connection.createStatement().executeUpdate(dropSQL);
        System.out.println("Drop");
        connection.createStatement().executeUpdate(createSQL);
        System.out.println("Create");
        connection.createStatement().executeUpdate(insertSQL);
        System.out.println("Insert");
        System.out.println("-- Test PassedTest -- " + count++);
        service = new QuestionServiceImpl();
        serviceTest = new TestServiceImpl();
        testDTO = serviceTest.get(testTestId).get();
        before = service.getAllByTest(testDTO);
    }

    @AfterEach
    void tearDown() throws FileNotFoundException, SQLException {
        connection.close();
        //   dropTable(connection);
    }

    @Test
    void whenGetAllQuestionsReturnListOfExistingQuestions() throws RepositoryException, ValidateException {
        // Long testTestId = 2l;
        //Integer numberOfQuestions = 2;
        // QuestionServiceImpl service = new QuestionServiceImpl();
        //  TestSeviceImpl serviceTest = new TestSeviceImpl();
        // TestDTO testDTO = serviceTest.get(testTestId).get();
        System.out.println("-- Test --");
        List<QuestionDTO> all = service.getAllByTest(testDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), numberOfQuestions);
    }

    @Test
    void whenGetAllQuestionsByTestIDReturnListsEmpty() throws RepositoryException, ValidateException {
        Long testAnotherTestId = 10l;
        Integer numberOfQuestionsAnother = 0;
        //  QuestionServiceImpl service = new QuestionServiceImpl();
        //  TestSeviceImpl serviceTest = new TestSeviceImpl();
        TestDTO selected = serviceTest.get(testAnotherTestId).get();
        assertNotNull(selected);
        System.out.println("-- Test --");
        List<QuestionDTO> all = service.getAllByTest(selected);
        all.forEach(x -> System.out.println(x));
        assertTrue(all.isEmpty());
        assertEquals(all.size(), numberOfQuestionsAnother);
    }

    @Test
    void whenGetFirstQuestionsShouldReturnFirstQuestionFromListOfQuestions() throws RepositoryException, ValidateException {
        List<QuestionDTO> all = service.getAllByTest(testDTO);
        System.out.println("-- Test -- GET --");
        all.forEach(x -> System.out.println(x));
        assertEquals(all.get(0), service.get(all.get(0).getId()).get());
    }

    @Test
    void addingQuestionShouldIncreaseListAndNotAffectExistingQuestion() throws RepositoryException, ValidateException {
        //  TestSeviceImpl service = new TestSeviceImpl();
        //  SubjectDTO subjectDTO = new SubjectServiceImpl().get(testSubjectId).get();
        //  List<TestDTO> before = service.getAllBySubject(subjectDTO);

        QuestionDTO questionDTO = createQuestionTestDto(testDTO);
        System.out.println(testDTO);
        System.out.println(questionDTO);
        QuestionDTO created = service.create(questionDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        questionDTO.setId(created.getId());
        assertEquals(questionDTO, created);
        List<QuestionDTO> all = service.getAllByTest(testDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(before.size() + 1, all.size());
    }

    @Test
    void whenUpdatedQuestionShouldNotIncreaseDecreaseListAndAffectOnUpdatedQuestion() throws RepositoryException, ValidateException {
        //  SubjectServiceImpl service = new SubjectServiceImpl();
        // SubjectDTO created = service.create(createTestSubjectDto());
        //   SubjectDTO subjectDTO = new SubjectServiceImpl().get(testSubjectId).get();
        //   TestSeviceImpl service = new TestSeviceImpl();
        //  List<TestDTO> before = service.getAllBySubject(subjectDTO);

        QuestionDTO questionDTO = createQuestionTestDto(testDTO);
        QuestionDTO created = service.create(questionDTO);
        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());

//        SubjectDTO newSubjectDTO = SubjectDTO.builder()
//                .setId(created.getId())
//                .setSubject("anotherTestSubject")
//                .build();

        QuestionDTO newQuestionDTO = QuestionDTO.builder()
                .setId(created.getId())
                .setTestId(testDTO.getId())
                .setNumberOfQuestion(3)
                .setQuestion("newTestQuestion3")
                .setAnswer1("newTestAnswer1")
                .setAnswer2("newTestAnswer2")
                .setAnswer3("newTestAnswer3")
                .setAnswer4("newTestAnswer4")
                .setAnswer5("newTestAnswer5")
                .setCorrectAnswer1(true)
                .setCorrectAnswer2(true)
                .setCorrectAnswer3(false)
                .setCorrectAnswer4(true)
                .setCorrectAnswer5(true)
                .build();
//                .setSubjectId(subjectDTO.getId())
//                .setTest("NewTest")
//                .setDifficulty(10)
//                .setRequests(0l)
//                .setTime(5l)
//                .build();

        QuestionDTO updated = service.update(newQuestionDTO).get();
        assertNotEquals(updated, created);
        assertEquals(newQuestionDTO, updated);
        assertEquals(updated, service.get(updated.getId()).get());
        List<QuestionDTO> all = service.getAllByTest(testDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(before.size() + 1, all.size());
    }

    @Test
    void whenDeletedUserShouldDecreaseListAndNotAffectExistingUsersList() throws RepositoryException, ValidateException {
        //    SubjectDTO subjectDTO = new SubjectServiceImpl().get(testSubjectId).get();
        //    TestSeviceImpl service = new TestSeviceImpl();
        //   List<TestDTO> before = service.getAllBySubject(subjectDTO);

        QuestionDTO questionDTO = createQuestionTestDto(testDTO);
        QuestionDTO created = service.create(questionDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        questionDTO.setId(created.getId());
        assertEquals(questionDTO, created);
        assertEquals(service.getAllByTest(testDTO).size(), before.size() + 1);
        System.out.println(created.getId());
        assertTrue(service.delete(created.getId()));
        List<QuestionDTO> all = service.getAllByTest(testDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), before.size());
        assertTrue(all.equals(before));

        assertTrue(service.delete(all.get(1).getId()));
        List<QuestionDTO> allNext = service.getAllByTest(testDTO);
        allNext.forEach(x -> System.out.println(x));
        assertEquals(allNext.size(), all.size() - 1);
    }

    @Test
    void whenGetScoreReturnRightScore() throws RepositoryException, ValidateException {
        //  Long testTestId = 2l;
        // Integer numberOfQuestions = 2;
        //  QuestionServiceImpl service = new QuestionServiceImpl();
        //  TestSeviceImpl serviceTest = new TestSeviceImpl();
        // TestDTO testDTO = serviceTest.get(testTestId).get();
        System.out.println("-- Test --");
        List<QuestionDTO> all = service.getAllByTest(testDTO)
                .stream()
                .sorted(Comparator.comparingInt(QuestionDTO::getNumberOfQuestion))
                .collect(Collectors.toList());
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), numberOfQuestions);
        LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        questionToAnswerMap.put(all.get(0), getArrayTrueAnswerFromQuestion(all.get(0)));
        questionToAnswerMap.put(all.get(1), getArrayTrueAnswerFromQuestion(all.get(1)));
        assertEquals(100, service.getScore(questionToAnswerMap, 2));
    }

    @Test
    void whenGetScoreReturnNotScore() throws RepositoryException, ValidateException {
        // Long testTestId = 2l;
        //Integer numberOfQuestions = 2;
        //  QuestionServiceImpl service = new QuestionServiceImpl();
        //  TestSeviceImpl serviceTest = new TestSeviceImpl();
        // TestDTO testDTO = serviceTest.get(testTestId).get();
        System.out.println("-- Test --");
        List<QuestionDTO> all = service.getAllByTest(testDTO)
                .stream()
                .sorted(Comparator.comparingInt(QuestionDTO::getNumberOfQuestion))
                .collect(Collectors.toList());
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), numberOfQuestions);
        LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        questionToAnswerMap.put(all.get(0), new String[]{"1"});
        questionToAnswerMap.put(all.get(1), new String[]{"1"});
        assertEquals(0, service.getScore(questionToAnswerMap, 2));
    }

    @Test
    void whenGetScoreReturnHalfScore() throws RepositoryException, ValidateException {
        // Long testTestId = 2l;
        // Integer numberOfQuestions = 2;
        //  QuestionServiceImpl service = new QuestionServiceImpl();
        //   TestSeviceImpl serviceTest = new TestSeviceImpl();
        // TestDTO testDTO = serviceTest.get(testTestId).get();
        System.out.println("-- Test --");
        List<QuestionDTO> all = service.getAllByTest(testDTO)
                .stream()
                .sorted(Comparator.comparingInt(QuestionDTO::getNumberOfQuestion))
                .collect(Collectors.toList());
        all.forEach(x -> System.out.println(x));
        System.out.println(getArrayTrueAnswerFromQuestion(all.get(0)));
        System.out.println(getArrayTrueAnswerFromQuestion(all.get(1)));
        assertEquals(all.size(), numberOfQuestions);
        LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        questionToAnswerMap.put(all.get(0), new String[]{"1"});
        questionToAnswerMap.put(all.get(1), getArrayTrueAnswerFromQuestion(all.get(1)));
        assertEquals(50, service.getScore(questionToAnswerMap, 2));

        questionToAnswerMap.clear();
        // LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        questionToAnswerMap.put(all.get(0), getArrayTrueAnswerFromQuestion(all.get(0)));
        questionToAnswerMap.put(all.get(1), new String[]{"1"});
        assertEquals(50, service.getScore(questionToAnswerMap, 2));
        assertEquals(33, service.getScore(questionToAnswerMap, 3));
        assertEquals(25, service.getScore(questionToAnswerMap, 4));
    }

    private String[] getArrayTrueAnswerFromQuestion(final QuestionDTO questionDTO) {
        List<Boolean> listOfAnswers = new ArrayList<>();
        listOfAnswers.add(questionDTO.isCorrectAnswer1());
        listOfAnswers.add(questionDTO.isCorrectAnswer2());
        listOfAnswers.add(questionDTO.isCorrectAnswer3());
        listOfAnswers.add(questionDTO.isCorrectAnswer4());
        listOfAnswers.add(questionDTO.isCorrectAnswer5());
        long countCorrectAnswers = listOfAnswers.stream()
                .filter(x -> x)
                .count();
        System.out.println(countCorrectAnswers);
        String[] numArrayOfCorrectAnswers = new String[(int) countCorrectAnswers];
        int count = 0;
        for (int i = 0; i < listOfAnswers.size(); i++) {
            if (listOfAnswers.get(i)) {
                numArrayOfCorrectAnswers[count] = Integer.toString(i + 1);
                count++;
            }
        }
        Arrays.stream(numArrayOfCorrectAnswers).forEach(x -> System.out.print(x));
        return numArrayOfCorrectAnswers;
    }

    @Test
    void whenGetNumberOfRowsQuestionReturnNumberOfRowsOfExistingQuestions() throws RepositoryException, ValidateException {
        // Long testSubjectId = 2l;
        // TestSeviceImpl service = new TestSeviceImpl();
        // SubjectServiceImpl serviceSubject = new SubjectServiceImpl();
        //  SubjectDTO selected = new SubjectServiceImpl().get(testSubjectId).get();
        Integer rows = service.getNumberOfRows(testDTO);
        List<QuestionDTO> all = service.getAllByTest(testDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), rows);
    }

    @Test
    void whenGetPageQuestionsReturnListOfExistingQuestions() throws RepositoryException, ValidateException {
        List<QuestionDTO> page = service.getPage(testDTO, createQuestionPagination(service.getNumberOfRows(testDTO)));
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 2);

        System.out.println("test page----2---");
        Pagination pagination = Pagination.builder()
                .setRows(service.getNumberOfRows(testDTO))
                .setCurrentPage("1")
                .setRecordsPerPage("1") //test validation pagination
                .setSort(Sort.DESC.getSort())
                .setSortColumn("question")
                .setDefaultColumn("numquestion")
                .build();
        page = service.getPage(testDTO, pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 2);
    }

    private Pagination createQuestionPagination(final Integer row) {
        return Pagination.builder()
                .setRows(row)
                .setCurrentPage("1")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("numquestion")
                .setDefaultColumn("question")
                .build();
    }

    private QuestionDTO createQuestionTestDto(final TestDTO testDTO) {
        return QuestionDTO.builder()
                // .setId(testEntity.getId())
                .setTestId(testDTO.getId())
                .setNumberOfQuestion(3)
                .setQuestion("testQuestion3")
                .setAnswer1("testAnswer1")
                .setAnswer2("testAnswer2")
                .setAnswer3("testAnswer3")
                .setAnswer4("testAnswer4")
                .setAnswer5("testAnswer5")
                .setCorrectAnswer1(false)
                .setCorrectAnswer2(true)
                .setCorrectAnswer3(true)
                .setCorrectAnswer4(true)
                .setCorrectAnswer5(false)
                .build();
    }
}
