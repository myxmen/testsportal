package com.vl.testing.integration;

import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.persistence.enums.SortColumn;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaginationIT {

    @Test
    void complianceCheckColumns() {

        System.out.println("\n-- IntegrationTest --\n");
        for (DefaultColumn val : DefaultColumn.values()) {
//            System.out.println(val.getColumn());
//            System.out.println(Column.isColumn(val.getColumn().toUpperCase()));
            assertTrue(SortColumn.isColumn(val.getColumn().toUpperCase()));
//            assertEquals( true,false );
        }
    }
}

