package com.vl.testing.integration;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.daofactory.ConnectionPoolHolder;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.*;
import java.sql.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TstServiceIT {
    private final DataSource dataSource = ConnectionPoolHolder.getDataSource();
    private static Connection connection;
    private static final String CREATE_TABLES = "src/test/resources/create_table.sql";
    private static final String DROP_TABLES = "src/test/resources/drop_table.sql";
    private static final String INSERT_ENTITIES_INTO_TABLES = "src/test/resources/insert_into_table.sql";
    private static String createSQL;
    private static String dropSQL;
    private static String insertSQL;
    private static int count = 1;
    private static Long testSubjectId = 2l;
    private static TestServiceImpl service;
    private static SubjectDTO subjectDTO;
    private static List<TestDTO> before;

    @BeforeAll
    static void globalSetUp() throws IOException {
        createSQL = Files.readString(Path.of(CREATE_TABLES));
        System.out.println(createSQL);
        dropSQL = Files.readString(Path.of(DROP_TABLES));
        System.out.println(dropSQL);
        insertSQL = Files.readString(Path.of(INSERT_ENTITIES_INTO_TABLES));
        System.out.println(insertSQL);
        System.out.println("-- Test Test --");
    }

    @BeforeEach
    void init() throws SQLException, RepositoryException, ValidateException {
        connection = dataSource.getConnection();
        connection.createStatement().executeUpdate(dropSQL);
        System.out.println("Drop");
        connection.createStatement().executeUpdate(createSQL);
        System.out.println("Create");
        connection.createStatement().executeUpdate(insertSQL);
        System.out.println("Insert");
        System.out.println("-- Test Test -- " + count++);
        service = new TestServiceImpl();
        subjectDTO = new SubjectServiceImpl().get(testSubjectId).get();
        before = service.getAllBySubject(subjectDTO);
    }

    @AfterEach
    void tearDown() throws SQLException {
        connection.close();
        //   dropTable(connection);
    }

    @Test
    void whenGetAllTestsBySubjectIDReturnListOfExistingTests() throws RepositoryException, ValidateException {
        // Long testSubjectId = 2l;
        Integer numberOfTests = 2;
        //  SubjectServiceImpl serviceSubject = new SubjectServiceImpl();
        // TestSeviceImpl service = new TestSeviceImpl();
        //  SubjectDTO selected = serviceSubject.get(testSubjectId).get();
        //  assertNotNull(selected);
        System.out.println("-- Test --");
        List<TestDTO> all = new TestServiceImpl().getAllBySubject(subjectDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), numberOfTests);
    }

    @Test
    void whenGetAllTestsBySubjectIDReturnListsEmpty() throws RepositoryException, ValidateException {
        Long testAnotherSubjectId = 3l;
        Integer numberOfTests = 0;
        // SubjectServiceImpl serviceSubject = new SubjectServiceImpl();
        //  TestSeviceImpl service = new TestSeviceImpl();
        SubjectDTO selected = new SubjectServiceImpl().get(testAnotherSubjectId).get();
        assertNotNull(selected);
        System.out.println("-- Test --");
        List<TestDTO> all = new TestServiceImpl().getAllBySubject(selected);
        all.forEach(x -> System.out.println(x));
        assertTrue(all.isEmpty());
        assertEquals(all.size(), numberOfTests);
    }

    @Test
    void addingTestShouldIncreaseListAndNotAffectExistingTest() throws RepositoryException, ValidateException {
        //  TestSeviceImpl service = new TestSeviceImpl();
        //  SubjectDTO subjectDTO = new SubjectServiceImpl().get(testSubjectId).get();
        //  List<TestDTO> before = service.getAllBySubject(subjectDTO);

        TestDTO testDTO = createTestSubjectDto(subjectDTO);
        TestDTO created = service.create(testDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        testDTO.setId(created.getId());
        assertEquals(testDTO, created);
        List<TestDTO> all = service.getAllBySubject(subjectDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(before.size() + 1, all.size());
    }

    @Test
    void whenDeletedUserShouldDecreaseListAndNotAffectExistingUsersList() throws RepositoryException, ValidateException {
        //    SubjectDTO subjectDTO = new SubjectServiceImpl().get(testSubjectId).get();
        //    TestSeviceImpl service = new TestSeviceImpl();
        //   List<TestDTO> before = service.getAllBySubject(subjectDTO);

        TestDTO testDTO = createTestSubjectDto(subjectDTO);
        TestDTO created = service.create(testDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        testDTO.setId(created.getId());
        assertEquals(testDTO, created);
        assertEquals(service.getAllBySubject(subjectDTO).size(), before.size() + 1);
        System.out.println(created.getId());
        assertTrue(service.delete(created.getId()));
        List<TestDTO> all = service.getAllBySubject(subjectDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), before.size());
        assertTrue(all.equals(before));

        assertTrue(service.delete(all.get(1).getId()));
        List<TestDTO> allNext = service.getAllBySubject(subjectDTO);
        allNext.forEach(x -> System.out.println(x));
        assertEquals(allNext.size(), all.size() - 1);
    }

    @Test
    void whenUpdatedTestShouldNotIncreaseDecreaseListAndAffectOnUpdatedTest() throws RepositoryException, ValidateException {
        //  SubjectServiceImpl service = new SubjectServiceImpl();
        // SubjectDTO created = service.create(createTestSubjectDto());
        //   SubjectDTO subjectDTO = new SubjectServiceImpl().get(testSubjectId).get();
        //   TestSeviceImpl service = new TestSeviceImpl();
        //  List<TestDTO> before = service.getAllBySubject(subjectDTO);

        TestDTO testDTO = createTestSubjectDto(subjectDTO);
        TestDTO created = service.create(testDTO);
        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());

//        SubjectDTO newSubjectDTO = SubjectDTO.builder()
//                .setId(created.getId())
//                .setSubject("anotherTestSubject")
//                .build();

        TestDTO newTestDTO = TestDTO.builder()
                .setId(created.getId())
                .setSubjectId(subjectDTO.getId())
                .setTest("NewTest")
                .setDifficulty(10)
                .setRequests(0l)
                .setTime(5l)
                .build();

        TestDTO updated = service.update(newTestDTO).get();
        assertNotEquals(updated,created);
        assertEquals(newTestDTO, updated);
        assertEquals(updated, service.get(updated.getId()).get());
        List<TestDTO> all = service.getAllBySubject(subjectDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(before.size() + 1, all.size());
    }

    @Test
    void whenGetNumberOfRowsTestReturnNumberOfRowsOfExistingTest() throws RepositoryException, ValidateException {
        // Long testSubjectId = 2l;
        // TestSeviceImpl service = new TestSeviceImpl();
        // SubjectServiceImpl serviceSubject = new SubjectServiceImpl();
        //  SubjectDTO selected = new SubjectServiceImpl().get(testSubjectId).get();
        Integer rows = service.getNumberOfRows(subjectDTO);
        List<TestDTO> all = service.getAllBySubject(subjectDTO);
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), rows);
    }

    @Test
    void whenIncreaseRequestOfTestReturnIncreaseNumberOfRequestOfTest() throws RepositoryException, ValidateException {
        // Long testSubjectId = 2l;
        // TestSeviceImpl service = new TestSeviceImpl();
        //  SubjectServiceImpl serviceSubject = new SubjectServiceImpl();
        //  SubjectDTO subjectDTO = new SubjectServiceImpl().get(testSubjectId).get();
        Pagination pagination = createTestPagination(service.getNumberOfRows(subjectDTO));

//                Pagination.builder()
//                .setRows(service.getNumberOfRows(subjectDTO))
//                .setCurrentPage("1")
//                .setRecordsPerPage("5")
//                .setSort(Sort.ASC.getSort())
//                .setSortColumn("test")
//                .setDefaultColumn("time")
//                .build();

        List<TestDTO> page = service.getPage(subjectDTO, pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 2);

        TestDTO testDTO = page.get(0);
        System.out.println(testDTO);
        service.increaseRequestOfTest(testDTO);
        TestDTO increasedTestDTO = service.getPage(subjectDTO, pagination).get(0);
        System.out.println(increasedTestDTO);
        assertEquals(testDTO.getRequests() + 1, increasedTestDTO.getRequests());
    }

    @Test
    void whenGetPageTestsReturnListOfExistingTests() throws RepositoryException, ValidateException {
        // Long testSubjectId = 2l;
        // TestSeviceImpl service = new TestSeviceImpl();
        //SubjectServiceImpl serviceSubject = new SubjectServiceImpl();
        //  SubjectDTO testDTO = new SubjectServiceImpl().get(testSubjectId).get();
        // Pagination pagination = createTestPagination(service.getNumberOfRows(testDTO));

//        Pagination.builder()
//                .setRows(service.getNumberOfRows(testDTO))
//                .setCurrentPage("1")
//                .setRecordsPerPage("5")
//                .setSort(Sort.ASC.getSort())
//                .setSortColumn("test")
//                .setDefaultColumn("time")
//                .build();

        List<TestDTO> page = service.getPage(subjectDTO, createTestPagination(service.getNumberOfRows(subjectDTO)));
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 2);

        System.out.println("test page----2---");
        Pagination pagination = Pagination.builder()
                .setRows(service.getNumberOfRows(subjectDTO))
                .setCurrentPage("1")
                .setRecordsPerPage("1") //test validation pagination
                .setSort(Sort.DESC.getSort())
                .setSortColumn("time")
                .setDefaultColumn("test")
                .build();
        page = service.getPage(subjectDTO, pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), 2);
    }

    private TestDTO createTestSubjectDto(final SubjectDTO subjectDTO) {
        return TestDTO.builder()
                // .setId(testEntity.getId())
                .setSubjectId(subjectDTO.getId())
                .setTest("Test")
                .setDifficulty(88)
                .setRequests(999l)
                .setTime(111l)
                .build();
    }

    private Pagination createTestPagination(final Integer row) {
        return Pagination.builder()
                .setRows(row)
                .setCurrentPage("1")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("test")
                .setDefaultColumn("time")
                .build();
    }
}
