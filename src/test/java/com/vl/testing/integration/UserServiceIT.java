package com.vl.testing.integration;

import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.service.exeptions.PasswordException;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.daofactory.ConnectionPoolHolder;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.*;
import java.sql.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UserServiceIT {
    private final DataSource dataSource = ConnectionPoolHolder.getDataSource();
    private static Connection connection;
    private static final String CREATE_TABLES = "src/test/resources/create_table.sql";
    private static final String DROP_TABLES = "src/test/resources/drop_table.sql";
    private static final String INSERT_ENTITIES_INTO_TABLES = "src/test/resources/insert_into_table.sql";
    private static String createSQL;
    private static String dropSQL;
    private static String insertSQL;
    private static int count = 1;
    private static int usersInTable;
    private static UserServiceImpl service;

//    @Test
//    public void testApp() {
//        System.out.println("Integration test");
//        assertEquals(true, true);
//    }

    @BeforeAll
    static void globalSetUp() throws IOException {
        createSQL = Files.readString(Path.of(CREATE_TABLES));
        System.out.println(createSQL);
        dropSQL = Files.readString(Path.of(DROP_TABLES));
        System.out.println(dropSQL);
        insertSQL = Files.readString(Path.of(INSERT_ENTITIES_INTO_TABLES));
        System.out.println(insertSQL);
        System.out.println("-- Test User --");
    }

    @BeforeEach
    void init() throws SQLException, RepositoryException {
        connection = dataSource.getConnection();
        connection.createStatement().executeUpdate(dropSQL);
        System.out.println("Drop");
        connection.createStatement().executeUpdate(createSQL);
        System.out.println("Create");
        connection.createStatement().executeUpdate(insertSQL);
        System.out.println("Insert");
        System.out.println("-- Test User -- " + count++);
        service = new UserServiceImpl();
        usersInTable = service.getAll().size();
    }

    @AfterEach
    void tearDown() throws SQLException {
        connection.close();
        //   dropTable(connection);
    }

    @Test
    void addingUserShouldIncreaseListAndNotAffectExistingUser() throws RepositoryException, ValidateException {
        UserDTO userDTO = createTestUserDto();
        //   UserServiceImpl service = new UserServiceImpl();

        UserDTO created = service.create(userDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        userDTO.setId(created.getId());
        assertEquals(userDTO, created);
        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(usersInTable + 1, all.size());
    }

    @Test
    void whenGetUserEqualsAddedUser() throws RepositoryException, ValidateException {
        UserDTO userDTO = createTestUserDto();
        //  UserServiceImpl service = new UserServiceImpl();

        UserDTO created = service.create(userDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        userDTO.setId(created.getId());
        assertEquals(userDTO, created);

        UserDTO selected = service.get(created.getId()).get();
        assertEquals(selected, userDTO);

        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(usersInTable + 1, all.size());
    }

    @Test
    void whenGetByNameUserEqualsAddedUser() throws RepositoryException, PasswordException, ValidateException {
        UserDTO userDTO = createTestUserDto();
        //   UserServiceImpl service = new UserServiceImpl();
        System.out.println(userDTO);
        String password = userDTO.getPassword();

        UserDTO created = service.create(userDTO);
        System.out.println(userDTO);
        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        userDTO.setId(created.getId());
        assertEquals(userDTO, created);

        created.setPassword(password);
        System.out.println(created);
        UserDTO selected = service.getByNameAndPassword(created).get();
        // selected.setPassword(created);
        assertEquals(selected, userDTO);

        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(usersInTable + 1, all.size());
    }

    @Test
    void whenUpdatedUserShouldNotIncreaseDecreaseListAndAffectOnUpdatedUser() throws RepositoryException, ValidateException {
        //   UserServiceImpl service = new UserServiceImpl();
        UserDTO created = service.create(createTestUserDto());
        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());

        UserDTO newUserDTO = UserDTO.builder()
                .setUsername("Smirnoff")
                .setPassword("12345678")
                .setAdmin(true)
                .setBan(false)
                .setLocale(Localization.RU.getLocale())
                .setId(created.getId())
                .build();

        UserDTO updated = service.update(newUserDTO).get();
        assertNotEquals(updated, created);
        assertEquals(newUserDTO, updated);
        assertEquals(updated, service.get(updated.getId()).get());
        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(usersInTable + 1, all.size());
    }

    @Test
    void whenUpdatedUsersFieldsShouldNotIncreaseDecreaseListAndAffectOnUpdatedUser() throws RepositoryException, ValidateException {
        //   UserServiceImpl service = new UserServiceImpl();
        UserDTO created = service.create(createTestUserDto());
        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());

        UserDTO newUserDTO = UserDTO.builder()
                .setUsername("Smirnoff")
                .setPassword("12345678")
                .setAdmin(false)
                .setBan(true)
                .setLocale(Localization.EN.getLocale())
                .setId(created.getId())
                .build();

        UserDTO updated = service.updateUsersFields(newUserDTO).get();
        System.out.println(updated);
        assertEquals(newUserDTO, updated);
        assertEquals(newUserDTO.getId(), updated.getId());
        assertEquals(newUserDTO.getLocale(), updated.getLocale());
        assertEquals(newUserDTO.getUsername(), updated.getUsername());
        assertEquals(newUserDTO.getPassword(), updated.getPassword());
        assertEquals(newUserDTO.isAdmin(), updated.isAdmin());
        assertEquals(newUserDTO.isBan(), updated.isBan());

        UserDTO getUpdated = service.get(updated.getId()).get();
        System.out.println(getUpdated);
        assertEquals(updated, getUpdated);
        assertEquals(newUserDTO.getId(), getUpdated.getId());
        assertEquals(newUserDTO.getLocale(), getUpdated.getLocale());
        assertEquals(newUserDTO.getUsername(), getUpdated.getUsername());
        assertEquals(newUserDTO.getPassword(), getUpdated.getPassword());
        //  assertTrue(BCrypt.checkpw(userDTO.getPassword(), getUserDTO.getPassword()));
        assertNotEquals(newUserDTO.isAdmin(), getUpdated.isAdmin());
        assertNotEquals(newUserDTO.isBan(), getUpdated.isBan());

        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(usersInTable + 1, all.size());
    }

    @Test
    void whenUpdatedAdminFieldsShouldNotIncreaseDecreaseListAndAffectOnUpdatedUser() throws RepositoryException, ValidateException {
        //   UserServiceImpl service = new UserServiceImpl();
        UserDTO created = service.create(createTestUserDto());
        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());

        UserDTO newUserDTO = UserDTO.builder()
                .setUsername("Smirnoff")
                .setPassword("12345678")
                .setAdmin(false)
                .setBan(true)
                .setLocale(Localization.EN.getLocale())
                .setId(created.getId())
                .build();

        UserDTO updated = service.updateAdminsFields(newUserDTO).get();
        System.out.println(updated);
        assertEquals(newUserDTO, updated);
        assertEquals(newUserDTO.getId(), updated.getId());
        assertEquals(newUserDTO.getLocale(), updated.getLocale());
        assertEquals(newUserDTO.getUsername(), updated.getUsername());
        assertEquals(newUserDTO.getPassword(), updated.getPassword());
        assertEquals(newUserDTO.isAdmin(), updated.isAdmin());
        assertEquals(newUserDTO.isBan(), updated.isBan());

        UserDTO getUpdated = service.get(updated.getId()).get();
        System.out.println(getUpdated);
        assertNotEquals(updated, getUpdated);
        assertEquals(newUserDTO.getId(), getUpdated.getId());
        assertNotEquals(newUserDTO.getLocale(), getUpdated.getLocale());
        assertNotEquals(newUserDTO.getUsername(), getUpdated.getUsername());
        assertEquals(newUserDTO.getPassword(), getUpdated.getPassword());
        assertEquals(newUserDTO.isAdmin(), getUpdated.isAdmin());
        assertEquals(newUserDTO.isBan(), getUpdated.isBan());

        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(usersInTable + 1, all.size());
    }

    @Test
    void whenDeletedUserShouldDecreaseListAndNotAffectExistingUsersList() throws RepositoryException, ValidateException {
        UserDTO userDTO = createTestUserDto();
        //   UserServiceImpl service = new UserServiceImpl();
        List<UserDTO> userDTOList = service.getAll();
//        userDTO.setUsername("Smirnov");
//        userDTO.setPassword("dfgfghnghm");
//        // userDTO.setRole(true);
//        userDTO.setAdmin(true);
//        userDTO.setBan(false);
//        newUserDTO.setLocale(Localization.RU.getLocale());
        UserDTO created = service.create(userDTO);

        assertNotNull(created);
        System.out.println(created);
        assertNotNull(created.getId());
        userDTO.setId(created.getId());
        assertEquals(userDTO, created);
        assertEquals(service.getAll().size(), userDTOList.size() + 1);
        System.out.println(created.getId());
        assertTrue(service.delete(created.getId()));
        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertTrue(all.equals(userDTOList));

        assertTrue(service.delete(1l));
        List<UserDTO> allNext = service.getAll();
        allNext.forEach(x -> System.out.println(x));
        assertEquals(allNext.size(), all.size() - 1);
    }

    @Test
    void whenGetAllUsersReturnListOfExistingUsers() throws RepositoryException {
        System.out.println("-- Test --");
        // UserServiceImpl service = new UserServiceImpl();
        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(usersInTable, all.size());
    }

    @Test
    void whenGetPageLoggedUsersReturnListOfExistingLoggedUsers() throws RepositoryException, ValidateException {
        System.out.println("-- Test --");
        Integer defaultRecordsPerPage = 5;
        //   UserServiceImpl service = new UserServiceImpl();
        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));

        Pagination pagination = Pagination.builder()
                .setRows(service.getNumberOfRowsLoggedUsers(all))
                .setCurrentPage("1")
                .setRecordsPerPage("3")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("admin")
                .setDefaultColumn("user")
                .build();
        List<UserDTO> page = service.getPageLoggedUsers(all, pagination);
        assertEquals(page.size(), defaultRecordsPerPage);
        assertEquals(all.size(), usersInTable);
    }

    @Test
    void whenGetNumberOfRowsLoggedUsersReturnNumberOfRowsLoggedUsers() throws RepositoryException {
        // UserServiceImpl service = new UserServiceImpl();
        List<UserDTO> all = service.getAll();
        assertEquals(all.size(), usersInTable);
        assertEquals(service.getNumberOfRowsLoggedUsers(all), usersInTable);
    }

    @Test
    void whenGetNumberOfRowsUsersReturnNumberOfRowsOfExistingUsers() throws RepositoryException {
        //  UserServiceImpl service = new UserServiceImpl();
        Integer rows = service.getNumberOfRows();
        System.out.println("rows" + rows);
        List<UserDTO> all = service.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), rows);
    }

    @Test
    void whenGetPageUsersReturnListOfExistingUsers() throws RepositoryException, ValidateException {
        Integer numberOfTestsFiveRecords = 5;
        Integer numberOfTestsTenRecords = 6;
        //   UserServiceImpl service = new UserServiceImpl();
        Pagination pagination = Pagination.builder()
                .setRows(service.getNumberOfRows())
                .setCurrentPage("1")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("user")
                .setDefaultColumn("user")
                .build();
        List<UserDTO> page = service.getPage(pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), numberOfTestsFiveRecords);

        pagination = Pagination.builder()
                .setRows(service.getNumberOfRows())
                .setCurrentPage("1")
                .setRecordsPerPage("10")
                .setSort(Sort.DESC.getSort())
                .setSortColumn("user")
                .setDefaultColumn("user")
                .build();
        page = service.getPage(pagination);
        page.forEach(x -> System.out.println(x));
        assertEquals(page.size(), numberOfTestsTenRecords);
    }

    private UserDTO createTestUserDto() {
//        UserDTO userDTO = new UserDTO();
//        userDTO.setUsername("Smirnov");
//        userDTO.setPassword("1111");
//        // userDTO.setRole(true);
//        userDTO.setAdmin(true);
//        userDTO.setBan(false);
//        userDTO.setLocale(Localization.RU.getLocale());
        //return userDTO;
        return UserDTO.builder()
                .setUsername("Smirnov")
                .setPassword("1111")
                .setAdmin(true)
                .setBan(false)
                .setLocale(Localization.RU.getLocale())
                .build();
    }
}
