package com.vl.testing.unit.controller.filter;

import com.vl.testing.controller.filters.HistoryFilter;
import org.junit.jupiter.api.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class HistoryFilterTest {
    private static HistoryFilter historyFilter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain filterChain;

    @BeforeAll
    static void initGlobal() {
        filterChain = mock(FilterChain.class);
        historyFilter = new HistoryFilter();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
    }

    @DisplayName("Set")
    @Test
    public void testFilterSet() throws IOException, ServletException {
        historyFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
        verify(response, times(1)).setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        verify(response, times(1)).setHeader("Pragma", "no-cache");
        verify(response, times(1)).setDateHeader("Expires", 0);
    }
}
