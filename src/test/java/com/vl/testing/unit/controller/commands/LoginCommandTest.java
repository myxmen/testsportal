package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.CommandUtility;
import com.vl.testing.controller.commands.impl.LoginCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.util.TextLocale;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.exeptions.PasswordException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class LoginCommandTest {

    private static UserServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static UserDTO userDTO;
    private static TextLocale textLocale;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(UserServiceImpl.class);
        command = new LoginCommand(service);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        textLocale = mock(TextLocale.class);
        method = "Get";
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw = new MockedRequestWrapper(request);
        userDTO = UserDTO.builder()
                .setId(3l)
                .setUsername("Admin")
                .setPassword("12345678")
                .setBan(false)
                .setAdmin(false)
                .setLocale(Localization.RU.getLocale())
                .build();
        mrw.setAttribute("name", userDTO.getUsername());
        mrw.setAttribute("pass", userDTO.getPassword());
    }

    @DisplayName("Return URL when User logged")
    @Test
    void whenExecuteReturnURLLoggedUser() throws RepositoryException, ValidateException, PasswordException {
        when(service.getByNameAndPassword(any(UserDTO.class))).thenReturn(Optional.of(userDTO));

        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() -> CommandUtility.checkUserIsLogged(mrw, userDTO)).thenReturn(true);
            assertEquals("redirect:/login.jsp", command.execute(mrw, method));
        }
        verify(session, atMost(6)).setAttribute("errorpage", Error.LOGGED_USER.getMessage());
        verify(session, atMost(6)).setAttribute("errormessage", userDTO.getUsername());
        verify(session, atMost(6)).setAttribute("errordto", userDTO);
    }

    @DisplayName("Return URL when User banned")
    @Test
    void whenExecuteReturnURLBannedUser() throws RepositoryException, ValidateException, PasswordException {
        userDTO.setBan(true);
        when(service.getByNameAndPassword(any(UserDTO.class))).thenReturn(Optional.of(userDTO));

        assertEquals("redirect:/login.jsp", command.execute(mrw, method));

        verify(session, atMost(6)).setAttribute("errorpage", Error.BANNED_USER.getMessage());
        verify(session, atMost(6)).setAttribute("errormessage", userDTO.getUsername());
        verify(session, atMost(6)).setAttribute("errordto", userDTO);
    }

    @DisplayName("Return URL when User is empty")
    @Test
    void whenExecuteReturnURLEmptyUser() throws RepositoryException, ValidateException, PasswordException {
        when(service.getByNameAndPassword(any(UserDTO.class))).thenReturn(Optional.empty());

        assertEquals("redirect:/login.jsp", command.execute(mrw, method));

        verify(session, atMost(6)).setAttribute("errorpage", Error.INVALID_LOGIN.getMessage());
        verify(session, atMost(6)).setAttribute("errormessage", userDTO.getUsername());
        verify(session, atMost(6)).setAttribute("errordto", userDTO);
    }

    @DisplayName("Return URL when User is user")
    @Test
    void whenExecuteReturnURLUser() throws RepositoryException, ValidateException, PasswordException {
        when(service.getByNameAndPassword(any(UserDTO.class))).thenReturn(Optional.of(userDTO));
        //  when(session.getAttribute("text")).thenReturn(textLocale);
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() -> CommandUtility.checkUserIsLogged(mrw, userDTO)).thenReturn(false);
            try (MockedStatic<TextLocale> mockedLocale = mockStatic(TextLocale.class)) {
                mockedLocale.when(() -> TextLocale.getCurrentInstance(mrw)).thenReturn(textLocale);
                assertEquals("redirect:/user", command.execute(mrw, method));
                mockedStatic.verify(() -> CommandUtility.setUserRole(mrw, userDTO));
            }
        }
        verify(textLocale, atMost(6)).setLocale(Localization.RU.getLocale());
        verify(session, atMost(6)).setAttribute("language", userDTO.getLocale());
    }

    @DisplayName("Return URL when User is admin")
    @Test
    void whenExecuteReturnURLAdmin() throws RepositoryException, ValidateException, PasswordException {
        userDTO.setAdmin(true);
        when(service.getByNameAndPassword(any(UserDTO.class))).thenReturn(Optional.of(userDTO));
        //  when(session.getAttribute("text")).thenReturn(textLocale);
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() -> CommandUtility.checkUserIsLogged(mrw, userDTO)).thenReturn(false);
            try (MockedStatic<TextLocale> mockedLocale = mockStatic(TextLocale.class)) {
                mockedLocale.when(() -> TextLocale.getCurrentInstance(mrw)).thenReturn(textLocale);
                assertEquals("redirect:/admin", command.execute(mrw, method));
                mockedStatic.verify(() -> CommandUtility.setUserRole(mrw, userDTO));
            }
        }
        verify(textLocale, atMost(6)).setLocale(Localization.RU.getLocale());
        verify(session, atMost(6)).setAttribute("language", userDTO.getLocale());
    }

    @DisplayName("Return URL when Users field admin null")
    @Test
    void whenExecuteWithNullAdminFieldReturnURLLogin() throws RepositoryException, ValidateException, PasswordException {
        userDTO.setAdmin(null);
        when(service.getByNameAndPassword(any(UserDTO.class))).thenReturn(Optional.of(userDTO));
        assertEquals("/login", command.execute(mrw, method));
    }

    @DisplayName("Return URL when Users field ban null")
    @Test
    void whenExecuteWithNullBanFieldReturnURLLogin() throws RepositoryException, ValidateException, PasswordException {
        userDTO.setBan(null);
        when(service.getByNameAndPassword(any(UserDTO.class))).thenReturn(Optional.of(userDTO));
        assertEquals("/login", command.execute(mrw, method));
    }

    @DisplayName("ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException, PasswordException {
        when(service.getByNameAndPassword(any(UserDTO.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.USERDTO_IS_EMPTY, Error.INVALID_LOGIN)));

        assertEquals(command.execute(mrw, method), "redirect:/login.jsp");
        verify(session, atMost(6)).setAttribute("errorpage", Error.INVALID_LOGIN);
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws ValidateException, RepositoryException, PasswordException {
        when(service.getByNameAndPassword(any(UserDTO.class)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals(command.execute(mrw, method), "redirect:/login.jsp");
        verify(session, atMost(6)).setAttribute("errorpage", Error.ERROR_DATABASE.getMessage());
        verify(session, atMost(6)).setAttribute("errormessage", "Test Error");
    }

    @DisplayName("PasswordException")
    @Test
    void whenExecuteReturnPasswordException() throws ValidateException, RepositoryException, PasswordException {
        when(service.getByNameAndPassword(any(UserDTO.class)))
                .thenThrow(new PasswordException("Test Error"));

        assertEquals(command.execute(mrw, method), "redirect:/login.jsp");
        verify(session, atMost(6)).setAttribute("errorpage", Error.INVALID_LOGIN.getMessage());
        verify(session, atMost(6)).setAttribute("errormessage", userDTO.getUsername());
        verify(session, atMost(6)).setAttribute("errordto", userDTO);
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        LoginCommand command = new LoginCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        UserServiceImpl service = null;
        try {
            field = command.getClass().getDeclaredField(all[1].getName());

            field.setAccessible(true);
            service = (UserServiceImpl) field.get(command);
            System.out.println(service);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(UserServiceImpl.class, service.getClass());
    }
}
