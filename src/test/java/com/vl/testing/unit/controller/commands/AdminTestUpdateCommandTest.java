package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminTestUpdateCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.*;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminTestUpdateCommandTest {
    private static TestServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static TestDTO testDTO;
    private static Long subId = 3l;
    private static String subjectId = Long.toString(subId);
    private static Long tstId = 44l;
    private static String testId = Long.toString(tstId);
    private static String testName = "Test Test";
    private static String difficulty = "75";
    private static String time = "45";
    private static String requests = "99";
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(TestServiceImpl.class);
        command = new AdminTestUpdateCommand(service);
        request = mock(HttpServletRequest.class);
        testDTO = mock(TestDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("testid", testId);
        mrw.setAttribute("subjectid", subjectId);
        mrw.setAttribute("difficulty", difficulty);
        mrw.setAttribute("time", time);
        mrw.setAttribute("test", testName);
        mrw.setAttribute("requests", requests);
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException, RepositoryException {
        when(service.get(tstId)).thenReturn(Optional.of(testDTO));

        assertEquals("/WEB-INF/admin/admintestupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertEquals(testDTO, (mrw.getAttribute("testone"))),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid")))
        );
    }

    @DisplayName("Return NumberFormatException")
    @Test
    void whenExecuteReturnNumberFormatException() throws ValidateException, RepositoryException {
        mrw.setAttribute("subjectid", "Test");

        assertEquals("redirect:/admin", command.execute(mrw, method));
    }

    @DisplayName("Return POST URL")
    @Test
    void whenExecuteReturnPostURL() throws ValidateException, RepositoryException {
        method = "Post";
        when(service.update(any(TestDTO.class))).thenReturn(Optional.of(testDTO));

        assertEquals("redirect:/admin/test?subjectid=" + subjectId, command.execute(mrw, method));
    }

    @DisplayName("Return POST NumberFormatException")
    @Test
    void whenExecuteReturnNumberPostFormatException() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("difficulty", "Test");
        when(service.get(tstId)).thenReturn(Optional.of(testDTO));

        assertEquals("/WEB-INF/admin/admintestupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertEquals(testDTO, (mrw.getAttribute("testone"))),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(Error.ERROR_TEST.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(testName, mrw.getAttribute("errormessage")),
                () -> assertEquals("validation", mrw.getAttribute("val"))
        );
    }

    @DisplayName("Return POST ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        method = "Post";
        when(service.update(any(TestDTO.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TESTDTO_IS_EMPTY, Error.ERROR_TEST)));
        when(service.get(tstId)).thenReturn(Optional.of(testDTO));

        assertEquals("/WEB-INF/admin/admintestupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertEquals(testDTO, (mrw.getAttribute("testone"))),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(Error.ERROR_TEST.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(testName, mrw.getAttribute("errormessage")),
                () -> assertEquals("validation", mrw.getAttribute("val"))
        );
    }

    @DisplayName("Return POST RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {
        method = "Post";
        when(service.update(any(TestDTO.class)))
                .thenThrow(new RepositoryException("Test Error"));
        when(service.get(tstId)).thenReturn(Optional.of(testDTO));

        assertEquals("/WEB-INF/admin/admintestupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(tstId, mrw.getAttribute("testid")),
                () -> assertEquals(testDTO, mrw.getAttribute("testone")),
                () -> assertEquals(subId, mrw.getAttribute("subjectid")),
                () -> assertEquals(Error.ERROR_TEST.getMessage(), mrw.getAttribute("error")),
                () -> assertEquals(testName, mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("val"))
        );
    }

    @DisplayName("Return GET ValidateExeption")
    @Test
    void whenExecuteReturnGetValidateExeption() throws ValidateException, RepositoryException {
        when(service.get(tstId))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TEST_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/admin/admintestupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertNull(mrw.getAttribute("testone")),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.TEST_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("val"))
        );
    }

    @DisplayName("Return GET RepositoryException")
    @Test
    void whenExecuteReturnGetRepositoryException() throws ValidateException, RepositoryException {
        when(service.get(tstId))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/admintestupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertNull(mrw.getAttribute("testone")),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("val"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminTestUpdateCommand command = new AdminTestUpdateCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        TestServiceImpl service = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            service = (TestServiceImpl) field1.get(command);
            System.out.println(service);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(TestServiceImpl.class, service.getClass());
    }
}
