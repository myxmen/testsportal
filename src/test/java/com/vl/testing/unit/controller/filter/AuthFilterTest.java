package com.vl.testing.unit.controller.filter;

import com.vl.testing.controller.filters.AuthFilter;
import com.vl.testing.dto.UserDTO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

public class AuthFilterTest {
    private static AuthFilter authFilter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain filterChain;
    private static HttpSession session;
    private static ServletContext context;
    private static UserDTO userDTO;
    private static String path;

    @BeforeAll
    static void initGlobal() {
        filterChain = mock(FilterChain.class);
        authFilter = new AuthFilter();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        context = mock(ServletContext.class);
        when(request.getSession()).thenReturn(session);
        when(request.getServletContext()).thenReturn(context);
    }

    @BeforeEach
    void setUp() {
        userDTO = mock(UserDTO.class);
        when(session.getAttribute("user")).thenReturn(userDTO);
        when(context.getAttribute("loggedUsers")).thenReturn(userDTO);
    }

    @AfterEach
    void resetSet() {
        reset(response, filterChain, session, userDTO);
    }

    @DisplayName("User is admin")
    @Test
    public void testDoFilterWhenUserIsAdmin() throws IOException, ServletException {
        path = "/admin/";
        when(request.getRequestURI()).thenReturn(path);
        when(userDTO.isAdmin()).thenReturn(true);

        authFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
    }

    @DisplayName("User is user")
    @Test
    public void testDoFilterWhenUserIsUser() throws IOException, ServletException {
        path = "/user/";
        when(request.getRequestURI()).thenReturn(path);
        when(userDTO.isAdmin()).thenReturn(false);

        authFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
    }

    @DisplayName("User is null")
    @Test
    public void testDoFilterWhenUserNull() throws IOException, ServletException {
        path = "/admin/";
        when(request.getRequestURI()).thenReturn(path);
        when(session.getAttribute("user")).thenReturn(null, userDTO);
        when(userDTO.isAdmin()).thenReturn(null);

        authFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
    }

    @DisplayName("User is Logout")
    @ParameterizedTest
    @MethodSource("testCases")
    public void testDoFilterWhenUserLogout(final String path) throws IOException, ServletException {
        when(request.getRequestURI()).thenReturn(path);
        when(userDTO.isAdmin()).thenReturn(false);

        authFilter.doFilter(request, response, filterChain);
        verify(response, times(1)).sendRedirect("/logout");
    }

    @DisplayName("Admin is Logout")
    @ParameterizedTest
    @MethodSource("testCases")
    public void testDoFilterWhenAdminLogout(final String path) throws IOException, ServletException {
        when(request.getRequestURI()).thenReturn(path);
        when(userDTO.isAdmin()).thenReturn(true);

        authFilter.doFilter(request, response, filterChain);
        verify(response, times(1)).sendRedirect("/logout");
    }

    static Stream<Arguments> testCases() {

        return Stream.of(
                Arguments.of("/login"),
                Arguments.of("/login.jsp"),
                Arguments.of("/signup.jsp"),
                Arguments.of("/signup"),
                Arguments.of("/"),
                Arguments.of("/index.jsp")
        );
    }
}
