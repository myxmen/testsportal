package com.vl.testing.unit.controller.filter;

import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.filters.LocaleFilter;
import com.vl.testing.controller.util.TextLocale;
import org.junit.jupiter.api.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class LocaleFilterTest {
    private static LocaleFilter localeFilter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain filterChain;
    private static HttpSession session;
    private static TextLocale textLocale;

    @BeforeAll
    static void initGlobal() {
        filterChain = mock(FilterChain.class);
        localeFilter = new LocaleFilter();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        textLocale = mock(TextLocale.class);
    }

    @BeforeEach
    void setUp() {
        when(request.getSession()).thenReturn(session);
    }

    @AfterEach
    void resetSet() {
        reset(filterChain, session, request);
    }

    @DisplayName("Locale null")
    @Test
    public void testFilterLocaleNull() throws IOException, ServletException {
        when(request.getParameter("language")).thenReturn(null);
        final Locale defaut = Localization.EN.getLocale();
        when(session.getAttribute("language")).thenReturn(null, defaut, defaut);
        when(session.getAttribute("text")).thenReturn(null);
        // try (MockedStatic<TextLocale> mockedStatic = mockStatic(TextLocale.class)) {
        //     mockedStatic.verify(() -> TextLocale.setFor(request));
        localeFilter.doFilter(request, response, filterChain);
        //  }

        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
        verify(session).setAttribute("language", defaut);
        verify(request).setAttribute("language", "EN");
    }

    @DisplayName("Error null")
    @Test
    public void testFilterErrorsNull() throws IOException, ServletException {
        when(request.getParameter("language")).thenReturn("RU");
        final Locale defaut = Localization.EN.getLocale();
        final Locale next = Localization.RU.getLocale();
        when(session.getAttribute("language")).thenReturn(defaut, next, next);
        when(session.getAttribute("text")).thenReturn(textLocale);

        // try (MockedStatic<TextLocale> mockedStatic = mockStatic(TextLocale.class)) {
        //     mockedStatic.when(() -> TextLocale.getCurrentInstance(request)).thenReturn(new TextLocale(Localization.EN.getLocale()));
        localeFilter.doFilter(request, response, filterChain);
        //  }
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
        verify(session).setAttribute("language", Localization.RU.getLocale());
        verify(request).setAttribute("language", "RU");
    }

}
