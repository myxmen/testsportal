package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminSubjectAddCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.Arrays;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminSubjectAddCommandTest {
    private static SubjectServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static SubjectDTO subjectDTO;
    private static Long id = 3l;
    private static String subjectName = "Test Subject";
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(SubjectServiceImpl.class);
        command = new AdminSubjectAddCommand(service);
        request = mock(HttpServletRequest.class);
        subjectDTO = mock(SubjectDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Post";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("id", Long.toString(id));
        mrw.setAttribute("subject", subjectName);
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Return GET URL")
    @Test
    void whenExecuteReturnGetURL() {
        method = "Get";

        assertEquals("/WEB-INF/admin/adminsubjectadd.jsp", command.execute(mrw, method));
    }

    @DisplayName("Return POST URL")
    @Test
    void whenExecuteReturnPostURL() throws ValidateException, RepositoryException {
        when(service.create(any(SubjectDTO.class))).thenReturn(subjectDTO);
        when(subjectDTO.getId()).thenReturn(id);

        assertEquals("redirect:/admin/subject", command.execute(mrw, method));
    }

    @DisplayName("Return POST ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        when(service.create(any(SubjectDTO.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.SUBJECTDTO_IS_EMPTY, Error.ERROR_SUBJECT)));

        assertEquals("/WEB-INF/admin/adminsubjectadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_SUBJECT.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("validation", (mrw.getAttribute("val"))),
                () -> assertEquals(subjectName, (mrw.getAttribute("errormessage")))
        );
    }

    @DisplayName("Return POST RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {
        when(service.create(any(SubjectDTO.class)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/adminsubjectadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_SUBJECT.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(subjectName, (mrw.getAttribute("errormessage")))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminSubjectAddCommand command = new AdminSubjectAddCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        SubjectServiceImpl service = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            service = (SubjectServiceImpl) field1.get(command);
            System.out.println(service);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(SubjectServiceImpl.class, service.getClass());
    }
}
