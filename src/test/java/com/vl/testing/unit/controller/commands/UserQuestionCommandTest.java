package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.UserQuestionCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UserQuestionCommandTest {
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static TestDTO existingTestDTO;
    private static SubjectDTO existingSubjectDTO;
    private static QuestionDTO existingQuestionDTO;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        command = new UserQuestionCommand();
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        // method = "Get";
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        existingQuestionDTO = QuestionDTO.builder()
                .setId(22l)
                .setTestId(4l)
                .setNumberOfQuestion(3)
                .setQuestion("newTestQuestion3")
                .setAnswer1("newTestAnswer1")
                .setAnswer2("newTestAnswer2")
                .setAnswer3("newTestAnswer3")
                .setAnswer4("newTestAnswer4")
                .setAnswer5("newTestAnswer5")
                .setCorrectAnswer1(true)
                .setCorrectAnswer2(true)
                .setCorrectAnswer3(false)
                .setCorrectAnswer4(true)
                .setCorrectAnswer5(true)
                .build();
        existingTestDTO = TestDTO.builder()
                .setId(4l)
                .setSubjectId(3l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .setQuestions(List.of(existingQuestionDTO, existingQuestionDTO))
                .build();
        existingSubjectDTO = SubjectDTO.builder()
                .setId(3l)
                .setSubject("Test")
                .setTests(existingTestDTO)
                .build();
    }

    @DisplayName("Return URL when Subject null")
    @Test
    void whenExecuteWithNullSubjectReturnURLSubject() {
        when(session.getAttribute("subject")).thenReturn(null);
        assertEquals("redirect:/user/subject", command.execute(mrw, method));
    }

    @DisplayName("Return URL when Subject`s Test null")
    @Test
    void whenExecuteWithNullSubjectsTestReturnURLTest() {
        existingSubjectDTO.setTests(null);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        assertEquals("redirect:/user/test", command.execute(mrw, method));
    }

    @DisplayName("Return URL when Subject`s Test`s questions null")
    @Test
    void whenExecuteWithNullSubjectsTestsQuestionReturnURLTest() {
        existingSubjectDTO.getTest().setQuestions(null);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        assertEquals("redirect:/user/test", command.execute(mrw, method));
    }

    @DisplayName("Return URL when question`s Count null")
    @Test
    void whenExecuteWithNullQuestionsCountReturnURLPretest() {
        when(session.getAttribute("count")).thenReturn(null);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        System.out.println(session.getAttribute("count"));
        assertEquals("redirect:/user/pretest", command.execute(mrw, method));

    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() {
        Integer count = 1;
        when(session.getAttribute("count")).thenReturn(count);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(session.getAttribute("timer")).thenReturn(Instant.now().toEpochMilli());
        when(session.getAttribute("duration")).thenReturn(existingTestDTO.getTime() * 60);

        assertEquals("/WEB-INF/user/userquestion.jsp", command.execute(mrw, method));
        System.out.println(mrw.getAttribute("quest"));
        System.out.println(mrw.getAttribute("questions"));
        System.out.println(mrw.getAttribute("count"));
        System.out.println(mrw.getAttribute("timeleft"));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(existingQuestionDTO.getQuestion(), (mrw.getAttribute("quest"))),
                () -> assertEquals(List.of(existingQuestionDTO.getAnswer1(), existingQuestionDTO.getAnswer2(),
                        existingQuestionDTO.getAnswer3(), existingQuestionDTO.getAnswer4(), existingQuestionDTO.getAnswer5()),
                        (mrw.getAttribute("questions"))),
                () -> assertEquals(existingTestDTO.getTime() * 60, (mrw.getAttribute("timeleft"))),
                () -> assertEquals(count + 1, (mrw.getAttribute("count")))
        );
    }

    @DisplayName("Return URL with null answer5")
    @Test
    void whenExecuteWithNullAnserReturnURL() {
        Integer count = 1;
        existingQuestionDTO.setAnswer5(null);
        when(session.getAttribute("count")).thenReturn(count);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(session.getAttribute("timer")).thenReturn(Instant.now().toEpochMilli());
        when(session.getAttribute("duration")).thenReturn(existingTestDTO.getTime() * 60);

        assertEquals("/WEB-INF/user/userquestion.jsp", command.execute(mrw, method));
        System.out.println(mrw.getAttribute("quest"));
        System.out.println(mrw.getAttribute("questions"));
        System.out.println(mrw.getAttribute("count"));
        System.out.println(mrw.getAttribute("timeleft"));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(existingQuestionDTO.getQuestion(), (mrw.getAttribute("quest"))),
                () -> assertEquals(List.of(existingQuestionDTO.getAnswer1(), existingQuestionDTO.getAnswer2(),
                        existingQuestionDTO.getAnswer3(), existingQuestionDTO.getAnswer4()),
                        (mrw.getAttribute("questions"))),
                () -> assertEquals(existingTestDTO.getTime() * 60, (mrw.getAttribute("timeleft"))),
                () -> assertEquals(count + 1, (mrw.getAttribute("count")))
        );
    }

    @DisplayName("Return URL with error")
    @Test
    void whenExecuteWithErrorReturnURL() {
        method = "Post";
        Integer count = 1;
        when(session.getAttribute("count")).thenReturn(count);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(session.getAttribute("timer")).thenReturn(Instant.now().toEpochMilli());
        when(session.getAttribute("duration")).thenReturn(existingTestDTO.getTime() * 60);

        assertEquals("/WEB-INF/user/userquestion.jsp", command.execute(mrw, method));
        System.out.println(mrw.getAttribute("quest"));
        System.out.println(mrw.getAttribute("questions"));
        System.out.println(mrw.getAttribute("count"));
        System.out.println(mrw.getAttribute("timeleft"));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_ANSWER.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(existingQuestionDTO.getQuestion(), (mrw.getAttribute("quest"))),
                () -> assertEquals(List.of(existingQuestionDTO.getAnswer1(), existingQuestionDTO.getAnswer2(),
                        existingQuestionDTO.getAnswer3(), existingQuestionDTO.getAnswer4(), existingQuestionDTO.getAnswer5()),
                        (mrw.getAttribute("questions"))),
                () -> assertEquals(existingTestDTO.getTime() * 60, (mrw.getAttribute("timeleft"))),
                () -> assertEquals(count + 1, (mrw.getAttribute("count")))
        );
    }

    @DisplayName("Return URL with answers")
    @Test
    void whenExecuteWithAnswersReturnURL() {
        method = "Post";
        String[] answers = new String[]{"2", "3"};
        mrw.setAttribute("answer", answers);
        LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        Integer count = 0;
        when(session.getAttribute("questiontoanswer")).thenReturn(questionToAnswerMap);
        when(session.getAttribute("count")).thenReturn(count);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(session.getAttribute("timer")).thenReturn(Instant.now().toEpochMilli());
        when(session.getAttribute("duration")).thenReturn(existingTestDTO.getTime() * 60);

        assertEquals("/WEB-INF/user/userquestion.jsp", command.execute(mrw, method));
        System.out.println(mrw.getAttribute("quest"));
        System.out.println(mrw.getAttribute("questions"));
        System.out.println(mrw.getAttribute("count"));
        System.out.println(mrw.getAttribute("timeleft"));
        verify(session, atMost(3)).setAttribute(eq("questiontoanswer"), any(LinkedHashMap.class));
        verify(session).setAttribute(eq("count"), eq(count + 1));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(existingQuestionDTO.getQuestion(), (mrw.getAttribute("quest"))),
                () -> assertEquals(List.of(existingQuestionDTO.getAnswer1(), existingQuestionDTO.getAnswer2(),
                        existingQuestionDTO.getAnswer3(), existingQuestionDTO.getAnswer4(), existingQuestionDTO.getAnswer5()),
                        (mrw.getAttribute("questions"))),
                () -> assertEquals(existingTestDTO.getTime() * 60, (mrw.getAttribute("timeleft"))),
                () -> assertEquals(count + 1 + 1, (mrw.getAttribute("count")))
        );
    }

    @DisplayName("Return URL end tests")
    @Test
    void whenExecuteReturnURLEndTests() {
        method = "Post";
        String[] answers = new String[]{"2", "3"};
        mrw.setAttribute("answer", answers);
        LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        Integer count = 1;
        when(session.getAttribute("questiontoanswer")).thenReturn(questionToAnswerMap);
        when(session.getAttribute("count")).thenReturn(count);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(session.getAttribute("timer")).thenReturn(Instant.now().toEpochMilli());
        when(session.getAttribute("duration")).thenReturn(existingTestDTO.getTime() * 60);

        assertEquals("redirect:/user/endtest", command.execute(mrw, method));
        verify(session, atMost(3)).setAttribute(eq("questiontoanswer"), any(LinkedHashMap.class));
    }

    @DisplayName("Return URL with first count")
    @Test
    void whenExecuteWithFirstCountReturnURL() {
        Integer count = 0;
        when(session.getAttribute("count")).thenReturn(count);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(session.getAttribute("timer")).thenReturn(Instant.now().toEpochMilli());
        when(session.getAttribute("duration")).thenReturn(existingTestDTO.getTime() * 60);

        assertEquals("/WEB-INF/user/userquestion.jsp", command.execute(mrw, method));
        verify(session).setAttribute(eq("timer"), any(Long.class));
        System.out.println(mrw.getAttribute("quest"));
        System.out.println(mrw.getAttribute("questions"));
        System.out.println(mrw.getAttribute("count"));
        System.out.println(mrw.getAttribute("timeleft"));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(existingQuestionDTO.getQuestion(), (mrw.getAttribute("quest"))),
                () -> assertEquals(List.of(existingQuestionDTO.getAnswer1(), existingQuestionDTO.getAnswer2(),
                        existingQuestionDTO.getAnswer3(), existingQuestionDTO.getAnswer4(), existingQuestionDTO.getAnswer5()),
                        (mrw.getAttribute("questions"))),
                () -> assertEquals(existingTestDTO.getTime() * 60, (mrw.getAttribute("timeleft"))),
                () -> assertEquals(count + 1, (mrw.getAttribute("count")))
        );
    }

    @DisplayName("Return URL end Time")
    @Test
    void whenExecuteReturnURLEndTime() {
        method = "Post";
        String[] answers = new String[]{"2", "3"};
        mrw.setAttribute("answer", answers);
        LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        Integer count = 0;
        when(session.getAttribute("questiontoanswer")).thenReturn(questionToAnswerMap);
        when(session.getAttribute("count")).thenReturn(count);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(session.getAttribute("timer")).thenReturn(Instant.now().toEpochMilli());
        when(session.getAttribute("duration")).thenReturn(-1l);

        assertEquals("redirect:/user/endtest", command.execute(mrw, method));
        verify(session, atMost(3)).setAttribute(eq("questiontoanswer"), any(LinkedHashMap.class));
    }
}
