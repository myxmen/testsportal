package com.vl.testing.unit.controller.util;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.validation.enums.ExceptionMessage;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.servlet.http.HttpServletRequest;

import java.util.stream.Stream;

import static com.vl.testing.controller.util.SetErrorAtribute.setRequestErrorAttributes;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class SetErrorAtributeTest {
    private static HttpServletRequest request;
    private static String error = Error.ERROR_SUBJECT.getMessage();
    private static String val = "validation";
    private static String errormessage = "Test";

    @BeforeAll
    static void initGlobal() {
        request = mock(HttpServletRequest.class);
    }

    @AfterEach
    void resetSet() {
        reset(request);
    }

    @DisplayName("Return True when Set 3 attributes")
    @Test
    void whenSetRequestErrorThreeAttributesReturnTrue() {
        assertTrue(setRequestErrorAttributes(error, val, errormessage, request));
        verify(request).setAttribute("error", error);
        verify(request).setAttribute("val", val);
        verify(request).setAttribute("errormessage", errormessage);
    }

    @DisplayName("Return True when Set 2 attributes")
    @Test
    void whenSetRequestErrorTwoAttributesReturnTrue() {
        assertTrue(setRequestErrorAttributes(error, errormessage, request));
        verify(request).setAttribute("error", error);
        verify(request).setAttribute("errormessage", errormessage);
    }

    @DisplayName("Return True when null attributes")
    @ParameterizedTest
    @MethodSource("testCases")
    void whenGetScoreReturnScore(final String error, final String val, final String errormessage) {
        assertTrue(setRequestErrorAttributes(error, val, errormessage, request));
        assertTrue(setRequestErrorAttributes(error, errormessage, request));
    }

    static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(null, null, null),
                Arguments.of(error, null, errormessage),
                Arguments.of(error, val, null),
                Arguments.of(null, val, errormessage),
                Arguments.of(error, null, null),
                Arguments.of(null, null, errormessage)
        );
    }
}
