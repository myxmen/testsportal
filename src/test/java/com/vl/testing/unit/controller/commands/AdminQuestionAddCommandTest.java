package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminQuestionAddCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Stream;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminQuestionAddCommandTest {
    private static QuestionServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static QuestionDTO questionDTO;
    private static Long testId = 44l;
    private static Long createdId = 102l;
    private static Integer numberOfQuestion = 5;
    private static String question = "Test Question";
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(QuestionServiceImpl.class);
        command = new AdminQuestionAddCommand(service);
        request = mock(HttpServletRequest.class);
        questionDTO = mock(QuestionDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Post";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("testid", Long.toString(testId));
        when(questionDTO.getId()).thenReturn(createdId);
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Return GET URL")
    @Test
    void whenExecuteReturnGetURL() {
        method = "Get";

        assertEquals("/WEB-INF/admin/adminquestionadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(testId, (mrw.getAttribute("testid")))
        );
    }

    @DisplayName("NumberFormatException")
    @Test
    void whenExecuteReturnNumberFormatException() {
        mrw.setAttribute("testid", "Test");

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("Post")
    @ParameterizedTest
    @MethodSource("testCases")
    void whenGetScoreReturnPost(final QuestionDTO testDTO) throws ValidateException, RepositoryException {
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("testid", Long.toString(testId));
        mrw.setAttribute("qnumber", String.valueOf(testDTO.getNumberOfQuestion()));
        mrw.setAttribute("question", testDTO.getQuestion());
        mrw.setAttribute("answer1", testDTO.getAnswer1());
        mrw.setAttribute("answer2", testDTO.getAnswer2());
        mrw.setAttribute("answer3", testDTO.getAnswer3());
        mrw.setAttribute("answer4", testDTO.getAnswer4());
        mrw.setAttribute("answer5", testDTO.getAnswer5());
        if (testDTO.isCorrectAnswer1() != null)
            mrw.setAttribute("correctanswer1", String.valueOf(testDTO.isCorrectAnswer1()));
        if (testDTO.isCorrectAnswer2() != null)
            mrw.setAttribute("correctanswer2", String.valueOf(testDTO.isCorrectAnswer2()));
        if (testDTO.isCorrectAnswer3() != null)
            mrw.setAttribute("correctanswer3", String.valueOf(testDTO.isCorrectAnswer3()));
        if (testDTO.isCorrectAnswer4() != null)
            mrw.setAttribute("correctanswer4", String.valueOf(testDTO.isCorrectAnswer4()));
        if (testDTO.isCorrectAnswer5() != null)
            mrw.setAttribute("correctanswer5", String.valueOf(testDTO.isCorrectAnswer5()));
        when(service.create(any(QuestionDTO.class))).thenReturn(questionDTO);

        assertEquals("redirect:/admin/question?testid=" + testId, command.execute(mrw, method));
    }

    static Stream<Arguments> testCases() {

        return Stream.of(
                Arguments.of(QuestionDTO.builder()
                        .setTestId(testId)
                        .setNumberOfQuestion(numberOfQuestion)
                        .setQuestion(question)
                        .setAnswer1("Test Answer 1")
                        .setAnswer2("Test Answer 2")
                        .setAnswer3("Test Answer 3")
                        .setAnswer4("Test Answer 4")
                        .setAnswer5("Test Answer 5")
                        .setCorrectAnswer1(true)
                        .build()),

                Arguments.of(QuestionDTO.builder()
                        .setTestId(testId)
                        .setNumberOfQuestion(numberOfQuestion)
                        .setQuestion(question)
                        .setAnswer1("Test Answer 1")
                        .setAnswer2("Test Answer 2")
                        .setAnswer3("")
                        .setAnswer4("")
                        .setAnswer5("")
                        .setCorrectAnswer2(true)
                        .build()),

                Arguments.of(QuestionDTO.builder()
                        .setTestId(testId)
                        .setNumberOfQuestion(numberOfQuestion)
                        .setQuestion(question)
                        .setAnswer1("Test Answer 1")
                        .setAnswer2("Test Answer 2")
                        .setAnswer3("Test Answer 3")
                        .setAnswer4("")
                        .setAnswer5("")
                        .setCorrectAnswer2(true)
                        .build()),

                Arguments.of(QuestionDTO.builder()
                        .setTestId(testId)
                        .setNumberOfQuestion(numberOfQuestion)
                        .setQuestion(question)
                        .setAnswer1("Test Answer 1")
                        .setAnswer2("Test Answer 2")
                        .setAnswer3("Test Answer 3")
                        .setAnswer4("Test Answer 4")
                        .setAnswer5("")
                        .setCorrectAnswer1(true)
                        .setCorrectAnswer3(true)
                        .build())
        );
    }

    @DisplayName("Return POST RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {
        mrw.setAttribute("qnumber", String.valueOf(numberOfQuestion));
        mrw.setAttribute("question", question);
        mrw.setAttribute("answer1", "Test Answer 1");
        mrw.setAttribute("answer2", "Test Answer 2");
        mrw.setAttribute("answer3", "");
        mrw.setAttribute("answer4", "");
        mrw.setAttribute("answer5", "");
        mrw.setAttribute("correctanswer2", String.valueOf(true));

        when(service.create(any(QuestionDTO.class)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/adminquestionadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_QUESTION.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(String.valueOf(numberOfQuestion), (mrw.getAttribute("errormessage"))),
                () -> assertEquals(testId, (mrw.getAttribute("testid")))
        );
    }

    @DisplayName("Return POST ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        mrw.setAttribute("qnumber", String.valueOf(numberOfQuestion));
        mrw.setAttribute("question", question);
        mrw.setAttribute("answer1", "Test Answer 1");
        mrw.setAttribute("answer2", "Test Answer 2");
        mrw.setAttribute("answer3", "");
        mrw.setAttribute("answer4", "");
        mrw.setAttribute("answer5", "");
        mrw.setAttribute("correctanswer1", String.valueOf(true));

        when(service.create(any(QuestionDTO.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.QUESTIONDTO_IS_EMPTY, Error.ERROR_QUESTION)));

        assertEquals("/WEB-INF/admin/adminquestionadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_QUESTION.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(question, (mrw.getAttribute("errormessage"))),
                () -> assertEquals("validation", (mrw.getAttribute("val"))),
                () -> assertEquals(testId, (mrw.getAttribute("testid")))
        );
    }

    @DisplayName("Return POST NumberFormatException")
    @Test
    void whenExecuteReturnPostNumberFormatException() throws ValidateException, RepositoryException {
        mrw.setAttribute("qnumber", "Test");
        mrw.setAttribute("question", question);
        mrw.setAttribute("answer1", "Test Answer 1");
        mrw.setAttribute("answer2", "Test Answer 2");
        mrw.setAttribute("answer3", "");
        mrw.setAttribute("answer4", "");
        mrw.setAttribute("answer5", "");
        mrw.setAttribute("correctanswer1", String.valueOf(true));
        mrw.setAttribute("qnumber", "Test");

        when(service.create(any(QuestionDTO.class))).thenReturn(questionDTO);

        assertEquals("/WEB-INF/admin/adminquestionadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_QUESTION.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(question, (mrw.getAttribute("errormessage"))),
                () -> assertEquals("validation", (mrw.getAttribute("val"))),
                () -> assertEquals(testId, (mrw.getAttribute("testid")))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminQuestionAddCommand command = new AdminQuestionAddCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        QuestionServiceImpl service = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            service = (QuestionServiceImpl) field1.get(command);
            System.out.println(service);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(QuestionServiceImpl.class, service.getClass());
    }
}
