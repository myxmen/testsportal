package com.vl.testing.unit.controller.listener;

import com.vl.testing.controller.enums.ContextAttribute;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.listener.SessionListener;
import com.vl.testing.dto.UserDTO;
import org.junit.jupiter.api.*;

import javax.servlet.ServletContext;
import javax.servlet.http.*;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class ListenerTest {
    private static HttpSessionEvent event;
    private static HttpSession session;
    private static ServletContext context;
    private static UserDTO userDTO;

    @BeforeAll
    static void initGlobal() {

        event = mock(HttpSessionEvent.class);
        session = mock(HttpSession.class);
        context = mock(ServletContext.class);
        when(event.getSession()).thenReturn(session);
        when(session.getServletContext()).thenReturn(context);
    }

    @BeforeEach
    void setUp() {
        userDTO = UserDTO.builder()
                .setUsername("Admin")
                .setPassword("12345678")
                .setBan(false)
                .setAdmin(false)
                .setLocale(Localization.RU.getLocale())
                .build();
    }

    @DisplayName("Session Created")
    @Test
    void whenSessionCreatedSetInterval() {
        new SessionListener().sessionCreated(event);
        verify(session).setMaxInactiveInterval(20 * 60);
    }

    @DisplayName("Session Destroyed")
    @Test
    void whenSessionDestroyedReturnEmptuListOfLoggedUsers() {
        HashSet<UserDTO> loggedUsers = new HashSet<UserDTO>();
        loggedUsers.add(userDTO);
        when(context.getAttribute(ContextAttribute.LOGGED_USERS.getAttribute())).thenReturn(loggedUsers);
        when(session.getAttribute("user")).thenReturn(userDTO);

        new SessionListener().sessionDestroyed(event);
        assertTrue(loggedUsers.isEmpty());
        verify(context).setAttribute(ContextAttribute.LOGGED_USERS.getAttribute(), loggedUsers);
    }
}
