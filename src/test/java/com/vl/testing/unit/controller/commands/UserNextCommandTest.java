package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.UserNextCommand;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UserNextCommandTest {
    private static Command command = new UserNextCommand();
    private static HttpServletRequest request = mock(HttpServletRequest.class);
    private static HttpSession session = mock(HttpSession.class);
    private static String method = "Get";

    @Test
    void whenExecuteReturnString() {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("subject")).thenReturn("Subject");
        assertEquals(command.execute(request, method), "/WEB-INF/user/usernext.jsp");
    }
}
