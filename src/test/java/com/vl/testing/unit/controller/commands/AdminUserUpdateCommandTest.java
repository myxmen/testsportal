package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminUserUpdateCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminUserUpdateCommandTest {
    private static UserServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static UserDTO userDTO;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(UserServiceImpl.class);
        command = new AdminUserUpdateCommand(service);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        userDTO = null;
        method = "Post";
        mrw = new MockedRequestWrapper(request);
        userDTO = UserDTO.builder()
                .setId(3l)
                .setUsername("User")
                .setPassword("12345678")
                .setBan(false)
                .setAdmin(false)
                .setLocale(Localization.RU.getLocale())
                .build();
        mrw.setAttribute("id", Long.toString(userDTO.getId()));
        when(session.getAttribute("user")).thenReturn(UserDTO.builder().setId(4l).build());
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() {
        reset(session);
        when(session.getAttribute("user")).thenReturn(UserDTO.builder().setId(userDTO.getId()).setUsername("TestUser").build());

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
        reset(session);
    }

    @DisplayName("Return GET URL")
    @Test
    void whenExecuteReturnGetURL() throws ValidateException, RepositoryException {
        method = "Get";
        when(service.get(userDTO.getId())).thenReturn(Optional.of(userDTO));

        assertEquals("/WEB-INF/admin/adminuserupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(userDTO.getId(), (mrw.getAttribute("id"))),
                () -> assertEquals(userDTO, (mrw.getAttribute("userone")))
        );
    }

    @DisplayName("Return URL when choise null")
    @Test
    void whenExecuteReturnURLWhenChoiceNull() throws ValidateException, RepositoryException {
        userDTO.setBan(true);
        userDTO.setAdmin(true);
        when(service.get(userDTO.getId())).thenReturn(Optional.of(userDTO));
        //   mrw.setAttribute("choices", null);

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
        verify(service).updateAdminsFields(userDTO);
        assertAll("Set Attribute equals expected",
                () -> assertFalse(userDTO.isBan()),
                () -> assertFalse(userDTO.isAdmin())
        );
    }

    @DisplayName("Return URL when choice all")
    @Test
    void whenExecuteReturnURLWhenChoiceAll() throws ValidateException, RepositoryException {
        when(service.get(userDTO.getId())).thenReturn(Optional.of(userDTO));
        mrw.setAttribute("choice", new String[]{"ban", "admin"});

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
        verify(service).updateAdminsFields(userDTO);
        assertAll("Set Attribute equals expected",
                () -> assertTrue(userDTO.isBan()),
                () -> assertTrue(userDTO.isAdmin())
        );
    }

    @DisplayName("Return URL when choice ban")
    @Test
    void whenExecuteReturnURLWhenChoiceBan() throws ValidateException, RepositoryException {
        when(service.get(userDTO.getId())).thenReturn(Optional.of(userDTO));
        mrw.setAttribute("choice", new String[]{"ban"});

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
        verify(service).updateAdminsFields(userDTO);
        assertAll("Set Attribute equals expected",
                () -> assertTrue(userDTO.isBan()),
                () -> assertFalse(userDTO.isAdmin())
        );
    }

    @DisplayName("Return URL when choice admin")
    @Test
    void whenExecuteReturnURLWhenChoiceAdmin() throws ValidateException, RepositoryException {
        when(service.get(userDTO.getId())).thenReturn(Optional.of(userDTO));
        mrw.setAttribute("choice", new String[]{"admin"});

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
        verify(service).updateAdminsFields(userDTO);
        assertAll("Set Attribute equals expected",
                () -> assertFalse(userDTO.isBan()),
                () -> assertTrue(userDTO.isAdmin())
        );
    }

    @DisplayName("NoSuchElementException")
    @Test
    void whenExecuteReturnNoSuchElementException() throws ValidateException, RepositoryException {
        when(service.get(userDTO.getId()))
                .thenThrow(new NoSuchElementException("Test Error"));

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
    }

    @DisplayName("ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        when(service.get(userDTO.getId()))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.USER_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/admin/adminuserupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(userDTO.getId(), (mrw.getAttribute("id"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.USER_ID_IS_EMPTY.getMessage(), (mrw.getAttribute("errormessage")))
        );
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws ValidateException, RepositoryException {
        when(service.get(userDTO.getId()))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/adminuserupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(userDTO.getId(), (mrw.getAttribute("id"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", (mrw.getAttribute("errormessage")))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminUserUpdateCommand command = new AdminUserUpdateCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        UserServiceImpl service = null;
        try {
            field = command.getClass().getDeclaredField(all[1].getName());

            field.setAccessible(true);
            service = (UserServiceImpl) field.get(command);
            System.out.println(service);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(UserServiceImpl.class, service.getClass());
    }
}
