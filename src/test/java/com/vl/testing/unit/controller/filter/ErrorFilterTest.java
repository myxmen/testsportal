package com.vl.testing.unit.controller.filter;

import com.vl.testing.controller.filters.ErrorFilter;
import com.vl.testing.dto.UserDTO;
import org.junit.jupiter.api.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ErrorFilterTest {
    private static ErrorFilter errorFilter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain filterChain;
    private static HttpSession session;

    @BeforeAll
    static void initGlobal() {
        filterChain = mock(FilterChain.class);
        errorFilter = new ErrorFilter();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
    }

    @BeforeEach
    void setUp() {
        when(request.getSession()).thenReturn(session);
    }

    @AfterEach
    void resetSet() {
        reset(filterChain, session, request);
    }

    @DisplayName("Error null")
    @Test
    public void testFilterErrorsNull() throws IOException, ServletException {
        when(session.getAttribute("errorpage")).thenReturn(null);
        when(session.getAttribute("errormessage")).thenReturn(null);
        when(session.getAttribute("errordto")).thenReturn(null);

        errorFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
    }

    @DisplayName("Errorpage is present")
    @Test
    public void testFilterErrorpageIsPresent() throws IOException, ServletException {
        String test = "Test";
        when(session.getAttribute("errorpage")).thenReturn(test);
        when(session.getAttribute("errormessage")).thenReturn(null);
        when(session.getAttribute("errordto")).thenReturn(null);

        errorFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
        verify(request, times(1)).setAttribute("error", test);
    }

    @DisplayName("Errormessage is present")
    @Test
    public void testFilterErrormessageIsPresent() throws IOException, ServletException {
        String test = "Test";
        when(session.getAttribute("errorpage")).thenReturn(null);
        when(session.getAttribute("errormessage")).thenReturn(test);
        when(session.getAttribute("errordto")).thenReturn(null);

        errorFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
        verify(request).setAttribute("errormessage", test);
    }

    @DisplayName("Errordto is present")
    @Test
    public void testFilterErrordtoIsPresent() throws IOException, ServletException {
        String test = "Test";
        UserDTO userDTO = mock(UserDTO.class);
        when(session.getAttribute("errorpage")).thenReturn(null);
        when(session.getAttribute("errormessage")).thenReturn(test);
        when(session.getAttribute("errordto")).thenReturn(userDTO);

        errorFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
        verify(request).setAttribute("errordto", userDTO);
    }
}
