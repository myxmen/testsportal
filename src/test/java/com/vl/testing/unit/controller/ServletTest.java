package com.vl.testing.unit.controller;

import com.vl.testing.controller.Servlet;
import com.vl.testing.controller.commands.Command;
import org.apache.logging.log4j.*;
import org.junit.jupiter.api.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

import static org.mockito.Mockito.*;

public class ServletTest {
    private static ServletConfig config;
    private static ServletContext context;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static RequestDispatcher dispatcher;

    @BeforeAll
    static void initGlobal() {
        config = mock(ServletConfig.class);
        context = mock(ServletContext.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        dispatcher = mock(RequestDispatcher.class);

    }

    @BeforeEach
    void setUp() {
        when(config.getServletContext()).thenReturn(context);
    }

    @AfterEach
    void resetSet() {
        reset(request, dispatcher, context);
    }

    @DisplayName("Init set")
    @Test
    void whenServletInitSetAttribute() {
        Servlet servlet = new Servlet();
        servlet.init(config);
        verify(context).setAttribute("loggedUsers", new HashSet<String>());
    }

    @DisplayName("doGet & doPost get Init Path")
    @Test
    public void whenDoGetDoPostGetInitPath() throws IOException, ServletException {
        when(request.getRequestURI()).thenReturn("/");
        when(request.getRequestDispatcher("/test/index.jsp")).thenReturn(dispatcher);
        Servlet servlet = getServlet();

        servlet.doGet(request, response);
        verify(dispatcher).forward(request, response);

        servlet.doPost(request, response);
        verify(dispatcher, atMost(2)).forward(request, response);
    }

    @DisplayName("doGet & doPost get empty path")
    @Test
    public void whenDoGetAndDoPastWithEmptyPathReturnLogout() throws IOException, ServletException {
        when(request.getRequestURI()).thenReturn("/error");
        when(request.getRequestDispatcher("/logout")).thenReturn(dispatcher);
        Servlet servlet = getServlet();

        servlet.doGet(request, response);
        verify(dispatcher).forward(request, response);

        servlet.doPost(request, response);
        verify(dispatcher, atMost(2)).forward(request, response);
    }

    @DisplayName("doGet & doPost get redirect")
    @Test
    public void whenDoPostReturnRedirect() throws IOException, ServletException {
        when(request.getRequestURI()).thenReturn("/next");
        Servlet servlet = getServlet();

        servlet.doGet(request, response);
        verify(response).sendRedirect("/test/next.jsp");

        servlet.doPost(request, response);
        verify(response, atMost(2)).sendRedirect("/test/next.jsp");
    }

    public Servlet getServlet() {
        Servlet servlet = new Servlet();
        servlet.init(config);
        Field[] all = servlet.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        Map<String, Command> commands = new HashMap<>();

        commands.put("/",
                new TestIndexCommand());
        commands.put("/next",
                new TestRedirectCommand());

        try {
            field1 = servlet.getClass().getDeclaredField(all[2].getName());

            field1.setAccessible(true);

            field1.set(servlet, commands);
            System.out.println(commands);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return servlet;
    }

    public class TestIndexCommand implements Command {
        private final Logger LOGGER = LogManager.getLogger(TestIndexCommand.class);

        @Override
        public String execute(HttpServletRequest request, String method) {
            LOGGER.info("Return Index Path");
            return "/test/index.jsp";
        }
    }

    public class TestRedirectCommand implements Command {
        private final Logger LOGGER = LogManager.getLogger(TestRedirectCommand.class);

        @Override
        public String execute(HttpServletRequest request, String method) {
            LOGGER.info("Return Redirect Path");
            return "redirect:/test/next.jsp";
        }
    }
}
