package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminNextCommand;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class AdminNextCommandTest {
    private static Command command = new AdminNextCommand();
    private static HttpServletRequest request = mock(HttpServletRequest.class);
    private static String method = "Get";

    @Test
    void whenExecuteReturnString() {
        when(request.getParameter("id")).thenReturn("12");
        assertEquals(command.execute(request, method), "/WEB-INF/admin/adminnext.jsp");
    }
}
