package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminQuestionDeleteCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.*;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminQuestionDeleteCommandTest {
    private static QuestionServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static QuestionDTO questionDTO;
    private static Long testId = 44l;
    private static Long questionId = 102l;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(QuestionServiceImpl.class);
        command = new AdminQuestionDeleteCommand(service);
        request = mock(HttpServletRequest.class);
        questionDTO = mock(QuestionDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Post";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("testid", Long.toString(testId));
        mrw.setAttribute("questionid", Long.toString(questionId));
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Get Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException, RepositoryException {
        method = "Get";
        when(service.get(eq(questionId))).thenReturn(Optional.of(questionDTO));

        assertEquals("/WEB-INF/admin/adminquestiondelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(questionDTO, (mrw.getAttribute("questionone")))
        );
    }

    @DisplayName("Get Return RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws ValidateException, RepositoryException {
        method = "Get";
        when(service.get(eq(questionId)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/adminquestiondelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("Get Return ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        method = "Get";
        when(service.get(eq(questionId)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.QUESTION_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/admin/adminquestiondelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.QUESTION_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("Return NumberFormatException")
    @Test
    void whenExecuteReturnNumberFormatException() {
        mrw.setAttribute("questionid", "Test");

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("Return POST URL")
    @Test
    void whenExecuteReturnPostURL() throws ValidateException, RepositoryException {

        assertEquals("redirect:/admin/question?testid=" + testId, command.execute(mrw, method));
        verify(service).delete(questionId);
    }

    @DisplayName("Post return NumberFormatException")
    @Test
    void whenExecutePostReturnNumberFormatException() {
        mrw.setAttribute("testid", "Test");

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("POST Return RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {

        when(service.delete(questionId))
                .thenThrow(new RepositoryException("Test Error"));
        when(service.get(eq(questionId))).thenReturn(Optional.of(questionDTO));

        assertEquals("/WEB-INF/admin/adminquestiondelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage")),
                () -> assertEquals(questionDTO, mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("POST Return ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        when(service.delete(questionId))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.QUESTION_ID_IS_EMPTY, Error.ERROR_PAGE)));
        when(service.get(eq(questionId))).thenReturn(Optional.of(questionDTO));

        assertEquals("/WEB-INF/admin/adminquestiondelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.QUESTION_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage")),
                () -> assertEquals(questionDTO, mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminQuestionDeleteCommand command = new AdminQuestionDeleteCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        QuestionServiceImpl service = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            service = (QuestionServiceImpl) field1.get(command);
            System.out.println(service);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(QuestionServiceImpl.class, service.getClass());
    }

}
