package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.UserCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.PassedTestServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

public class UserCommandTest {
    private static PassedTestServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static UserDTO userDTO;
    private static TestDTO existingTestDTO;
    private static PassedTestDTO existingPassedTestDTO;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(PassedTestServiceImpl.class);
        command = new UserCommand(service);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        method = "Get";
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw = new MockedRequestWrapper(request);
        userDTO = UserDTO.builder()
                .setId(3l)
                .setUsername("Admin")
                .setPassword("12345678")
                .setBan(false)
                .setAdmin(false)
                .setLocale(Localization.RU.getLocale())
                .build();
        existingTestDTO = TestDTO.builder()
                .setId(4l)
                .setSubjectId(2l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        existingPassedTestDTO = PassedTestDTO.builder()
                .setId(2l)
                .setTest(existingTestDTO)
                .setResult(75)
                .build();
        when(session.getAttribute("user")).thenReturn(userDTO);
        mrw.setAttribute("page", "3");
        mrw.setAttribute("pagination", "5");
        mrw.setAttribute("sort", "desc");
        mrw.setAttribute("sortcolumn", "test_column");
    }

    @DisplayName("Return URL when User null")
    @Test
    void whenExecuteWithNullUserReturnURLLogout() {
        when(session.getAttribute("user")).thenReturn(null);
        assertEquals("redirect:/logout", command.execute(mrw, method));
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException, RepositoryException {
        List<PassedTestDTO> all = List.of(existingPassedTestDTO, existingPassedTestDTO);
        when(service.getNumberOfRows(eq(userDTO))).thenReturn(26);
        when(service.getPage(eq(userDTO), any(Pagination.class))).thenReturn(all);
        assertEquals("/WEB-INF/user/userbasis.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(3, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(6, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("desc", (mrw.getAttribute("sort"))),
                () -> assertEquals("test", (mrw.getAttribute("sortcolumn"))),
                () -> assertEquals(2, ((List<PassedTestDTO>) mrw.getAttribute("passtests")).size()),
                () -> assertNull((mrw.getAttribute("error"))),
                () -> assertNull((mrw.getAttribute("errormessage")))
        );
    }

    @DisplayName("getNumberOfRows ValidateExeption")
    @Test
    void whenExecuteGetNumberOfRowsReturnValidateExeption() throws ValidateException, RepositoryException {
        List<PassedTestDTO> all = List.of(existingPassedTestDTO, existingPassedTestDTO);
        when(service.getPage(eq(userDTO), any(Pagination.class))).thenReturn(all);
        when(service.getNumberOfRows(userDTO))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.USER_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/user/userbasis.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(0, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("desc", (mrw.getAttribute("sort"))),
                () -> assertEquals("test", (mrw.getAttribute("sortcolumn"))),
                () -> assertEquals(2, ((List<PassedTestDTO>) mrw.getAttribute("passtests")).size()),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.USER_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("getPage ValidateExeption")
    @Test
    void whenExecuteGetPageReturnValidateExeption() throws ValidateException, RepositoryException {
        when(service.getNumberOfRows(eq(userDTO))).thenReturn(26);
        when(service.getPage(eq(userDTO), any(Pagination.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/user/userbasis.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(3, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(6, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("desc", (mrw.getAttribute("sort"))),
                () -> assertEquals("test", (mrw.getAttribute("sortcolumn"))),
                () -> assertNull( mrw.getAttribute("passtests")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.PAGINATION_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("getPage RepositoryException")
    @Test
    void whenExecuteGetPageReturnRepositoryException() throws ValidateException, RepositoryException {
        when(service.getNumberOfRows(userDTO)).thenReturn(26);
        when(service.getPage(eq(userDTO), any(Pagination.class)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/user/userbasis.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(3, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(6, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("desc", (mrw.getAttribute("sort"))),
                () -> assertEquals("test", (mrw.getAttribute("sortcolumn"))),
                () -> assertNull( mrw.getAttribute("passtests")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("getNumberOfRows RepositoryException")
    @Test
    void whenExecuteGetNumberOfRowsReturnRepositoryException() throws ValidateException, RepositoryException {
        List<PassedTestDTO> all = List.of(existingPassedTestDTO, existingPassedTestDTO);
        when(service.getPage(eq(userDTO), any(Pagination.class))).thenReturn(all);
        when(service.getNumberOfRows(eq(userDTO)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/user/userbasis.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(0, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("desc", (mrw.getAttribute("sort"))),
                () -> assertEquals("test", (mrw.getAttribute("sortcolumn"))),
                () -> assertEquals(2, ((List<PassedTestDTO>) mrw.getAttribute("passtests")).size()),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        UserCommand command = new UserCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        PassedTestServiceImpl service = null;
        try {
            field = command.getClass().getDeclaredField(all[2].getName());

            field.setAccessible(true);
            service = (PassedTestServiceImpl) field.get(command);
            System.out.println(service);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(PassedTestServiceImpl.class, service.getClass());
    }
}
