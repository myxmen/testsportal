package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.CommandUtility;
import com.vl.testing.controller.commands.impl.SignupCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.util.TextLocale;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SignupCommandTest {
    private static UserServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static UserDTO userDTO;
    private static UserDTO errorUserDTO;
    private static TextLocale textLocale;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(UserServiceImpl.class);
        command = new SignupCommand(service);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        method = "Get";
        textLocale = mock(TextLocale.class);
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw=null;
        userDTO=null;
        mrw = new MockedRequestWrapper(request);
        userDTO = UserDTO.builder()
                .setId(3l)
                .setUsername("Admin")
                .setPassword("12345678")
                .setBan(false)
                .setAdmin(false)
                .setLocale(Localization.RU.getLocale())
                .build();
        errorUserDTO = UserDTO.builder()
                .setUsername(userDTO.getUsername())
                .setPassword(userDTO.getPassword())
                .build();
        mrw.setAttribute("login", userDTO.getUsername());
        mrw.setAttribute("password", userDTO.getPassword());
        mrw.setAttribute("repeatpass", userDTO.getPassword());
        mrw.setAttribute("userlang", Localization.getNameByLocale(userDTO.getLocale()));
    }

    @DisplayName("Return URL when User logged")
    @Test
    void whenExecuteReturnURLIfLoggedUser() throws RepositoryException, ValidateException {
        when(service.create(UserDTO.builder()
                .setUsername(userDTO.getUsername())
                .setPassword(userDTO.getPassword())
                .setBan(false)
                .setAdmin(false)
                .setLocale(userDTO.getLocale())
                .build()))
                .thenReturn(userDTO);
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() -> CommandUtility.checkUserIsLogged(mrw, userDTO)).thenReturn(true);
            assertEquals("redirect:/signup.jsp", command.execute(mrw, method));
        }
        verify(session, atMost(6)).setAttribute("errorpage", Error.LOGGED_USER.getMessage());
        verify(session, atMost(6)).setAttribute("errormessage", userDTO.getUsername());
        verify(session, atMost(6)).setAttribute("errordto", userDTO);
    }

    @DisplayName("Return URL when User don`t has Id")
    @Test
    void whenExecuteReturnURLIfUserDontHasId() throws RepositoryException, ValidateException {
        userDTO.setId(null);
        when(service.create(eq(userDTO))).thenReturn(userDTO);
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() -> CommandUtility.checkUserIsLogged(mrw, userDTO)).thenReturn(false);
            assertEquals("/signup.jsp", command.execute(mrw, method));
        }
    }

    @DisplayName("Return URL when User`s passwords don`t equals")
    @Test
    void whenExecuteReturnURLIfUsersPasswordsDontEqual() {
        userDTO.setId(null);
        mrw.setAttribute("repeatpass", "1234");

        assertEquals("redirect:/signup.jsp", command.execute(mrw, method));
        verify(session, atMost(6)).setAttribute("errorpage", Error.INVALID_PASSWORD.getMessage());
        verify(session, atMost(6)).setAttribute("errormessage", userDTO.getPassword());
        verify(session, atMost(6)).setAttribute("errordto", errorUserDTO);
    }

    @DisplayName("Return URL when User is created")
    @Test
    void whenExecuteReturnURLCreatedUser() throws RepositoryException, ValidateException {
        when(service.create(any(UserDTO.class))).thenReturn(userDTO);
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() -> CommandUtility.checkUserIsLogged(mrw, userDTO)).thenReturn(false);
            try (MockedStatic<TextLocale> mockedLocale = mockStatic(TextLocale.class)) {
                mockedLocale.when(() -> TextLocale.getCurrentInstance(mrw)).thenReturn(textLocale);
                assertEquals("redirect:/user", command.execute(mrw, method));
                mockedStatic.verify(() -> CommandUtility.setUserRole(mrw, userDTO));
            }
        }
        verify(textLocale, atMost(6)).setLocale(Localization.RU.getLocale());
        verify(session, atMost(6)).setAttribute("language", userDTO.getLocale());
    }

    @DisplayName("ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        userDTO.setId(null);
        when(service.create(userDTO))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.USERDTO_IS_EMPTY, Error.INVALID_LOGIN)));

        assertEquals(command.execute(mrw, method), "redirect:/signup.jsp");
        verify(session, atMost(6)).setAttribute("errorpage", Error.INVALID_LOGIN);
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws ValidateException, RepositoryException {
        userDTO.setId(null);
        System.out.println(userDTO);
        when(service.create(eq(userDTO)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals(command.execute(mrw, method), "redirect:/signup.jsp");
        verify(session, atMost(6)).setAttribute("errorpage", Error.INVALID_LOGIN.getMessage());
        verify(session, atMost(6)).setAttribute("errormessage", userDTO.getUsername());
        verify(session, atMost(6)).setAttribute("errordto", errorUserDTO);
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        SignupCommand command = new SignupCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        UserServiceImpl service = null;
        try {
            field = command.getClass().getDeclaredField(all[1].getName());

            field.setAccessible(true);
            service = (UserServiceImpl) field.get(command);
            System.out.println(service);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(UserServiceImpl.class, service.getClass());
    }
}
