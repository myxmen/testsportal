package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminTestDeleteCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.*;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

public class AdminTestDeleteCommandTest {
    private static TestServiceImpl testService;
    private static QuestionServiceImpl questionService;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static TestDTO testDTO;
    private static Long subId = 3l;
    private static String subjectId = Long.toString(subId);
    private static Long tstId = 44l;
    private static String testId = Long.toString(tstId);
    private static Integer numberOfRows = 55;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() throws ValidateException, RepositoryException {
        questionService = mock(QuestionServiceImpl.class);
        testService = mock(TestServiceImpl.class);
        command = new AdminTestDeleteCommand(testService, questionService);
        request = mock(HttpServletRequest.class);
        testDTO = mock(TestDTO.class);
        when(questionService.getNumberOfRows(testDTO)).thenReturn(numberOfRows);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("subjectid", subjectId);
        mrw.setAttribute("testid", testId);
    }

    @AfterEach
    void resetSet() {
          reset(testService);
    }

    @DisplayName("Return GET URL")
    @Test
    void whenExecuteReturnGetURL() throws ValidateException, RepositoryException {
        when(testService.get(tstId)).thenReturn(Optional.of(testDTO));

        assertEquals("/WEB-INF/admin/admintestdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertEquals(testDTO, (mrw.getAttribute("testone"))),
                () -> assertEquals(numberOfRows, (mrw.getAttribute("questions")))
        );
    }

    @DisplayName("Return GET NumberFormatException")
    @Test
    void whenExecuteReturnGetNumberFormatException() {
        mrw.setAttribute("subjectid", "Test");

        assertEquals("redirect:/admin", command.execute(mrw, method));
    }

    @DisplayName("Return POST URL")
    @Test
    void whenExecuteReturnPostURL() throws ValidateException, RepositoryException {
        method = "Post";

        assertEquals("redirect:/admin/test?subjectid=" + subjectId, command.execute(mrw, method));
        verify(testService).delete(tstId);
    }

    @DisplayName("Return POST ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        method = "Post";
        when(testService.delete(tstId))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TEST_ID_IS_EMPTY, Error.ERROR_PAGE)));
        when(testService.get(tstId)).thenReturn(Optional.of(testDTO));

        assertEquals("/WEB-INF/admin/admintestdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.TEST_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage")),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertEquals(testDTO, (mrw.getAttribute("testone"))),
                () -> assertEquals(numberOfRows, (mrw.getAttribute("questions")))
        );
    }

    @DisplayName("Return POST RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {
        method = "Post";
        when(testService.delete(tstId))
                .thenThrow(new RepositoryException("Test Error"));
        when(testService.get(tstId)).thenReturn(Optional.of(testDTO));

        assertEquals("/WEB-INF/admin/admintestdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage")),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertEquals(testDTO, (mrw.getAttribute("testone"))),
                () -> assertEquals(numberOfRows, (mrw.getAttribute("questions")))
        );
    }

    @DisplayName("Return GET ValidateExeption")
    @Test
    void whenExecuteReturnGetValidateExeption() throws ValidateException, RepositoryException {
        when(testService.get(tstId))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TEST_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/admin/admintestdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.TEST_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage")),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertNull(mrw.getAttribute("testone")),
                () -> assertNull(mrw.getAttribute("questions"))
        );
    }

    @DisplayName("Return GET RepositoryException")
    @Test
    void whenExecuteReturnGetRepositoryException() throws ValidateException, RepositoryException {
        when(testService.get(tstId))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/admintestdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage")),
                () -> assertEquals(subId, (mrw.getAttribute("subjectid"))),
                () -> assertEquals(tstId, (mrw.getAttribute("testid"))),
                () -> assertNull(mrw.getAttribute("testone")),
                () -> assertNull(mrw.getAttribute("questions"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminTestDeleteCommand command = new AdminTestDeleteCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Field field2;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        TestServiceImpl testService = null;
        QuestionServiceImpl questionService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            testService = (TestServiceImpl) field1.get(command);
            System.out.println(testService);

            field2 = command.getClass().getDeclaredField(all[2].getName());

            field2.setAccessible(true);
            questionService = (QuestionServiceImpl) field2.get(command);
            System.out.println(questionService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(QuestionServiceImpl.class, questionService.getClass());
        assertEquals(TestServiceImpl.class, testService.getClass());
    }
}
