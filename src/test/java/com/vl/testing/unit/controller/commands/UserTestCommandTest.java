package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.UserTestCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UserTestCommandTest {
    private static TestServiceImpl testService;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static SubjectDTO subjectDTO;
    private static TestDTO existingTestDTO;
    private static MockedRequestWrapper mrw;
    private static Integer rows;
    private static String id;

    @BeforeAll
    static void initGlobal() {
        testService = mock(TestServiceImpl.class);
        command = new UserTestCommand(testService);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        subjectDTO = mock(SubjectDTO.class);
        method = "Get";
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw = new MockedRequestWrapper(request);
        existingTestDTO = TestDTO.builder()
                .setId(4l)
                .setSubjectId(3l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        rows = 23;
        id = "3";
    }

    @AfterEach
    void resetSet() {
        reset(testService);
    }

    @DisplayName("Return URL when Subject is null")
    @Test
    void whenExecuteSubjectIsNullReturnURLSubject() {
        method = "Post";
        when(session.getAttribute("subject")).thenReturn(null);

        assertEquals("redirect:/user/subject", command.execute(mrw, method));
    }

    @DisplayName("Return URL when Test is selected")
    @Test
    void whenExecuteTestIsSelectedReturnURLPretest() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("id", id);
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        when(testService.get(eq(Long.parseLong(id)))).thenReturn(Optional.of(existingTestDTO));

        assertEquals("redirect:/user/pretest", command.execute(mrw, method));
        verify(session).setAttribute("subject", subjectDTO);
        verify(subjectDTO).setTests(existingTestDTO);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException, RepositoryException {
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        List<TestDTO> page = List.of(existingTestDTO, existingTestDTO);
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(rows);
        when(testService.getPage(eq(subjectDTO), any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/user/usertest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("test"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(5, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("test", mrw.getAttribute("sortcolumn"))
        );
    }

    @DisplayName("Return URL Get Test return RepositoryException")
    @Test
    void whenExecuteGetTestReturnURLwithRepositoryException() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("id", id);
        when(testService.get(eq(Long.parseLong(id)))).thenThrow(new RepositoryException("Test Error"));
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        List<TestDTO> page = List.of(existingTestDTO, existingTestDTO);
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(rows);
        when(testService.getPage(eq(subjectDTO), any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/user/usertest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("test"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(5, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("test", mrw.getAttribute("sortcolumn")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return URL Get Test return ValidateExeption")
    @Test
    void whenExecuteGetTestReturnURLwithValidateExeption() throws ValidateException, RepositoryException {
        method = "Post";
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        mrw.setAttribute("id", id);
        when(testService.get(eq(Long.parseLong(id))))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TEST_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("redirect:/user", command.execute(mrw, method));
    }

    @DisplayName("Return URL Get Test return NumberFormatException")
    @Test
    void whenExecuteGetTestReturnURLwithVNumberFormatException() throws ValidateException, RepositoryException {
        method = "Post";
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        mrw.setAttribute("id", id);
        when(testService.get(eq(Long.parseLong(id))))
                .thenThrow(new NumberFormatException("NumberFormatException Error"));

        assertEquals("redirect:/user", command.execute(mrw, method));
    }

    @DisplayName("Return URL GetNumberOfRows return RepositoryException")
    @Test
    void whenExecuteGetNumberOfRowsReturnURLwithRepositoryException() throws ValidateException, RepositoryException {
        when(testService.getNumberOfRows(subjectDTO))
                .thenThrow(new RepositoryException("Test Error"));
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        List<TestDTO> page = List.of(existingTestDTO, existingTestDTO);
        when(testService.getPage(eq(subjectDTO), any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/user/usertest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("test"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(0, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("test", mrw.getAttribute("sortcolumn")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return URL GetNumberOfRows return ValidateExeption")
    @Test
    void whenExecuteGetNumberOfRowsReturnURLwithValidateExeption() throws ValidateException, RepositoryException {
        when(testService.getNumberOfRows(subjectDTO))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.SUBJECT_ID_IS_EMPTY, Error.ERROR_PAGE)));
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        List<TestDTO> page = List.of(existingTestDTO, existingTestDTO);
        when(testService.getPage(eq(subjectDTO), any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/user/usertest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("test"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(0, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("test", mrw.getAttribute("sortcolumn")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(getValidateException(ExceptionMessage.SUBJECT_ID_IS_EMPTY, Error.ERROR_PAGE), mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return URL GetPage return RepositoryException")
    @Test
    void whenExecuteGetPageReturnURLwithRepositoryException() throws ValidateException, RepositoryException {
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(rows);
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        when(testService.getPage(eq(subjectDTO), any(Pagination.class))).thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/user/usertest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(null, (mrw.getAttribute("test"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(5, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("test", mrw.getAttribute("sortcolumn")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return URL GetPage return ValidateExeption")
    @Test
    void whenExecuteGetPageReturnURLwithValidateExeption() throws ValidateException, RepositoryException {
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(rows);
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        when(testService.getPage(eq(subjectDTO), any(Pagination.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/user/usertest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(null, (mrw.getAttribute("test"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(5, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("test", mrw.getAttribute("sortcolumn")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE), mrw.getAttribute("errormessage"))
        );
    }


    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        UserTestCommand command = new UserTestCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        TestServiceImpl testService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            testService = (TestServiceImpl) field1.get(command);
            System.out.println(testService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(TestServiceImpl.class, testService.getClass());
    }

}
