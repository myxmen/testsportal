package com.vl.testing.unit.controller.util;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.persistence.enums.Sort;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class PaginationTest {
    private static String sort;
    private static String sortColumn;
    private static String defaultColumn;

    private static Integer currentPage;
    private static Integer recordsPerPage;

    @BeforeAll
    static void initGlobal() {
        sort = Sort.ASC.getSort();
        sortColumn = "subject";
        defaultColumn = "test";
        currentPage = 2;
        recordsPerPage = 5;
    }

    @DisplayName("Equals and Hash")
    @Test
    void whenCreatedPaginationReturnEqualAndHashUnit() {
        Pagination pagination = Pagination.builder()
                .setRows(23)
                .setCurrentPage(String.valueOf(currentPage))
                .setRecordsPerPage(String.valueOf(recordsPerPage))
                .setSort(sort)
                .setSortColumn(sortColumn)
                .setDefaultColumn(defaultColumn)
                .build();

        Pagination paginationTest = Pagination.builder()
                .setRows(23)
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("subject")
                .setDefaultColumn("test")
                .build();

        assertEquals(pagination, paginationTest);
        assertEquals(pagination.hashCode(), paginationTest.hashCode());
    }

    @DisplayName("Set Pagination extreme")
    @ParameterizedTest
    @MethodSource("testCases")
    void whenGetScoreReturnPost(final Integer rows, final String currentPage, final String recordsPerPage,
                                final String inSort, final String inSortColumn, final String inDefaultColumn,
                                final Integer outCurrentPage, final Integer startRows, final Integer outRecordsPerPage,
                                final Integer nOfPages, final String outSort, final String outSortColumn) {
        Pagination pagination = Pagination.builder()
                .setRows(rows)
                .setCurrentPage(currentPage)
                .setRecordsPerPage(recordsPerPage)
                .setSort(inSort)
                .setSortColumn(inSortColumn)
                .setDefaultColumn(inDefaultColumn)
                .build();
        System.out.println(pagination);

        assertAll("Set Attribute equals expected",
                () -> assertEquals(outCurrentPage, (pagination.getCurrentPage())),
                () -> assertEquals(startRows, (pagination.getStartRows())),
                () -> assertEquals(outRecordsPerPage, (pagination.getRecordsPerPage())),
                () -> assertEquals(nOfPages, (pagination.getNOfPages())),
                () -> assertEquals(outSort, (pagination.getSort())),
                () -> assertEquals(outSortColumn, (pagination.getSortColumn()))
        );
    }

    static Stream<Arguments> testCases() {

        return Stream.of(
                Arguments.of(23, "2", "10", Sort.ASC.getSort(), "subject", "test", 2, 10, 10, 3, "asc", "subject"),
                Arguments.of(23, "2", "10", Sort.ASC.getSort().toLowerCase(), "subject", "test", 2, 10, 10, 3, "asc", "subject"),
                Arguments.of(23, "2", "10", Sort.DESC.getSort(), "subject", "test", 2, 10, 10, 3, "desc", "subject"),
                Arguments.of(23, "2", "10", Sort.DESC.getSort().toLowerCase(), "subject", "test", 2, 10, 10, 3, "desc", "subject"),
                Arguments.of(23, "2", "10", "fail", "subject", "test", 2, 10, 10, 3, "asc", "subject"),
                Arguments.of(23, "2", "10", null, "subject", "test", 2, 10, 10, 3, "asc", "subject"),
                Arguments.of(23, "2", "10", Sort.ASC.getSort(), "fail", "test", 2, 10, 10, 3, "asc", "test"),
                Arguments.of(23, "2", "10", Sort.ASC.getSort(), null, "test", 2, 10, 10, 3, "asc", "test"),
                Arguments.of(0, "2", "10", Sort.ASC.getSort(), "subject", "test", 1, 0, 10, 0, "asc", "subject"),
                Arguments.of(23, null, "10", Sort.ASC.getSort(), "subject", "test", 1, 0, 10, 3, "asc", "subject"),
                Arguments.of(23, "test", "10", Sort.ASC.getSort(), "subject", "test", 1, 0, 10, 3, "asc", "subject"),
                Arguments.of(23, "-2", "10", Sort.ASC.getSort(), "subject", "test", 1, 0, 10, 3, "asc", "subject"),
                Arguments.of(23, "0", "10", Sort.ASC.getSort(), "subject", "test", 1, 0, 10, 3, "asc", "subject"),
                Arguments.of(23, "4", "10", Sort.ASC.getSort(), "subject", "test", 3, 20, 10, 3, "asc", "subject"),
                Arguments.of(23, "1", "10", Sort.ASC.getSort(), "subject", "test", 1, 0, 10, 3, "asc", "subject"),
                Arguments.of(23, "3", "10", Sort.ASC.getSort(), "subject", "test", 3, 20, 10, 3, "asc", "subject"),
                Arguments.of(23, "2", null, Sort.ASC.getSort(), "subject", "test", 2, 5, 5, 5, "asc", "subject"),
                Arguments.of(23, "2", "test", Sort.ASC.getSort(), "subject", "test", 2, 5, 5, 5, "asc", "subject"),
                Arguments.of(23, "2", "-10", Sort.ASC.getSort(), "subject", "test", 2, 5, 5, 5, "asc", "subject")
        );
    }
}
