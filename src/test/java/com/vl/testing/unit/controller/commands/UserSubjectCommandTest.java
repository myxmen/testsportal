package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.UserSubjectCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class UserSubjectCommandTest {
    private static SubjectServiceImpl subjectService;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static TestDTO testDTO;
    private static SubjectDTO existingSubjectDTO;
    private static MockedRequestWrapper mrw;
    private static Integer rows;
    private static String id;

    @BeforeAll
    static void initGlobal() {
        subjectService = mock(SubjectServiceImpl.class);
        command = new UserSubjectCommand(subjectService);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        testDTO = mock(TestDTO.class);
        method = "Get";
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw = new MockedRequestWrapper(request);
        existingSubjectDTO = SubjectDTO.builder()
                .setId(3l)
                .setSubject("Test")
                .setTests(testDTO)
                .build();
        rows = 23;
        id = "3";
    }

    @AfterEach
    void resetSet() {
        reset(subjectService);
    }

    @DisplayName("Return URL when Subject is selected")
    @Test
    void whenExecuteSubjectIsSelectedReturnURLTest() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("id", id);
        when(subjectService.get(eq(Long.parseLong(id)))).thenReturn(Optional.of(existingSubjectDTO));

        assertEquals("redirect:/user/test", command.execute(mrw, method));
        verify(session).setAttribute("subject", existingSubjectDTO);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException, RepositoryException {
        List<SubjectDTO> page = List.of(existingSubjectDTO, existingSubjectDTO);
        when(subjectService.getNumberOfRows()).thenReturn(rows);
        when(subjectService.getPage(any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/user/usersubject.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("subject"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(5, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("subject", mrw.getAttribute("sortcolumn"))
        );
    }

    @DisplayName("Return URL with empty Id")
    @Test
    void whenExecutePostMethodWithEmptyIDReturnURL() throws ValidateException, RepositoryException {
        method = "Post";
        id = "";
        mrw.setAttribute("id", id);
        List<SubjectDTO> page = List.of(existingSubjectDTO, existingSubjectDTO);
        when(subjectService.getNumberOfRows()).thenReturn(rows);
        when(subjectService.getPage(any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/user/usersubject.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("subject"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(5, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("subject", mrw.getAttribute("sortcolumn"))
        );
    }

    @DisplayName("Return URL Get Subject return RepositoryException")
    @Test
    void whenExecuteGetSubjectReturnURLwithRepositoryException() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("id", id);
        when(subjectService.get(eq(Long.parseLong(id)))).thenThrow(new RepositoryException("Test Error"));
        List<SubjectDTO> page = List.of(existingSubjectDTO, existingSubjectDTO);
        when(subjectService.getNumberOfRows()).thenReturn(rows);
        when(subjectService.getPage(any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/user/usersubject.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("subject"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(5, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("subject", mrw.getAttribute("sortcolumn")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return URL Get Subject return ValidateExeption")
    @Test
    void whenExecuteGetSubjectReturnURLwithValidateExeption() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("id", id);
        when(subjectService.get(eq(Long.parseLong(id))))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.SUBJECT_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("redirect:/user", command.execute(mrw, method));
    }

    @DisplayName("Return URL Get Subject return NumberFormatException")
    @Test
    void whenExecuteGetSubjectReturnURLwithNumberFormatException() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("id", id);
        when(subjectService.get(eq(Long.parseLong(id))))
                .thenThrow(new NumberFormatException("NumberFormatException Error"));

        assertEquals("redirect:/user", command.execute(mrw, method));
    }

    @DisplayName("Return URL GetNumberOfRows return RepositoryException")
    @Test
    void whenExecuteGetNumberOfRowsReturnURLwithRepositoryException() throws ValidateException, RepositoryException {
        when(subjectService.getNumberOfRows())
                .thenThrow(new RepositoryException("Test Error"));
        List<SubjectDTO> page = List.of(existingSubjectDTO, existingSubjectDTO);
        when(subjectService.getPage(any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/user/usersubject.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("subject"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(0, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("subject", mrw.getAttribute("sortcolumn")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return URL GetPage return RepositoryException")
    @Test
    void whenExecuteGetPageReturnURLwithRepositoryException() throws ValidateException, RepositoryException {
        when(subjectService.getPage(any(Pagination.class)))
                .thenThrow(new RepositoryException("Test Error"));
        when(subjectService.getNumberOfRows()).thenReturn(rows);

        assertEquals("/WEB-INF/user/usersubject.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(null, (mrw.getAttribute("subject"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(5, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("subject", mrw.getAttribute("sortcolumn")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return URL GetPage return ValidateExeption")
    @Test
    void whenExecuteGetPageReturnURLwithValidateExeption() throws ValidateException, RepositoryException {
        when(subjectService.getPage(any(Pagination.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE)));
        when(subjectService.getNumberOfRows()).thenReturn(rows);

        assertEquals("redirect:/user", command.execute(mrw, method));
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        UserSubjectCommand command = new UserSubjectCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        SubjectServiceImpl subjectService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            subjectService = (SubjectServiceImpl) field1.get(command);
            System.out.println(subjectService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(SubjectServiceImpl.class, subjectService.getClass());
    }

}
