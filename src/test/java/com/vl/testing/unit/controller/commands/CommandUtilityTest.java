package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.impl.CommandUtility;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.UserDTO;
import org.junit.jupiter.api.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static com.vl.testing.controller.enums.Localization.getNameByLocale;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CommandUtilityTest {
    private static HttpServletRequest request;
    private static HttpSession session;
    private static ServletContext servletContext;
    private static UserDTO userDTO;
    private static UserDTO createdUser;

    @BeforeAll
    static void initGlobal() {
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        servletContext = mock(ServletContext.class);
        when(request.getSession()).thenReturn(session);
        when(session.getServletContext()).thenReturn(servletContext);
    }

    @BeforeEach
    void setUp() {
        userDTO = UserDTO.builder()
                .setId(3l)
                .setUsername("User")
                .setPassword("12345678")
                .setBan(false)
                .setAdmin(false)
                .setLocale(Localization.RU.getLocale())
                .setLoginTime(LocalDateTime.now())
                .setPassedTests(List.of(mock(PassedTestDTO.class)))
                .build();
        createdUser = UserDTO.builder()
                .setId(userDTO.getId())
                .setUsername("TestUser")
                .setPassword("11111111")
                .setBan(userDTO.isBan())
                .setAdmin(userDTO.isAdmin())
                .setLocale(Localization.EN.getLocale())
                .build();
        when(session.getAttribute("user")).thenReturn(userDTO);
    }

    @DisplayName("SetUserRole")
    @Test
    void whenSetUserRoleSetAttribute() {
        CommandUtility.setUserRole(request, userDTO);
        userDTO.setPassword("");

        verify(session).setAttribute("user", userDTO);
    }

    @DisplayName("СheckUserIsLogged is true")
    @Test
    void whenСheckUserIsLoggedReturnTrue() {
        HashSet<UserDTO> loggedUsers = new HashSet<>();
        loggedUsers.add(userDTO);
        when(servletContext.getAttribute(eq("loggedUsers"))).thenReturn(loggedUsers);

        assertTrue(CommandUtility.checkUserIsLogged(request, userDTO));
    }

    @DisplayName("СheckUserIsLogged is false")
    @Test
    void whenСheckUserIsLoggedReturnFalse() {
        HashSet<UserDTO> loggedUsers = new HashSet<>();
        when(servletContext.getAttribute(eq("loggedUsers"))).thenReturn(loggedUsers);

        assertFalse(CommandUtility.checkUserIsLogged(request, userDTO));
        assertTrue(loggedUsers.contains(userDTO));
        verify(servletContext).setAttribute("loggedUsers", loggedUsers);
    }

    @DisplayName("GetNewSessionUserDTO")
    @Test
    void whenGetNewSessionUserDTOReturnUserDTO() {
        assertEquals(CommandUtility
                        .getNewSessionUserDTO(request,
                                createdUser.getUsername(), createdUser.getPassword(), getNameByLocale(createdUser.getLocale())),
                createdUser);
    }

    @DisplayName("UpdateLoggedUser return true")
    @Test
    void whenUpdateLoggedUserReturnTrue() {
        HashSet<UserDTO> loggedUsers = new HashSet<>();
        loggedUsers.add(userDTO);
        when(request.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttribute(eq("loggedUsers"))).thenReturn(loggedUsers);

        assertTrue(CommandUtility.updateLoggedUser(request, Optional.of(createdUser)));
        assertTrue(loggedUsers.contains(createdUser));
        verify(servletContext).setAttribute("loggedUsers", loggedUsers);
        verify(session).setAttribute("user", createdUser);
    }

    @DisplayName("UpdateLoggedUser return false")
    @Test
    void whenUpdateLoggedUserReturnFalse() {
        when(request.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttribute(eq("loggedUsers"))).thenReturn(new HashSet<UserDTO>());

        assertFalse(CommandUtility.updateLoggedUser(request, Optional.of(createdUser)));
    }
}
