package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class AdminCommandTest {
    private static UserServiceImpl userService;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static ServletContext servletContext;
    private static String method;
    private static MockedRequestWrapper mrw;
    private static UserDTO userDTO;

    @BeforeAll
    static void initGlobal() {
        userService = mock(UserServiceImpl.class);
        command = new AdminCommand(userService);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        servletContext = mock(ServletContext.class);
        userDTO = mock(UserDTO.class);
        method = "Get";
        when(request.getSession()).thenReturn(session);
        when(session.getServletContext()).thenReturn(servletContext);
    }

    @BeforeEach
    void setUp() {
        mrw = new MockedRequestWrapper(request);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException {
        HashSet<UserDTO> loggedUsers = new HashSet<>();
        loggedUsers.add(userDTO);
        List<UserDTO> page = new ArrayList<>(loggedUsers);
        when(servletContext.getAttribute(eq("loggedUsers"))).thenReturn(loggedUsers);
        when(userService.getNumberOfRowsLoggedUsers(anyList())).thenReturn(loggedUsers.size());
        when(userService.getPageLoggedUsers(anyList(), any(Pagination.class))).thenReturn(page);

        assertEquals("/WEB-INF/admin/adminbasis.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(page, (mrw.getAttribute("users"))),
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, mrw.getAttribute("pagination")),
                () -> assertEquals(0, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", mrw.getAttribute("sort")),
                () -> assertEquals("user", mrw.getAttribute("sortcolumn"))
        );
    }

    @DisplayName("Return ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException {
        HashSet<UserDTO> loggedUsers = new HashSet<>();
        loggedUsers.add(userDTO);
        when(servletContext.getAttribute(eq("loggedUsers"))).thenReturn(loggedUsers);
        when(userService.getNumberOfRowsLoggedUsers(anyList())).thenReturn(loggedUsers.size());
        when(userService.getPageLoggedUsers(anyList(), any(Pagination.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("redirect:/admin", command.execute(mrw, method));
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminCommand command = new AdminCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        UserServiceImpl userService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            userService = (UserServiceImpl) field1.get(command);
            System.out.println(userService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(UserServiceImpl.class, userService.getClass());
    }
}
