package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.CommandUtility;
import com.vl.testing.controller.commands.impl.UserEditCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UserEditCommandTest {
    private static UserServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static UserDTO userDTO;
    private static UserDTO errorUserDTO;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(UserServiceImpl.class);
        command = new UserEditCommand(service);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        userDTO = null;
        method = "Post";
        mrw = new MockedRequestWrapper(request);
        userDTO = UserDTO.builder()
                .setId(3l)
                .setUsername("User")
                .setPassword("12345678")
                .setBan(false)
                .setAdmin(false)
                .setLocale(Localization.RU.getLocale())
                .build();
        errorUserDTO = UserDTO.builder()
                .setUsername(userDTO.getUsername())
                .setPassword(userDTO.getPassword())
                .build();
        mrw.setAttribute("login", userDTO.getUsername());
        mrw.setAttribute("password", userDTO.getPassword());
        mrw.setAttribute("repeatpass", userDTO.getPassword());
        mrw.setAttribute("userlang", Localization.getNameByLocale(userDTO.getLocale()));
    }

    @AfterEach
    void resetSet() {
        reset(session);
    }

    @DisplayName("Return GET URL")
    @Test
    void whenExecuteReturnGetURL() {
        method = "Get";

        assertEquals("/WEB-INF/user/useredit.jsp", command.execute(mrw, method));
    }

    @DisplayName("Return URL when User`s passwords don`t equals")
    @Test
    void whenExecuteReturnURLIfUsersPasswordsDontEqual() {
        mrw.setAttribute("repeatpass", "1234");

        assertEquals("redirect:/user/edit", command.execute(mrw, method));
        verify(session).setAttribute("errorpage", Error.INVALID_PASSWORD.getMessage());
        verify(session).setAttribute("errormessage", userDTO.getPassword());
        verify(session).setAttribute("errordto", errorUserDTO);
    }

    @DisplayName("Return URL when User is edited")
    @Test
    void whenExecuteReturnURLEditedUser() throws RepositoryException, ValidateException {
        when(service.updateUsersFields(eq(userDTO))).thenReturn(Optional.of(userDTO));
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() ->
                    CommandUtility.getNewSessionUserDTO(mrw, userDTO.getUsername(), userDTO.getPassword(), Localization.getNameByLocale(userDTO.getLocale())))
                    .thenReturn(userDTO);
            mockedStatic.when(() -> CommandUtility.updateLoggedUser(mrw, Optional.of(userDTO))).thenReturn(true);
            assertEquals("redirect:/user", command.execute(mrw, method));
        }
    }

    @DisplayName("Return URL when User is empty")
    @Test
    void whenExecuteReturnURLEmptyUser() throws RepositoryException, ValidateException {
        when(service.updateUsersFields(eq(userDTO))).thenReturn(Optional.empty());
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() ->
                    CommandUtility.getNewSessionUserDTO(mrw, userDTO.getUsername(), userDTO.getPassword(), Localization.getNameByLocale(userDTO.getLocale())))
                    .thenReturn(userDTO);
            assertEquals("redirect:/user/edit", command.execute(mrw, method));
        }
        verify(session).setAttribute("errorpage", Error.ERROR_UPDATE.getMessage());
        verify(session).setAttribute("errormessage", userDTO.getUsername());
        verify(session).setAttribute("errordto", errorUserDTO);
    }

    @DisplayName("Return URL when User`s ID null")
    @Test
    void whenExecuteReturnURLUserIDNull() throws RepositoryException, ValidateException {
        userDTO.setId(null);
        when(service.updateUsersFields(userDTO)).thenReturn(Optional.of(userDTO));
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() ->
                    CommandUtility.getNewSessionUserDTO(mrw, userDTO.getUsername(), userDTO.getPassword(), Localization.getNameByLocale(userDTO.getLocale())))
                    .thenReturn(userDTO);
            assertEquals("redirect:/user/edit", command.execute(mrw, method));
        }
        verify(session).setAttribute("errorpage", Error.ERROR_UPDATE.getMessage());
        verify(session).setAttribute("errormessage", userDTO.getUsername());
        verify(session).setAttribute("errordto", errorUserDTO);
    }

    @DisplayName("Return URL when User empty into session")
    @Test
    void whenExecuteReturnURLUserEmptyIntoSession() throws RepositoryException, ValidateException {
        when(service.updateUsersFields(userDTO)).thenReturn(Optional.of(userDTO));
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() ->
                    CommandUtility.getNewSessionUserDTO(mrw, userDTO.getUsername(), userDTO.getPassword(), Localization.getNameByLocale(userDTO.getLocale())))
                    .thenReturn(userDTO);
            mockedStatic.when(() -> CommandUtility.updateLoggedUser(mrw, Optional.of(userDTO))).thenReturn(false);
            assertEquals("redirect:/logout", command.execute(mrw, method));
        }
    }

    @DisplayName("ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        when(service.updateUsersFields(userDTO))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.USERDTO_IS_EMPTY, Error.INVALID_LOGIN)));
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() ->
                    CommandUtility.getNewSessionUserDTO(mrw, userDTO.getUsername(), userDTO.getPassword(), Localization.getNameByLocale(userDTO.getLocale())))
                    .thenReturn(userDTO);
            assertEquals("redirect:/user/edit", command.execute(mrw, method));
        }
        verify(session).setAttribute("errorpage", Error.INVALID_LOGIN.getMessage());
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws ValidateException, RepositoryException {
        System.out.println(userDTO);
        when(service.updateUsersFields(userDTO))
                .thenThrow(new RepositoryException("Test Error"));
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {
            mockedStatic.when(() ->
                    CommandUtility.getNewSessionUserDTO(mrw, userDTO.getUsername(), userDTO.getPassword(), Localization.getNameByLocale(userDTO.getLocale())))
                    .thenReturn(userDTO);
            assertEquals("redirect:/user/edit", command.execute(mrw, method));
        }
        verify(session).setAttribute("errorpage", Error.INVALID_LOGIN.getMessage());
        verify(session).setAttribute("errormessage", userDTO.getUsername());
        verify(session).setAttribute("errordto", errorUserDTO);
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        UserEditCommand command = new UserEditCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        UserServiceImpl service = null;
        try {
            field = command.getClass().getDeclaredField(all[1].getName());

            field.setAccessible(true);
            service = (UserServiceImpl) field.get(command);
            System.out.println(service);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(UserServiceImpl.class, service.getClass());
    }
}
