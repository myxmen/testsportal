package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminSubjectCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminSubjectCommandTest {
    private static SubjectServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static SubjectDTO subjectDTO;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(SubjectServiceImpl.class);
        command = new AdminSubjectCommand(service);
        request = mock(HttpServletRequest.class);
        subjectDTO = mock(SubjectDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("sort", "asc");
        mrw.setAttribute("sortcolumn", "subject");
        mrw.setAttribute("pagination", "5");
        mrw.setAttribute("page", "3");
        mrw.setAttribute("noofpages", "6");
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnString() throws RepositoryException, ValidateException {
        List<SubjectDTO> all = List.of(subjectDTO, subjectDTO);
        when(service.getNumberOfRows()).thenReturn(26);
        when(service.getPage(any(Pagination.class))).thenReturn(all);


        assertEquals(command.execute(mrw, method), "/WEB-INF/admin/adminsubject.jsp");

        assertAll("Set Attribute equals expected",
                () -> assertEquals(3, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(6, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("subject", (mrw.getAttribute("sortcolumn"))),
                () -> assertEquals(all, mrw.getAttribute("subject")),
                () -> assertNull((mrw.getAttribute("error"))),
                () -> assertNull((mrw.getAttribute("errormessage")))
        );
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws RepositoryException {
        when(service.getNumberOfRows()).thenThrow(new RepositoryException("Test Error"));

        assertEquals(command.execute(mrw, method), "/WEB-INF/admin/adminsubject.jsp");
        assertAll("Set Attribute equals expected",
                () -> assertEquals("3", (mrw.getAttribute("page"))),
                () -> assertEquals("5", (mrw.getAttribute("pagination"))),
                () -> assertEquals("6", (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("subject", (mrw.getAttribute("sortcolumn"))),
                () -> assertNull(mrw.getAttribute("subject")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        when(service.getNumberOfRows()).thenReturn(26);
        when(service.getPage(any(Pagination.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminSubjectCommand command = new AdminSubjectCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        SubjectServiceImpl service = null;
        try {
            field = command.getClass().getDeclaredField(all[1].getName());

            field.setAccessible(true);
            service = (SubjectServiceImpl) field.get(command);
            System.out.println(service);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(SubjectServiceImpl.class, service.getClass());
    }
}
