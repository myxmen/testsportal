package com.vl.testing.unit.controller.filter;

import com.vl.testing.controller.filters.AdminFilter;
import com.vl.testing.dto.UserDTO;
import org.junit.jupiter.api.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class AdminFilterTest {
    private static AdminFilter adminFilter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain filterChain;
    private static HttpSession session;
    private static UserDTO userDTO;

    @BeforeAll
    static void initGlobal() {
        filterChain = mock(FilterChain.class);
        adminFilter = new AdminFilter();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        userDTO = mock(UserDTO.class);
        when(session.getAttribute("user")).thenReturn(userDTO);
    }

    @AfterEach
    void resetSet() {
        reset(response, userDTO);
    }

    @DisplayName("User is admin")
    @Test
    public void testDoFilterWhenUserIsAdmin() throws IOException, ServletException {
        when(userDTO.isAdmin()).thenReturn(true);

        adminFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
    }

    @DisplayName("User is user")
    @Test
    public void testDoFilterWhenUserIsUser() throws IOException, ServletException {
        when(userDTO.isAdmin()).thenReturn(false);

        adminFilter.doFilter(request, response, filterChain);
        verify(response, times(1)).sendRedirect("/logout");
    }

    @DisplayName("User admin field is null")
    @Test
    public void testDoFilterWhenAdminfieldNull() throws IOException, ServletException {
        when(userDTO.isAdmin()).thenReturn(null);

        adminFilter.doFilter(request, response, filterChain);
        verify(response, times(1)).sendRedirect("/logout");
    }
}
