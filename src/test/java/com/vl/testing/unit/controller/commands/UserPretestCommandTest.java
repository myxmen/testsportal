package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.UserPretestCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UserPretestCommandTest {
    private static QuestionServiceImpl questionService;
    private static TestServiceImpl testService;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static TestDTO existingTestDTO;
    private static SubjectDTO existingSubjectDTO;
    private static QuestionDTO questionDTO;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        questionService = mock(QuestionServiceImpl.class);
        testService = mock(TestServiceImpl.class);
        command = new UserPretestCommand(questionService, testService);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        questionDTO = mock(QuestionDTO.class);
        method = "Get";
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw = new MockedRequestWrapper(request);
        existingTestDTO = TestDTO.builder()
                .setId(4l)
                .setSubjectId(2l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        existingSubjectDTO = SubjectDTO.builder()
                .setId(3l)
                .setSubject("Test")
                .setTests(existingTestDTO)
                .build();
    }

    @DisplayName("Return URL when Subject null")
    @Test
    void whenExecuteWithNullSubjectReturnURLSubject() {
        when(session.getAttribute("subject")).thenReturn(null);
        assertEquals("redirect:/user/subject", command.execute(mrw, method));
    }

    @DisplayName("Return URL when Subject`s Test null")
    @Test
    void whenExecuteWithNullSubjectsTestReturnURLTest() {
        existingSubjectDTO.setTests(null);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        assertEquals("redirect:/user/test", command.execute(mrw, method));
    }

    @DisplayName("Return Pretest URL when IncreaseRequestOfTest false")
    @Test
    void whenExecuteWithFalseIncreaseRequestOfTestReturnPretestURL() throws ValidateException, RepositoryException {
        List<QuestionDTO> all = List.of(questionDTO, questionDTO);
        when(questionDTO.getNumberOfQuestion()).thenReturn(1);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(questionService.getAllByTest(existingSubjectDTO.getTest())).thenReturn(all);
        when(testService.increaseRequestOfTest(existingTestDTO)).thenReturn(false);

        assertEquals("/WEB-INF/user/userpretest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Can not update the field of test's request", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return Pretest URL when Questions list is empty")
    @Test
    void whenExecuteWithEmptyListOfQuestionReturnPretestURL() throws ValidateException, RepositoryException {
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(questionService.getAllByTest(eq(existingSubjectDTO.getTest()))).thenReturn(Collections.emptyList());
        when(testService.increaseRequestOfTest(existingTestDTO)).thenReturn(true);

        assertEquals("/WEB-INF/user/userpretest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("List of questions for this test is empty", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return URL ")
    @Test
    void whenExecuteReturnPretestURL() throws ValidateException, RepositoryException {
        List<QuestionDTO> all = List.of(questionDTO, questionDTO);
        when(questionDTO.getNumberOfQuestion()).thenReturn(1);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(questionService.getAllByTest(existingSubjectDTO.getTest())).thenReturn(all);
        when(testService.increaseRequestOfTest(existingTestDTO)).thenReturn(true);

        assertEquals("/WEB-INF/user/userpretest.jsp", command.execute(mrw, method));
        verify(session).setAttribute("subject", existingSubjectDTO);
        verify(session).setAttribute("count", 0);
        verify(session).setAttribute("questiontoanswer", Collections.emptyMap());
        verify(session).setAttribute("duration", existingTestDTO.getTime() * 60);
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws ValidateException, RepositoryException {
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(questionService.getAllByTest(existingSubjectDTO.getTest()))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/user/userpretest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("ValidateExeption")
    @Test
    void whenExecuteReturnRepositoryValidateExeption() throws ValidateException, RepositoryException {
        List<QuestionDTO> all = List.of(questionDTO, questionDTO);
        when(questionDTO.getNumberOfQuestion()).thenReturn(1);
        when(session.getAttribute("subject")).thenReturn(existingSubjectDTO);
        when(questionService.getAllByTest(existingSubjectDTO.getTest())).thenReturn(all);
        when(testService.increaseRequestOfTest(existingTestDTO))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TEST_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/user/userpretest.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(getValidateException(ExceptionMessage.TEST_ID_IS_EMPTY, Error.ERROR_PAGE), mrw.getAttribute("errormessage"))
        );
    }


    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        UserPretestCommand command = new UserPretestCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Field field2;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        QuestionServiceImpl questionService = null;
        TestServiceImpl testService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            questionService = (QuestionServiceImpl) field1.get(command);
            System.out.println(questionService);

            field2 = command.getClass().getDeclaredField(all[2].getName());

            field2.setAccessible(true);
            testService = (TestServiceImpl) field2.get(command);
            System.out.println(testService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(QuestionServiceImpl.class, questionService.getClass());
        assertEquals(TestServiceImpl.class, testService.getClass());
    }

}
