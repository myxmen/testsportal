package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminTestAddCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.Arrays;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminTestAddCommandTest {
    private static TestServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static TestDTO testDTO;
    private static Long id = 3l;
    private static String subjectId = Long.toString(id);
    private static String testName = "Test Test";
    private static String difficulty = "75";
    private static String time = "45";
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(TestServiceImpl.class);
        command = new AdminTestAddCommand(service);
        request = mock(HttpServletRequest.class);
        testDTO = mock(TestDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Post";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("subjectid", subjectId);
        mrw.setAttribute("difficulty", difficulty);
        mrw.setAttribute("time", time);
        mrw.setAttribute("test", testName);
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Return GET URL")
    @Test
    void whenExecuteReturnGetURL() {
        method = "Get";
        mrw.setAttribute("difficulty", null);
        mrw.setAttribute("time", null);
        mrw.setAttribute("test", null);

        assertEquals("/WEB-INF/admin/admintestadd.jsp", command.execute(mrw, method));
        assertEquals(id, (mrw.getAttribute("subjectid")));
    }

    @DisplayName("Return GET NumberFormatException")
    @Test
    void whenExecuteReturnGetNumberFormatException() {
        method = "Get";
        mrw.setAttribute("subjectid", "Test");
        mrw.setAttribute("difficulty", null);
        mrw.setAttribute("time", null);
        mrw.setAttribute("test", null);

        assertEquals("redirect:/admin", command.execute(mrw, method));
    }

    @DisplayName("Return GET Negative ID")
    @Test
    void whenExecuteReturnGetNegativeId() {
        method = "Get";
        mrw.setAttribute("subjectid", "0");
        mrw.setAttribute("difficulty", null);
        mrw.setAttribute("time", null);
        mrw.setAttribute("test", null);

        assertEquals("redirect:/admin", command.execute(mrw, method));
    }

    @DisplayName("Return POST URL")
    @Test
    void whenExecuteReturnPostURL() throws ValidateException, RepositoryException {

        when(service.create(any(TestDTO.class))).thenReturn(testDTO);
        when(testDTO.getId()).thenReturn(id);

        assertEquals("redirect:/admin/test?subjectid=" + subjectId, command.execute(mrw, method));
    }

    @DisplayName("Return POST ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        when(service.create(any(TestDTO.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TESTDTO_IS_EMPTY, Error.ERROR_TEST)));

        assertEquals("/WEB-INF/admin/admintestadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_TEST.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("validation", (mrw.getAttribute("val"))),
                () -> assertEquals(testName, (mrw.getAttribute("errormessage"))),
                () -> assertEquals(id, (mrw.getAttribute("subjectid")))
        );
    }

    @DisplayName("Return POST NumberFormatException")
    @Test
    void whenExecuteReturnPostNumberFormatException() throws ValidateException, RepositoryException {
        mrw.setAttribute("difficulty", "test");

        assertEquals("/WEB-INF/admin/admintestadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_TEST.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("validation", (mrw.getAttribute("val"))),
                () -> assertEquals(testName, (mrw.getAttribute("errormessage"))),
                () -> assertEquals(id, (mrw.getAttribute("subjectid")))
        );
    }

    @DisplayName("Return POST RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {
        when(service.create(any(TestDTO.class)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/admintestadd.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_TEST.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(testName, (mrw.getAttribute("errormessage"))),
                () -> assertEquals(id, (mrw.getAttribute("subjectid")))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminTestAddCommand command = new AdminTestAddCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        TestServiceImpl service = null;
        try {
            field1 = command.getClass().getDeclaredField(all[2].getName());

            field1.setAccessible(true);
            service = (TestServiceImpl) field1.get(command);
            System.out.println(service);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(TestServiceImpl.class, service.getClass());
    }
}
