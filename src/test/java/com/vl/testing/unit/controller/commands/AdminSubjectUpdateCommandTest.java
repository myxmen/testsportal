package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminSubjectUpdateCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.*;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.*;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminSubjectUpdateCommandTest {
    private static SubjectServiceImpl subjectService;
    private static TestServiceImpl testService;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static SubjectDTO subjectDTO;
    private static Long id = 3l;
    private static Integer numberOfTests = 55;
    private static String testSubject = "Test Subject";
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        subjectService = mock(SubjectServiceImpl.class);
        testService = mock(TestServiceImpl.class);
        command = new AdminSubjectUpdateCommand(subjectService, testService);
        request = mock(HttpServletRequest.class);
        subjectDTO = mock(SubjectDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("id", Long.toString(id));
    }

    @AfterEach
    void resetSet() {
        reset(subjectService);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException, RepositoryException {
        when(subjectService.get(eq(id))).thenReturn(Optional.of(subjectDTO));
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(numberOfTests);

        assertEquals("/WEB-INF/admin/adminsubjectupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(subjectDTO, (mrw.getAttribute("subjectone"))),
                () -> assertEquals(numberOfTests, (mrw.getAttribute("tests")))
        );
    }

    @DisplayName("Return NumberFormatException")
    @Test
    void whenExecuteReturnNumberFormatException() throws ValidateException, RepositoryException {
        mrw.setAttribute("id", "Test");

        assertEquals("redirect:/admin", command.execute(mrw, method));
    }

    @DisplayName("Return POST URL")
    @Test
    void whenExecuteReturnPostURL() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("subject", testSubject);

        assertEquals("redirect:/admin/subject", command.execute(mrw, method));
        verify(subjectService).update(eq(SubjectDTO.builder().setId(id).setSubject(testSubject).build()));
    }

    @DisplayName("Return POST ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("subject", testSubject);
        when(subjectService.update(any(SubjectDTO.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.SUBJECTDTO_IS_EMPTY, Error.ERROR_PAGE)));
        when(subjectService.get(eq(id))).thenReturn(Optional.of(subjectDTO));
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(numberOfTests);

        assertEquals("/WEB-INF/admin/adminsubjectupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(testSubject, mrw.getAttribute("errormessage")),
                () -> assertEquals("validation", mrw.getAttribute("val")),
                () -> assertEquals(subjectDTO, (mrw.getAttribute("subjectone"))),
                () -> assertEquals(numberOfTests, (mrw.getAttribute("tests")))
        );
    }

    @DisplayName("Return POST RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("subject", testSubject);
        when(subjectService.update(any(SubjectDTO.class)))
                .thenThrow(new RepositoryException("Test Error"));
        when(subjectService.get(eq(id))).thenReturn(Optional.of(subjectDTO));
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(numberOfTests);

        assertEquals("/WEB-INF/admin/adminsubjectupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(Error.ERROR_SUBJECT.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(testSubject, mrw.getAttribute("errormessage")),
                () -> assertEquals(subjectDTO, (mrw.getAttribute("subjectone"))),
                () -> assertEquals(numberOfTests, (mrw.getAttribute("tests")))
        );
    }

    @DisplayName("Return GET ValidateExeption")
    @Test
    void whenExecuteReturnGetValidateExeption() throws ValidateException, RepositoryException {
        when(subjectService.get(eq(id)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.SUBJECT_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/admin/adminsubjectupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.SUBJECT_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("subjectone")),
                () -> assertNull(mrw.getAttribute("tests"))
        );
    }

    @DisplayName("Return GET RepositoryException")
    @Test
    void whenExecuteReturnGetRepositoryException() throws ValidateException, RepositoryException {
        when(subjectService.get(eq(id)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/adminsubjectupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("subjectone")),
                () -> assertNull(mrw.getAttribute("tests"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminSubjectUpdateCommand command = new AdminSubjectUpdateCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Field field2;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        SubjectServiceImpl subjectService = null;
        TestServiceImpl testService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            subjectService = (SubjectServiceImpl) field1.get(command);
            System.out.println(subjectService);

            field2 = command.getClass().getDeclaredField(all[2].getName());

            field2.setAccessible(true);
            testService = (TestServiceImpl) field2.get(command);
            System.out.println(testService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(SubjectServiceImpl.class, subjectService.getClass());
        assertEquals(TestServiceImpl.class, testService.getClass());
    }
}
