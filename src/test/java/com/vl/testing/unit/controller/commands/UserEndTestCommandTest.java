package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.UserEndTestCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.*;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.PassedTestServiceImpl;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.atMost;

public class UserEndTestCommandTest {
    private static QuestionServiceImpl questionService;
    private static PassedTestServiceImpl passedTestService;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static TestDTO existingTestDTO;
    private static SubjectDTO subjectDTO;
    private static QuestionDTO questionDTO;
    private static UserDTO userDTO;
    private static LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        questionService = mock(QuestionServiceImpl.class);
        passedTestService = mock(PassedTestServiceImpl.class);
        userDTO = mock(UserDTO.class);
        questionToAnswerMap = mock(LinkedHashMap.class);
        command = new UserEndTestCommand(questionService, passedTestService);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        method = "Get";
        subjectDTO = mock(SubjectDTO.class);
        questionDTO = mock(QuestionDTO.class);
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        existingTestDTO = TestDTO.builder()
                .setId(4l)
                .setSubjectId(2l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .setQuestions(List.of(questionDTO, questionDTO))
                .build();
        mrw = new MockedRequestWrapper(request);
    }

    @DisplayName("Return URL when Test null")
    @Test
    void whenExecuteWithNullTestReturnURL() {
        when(session.getAttribute("questiontoanswer")).thenReturn(null);
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        when(subjectDTO.getTest()).thenReturn(existingTestDTO);

        assertEquals("redirect:/user", command.execute(mrw, method));
    }

    @DisplayName("Return URL when Map null")
    @Test
    void whenExecuteWithNullMapReturnURL() {
        when(session.getAttribute("questiontoanswer")).thenReturn(questionToAnswerMap);
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        when(subjectDTO.getTest()).thenReturn(null);

        assertEquals("redirect:/user", command.execute(mrw, method));
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException, RepositoryException {
        Integer result = 75;
        when(session.getAttribute("questiontoanswer")).thenReturn(questionToAnswerMap);
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        when(session.getAttribute("user")).thenReturn(userDTO);
        when(subjectDTO.getTest()).thenReturn(existingTestDTO);
        when(questionService.getScore(questionToAnswerMap, existingTestDTO.getQuestions().size())).thenReturn(result);
        when(passedTestService.create(any(PassedTestDTO.class), any(UserDTO.class))).thenReturn(PassedTestDTO.builder()
                .setId(3l)
                .setTest(existingTestDTO)
                .setResult(76)
                .build());

        assertEquals("/WEB-INF/user/userendtest.jsp", command.execute(mrw, method));
        assertEquals(75, (mrw.getAttribute("score")));
        verify(passedTestService, atMost(5)).create(any(PassedTestDTO.class), any(UserDTO.class));
        verify(session).removeAttribute("subject");
        verify(session).removeAttribute("count");
        verify(session).removeAttribute("timer");
        verify(session).removeAttribute("duration");
        verify(session).removeAttribute("questiontoanswer");
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws ValidateException, RepositoryException {
        Integer result = 75;
        when(session.getAttribute("questiontoanswer")).thenReturn(questionToAnswerMap);
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        when(session.getAttribute("user")).thenReturn(userDTO);
        when(subjectDTO.getTest()).thenReturn(existingTestDTO);
        when(questionService.getScore(questionToAnswerMap, existingTestDTO.getQuestions().size())).thenReturn(result);

        when(passedTestService.create(any(), any(UserDTO.class)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals(command.execute(mrw, method), "/WEB-INF/user/userendtest.jsp");
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("PasswordException")
    @Test
    void whenExecuteReturnPasswordException() throws ValidateException, RepositoryException {
        Integer result = 75;
        when(session.getAttribute("questiontoanswer")).thenReturn(questionToAnswerMap);
        when(session.getAttribute("subject")).thenReturn(subjectDTO);
        when(session.getAttribute("user")).thenReturn(userDTO);
        when(subjectDTO.getTest()).thenReturn(existingTestDTO);
        when(questionService.getScore(questionToAnswerMap, existingTestDTO.getQuestions().size())).thenReturn(result);

        when(passedTestService.create(any(PassedTestDTO.class), any(UserDTO.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.PASSEDTESTDTO_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals(command.execute(mrw, method), "/WEB-INF/user/userendtest.jsp");
        assertAll("Set Attribute equals expected",
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.PASSEDTESTDTO_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        UserEndTestCommand command = new UserEndTestCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Field field2;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        QuestionServiceImpl questionService = null;
        PassedTestServiceImpl passedTestService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            questionService = (QuestionServiceImpl) field1.get(command);
            System.out.println(questionService);

            field2 = command.getClass().getDeclaredField(all[2].getName());

            field2.setAccessible(true);
            passedTestService = (PassedTestServiceImpl) field2.get(command);
            System.out.println(passedTestService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(QuestionServiceImpl.class, questionService.getClass());
        assertEquals(PassedTestServiceImpl.class, passedTestService.getClass());
    }
}
