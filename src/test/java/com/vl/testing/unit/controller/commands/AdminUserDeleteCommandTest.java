package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminUserDeleteCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.PassedTestServiceImpl;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;
import java.util.*;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminUserDeleteCommandTest {
    private static UserServiceImpl service;
    private static PassedTestServiceImpl passedTestService;
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static String method;
    private static UserDTO userDTO;
    private static Long id = 3l;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(UserServiceImpl.class);
        passedTestService = mock(PassedTestServiceImpl.class);
        command = new AdminUserDeleteCommand(service, passedTestService);
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        userDTO = mock(UserDTO.class);
        when(request.getSession()).thenReturn(session);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("id", Long.toString(id));
        when(session.getAttribute("user")).thenReturn(UserDTO.builder().setId(4l).build());
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() {
        reset(session);
        when(session.getAttribute("user")).thenReturn(UserDTO.builder().setId(id).setUsername("TestUser").build());

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
        reset(session);
    }

    @DisplayName("Return GET URL")
    @Test
    void whenExecuteReturnGetURL() throws ValidateException, RepositoryException {
        when(service.get(id)).thenReturn(Optional.of(userDTO));
        when(passedTestService.getNumberOfRows(userDTO)).thenReturn(66);

        assertEquals("/WEB-INF/admin/adminuserdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(userDTO, (mrw.getAttribute("userone"))),
                () -> assertEquals(66, (mrw.getAttribute("tests")))
        );
    }

    @DisplayName("Return POST URL")
    @Test
    void whenExecuteReturnPostURL() throws ValidateException, RepositoryException {
        method = "Post";

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
        verify(service).delete(id);
    }

    @DisplayName("Return POST ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        method = "Post";
        when(service.get(id)).thenReturn(Optional.of(userDTO));
        when(passedTestService.getNumberOfRows(userDTO)).thenReturn(66);
        when(service.delete(id))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.USER_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/admin/adminuserdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(userDTO, (mrw.getAttribute("userone"))),
                () -> assertEquals(66, (mrw.getAttribute("tests"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.USER_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return POST RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {
        method = "Post";
        when(service.get(id)).thenReturn(Optional.of(userDTO));
        when(passedTestService.getNumberOfRows(userDTO)).thenReturn(66);
        when(service.delete(id))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/adminuserdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(userDTO, (mrw.getAttribute("userone"))),
                () -> assertEquals(66, (mrw.getAttribute("tests"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return GET ValidateExeption")
    @Test
    void whenExecuteReturnGetValidateExeption() throws ValidateException, RepositoryException {
        when(service.get(id))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.USER_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/admin/adminuserdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.USER_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Return GET NoSuchElementException")
    @Test
    void whenExecuteReturnGetNoSuchElementException() throws ValidateException, RepositoryException {
        when(service.get(id))
                .thenThrow(new NoSuchElementException("Test Error"));

        assertEquals("redirect:/admin/userset", command.execute(mrw, method));
    }

    @DisplayName("Return GET RepositoryException")
    @Test
    void whenExecuteReturnGetRepositoryException() throws ValidateException, RepositoryException {
        when(service.get(id))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/adminuserdelete.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(id, (mrw.getAttribute("id"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminUserDeleteCommand command = new AdminUserDeleteCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Field field2;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        UserServiceImpl userService = null;
        PassedTestServiceImpl passedTestService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            userService = (UserServiceImpl) field1.get(command);
            System.out.println(userService);

            field2 = command.getClass().getDeclaredField(all[2].getName());

            field2.setAccessible(true);
            passedTestService = (PassedTestServiceImpl) field2.get(command);
            System.out.println(passedTestService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(UserServiceImpl.class, userService.getClass());
        assertEquals(PassedTestServiceImpl.class, passedTestService.getClass());
    }
}
