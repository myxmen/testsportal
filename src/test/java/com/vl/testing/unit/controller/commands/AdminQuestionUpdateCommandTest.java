package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminQuestionUpdateCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.*;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminQuestionUpdateCommandTest {
    private static QuestionServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static QuestionDTO questionDTO;
    private static Long testId = 44l;
    private static Long questionId = 102l;
    private static Integer numberOfQuestion = 5;
    private static String question = "Test Question";
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(QuestionServiceImpl.class);
        command = new AdminQuestionUpdateCommand(service);
        request = mock(HttpServletRequest.class);
        questionDTO = mock(QuestionDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("questionid", Long.toString(questionId));
    }

    @AfterEach
    void resetSet() {
        reset(service);
    }

    @DisplayName("Get Return URL")
    @Test
    void whenExecuteReturnURL() throws ValidateException, RepositoryException {
        when(service.get(eq(questionId))).thenReturn(Optional.of(questionDTO));

        assertEquals("/WEB-INF/admin/adminquestionupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(questionDTO, (mrw.getAttribute("questionone")))
        );
    }

    @DisplayName("Get return NumberFormatException")
    @Test
    void whenExecuteGetReturnNumberFormatException() {
        mrw.setAttribute("questionid", "Test");

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("Get Return RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws ValidateException, RepositoryException {
        when(service.get(eq(questionId)))
                .thenThrow(new RepositoryException("Test Error"));

        assertEquals("/WEB-INF/admin/adminquestionupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("Get Return ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        when(service.get(eq(questionId)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.QUESTION_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals("/WEB-INF/admin/adminquestionupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.QUESTION_ID_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("Return POST URL")
    @Test
    void whenExecuteReturnPostURL() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("testid", String.valueOf(testId));
        mrw.setAttribute("qnumber", String.valueOf(numberOfQuestion));
        mrw.setAttribute("question", question);
        mrw.setAttribute("answer1", "Test Answer 1");
        mrw.setAttribute("answer2", "Test Answer 2");
        mrw.setAttribute("answer3", "");
        mrw.setAttribute("answer4", "");
        mrw.setAttribute("answer5", "");
        mrw.setAttribute("correctanswer2", String.valueOf(true));
        when(service.update(any(QuestionDTO.class))).thenReturn(Optional.of(questionDTO));

        assertEquals("redirect:/admin/question?testid=" + testId, command.execute(mrw, method));
    }

    @DisplayName("Post return NumberFormatException")
    @Test
    void whenExecutePostReturnNumberFormatException() {
        method = "Post";
        mrw.setAttribute("testid", "Test");
        mrw.setAttribute("qnumber", String.valueOf(numberOfQuestion));
        mrw.setAttribute("question", question);
        mrw.setAttribute("answer1", "Test Answer 1");
        mrw.setAttribute("answer2", "Test Answer 2");
        mrw.setAttribute("answer3", "Test Answer 3");
        mrw.setAttribute("answer4", "Test Answer 4");
        mrw.setAttribute("answer5", "");
        mrw.setAttribute("correctanswer3", String.valueOf(true));

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("POST Return RepositoryException")
    @Test
    void whenExecuteReturnPostRepositoryException() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("testid", String.valueOf(testId));
        mrw.setAttribute("qnumber", String.valueOf(numberOfQuestion));
        mrw.setAttribute("question", question);
        mrw.setAttribute("answer1", "Test Answer 1");
        mrw.setAttribute("answer2", "Test Answer 2");
        mrw.setAttribute("answer3", "Test Answer 3");
        mrw.setAttribute("answer4", "");
        mrw.setAttribute("answer5", "");
        mrw.setAttribute("correctanswer2", String.valueOf(true));
        mrw.setAttribute("correctanswer3", String.valueOf(true));
        when(service.update(any(QuestionDTO.class)))
                .thenThrow(new RepositoryException("Test Error"));
        when(service.get(eq(questionId))).thenReturn(Optional.of(questionDTO));

        assertEquals("/WEB-INF/admin/adminquestionupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_QUESTION.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(String.valueOf(numberOfQuestion), mrw.getAttribute("errormessage")),
                () -> assertEquals(questionDTO, mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("POST Return ValidateExeption")
    @Test
    void whenExecuteReturnPostValidateExeption() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("testid", String.valueOf(testId));
        mrw.setAttribute("qnumber", String.valueOf(numberOfQuestion));
        mrw.setAttribute("question", question);
        mrw.setAttribute("answer1", "Test Answer 1");
        mrw.setAttribute("answer2", "Test Answer 2");
        mrw.setAttribute("answer3", "Test Answer 3");
        mrw.setAttribute("answer4", "Test Answer 4");
        mrw.setAttribute("answer5", "Test Answer 5");
        mrw.setAttribute("correctanswer1", String.valueOf(true));
        mrw.setAttribute("correctanswer3", String.valueOf(true));
        mrw.setAttribute("correctanswer5", String.valueOf(true));
        when(service.update(any(QuestionDTO.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.QUESTION_ID_IS_EMPTY, Error.ERROR_PAGE)));
        when(service.get(eq(questionId))).thenReturn(Optional.of(questionDTO));

        assertEquals("/WEB-INF/admin/adminquestionupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(question, mrw.getAttribute("errormessage")),
                () -> assertEquals("validation", mrw.getAttribute("val")),
                () -> assertEquals(questionDTO, mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("POST Return NumberFormatException")
    @Test
    void whenExecuteReturnPostNumberFormatException() throws ValidateException, RepositoryException {
        method = "Post";
        mrw.setAttribute("testid", String.valueOf(testId));
        mrw.setAttribute("qnumber", "Error Test");
        mrw.setAttribute("question", question);
        mrw.setAttribute("answer1", "Test Answer 1");
        mrw.setAttribute("answer2", "Test Answer 2");
        mrw.setAttribute("answer3", "");
        mrw.setAttribute("answer4", "");
        mrw.setAttribute("answer5", "");
        mrw.setAttribute("correctanswer2", String.valueOf(true));

        when(service.get(eq(questionId))).thenReturn(Optional.of(questionDTO));

        assertEquals("/WEB-INF/admin/adminquestionupdate.jsp", command.execute(mrw, method));
        assertAll("Set Attribute equals expected",
                () -> assertEquals(questionId, (mrw.getAttribute("questionid"))),
                () -> assertEquals(Error.ERROR_QUESTION.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(question, mrw.getAttribute("errormessage")),
                () -> assertEquals("validation", mrw.getAttribute("val")),
                () -> assertEquals(questionDTO, mrw.getAttribute("questionone"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminQuestionUpdateCommand command = new AdminQuestionUpdateCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        QuestionServiceImpl service = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            service = (QuestionServiceImpl) field1.get(command);
            System.out.println(service);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(QuestionServiceImpl.class, service.getClass());
    }
}
