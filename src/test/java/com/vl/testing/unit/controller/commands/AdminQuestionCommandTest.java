package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminQuestionCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.*;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.*;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.*;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminQuestionCommandTest {
    private static QuestionServiceImpl questionService;
    private static SubjectServiceImpl subjectService;
    private static TestServiceImpl testService;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static TestDTO testDTO;
    private static SubjectDTO subjectDTO;
    private static QuestionDTO questionDTO;
    private static Long testId = 44l;
    private static Long subjectId = 3l;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() throws ValidateException, RepositoryException {
        questionService = mock(QuestionServiceImpl.class);
        subjectService = mock(SubjectServiceImpl.class);
        testService = mock(TestServiceImpl.class);
        command = new AdminQuestionCommand(questionService, testService, subjectService);
        request = mock(HttpServletRequest.class);
        testDTO = mock(TestDTO.class);
        subjectDTO = mock(SubjectDTO.class);
        questionDTO = mock(QuestionDTO.class);

        when(testDTO.getSubjectId()).thenReturn(subjectId);
        when(subjectService.get(subjectId)).thenReturn(Optional.of(subjectDTO));
        when(subjectDTO.getSubject()).thenReturn("Test Subject");
        when(subjectDTO.getId()).thenReturn(subjectId);
        when(testDTO.getTest()).thenReturn("Test Test");
        when(testDTO.getId()).thenReturn(testId);
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("subjectid", Long.toString(subjectId));
        mrw.setAttribute("testid", Long.toString(testId));
        mrw.setAttribute("sort", "asc");
        mrw.setAttribute("sortcolumn", "question");
        mrw.setAttribute("pagination", "5");
        mrw.setAttribute("page", "3");
        mrw.setAttribute("noofpages", "6");
    }

    @AfterEach
    void resetSet() {
        reset(questionService);
        reset(testService);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnString() throws RepositoryException, ValidateException {
        // when(subjectService.get(subjectId)).thenReturn(Optional.of(subjectDTO));
        when(testService.get(testId)).thenReturn(Optional.of(testDTO));
        List<QuestionDTO> all = List.of(questionDTO, questionDTO);
        when(questionService.getNumberOfRows(testDTO)).thenReturn(26);
        when(questionService.getPage(eq(testDTO), any(Pagination.class))).thenReturn(all);
        //  when(subjectDTO.getId()).thenReturn(id);
        //   when(subjectDTO.getSubject()).thenReturn("Test Subject");

        assertEquals(command.execute(mrw, method), "/WEB-INF/admin/adminquestion.jsp");

        assertAll("Set Attribute equals expected",
                () -> assertEquals(3, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(6, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("question", (mrw.getAttribute("sortcolumn"))),
                () -> assertEquals(all, mrw.getAttribute("question")),
                () -> assertEquals("Test Subject", mrw.getAttribute("subject")),
                () -> assertEquals("Test Test", mrw.getAttribute("test")),
                () -> assertEquals(subjectId, mrw.getAttribute("subjectid")),
                () -> assertEquals(testId, mrw.getAttribute("testid")),
                () -> assertNull((mrw.getAttribute("error"))),
                () -> assertNull((mrw.getAttribute("errormessage")))
        );
    }

    @DisplayName(".get return RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws RepositoryException, ValidateException {
        when(testService.get(testId))
                .thenThrow(new RepositoryException("Test Error"));
//        when(questionService.getNumberOfRows(testDTO))
//                .thenThrow(new RepositoryException("Test Error"));
        //  List<QuestionDTO> all = List.of(questionDTO, questionDTO);
        when(questionService.getPage(any(), any(Pagination.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TESTDTO_IS_EMPTY, Error.ERROR_TEST)));

        assertEquals(command.execute(mrw, method), "/WEB-INF/admin/adminquestion.jsp");
        assertAll("Set Attribute equals expected",
                () -> assertEquals(1, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(0, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("question", (mrw.getAttribute("sortcolumn"))),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals(ExceptionMessage.TESTDTO_IS_EMPTY.getMessage(), mrw.getAttribute("errormessage")),
                () -> assertNull(mrw.getAttribute("question")),
                () -> assertNull(mrw.getAttribute("subject")),
                () -> assertNull(mrw.getAttribute("test")),
                () -> assertEquals(String.valueOf(subjectId), mrw.getAttribute("subjectid")),
                () -> assertEquals(String.valueOf(testId), mrw.getAttribute("testid"))
        );
    }

    @DisplayName(".get return ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        when(testService.get(testId))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.TEST_ID_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("NumberFormatException")
    @Test
    void whenExecuteReturnNumberFormatException() {
        mrw.setAttribute("testid", "Test");

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName(".getPage return RepositoryException")
    @Test
    void whenExecuteGetPageReturnRepositoryException() throws RepositoryException, ValidateException {
        //  when(subjectService.get(subjectId)).thenReturn(Optional.of(subjectDTO));
        when(testService.get(testId)).thenReturn(Optional.of(testDTO));
        //   List<QuestionDTO> all = List.of(questionDTO, questionDTO);
        when(questionService.getNumberOfRows(testDTO)).thenReturn(26);
        when(questionService.getPage(eq(testDTO), any(Pagination.class)))
                .thenThrow(new RepositoryException("Test Error"));
        //  when(subjectDTO.getId()).thenReturn(id);
        //   when(subjectDTO.getSubject()).thenReturn("Test Subject");

        assertEquals(command.execute(mrw, method), "/WEB-INF/admin/adminquestion.jsp");

        assertAll("Set Attribute equals expected",
                () -> assertEquals(3, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(6, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("question", (mrw.getAttribute("sortcolumn"))),
                () -> assertNull(mrw.getAttribute("question")),
                () -> assertEquals("Test Subject", mrw.getAttribute("subject")),
                () -> assertEquals("Test Test", mrw.getAttribute("test")),
                () -> assertEquals(subjectId, mrw.getAttribute("subjectid")),
                () -> assertEquals(testId, mrw.getAttribute("testid")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminQuestionCommand command = new AdminQuestionCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Field field2;
        Field field3;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        SubjectServiceImpl subjectService = null;
        TestServiceImpl testService = null;
        QuestionServiceImpl questionService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            subjectService = (SubjectServiceImpl) field1.get(command);
            System.out.println(subjectService);

            field2 = command.getClass().getDeclaredField(all[2].getName());

            field2.setAccessible(true);
            testService = (TestServiceImpl) field2.get(command);
            System.out.println(testService);

            field3 = command.getClass().getDeclaredField(all[3].getName());

            field3.setAccessible(true);
            questionService = (QuestionServiceImpl) field3.get(command);
            System.out.println(questionService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(SubjectServiceImpl.class, subjectService.getClass());
        assertEquals(TestServiceImpl.class, testService.getClass());
        assertEquals(QuestionServiceImpl.class, questionService.getClass());
    }
}
