package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.AdminTestCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.*;


import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AdminTestCommandTest {
    private static SubjectServiceImpl subjectService;
    private static TestServiceImpl testService;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static TestDTO testDTO;
    private static SubjectDTO subjectDTO;
    private static Long id = 3l;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() throws ValidateException, RepositoryException {
        subjectService = mock(SubjectServiceImpl.class);
        testService = mock(TestServiceImpl.class);
        command = new AdminTestCommand(subjectService, testService);
        request = mock(HttpServletRequest.class);
        testDTO = mock(TestDTO.class);
        subjectDTO = mock(SubjectDTO.class);
        when(subjectService.get(id)).thenReturn(Optional.of(subjectDTO));
    }

    @BeforeEach
    void setUp() {
        mrw = null;
        method = "Get";
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("subjectid", Long.toString(id));
        mrw.setAttribute("sort", "asc");
        mrw.setAttribute("sortcolumn", "test");
        mrw.setAttribute("pagination", "5");
        mrw.setAttribute("page", "3");
        mrw.setAttribute("noofpages", "6");
    }

    @AfterEach
    void resetSet() {
        reset(testService);
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnString() throws RepositoryException, ValidateException {
        List<TestDTO> all = List.of(testDTO, testDTO);
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(26);
        when(testService.getPage(eq(subjectDTO), any(Pagination.class))).thenReturn(all);
        when(subjectDTO.getId()).thenReturn(id);
        when(subjectDTO.getSubject()).thenReturn("Test Subject");

        assertEquals(command.execute(mrw, method), "/WEB-INF/admin/admintest.jsp");

        assertAll("Set Attribute equals expected",
                () -> assertEquals(3, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(6, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("test", (mrw.getAttribute("sortcolumn"))),
                () -> assertEquals(all, mrw.getAttribute("test")),
                () -> assertEquals("Test Subject", mrw.getAttribute("subject")),
                () -> assertNull((mrw.getAttribute("error"))),
                () -> assertNull((mrw.getAttribute("errormessage")))
        );
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws RepositoryException, ValidateException {
        //  when(subjectService.get(id)).thenReturn(Optional.of(subjectDTO));
        when(testService.getNumberOfRows(subjectDTO)).thenThrow(new RepositoryException("Test Error"));

        assertEquals(command.execute(mrw, method), "/WEB-INF/admin/admintest.jsp");
        assertAll("Set Attribute equals expected",
                () -> assertEquals("3", (mrw.getAttribute("page"))),
                () -> assertEquals("5", (mrw.getAttribute("pagination"))),
                () -> assertEquals("6", (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("test", (mrw.getAttribute("sortcolumn"))),
                () -> assertNull(mrw.getAttribute("test")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        when(testService.getNumberOfRows(subjectDTO)).thenReturn(26);
        when(testService.getPage(eq(subjectDTO), any(Pagination.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("NumberFormatException")
    @Test
    void whenExecuteReturnNumberFormatException() {
        mrw.setAttribute("subjectid", "Test");

        assertEquals(command.execute(mrw, method), "redirect:/admin");
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        AdminTestCommand command = new AdminTestCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field1;
        Field field2;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        SubjectServiceImpl subjectService = null;
        TestServiceImpl testService = null;
        try {
            field1 = command.getClass().getDeclaredField(all[1].getName());

            field1.setAccessible(true);
            subjectService = (SubjectServiceImpl) field1.get(command);
            System.out.println(subjectService);

            field2 = command.getClass().getDeclaredField(all[2].getName());

            field2.setAccessible(true);
            testService = (TestServiceImpl) field2.get(command);
            System.out.println(testService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(SubjectServiceImpl.class, subjectService.getClass());
        assertEquals(TestServiceImpl.class, testService.getClass());
    }
}
