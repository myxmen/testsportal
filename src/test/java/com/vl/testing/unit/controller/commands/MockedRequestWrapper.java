package com.vl.testing.unit.controller.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.HashMap;
import java.util.Map;

public class MockedRequestWrapper extends HttpServletRequestWrapper {
    private Map<String, Object> m = new HashMap<String, Object>();
    private HttpServletRequest origReq = null;

    public MockedRequestWrapper(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
        origReq = httpServletRequest;
    }

    @Override
    public void setAttribute(String name, Object o) {
        //  m = new HashMap<String, Object>();
        m.put(name, o);
        super.setAttribute(name, o);
        origReq.setAttribute(name, o);
    }

    @Override
    public Object getAttribute(String name) {
        if (m != null) {
            return m.get(name);
        } else if (origReq.getAttribute(name) != null) {
            return origReq.getAttribute(name);
        } else {
            return super.getAttribute(name);
        }
    }

    @Override
    public String getParameter(String name) {
        if (m != null) {
            return (String) m.get(name);
        } else if (origReq.getParameter(name) != null) {
            return origReq.getParameter(name);
        } else {
            return super.getParameter(name);
        }
    }

    @Override
    public String[] getParameterValues(String name) {
        if (m != null) {
            return (String[]) m.get(name);
        } else if (origReq.getParameterValues(name) != null) {
            return origReq.getParameterValues(name);
        } else {
            return super.getParameterValues(name);
        }
    }
}

