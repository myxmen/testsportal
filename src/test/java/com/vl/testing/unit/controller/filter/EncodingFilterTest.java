package com.vl.testing.unit.controller.filter;

import com.vl.testing.controller.filters.EncodingFilter;
import org.junit.jupiter.api.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class EncodingFilterTest {
    private static EncodingFilter encodingFilter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain filterChain;

    @BeforeAll
    static void initGlobal() {
        filterChain = mock(FilterChain.class);
        encodingFilter = new EncodingFilter();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
    }

    @DisplayName("Set")
    @Test
    public void testFilterSet() throws IOException, ServletException {
        encodingFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
        verify(response, times(1)).setContentType("text/html");
        verify(response, times(1)).setCharacterEncoding("UTF-8");
        verify(request, times(1)).setCharacterEncoding("UTF-8");
    }
}
