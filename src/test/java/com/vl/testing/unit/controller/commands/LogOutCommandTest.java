package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.CommandUtility;
import com.vl.testing.controller.commands.impl.LogOutCommand;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.dto.UserDTO;
import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class LogOutCommandTest {
    private static Command command;
    private static HttpServletRequest request;
    private static HttpSession session;
    private static ServletContext context;
    private static String method;
    private static UserDTO userDTO;

    @BeforeAll
    static void initGlobal() {
        command = new LogOutCommand();
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        context = mock(ServletContext.class);
        method = "Get";
        when(request.getSession()).thenReturn(session);
        when(request.getServletContext()).thenReturn(context);
    }

    @BeforeEach
    void setUp() {
        userDTO = UserDTO.builder()
                .setUsername("Admin")
                .setPassword("12345678")
                .setBan(false)
                .setAdmin(false)
                .setLocale(Localization.RU.getLocale())
                .build();
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnURL() {
        HashSet<UserDTO> loggedUsers = new HashSet<UserDTO>();
        loggedUsers.add(userDTO);
        when(context.getAttribute("loggedUsers")).thenReturn(loggedUsers);
        when(session.getAttribute("user")).thenReturn(userDTO);
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {

            assertEquals("redirect:/", command.execute(request, method));
            assertTrue(loggedUsers.isEmpty());
            mockedStatic.verify(() -> CommandUtility.setUserRole(any(HttpServletRequest.class), any(UserDTO.class)));
            verify(context, atMost(3)).setAttribute("loggedUsers", loggedUsers);
        }
    }

    @DisplayName("Null User")
    @Test
    void whenExecuteReturnURLNullUser() {
        HashSet<UserDTO> loggedUsers = new HashSet<UserDTO>();
        loggedUsers.add(userDTO);
        when(context.getAttribute("loggedUsers")).thenReturn(loggedUsers);
        when(session.getAttribute("user")).thenReturn(UserDTO.builder()
                .setUsername("User")
                .setPassword("4321")
                .build());
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {

            assertEquals("redirect:/", command.execute(request, method));
            assertFalse(loggedUsers.isEmpty());
            mockedStatic.verify(() -> CommandUtility.setUserRole(any(HttpServletRequest.class), any(UserDTO.class)));
            verify(context, atMost(3)).setAttribute("loggedUsers", loggedUsers);
        }
    }

    @DisplayName("Another User")
    @Test
    void whenExecuteReturnURLAnotherUser() {
        HashSet<UserDTO> loggedUsers = new HashSet<UserDTO>();
        loggedUsers.add(userDTO);
        when(context.getAttribute("loggedUsers")).thenReturn(loggedUsers);
        when(session.getAttribute("user")).thenReturn(null);
        try (MockedStatic<CommandUtility> mockedStatic = mockStatic(CommandUtility.class)) {

            assertEquals("redirect:/", command.execute(request, method));
            assertFalse(loggedUsers.isEmpty());
            mockedStatic.verify(() -> CommandUtility.setUserRole(any(HttpServletRequest.class), any(UserDTO.class)));
            verify(context, atMost(3)).setAttribute("loggedUsers", loggedUsers);
        }
    }
}
