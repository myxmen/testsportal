package com.vl.testing.unit.controller.commands;

import com.vl.testing.controller.commands.Command;
import com.vl.testing.controller.commands.impl.IndexCommand;
import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;

import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static com.vl.testing.validation.util.ValidateUtil.getValidateException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class IndexCommandTest {
    private static SubjectServiceImpl service;
    private static Command command;
    private static HttpServletRequest request;
    private static String method;
    private static SubjectDTO subjectDTO;
    private static MockedRequestWrapper mrw;

    @BeforeAll
    static void initGlobal() {
        service = mock(SubjectServiceImpl.class);
        command = new IndexCommand(service);
        request = mock(HttpServletRequest.class);
        method = "Get";
        subjectDTO = mock(SubjectDTO.class);
    }

    @BeforeEach
    void setUp() {
        mrw = new MockedRequestWrapper(request);
        mrw.setAttribute("sort", "asc");
        mrw.setAttribute("sortcolumn", "subject");
        mrw.setAttribute("pagination", "5");
        mrw.setAttribute("page", "3");
        mrw.setAttribute("noofpages", "6");
    }

    @DisplayName("Return URL")
    @Test
    void whenExecuteReturnString() throws RepositoryException, ValidateException {
        // HttpServletRequest mockRequest = null;
        List<SubjectDTO> all = List.of(subjectDTO, subjectDTO);
        // mrw = new MockedRequestWrapper(request);
//        when(request.getParameter("sort")).thenReturn("subject");
//        when(request.getParameter("sortcolumn")).thenReturn("subject");
//        when(request.getParameter("pagination")).thenReturn("5");
//        when(request.getParameter("page")).thenReturn("2");
//        when(request.getParameter("noofpages")).thenReturn("100");
        when(service.getNumberOfRows()).thenReturn(26);
        when(service.getPage(any(Pagination.class))).thenReturn(all);
        // doReturn(all).when(service).getPage(Pagination.class);
//        mrw.setAttribute("sort", "subject");
//        mrw.setAttribute("sortcolumn", "subject");
//        mrw.setAttribute("pagination", "5");
//        mrw.setAttribute("page", "3");
//        mrw.setAttribute("noofpages", "6");

        assertEquals(command.execute(mrw, method), "/index.jsp");

        assertAll("Set Attribute equals expected",
                () -> assertEquals(3, (mrw.getAttribute("page"))),
                () -> assertEquals(5, (mrw.getAttribute("pagination"))),
                () -> assertEquals(6, (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("subject", (mrw.getAttribute("sortcolumn"))),
                () -> assertEquals(2, ((List<SubjectDTO>) mrw.getAttribute("subject")).size()),
                () -> assertNull((mrw.getAttribute("error"))),
                () -> assertNull((mrw.getAttribute("errormessage")))
        );
        System.out.println(mrw.getAttribute("subject"));
    }

    @DisplayName("RepositoryException")
    @Test
    void whenExecuteReturnRepositoryException() throws RepositoryException {
        when(service.getNumberOfRows()).thenThrow(new RepositoryException("Test Error"));
//        Throwable thrown = assertThrows(RepositoryException.class, () -> {
//            command.execute(mrw, method);
//        });
        assertEquals(command.execute(mrw, method), "/index.jsp");
        assertAll("Set Attribute equals expected",
                () -> assertEquals("3", (mrw.getAttribute("page"))),
                () -> assertEquals("5", (mrw.getAttribute("pagination"))),
                () -> assertEquals("6", (mrw.getAttribute("noofpages"))),
                () -> assertEquals("asc", (mrw.getAttribute("sort"))),
                () -> assertEquals("subject", (mrw.getAttribute("sortcolumn"))),
                () -> assertNull((List<SubjectDTO>) mrw.getAttribute("subject")),
                () -> assertEquals(Error.ERROR_PAGE.getMessage(), (mrw.getAttribute("error"))),
                () -> assertEquals("Test Error", mrw.getAttribute("errormessage"))
        );
    }

    @DisplayName("ValidateExeption")
    @Test
    void whenExecuteReturnValidateExeption() throws ValidateException, RepositoryException {
        when(service.getPage(any(Pagination.class)))
                .thenThrow(new ValidateException(getValidateException(ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE)));

        assertEquals(command.execute(mrw, method), "redirect:/");
    }

    @DisplayName("Constructor set service")
    @Test
    void whenConstructorEmptyShouldSetService() {
        IndexCommand command = new IndexCommand();

        Field[] all = command.getClass().getDeclaredFields();
        Field field;
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        SubjectServiceImpl service = null;
        try {
            field = command.getClass().getDeclaredField(all[1].getName());

            field.setAccessible(true);
            service = (SubjectServiceImpl) field.get(command);
            System.out.println(service);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        assertEquals(SubjectServiceImpl.class, service.getClass());
    }
}
