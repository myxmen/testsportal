package com.vl.testing.unit.validation;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateTestDTO;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class ValidateTestDTOTest {
    private static TestDTO existingTestDTO;
    private static ValidateTestDTO validateTestDTO;
    private static Long negativeId = -2l;
    private static Long id = 2l;
    private static String emptyString = " ";

    @BeforeEach
    void setUp() {
        validateTestDTO = new ValidateTestDTO();
        existingTestDTO = TestDTO.builder()
                .setId(id)
                .setSubjectId(4l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
    }

    @DisplayName("Null ID")
    @Test
    void whenTestsNullIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validateId(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Negative ID")
    @Test
    void whenTestsNegativeIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validateId(negativeId);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("validateID return Validate")
    @Test
    void whenValidateIdTestReturnValidate() throws ValidateException {
        assertEquals(validateTestDTO.validateId(id).getClass(), ValidateTestDTO.class);
    }

    @DisplayName("validate return Validate")
    @Test
    void whenValidateTestReturnValidate() throws ValidateException {
        assertEquals(validateTestDTO.validate(existingTestDTO).getClass(), ValidateTestDTO.class);
    }

    @DisplayName("Null TestDTO")
    @Test
    void whenNullTestReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TESTDTO_IS_EMPTY.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }

    @DisplayName("Null SubjectId")
    @Test
    void whenTestsNullSubjectIdFieldsReturnTrown() {
        existingTestDTO.setSubjectId(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(existingTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_SUBJECTID_IS_EMPTY.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }

    @DisplayName("Negative SubjectId")
    @Test
    void whenTestsNegativeSubjectIdFieldReturnTrown() {
        existingTestDTO.setSubjectId(negativeId);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(existingTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_SUBJECTID_IS_EMPTY.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }

    @DisplayName("Null Test")
    @Test
    void whenTestsNullTestFieldsReturnTrown() {
        existingTestDTO.setTest(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(existingTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_NAME_IS_EMPTY.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }

    @DisplayName("Empty Test")
    @Test
    void whenTestsEmptyTestFieldsReturnTrown() {
        existingTestDTO.setTest(emptyString);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(existingTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_NAME_IS_EMPTY.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }

    @DisplayName("Null Requests")
    @Test
    void whenTestsNullRequestsFieldsReturnTrown() {
        existingTestDTO.setRequests(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(existingTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_REQUEST_IS_INVALID.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }

    @DisplayName("Negative Requests")
    @Test
    void whenTestsNegativeRequestsFieldReturnTrown() {
        existingTestDTO.setRequests(negativeId);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(existingTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_REQUEST_IS_INVALID.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }

    @DisplayName("Null Time")
    @Test
    void whenTestsNullTimeFieldsReturnTrown() {
        existingTestDTO.setTime(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(existingTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_TIME_IS_INVALID.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }

    @DisplayName("Negative Time")
    @Test
    void whenTestsNegativeTimeFieldReturnTrown() {
        existingTestDTO.setTime(negativeId);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateTestDTO.validate(existingTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.TEST_TIME_IS_INVALID.getMessage() + "{" + Error.ERROR_TEST.getMessage() + "}"));
    }
}
