package com.vl.testing.unit.validation;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidatePaginationTest {
    private static ValidatePagination validatePagination;
    private static String sort = Sort.ASC.getSort();
    private static String sortColumn = "subject";
    private static String sortDefaultColumn = "subject";
    private static String currentPage = "2";
    private static String recordsPerPage = "5";
    private static Integer rows = 10;
    private static Integer sortField = 1;
    private static Integer sortColumnField = 2;
    private static Integer recordsPerPageField = 4;
    private static Integer startRowsField = 6;
    private static Integer negativeValue = -4;
    private static String emptyString = " ";

    @BeforeEach
    void setUp() {
        validatePagination = new ValidatePagination();
    }

    @DisplayName("validate return Validate")
    @Test
    void whenValidatePaginationReturnValidate() throws ValidateException {
        assertEquals(validatePagination.validate(mapperPagination()).getClass(), ValidatePagination.class);
    }

    @DisplayName("validate Pagination")
    @ParameterizedTest
    @MethodSource("testCases")
    void whenGetScoreReturnScore(final Pagination inFirst, final ExceptionMessage inSecond, final Error inThirth) {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validatePagination.validate(inFirst);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(inSecond.getMessage() + "{" + inThirth.getMessage() + "}"));
    }

    static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(null, ExceptionMessage.PAGINATION_IS_EMPTY, Error.ERROR_PAGE),

                Arguments.of(getStartRows(null), ExceptionMessage.PAGINATIONSTARTROWS_IS_EMPTY, Error.ERROR_PAGE),
                Arguments.of(getStartRows(negativeValue), ExceptionMessage.PAGINATIONSTARTROWS_IS_NEGATIVE, Error.ERROR_PAGE),

                Arguments.of(getRecordsPerPage(null), ExceptionMessage.PAGINATIONRECORDSPERPAGE_IS_EMPTY, Error.ERROR_PAGE),
                Arguments.of(getRecordsPerPage(negativeValue), ExceptionMessage.PAGINATIONRECORDSPERPAGE_IS_NEGATIVE, Error.ERROR_PAGE),

                Arguments.of(getSort(null), ExceptionMessage.PAGINATIONSORT_IS_EMPTY, Error.ERROR_PAGE),
                Arguments.of(getSort(emptyString), ExceptionMessage.PAGINATIONSORT_IS_INVALID, Error.ERROR_PAGE),

                Arguments.of(getSortColumn(null), ExceptionMessage.PAGINATIONSORTCOLUMN_IS_EMPTY, Error.ERROR_PAGE),
                Arguments.of(getSortColumn(emptyString), ExceptionMessage.PAGINATIONSORTCOLUMN_IS_INVALID, Error.ERROR_PAGE)
        );
    }

    static Pagination getStartRows(final Integer test) {
        Pagination pagination = mapperPagination();
        try {
            getAccessibleField(pagination, startRowsField).set(pagination, test);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return pagination;
    }

    static Pagination getRecordsPerPage(final Integer test) {
        Pagination pagination = mapperPagination();
        try {
            getAccessibleField(pagination, recordsPerPageField).set(pagination, test);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return pagination;
    }

    static Pagination getSort(final String test) {
        Pagination pagination = mapperPagination();
        try {
            getAccessibleField(pagination, sortField).set(pagination, test);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return pagination;
    }

    static Pagination getSortColumn(final String test) {
        Pagination pagination = mapperPagination();
        try {
            getAccessibleField(pagination, sortColumnField).set(pagination, test);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return pagination;
    }

    static Field getAccessibleField(final Pagination pagination, final Integer num) {
        Field[] all = pagination.getClass().getDeclaredFields();
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        Field field = null;
        try {
            field = pagination.getClass().getDeclaredField(all[num].getName());
            field.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return field;
    }

    static Pagination mapperPagination() {
        return Pagination.builder()
                .setRows(rows)
                .setCurrentPage(currentPage)
                .setRecordsPerPage(recordsPerPage)
                .setSort(sort)
                .setSortColumn(sortColumn)
                .setDefaultColumn(sortDefaultColumn)
                .build();
    }
}
