package com.vl.testing.unit.validation;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateSubjectDTO;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class ValidateSubjectDTOTest {
    private static SubjectDTO existingSubjectDTO;
    private static ValidateSubjectDTO validateSubjectDTO;
    private static Long negativeId = -2l;
    private static Long id = 2l;
    private static String emptyString = " ";

    @BeforeEach
    void setUp() {
        validateSubjectDTO = new ValidateSubjectDTO();
        existingSubjectDTO = SubjectDTO.builder()
                .setId(id)
                .setSubject("Test")
                .setTests(null)
                .build();
    }

    @DisplayName("ValidateNullID")
    @Test
    void whenSubjectsNullIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateSubjectDTO.validateId(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.SUBJECT_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("ValidateNegativeID")
    @Test
    void whenSubjectsNegativeIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateSubjectDTO.validateId(negativeId);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.SUBJECT_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("ValidateID")
    @Test
    void whenValidateIdSubjectReturnValidate() throws ValidateException {
        assertEquals(validateSubjectDTO.validateId(id).getClass(), ValidateSubjectDTO.class);
    }

    @DisplayName("Validate")
    @Test
    void whenValidateSubjectReturnValidate() throws ValidateException {
        assertEquals(validateSubjectDTO.validate(existingSubjectDTO).getClass(), ValidateSubjectDTO.class);
    }

    @DisplayName("ValidateSubject")
    @Test
    void whenNullSubjectsReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateSubjectDTO.validate(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.SUBJECTDTO_IS_EMPTY.getMessage() + "{" + Error.ERROR_SUBJECT.getMessage() + "}"));
    }

    @DisplayName("ValidateNullSubjectFields")
    @Test
    void whenSubjectsNullSubjectFieldsReturnTrown() {
        existingSubjectDTO.setSubject(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateSubjectDTO.validate(existingSubjectDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.SUBJECT_NAME_IS_EMPTY.getMessage() + "{" + Error.ERROR_SUBJECT.getMessage() + "}"));
    }

    @DisplayName("ValidateEmptySubjectFields")
    @Test
    void whenSubjectsEmptySubjectFieldsReturnTrown() {
        existingSubjectDTO.setSubject(emptyString);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateSubjectDTO.validate(existingSubjectDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.SUBJECT_NAME_IS_EMPTY.getMessage() + "{" + Error.ERROR_SUBJECT.getMessage() + "}"));
    }


}
