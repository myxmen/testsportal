package com.vl.testing.unit.validation;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.util.ValidateUtil;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class ValidateUtilTest {
    private static String exceptionAndErrorMessage;
    private static String exceptionMessage;
    private static String errorMessage;

    @BeforeEach
    void setUp() {
        exceptionMessage = ExceptionMessage.USER_ID_IS_EMPTY.getMessage();
        errorMessage = Error.ERROR_PAGE.getMessage();
        exceptionAndErrorMessage = exceptionMessage + "{" + errorMessage + "}";
    }

    @DisplayName("getValidateException")
    @Test
    void whenGetValidateExceptionStringsReturnStringExceptionMessageWithError() {
        String message = ValidateUtil.getValidateException(exceptionMessage, errorMessage);
        System.out.println(message);
        assertNotNull(message);
        assertTrue(message.contains(exceptionAndErrorMessage));
    }

    @DisplayName("getValidateException")
    @Test
    void whenGetValidateExceptionReturnStringExceptionMessageWithError() {
        String message = ValidateUtil.getValidateException(ExceptionMessage.USER_ID_IS_EMPTY, Error.ERROR_PAGE);
        System.out.println(message);
        assertNotNull(message);
        assertTrue(message.contains(exceptionAndErrorMessage));
    }

    @DisplayName("getExceptionMessage")
    @Test
    void whenGetExceptionMessageReturnStringExceptionMessage() {
        String message = ValidateUtil.getExceptionMessage(exceptionAndErrorMessage);
        System.out.println(message);
        assertNotNull(message);
        assertTrue(message.contains(exceptionMessage));
    }

    @DisplayName("getError")
    @Test
    void whenGetErrorReturnStringErrorMessage() {
        String message = ValidateUtil.getError(exceptionAndErrorMessage);
        System.out.println(message);
        assertNotNull(message);
        assertTrue(message.contains(errorMessage));
    }
}
