package com.vl.testing.unit.validation;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateUserDTO;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidateUserDTOTest {
    private static UserDTO existingUserDTO;
    private static ValidateUserDTO validateUserDTO;
    private static Long negativeId = -2l;
    private static Long id = 2l;
    private static String emptyString = "";

    @BeforeEach
    void setUp() {
        validateUserDTO = new ValidateUserDTO();
        existingUserDTO = UserDTO.builder()
                .setId(id)
                .setUsername("Test")
                .setPassword("12345678")
                .setAdmin(true)
                .setBan(false)
                .setLocale(Localization.RU.getLocale())
                .build();
    }

    @DisplayName("Null ID")
    @Test
    void whenUsersNullIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validateId(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USER_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Negative ID")
    @Test
    void whenUsersNegativeIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validateId(negativeId);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USER_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("validateID return Validate")
    @Test
    void whenValidateIdUserReturnValidate() throws ValidateException {
        assertEquals(validateUserDTO.validateId(id).getClass(), ValidateUserDTO.class);
    }

    @DisplayName("validate return Validate")
    @Test
    void whenValidateUserReturnValidate() throws ValidateException {
        assertEquals(validateUserDTO.validate(existingUserDTO).getClass(), ValidateUserDTO.class);
    }

    @DisplayName("validateAdminsFields return Validate")
    @Test
    void whenValidateAdminsFieldsUserReturnValidate() throws ValidateException {
        assertEquals(validateUserDTO.validateAdminsFields(existingUserDTO).getClass(), ValidateUserDTO.class);
    }

    @DisplayName("validateNameAndPassword return Validate")
    @Test
    void whenValidateNameAndPasswordUserReturnValidate() throws ValidateException {
        assertEquals(validateUserDTO.validateNameAndPassword(existingUserDTO).getClass(), ValidateUserDTO.class);
    }

    @DisplayName("Null Locale")
    @Test
    void whenValidateUsersNullLocaleFieldReturnTrown() {
        existingUserDTO.setLocale(null);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validate(existingUserDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USERFIELD_LOCALE_IS_EMPTY.getMessage() + "{" + Error.INVALID_LOGIN.getMessage() + "}"));
    }

    @DisplayName("Null Ban")
    @Test
    void whenValidateUsersNullBanFieldReturnTrown() {
        existingUserDTO.setBan(null);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validate(existingUserDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USERFIELD_BAN_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Null Admin")
    @Test
    void whenValidateUsersNullAdminFieldReturnTrown() {
        existingUserDTO.setAdmin(null);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validate(existingUserDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USERFIELD_ADMIN_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Null User")
    @Test
    void whenUsersNullUserDTOFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validateNameAndPassword(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USERDTO_IS_EMPTY.getMessage() + "{" + Error.INVALID_LOGIN.getMessage() + "}"));
    }

    @DisplayName("Null Username")
    @Test
    void whenUsersNullUsernameFieldsReturnTrown() {
        existingUserDTO.setUsername(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validateNameAndPassword(existingUserDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USERFIELD_USERNAME_IS_EMPTY.getMessage() + "{" + Error.INVALID_LOGIN.getMessage() + "}"));
    }

    @DisplayName("Empty Username")
    @Test
    void whenUsersEmptyUsernameFieldsReturnTrown() {
        existingUserDTO.setUsername(emptyString);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validateNameAndPassword(existingUserDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USERFIELD_USERNAME_IS_EMPTY.getMessage() + "{" + Error.INVALID_LOGIN.getMessage() + "}"));
    }

    @DisplayName("Null Password")
    @Test
    void whenUsersNullPasswordFieldsReturnTrown() {
        existingUserDTO.setPassword(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validateNameAndPassword(existingUserDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USERFIELD_PASSWORD_IS_EMPTY.getMessage() + "{" + Error.INVALID_PASSWORD.getMessage() + "}"));
    }

    @DisplayName("Empty Password")
    @Test
    void whenUsersEmptyPasswordFieldsReturnTrown() {
        existingUserDTO.setPassword(emptyString);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateUserDTO.validateNameAndPassword(existingUserDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.USERFIELD_PASSWORD_IS_EMPTY.getMessage() + "{" + Error.INVALID_PASSWORD.getMessage() + "}"));
    }
}
