package com.vl.testing.unit.validation;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidatePassedTestDTO;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class ValidatePassedTestDTOTest {
    private static TestDTO existingTestDTO;
    private static PassedTestDTO existingPassedTestDTO;
    private static ValidatePassedTestDTO validatePassedTestDTO;
    private static Long negativeID = -2l;
    private static Integer negativeResult = -25;
    private static Long id = 2l;

    @BeforeEach
    void setUp() {
        validatePassedTestDTO = new ValidatePassedTestDTO();
        existingTestDTO = TestDTO.builder()
                .setId(id + 1)
                .setSubjectId(4l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        existingPassedTestDTO = PassedTestDTO.builder()
                .setId(id)
                .setTest(existingTestDTO)
                .setResult(76)
                .build();
    }

    @DisplayName("ValidateNullID")
    @Test
    void whenPassedTestsNullIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validatePassedTestDTO.validateId(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.PASSEDTEST_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("ValidateNegativeID")
    @Test
    void whenPassedTestsNegativeIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validatePassedTestDTO.validateId(negativeID);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.PASSEDTEST_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("ValidateID")
    @Test
    void whenValidateIdPassedTestReturnValidate() throws ValidateException {
        assertEquals(validatePassedTestDTO.validateId(id).getClass(), ValidatePassedTestDTO.class);
    }

    @DisplayName("Validate")
    @Test
    void whenValidatePassedTestReturnValidate() throws ValidateException {
        assertEquals(validatePassedTestDTO.validate(existingPassedTestDTO).getClass(), ValidatePassedTestDTO.class);
    }

    @DisplayName("ValidateSubject")
    @Test
    void whenNullPassedTestsReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validatePassedTestDTO.validate(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.PASSEDTESTDTO_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Null TestId")
    @Test
    void whenPassedTestsNullTestIdFieldsReturnTrown() {
        existingPassedTestDTO.getTest().setId(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validatePassedTestDTO.validate(existingPassedTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.PASSEDTEST_TESTID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Negative TestId")
    @Test
    void whenPassedTestsNegativeTestIdFieldReturnTrown() {
        existingPassedTestDTO.getTest().setId(negativeID);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validatePassedTestDTO.validate(existingPassedTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.PASSEDTEST_TESTID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Null Result")
    @Test
    void whenPassedTestsNullResultFieldsReturnTrown() {
        existingPassedTestDTO.setResult(null);

        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validatePassedTestDTO.validate(existingPassedTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.PASSEDTEST_RESULT_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Negative Result")
    @Test
    void whenPassedTestsNegativeResultFieldReturnTrown() {
        existingPassedTestDTO.setResult(negativeResult);
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validatePassedTestDTO.validate(existingPassedTestDTO);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.PASSEDTEST_RESULT_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }
}
