package com.vl.testing.unit.validation;

import com.vl.testing.controller.enums.Error;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.validation.enums.ExceptionMessage;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateQuestionDTO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidateQuestionDTOTest {
    private static QuestionDTO existingQuestionDTO;
    private static ValidateQuestionDTO validateQuestionDTO;
    private static Long negativeID = -2l;
    private static Integer negativeValue = -4;
    private static Long id = 2l;
    private static String emptyString = " ";

    @BeforeEach
    void setUp() {
        validateQuestionDTO = new ValidateQuestionDTO();
        existingQuestionDTO = QuestionDTO.builder()
                .setId(null)
                .setTestId(id)
                .setNumberOfQuestion(3)
                .setQuestion("newTestQuestion3")
                .setAnswer1("newTestAnswer1")
                .setAnswer2("newTestAnswer2")
                .setAnswer3("newTestAnswer3")
                .setAnswer4("newTestAnswer4")
                .setAnswer5("newTestAnswer5")
                .setCorrectAnswer1(true)
                .setCorrectAnswer2(true)
                .setCorrectAnswer3(false)
                .setCorrectAnswer4(true)
                .setCorrectAnswer5(true)
                .build();
    }

    @DisplayName("Null ID")
    @Test
    void whenQuestionNullIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateQuestionDTO.validateId(null);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.QUESTION_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("Negative ID")
    @Test
    void whenQuestionNegativeIdFieldReturnTrown() {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateQuestionDTO.validateId(negativeID);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.QUESTION_ID_IS_EMPTY.getMessage() + "{" + Error.ERROR_PAGE.getMessage() + "}"));
    }

    @DisplayName("validateID return Validate")
    @Test
    void whenValidateIdQuestionReturnValidate() throws ValidateException {
        assertEquals(validateQuestionDTO.validateId(id).getClass(), ValidateQuestionDTO.class);
    }

    @DisplayName("validate return Validate")
    @Test
    void whenValidateQuestionReturnValidate() throws ValidateException {
        assertEquals(validateQuestionDTO.validate(existingQuestionDTO).getClass(), ValidateQuestionDTO.class);
    }

    @DisplayName("validate QuestionDTO")
    @ParameterizedTest
    @MethodSource("testCases")
    void whenGetScoreReturnScore(QuestionDTO inFirst, ExceptionMessage inSecond, Error inThirth) {
        Throwable thrown = assertThrows(ValidateException.class, () -> {
            validateQuestionDTO.validate(inFirst);
        });
        System.out.println(thrown);
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(inSecond.getMessage() + "{" + inThirth.getMessage() + "}"));
    }

    static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(null, ExceptionMessage.QUESTIONDTO_IS_EMPTY, Error.ERROR_QUESTION),

                Arguments.of(getTestId(null), ExceptionMessage.QUESTION_TESTID_IS_EMPTY, Error.ERROR_QUESTION),
                Arguments.of(getTestId(negativeID), ExceptionMessage.QUESTION_TESTID_IS_EMPTY, Error.ERROR_QUESTION),

                Arguments.of(getNumberOfQuestion(null), ExceptionMessage.QUESTION_NUMDER_OF_QUESTION_IS_EMPTY, Error.ERROR_QUESTION),
                Arguments.of(getNumberOfQuestion(negativeValue), ExceptionMessage.QUESTION_NUMDER_OF_QUESTION_IS_EMPTY, Error.ERROR_QUESTION),

                Arguments.of(getQuestion(null), ExceptionMessage.QUESTION_QUESTION_IS_EMPTY, Error.ERROR_QUESTION),
                Arguments.of(getQuestion(emptyString), ExceptionMessage.QUESTION_QUESTION_IS_EMPTY, Error.ERROR_QUESTION),

                Arguments.of(getAnswer1(null), ExceptionMessage.QUESTION_ANSWER1_IS_EMPTY, Error.ERROR_ANSWER1),
                Arguments.of(getAnswer1(emptyString), ExceptionMessage.QUESTION_ANSWER1_IS_EMPTY, Error.ERROR_ANSWER1),

                Arguments.of(getAnswer2(null), ExceptionMessage.QUESTION_ANSWER2_IS_EMPTY, Error.ERROR_ANSWER2),
                Arguments.of(getAnswer2(emptyString), ExceptionMessage.QUESTION_ANSWER2_IS_EMPTY, Error.ERROR_ANSWER2),

                Arguments.of(getAnswer3(emptyString), ExceptionMessage.QUESTION_ANSWER3_IS_EMPTY, Error.ERROR_QUESTION),

                Arguments.of(getAnswer4(emptyString), ExceptionMessage.QUESTION_ANSWER4_IS_EMPTY, Error.ERROR_QUESTION),

                Arguments.of(getAnswer5(emptyString), ExceptionMessage.QUESTION_ANSWER5_IS_EMPTY, Error.ERROR_QUESTION)
        );
    }

    static QuestionDTO getTestId(final Long test) {
        QuestionDTO dto = mapperDTO();
        dto.setTestId(test);
        return dto;
    }

    static QuestionDTO getNumberOfQuestion(final Integer test) {
        QuestionDTO dto = mapperDTO();
        dto.setNumberOfQuestion(test);
        return dto;
    }

    static QuestionDTO getQuestion(final String test) {
        QuestionDTO dto = mapperDTO();
        dto.setQuestion(test);
        return dto;
    }

    static QuestionDTO getAnswer1(final String test){
        QuestionDTO dto = mapperDTO();
        dto.setAnswer1(test);
        return dto;
    }

    static QuestionDTO getAnswer2(final String test){
        QuestionDTO dto = mapperDTO();
        dto.setAnswer2(test);
        return dto;
    }

    static QuestionDTO getAnswer3(final String test){
        QuestionDTO dto = mapperDTO();
        dto.setAnswer3(test);
        return dto;
    }

    static QuestionDTO getAnswer4(final String test){
        QuestionDTO dto = mapperDTO();
        dto.setAnswer4(test);
        return dto;
    }

    static QuestionDTO getAnswer5(final String test){
        QuestionDTO dto = mapperDTO();
        dto.setAnswer5(test);
        return dto;
    }

    static QuestionDTO mapperDTO() {
        return QuestionDTO.builder()
                .setId(id)
                .setTestId(id + 2)
                .setNumberOfQuestion(3)
                .setQuestion("newTestQuestion3")
                .setAnswer1("newTestAnswer1")
                .setAnswer2("newTestAnswer2")
                .setAnswer3("newTestAnswer3")
                .setAnswer4("newTestAnswer4")
                .setAnswer5("newTestAnswer5")
                .setCorrectAnswer1(true)
                .setCorrectAnswer2(true)
                .setCorrectAnswer3(false)
                .setCorrectAnswer4(true)
                .setCorrectAnswer5(true)
                .build();
    }
}
