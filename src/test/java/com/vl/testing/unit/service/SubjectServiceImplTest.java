package com.vl.testing.unit.service;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daofactory.JDBCDaoFactory;
import com.vl.testing.persistence.daos.SubjectDaoImpl;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;
import com.vl.testing.service.impl.SubjectServiceImpl;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateSubjectDTO;
import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SubjectServiceImplTest {
    private static SubjectServiceImpl mockSubjectService;
    private static DaoFactory daoFactory;
    private static SubjectDaoImpl mockSubjectDaoImpl;
    private static SubjectEntity existingSubjectEntity;
    private static SubjectDTO existingSubjectDTO;
    private static ValidateSubjectDTO mockValidateSubjectDTO;
    private static ValidatePagination mockValidatePagination;
    private static Long id;

    @BeforeAll
    static void initGlobal() throws ValidateException {
        daoFactory = mock(JDBCDaoFactory.class);
        // mockSubjectService = mock(SubjectServiceImpl.class);
        mockSubjectDaoImpl = mock(SubjectDaoImpl.class);
        mockValidatePagination = mock(ValidatePagination.class);
        mockValidateSubjectDTO = mock(ValidateSubjectDTO.class);
        mockSubjectService = new SubjectServiceImpl(daoFactory, mockValidateSubjectDTO, mockValidatePagination);
        when(daoFactory.createSubjectDao()).thenReturn(mockSubjectDaoImpl);
        when(mockValidateSubjectDTO.validate(any(SubjectDTO.class))).thenReturn(mockValidateSubjectDTO);
        when(mockValidateSubjectDTO.validateId(anyLong())).thenReturn(mockValidateSubjectDTO);
    }

    @BeforeEach
    void setUp() {
        id = 1l;
        existingSubjectEntity = SubjectEntity.builder()
                .setIdSubject(null)
                .setSubject("Test")
                .build();
        existingSubjectDTO = SubjectDTO.builder()
                .setId(null)
                .setSubject(existingSubjectEntity.getSubject())
                .setTests(null)
                .build();
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllSubjectsReturnListOfExistingSubjects() throws RepositoryException {
        List<SubjectEntity> all = List.of(existingSubjectEntity, existingSubjectEntity);

        when(mockSubjectDaoImpl.getAll()).thenReturn(all);
        List<SubjectDTO> actual = mockSubjectService.getAll();
        actual.forEach(x -> System.out.println(x));

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
        // assertEquals(all.size(), 20);
    }

    @DisplayName("Get")
    @Test
    void whenGetSubjectsEqualsOneOfGetSubjects() throws RepositoryException, ValidateException {
        existingSubjectEntity.setIdSubject(id);
        existingSubjectDTO.setId(id);

        when(mockSubjectDaoImpl.get(id)).thenReturn(Optional.of(existingSubjectEntity));
        SubjectDTO actual = mockSubjectService.get(id).get();
        System.out.println(actual);

        assertEquals(existingSubjectDTO, actual);
    }

    @DisplayName("Get empty subject")
    @Test
    void whenGetSubjectGetEmptySubjects() throws RepositoryException, ValidateException {
        when(mockSubjectDaoImpl.get(id)).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockSubjectService.get(id));
    }

    @DisplayName("Update")
    @Test
    void whenUpdateSubjectShouldReturnUpdatedSubject() throws RepositoryException, ValidateException {
        existingSubjectEntity.setIdSubject(id);
        existingSubjectDTO.setId(id);

        when(mockSubjectDaoImpl.update(existingSubjectEntity)).thenReturn(Optional.of(existingSubjectEntity));
        SubjectDTO updated = mockSubjectService.update(existingSubjectDTO).get();

        assertEquals(existingSubjectDTO, updated);
    }

    @DisplayName("Update empty subject")
    @Test
    void whenUpdateSubjectGetEmptySubjects() throws RepositoryException, ValidateException {
        existingSubjectEntity.setIdSubject(id);
        existingSubjectDTO.setId(id);

        when(mockSubjectDaoImpl.update(existingSubjectEntity)).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockSubjectService.update(existingSubjectDTO));
    }

    @DisplayName("Create")
    @Test
    void addingSubjectShouldReturnCreatedSubject() throws RepositoryException, ValidateException {
        SubjectEntity subjectEntityWithID = SubjectEntity.builder()
                .setIdSubject(id)
                .setSubject(existingSubjectEntity.getSubject())
                .build();

        when(mockSubjectDaoImpl.create(existingSubjectEntity)).thenReturn(subjectEntityWithID);
        SubjectDTO created = mockSubjectService.create(existingSubjectDTO);
        SubjectDTO expected = existingSubjectDTO;
        expected.setId(id);

        assertEquals(expected, created);
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedSubjectShouldReturnTrue() throws RepositoryException, ValidateException {
        when(mockSubjectDaoImpl.delete(id)).thenReturn(true);
        assertTrue(mockSubjectService.delete(id));
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageSubjectsReturnListOfExistingSubjects() throws RepositoryException, ValidateException {
        Pagination pagination = Pagination.builder()
                .setRows(23)
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("subject")
                .setDefaultColumn("subject")
                .build();
        when(mockValidatePagination.validate(pagination)).thenReturn(mockValidatePagination);
        System.out.println(pagination);

        SetPage setPage = SetPage.builder()
                .setStartRows(pagination.getStartRows())
                .setRecordsPerPage(pagination.getRecordsPerPage())
                .setSort(pagination.getSort())
                .setSortColumn(pagination.getSortColumn())
                .build();
        System.out.println(setPage);
//        SetPage setPage1 = new PaginationMapperService().paginationToSetPage(pagination);
//        System.out.println(setPage1);
        List<SubjectEntity> all = List.of(existingSubjectEntity, existingSubjectEntity);

        when(mockSubjectDaoImpl.getPage(setPage)).thenReturn(all);
        List<SubjectDTO> actual = mockSubjectService.getPage(pagination);

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsSubjectsReturnNumberOfRowsOfExistingSubjects() throws RepositoryException {
        Integer expectedRows = 23;
        when(mockSubjectDaoImpl.getNumberOfRows()).thenReturn(expectedRows);
        Integer rows = mockSubjectService.getNumberOfRows();

        assertEquals(expectedRows, rows);
    }

    @DisplayName("Equals Class Constructors")
    @Test
    void whenConstructorSubjectServiceEmptyShouldReturnEqualsSubjectService() {
        SubjectServiceImpl subjectService = new SubjectServiceImpl();

        Field[] all = subjectService.getClass().getDeclaredFields();
        Field field;
        Field field1;
        Field field2;
        DaoFactory daoFactory;
        ValidateSubjectDTO validateSubjectDTO;
        ValidatePagination validatePagination;
        SubjectServiceImpl subjectService2 = new SubjectServiceImpl();
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        try {
            field = subjectService.getClass().getDeclaredField(all[1].getName());
            field.setAccessible(true);
            daoFactory = (DaoFactory) field.get(subjectService);
            System.out.println(daoFactory);

            field1 = subjectService.getClass().getDeclaredField(all[2].getName());
            field1.setAccessible(true);
            validateSubjectDTO = (ValidateSubjectDTO) field1.get(subjectService);
            System.out.println(validateSubjectDTO);

            field2 = subjectService.getClass().getDeclaredField(all[3].getName());
            field2.setAccessible(true);
            validatePagination = (ValidatePagination) field2.get(subjectService);
            System.out.println(validatePagination.getClass());
            subjectService2 = new SubjectServiceImpl(daoFactory, validateSubjectDTO, validatePagination);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(subjectService.hashCode());
        System.out.println(subjectService2.hashCode());

        //  UserServiceImpl mockUserServuceImpl = mock(UserServiceImpl.class);

        assertTrue(subjectService.equals(subjectService));
        assertFalse(subjectService.equals(null));
        assertFalse(subjectService.equals(new SubjectServiceImpl(this.daoFactory, mockValidateSubjectDTO, mockValidatePagination)));
        assertFalse(subjectService.equals(mock(UserServiceImpl.class)));
        assertEquals(subjectService.hashCode(), subjectService2.hashCode());
        assertEquals(4, all.length);
        assertEquals(subjectService, subjectService2);
    }
}
