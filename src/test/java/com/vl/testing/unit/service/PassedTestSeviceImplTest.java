package com.vl.testing.unit.service;

import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daofactory.JDBCDaoFactory;
import com.vl.testing.persistence.daos.PassedTestDao;
import com.vl.testing.persistence.daos.PassedTestDaoImpl;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;
import com.vl.testing.service.impl.PassedTestServiceImpl;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidatePassedTestDTO;
import com.vl.testing.validation.impl.ValidateUserDTO;
import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PassedTestSeviceImplTest {
    private static PassedTestServiceImpl mockPassedTestService;
    private static DaoFactory daoFactory;
    private static PassedTestDao mockPassedTestDaoImpl;
    private static PassedTestEntity existingPassedTestEntity;
    private static PassedTestDTO existingPassedTestDTO;
    private static UserEntity existingUserEntity;
    private static UserDTO existingUserDTO;
    private static TestEntity existingTestEntity;
    private static TestDTO existingTestDTO;
    private static ValidatePassedTestDTO mockValidatePassedTestDTO;
    private static ValidateUserDTO mockValidateUserDTO;
    private static ValidatePagination mockValidatePagination;
    private static Long id;

    @BeforeAll
    static void initGlobal() throws ValidateException {
        daoFactory = mock(JDBCDaoFactory.class);
        mockPassedTestDaoImpl = mock(PassedTestDaoImpl.class);
        mockValidatePagination = mock(ValidatePagination.class);
        mockValidatePassedTestDTO = mock(ValidatePassedTestDTO.class);
        mockValidateUserDTO = mock(ValidateUserDTO.class);
        mockPassedTestService = new PassedTestServiceImpl(daoFactory, mockValidatePassedTestDTO, mockValidateUserDTO, mockValidatePagination);
        when(daoFactory.createPassedTestDao()).thenReturn(mockPassedTestDaoImpl);
        when(mockValidatePassedTestDTO.validate(any(PassedTestDTO.class))).thenReturn(mockValidatePassedTestDTO);
        when(mockValidatePassedTestDTO.validateId(anyLong())).thenReturn(mockValidatePassedTestDTO);
        when(mockValidateUserDTO.validate(any(UserDTO.class))).thenReturn(mockValidateUserDTO);
        when(mockValidateUserDTO.validateId(anyLong())).thenReturn(mockValidateUserDTO);
    }

    @BeforeEach
    void setUp() {
        id = 2l;
        Long testId = 22l;
        existingTestEntity = TestEntity.builder()
                .setIdTest(testId)
                .setSubjectId(2l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        existingTestDTO = TestDTO.builder()
                .setId(testId)
                .setSubjectId(existingTestEntity.getSubjectId())
                .setTest(existingTestEntity.getTest())
                .setDifficulty(existingTestEntity.getDifficulty())
                .setRequests(existingTestEntity.getRequests())
                .setTime(existingTestEntity.getTime())
                .build();
        existingPassedTestEntity = PassedTestEntity.builder()
                .setIdPassedTest(null)
                .setUserId(3l)
                .setTestId(testId)
                .setResult(76)
                .build();
        existingPassedTestDTO = PassedTestDTO.builder()
                .setId(existingPassedTestEntity.getIdPassedTest())
                .setTest(existingTestDTO)
                .setResult(existingPassedTestEntity.getResult())
                .build();
        existingUserEntity = UserEntity.builder()
                .setIdUser(null)
                .setUsername("TestName")
                .setPassword("$2a$12$j8DHSQIgt8sKHfuiDy22PuHJipcZmuRYcdfBwwWVu1TsfW1TIrOeG") //12345678
                .setAdmin(false)
                .setBan(false)
                //.setLocale(Localization.RU.name())
                .setLocale(Localization.getNameByLocale(Localization.RU.getLocale()))
                .build();
        existingUserDTO = UserDTO.builder()
                .setId(existingUserEntity.getIdUser())
                .setUsername(existingUserEntity.getUsername())
                .setPassword(existingUserEntity.getPassword())
                .setAdmin(existingUserEntity.isAdmin())
                .setBan(existingUserEntity.isBan())
                .setLocale(Localization.getLocaleByName(existingUserEntity.getLocale()))
                .build();
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllPassedTestsReturnListOfExistingPassedTests() throws RepositoryException, ValidateException {
        existingPassedTestEntity.setIdPassedTest(id);
        LinkedHashMap<PassedTestEntity, TestEntity> all = new LinkedHashMap<>();
        all.put(existingPassedTestEntity, existingTestEntity);

        TestEntity testEntity = TestEntity.builder()
                .setIdTest(existingTestEntity.getIdTest() + 1)
                .setSubjectId(existingTestEntity.getSubjectId())
                .setTest(existingTestEntity.getTest())
                .setDifficulty(existingTestEntity.getDifficulty())
                .setRequests(existingTestEntity.getRequests())
                .setTime(existingTestEntity.getTime())
                .build();

        PassedTestEntity passedTestEntity = PassedTestEntity.builder()
                .setIdPassedTest(existingPassedTestEntity.getIdPassedTest() + 1)
                .setUserId(existingPassedTestEntity.getUserId())
                .setTestId(testEntity.getIdTest())
                .setResult(existingPassedTestEntity.getResult())
                .build();

        all.put(passedTestEntity, testEntity);
        existingUserDTO.setId(id);
        existingUserEntity.setIdUser(id);
        when(mockPassedTestDaoImpl.getAllPassedTestFosUser(existingUserEntity)).thenReturn(all);

        List<PassedTestDTO> actual = mockPassedTestService.getAllPassedTestFosUser(existingUserDTO);
        actual.forEach(x -> System.out.println(x));

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }

    @DisplayName("Create")
    @Test
    void addingPassedTestShouldReturnCreatedPassedTest() throws RepositoryException, ValidateException {
        existingUserDTO.setId(id + 1);
        PassedTestEntity passedTestEntity = PassedTestEntity.builder()
                .setIdPassedTest(id)
                .setUserId(existingUserDTO.getId())
                .setTestId(existingTestDTO.getId())
                .setResult(existingPassedTestEntity.getResult())
                .build();

        when(mockPassedTestDaoImpl.create(existingPassedTestEntity)).thenReturn(passedTestEntity);
        PassedTestDTO created = mockPassedTestService.create(existingPassedTestDTO, existingUserDTO);
        System.out.println(created);
        PassedTestDTO expected = existingPassedTestDTO;
        expected.setId(id);
        System.out.println(expected);
        assertEquals(expected, created);
    }

    @DisplayName("Update")
    @Test
    void whenUpdatePassedTestShouldReturnUpdatedPassedTest() throws RepositoryException, ValidateException {
        existingPassedTestEntity.setIdPassedTest(id);
        existingPassedTestEntity.setUserId(null);
        System.out.println(existingPassedTestEntity);
        existingPassedTestDTO.setId(id);

        when(mockPassedTestDaoImpl.update(existingPassedTestEntity)).thenReturn(Optional.of(existingPassedTestEntity));
        PassedTestDTO updated = mockPassedTestService.update(existingPassedTestDTO).get();
        existingPassedTestDTO.setTest(null);

        assertEquals(existingPassedTestDTO, updated);
    }

    @DisplayName("Update empty PassedTest")
    @Test
    void whenUpdatePassedTestGetEmptyPassedTests() throws RepositoryException, ValidateException {
        existingPassedTestEntity.setIdPassedTest(id);
        existingPassedTestEntity.setUserId(null);
        existingPassedTestDTO.setId(id);

        when(mockPassedTestDaoImpl.update(existingPassedTestEntity)).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockPassedTestService.update(existingPassedTestDTO));
    }

    @DisplayName("getPage")
    @Test
    void whenGetPagePassedTestsReturnListOfExistingPassedTests() throws RepositoryException, ValidateException {
        Pagination pagination = Pagination.builder()
                .setRows(23)
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("test")
                .setDefaultColumn("test")
                .build();
        SetPage setPage = SetPage.builder()
                .setStartRows(pagination.getStartRows())
                .setRecordsPerPage(pagination.getRecordsPerPage())
                .setSort(pagination.getSort())
                .setSortColumn(pagination.getSortColumn())
                .build();
        when(mockValidatePagination.validate(pagination)).thenReturn(mockValidatePagination);

        existingPassedTestEntity.setIdPassedTest(id);
        existingUserDTO.setId(id);
        existingUserEntity.setIdUser(id);
        LinkedHashMap<PassedTestEntity, TestEntity> all = new LinkedHashMap<>();
        all.put(existingPassedTestEntity, existingTestEntity);
        TestEntity testEntity = TestEntity.builder()
                .setIdTest(existingTestEntity.getIdTest() + 1)
                .setSubjectId(existingTestEntity.getSubjectId())
                .setTest(existingTestEntity.getTest())
                .setDifficulty(existingTestEntity.getDifficulty())
                .setRequests(existingTestEntity.getRequests())
                .setTime(existingTestEntity.getTime())
                .build();
        PassedTestEntity passedTestEntity = PassedTestEntity.builder()
                .setIdPassedTest(existingPassedTestEntity.getIdPassedTest() + 1)
                .setUserId(existingPassedTestEntity.getUserId())
                .setTestId(testEntity.getIdTest())
                .setResult(existingPassedTestEntity.getResult())
                .build();
        all.put(passedTestEntity, testEntity);

        existingUserDTO.setId(id);
        existingUserEntity.setIdUser(id);
        when(mockPassedTestDaoImpl.getPage(existingUserEntity, setPage)).thenReturn(all);
        List<PassedTestDTO> actual = mockPassedTestService.getPage(existingUserDTO, pagination);

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }


    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsPassedTestsReturnNumberOfRowsOfExistingPassedTests() throws RepositoryException, ValidateException {
        Integer expectedRows = 23;
        existingUserDTO.setId(id);
        existingUserEntity.setIdUser(id);

        when(mockPassedTestDaoImpl.getNumberOfRows(existingUserEntity)).thenReturn(expectedRows);
        Integer rows = mockPassedTestService.getNumberOfRows(existingUserDTO);

        assertEquals(expectedRows, rows);
    }

    @DisplayName("Equals Class Constructors")
    @Test
    void whenConstructorTestServiceEmptyShouldReturnEqualsTestService() {
        PassedTestServiceImpl passedTestService = new PassedTestServiceImpl();

        Field[] all = passedTestService.getClass().getDeclaredFields();
        Field field;

        DaoFactory daoFactory;
        ValidatePassedTestDTO validatePassedTestDTO;
        ValidateUserDTO validateUserDTO;
        ValidatePagination validatePagination;
        PassedTestServiceImpl testService2 = new PassedTestServiceImpl();
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        try {
            field = passedTestService.getClass().getDeclaredField(all[1].getName());
            field.setAccessible(true);
            daoFactory = (DaoFactory) field.get(passedTestService);
            System.out.println(daoFactory);

            field = passedTestService.getClass().getDeclaredField(all[2].getName());
            field.setAccessible(true);
            validatePassedTestDTO = (ValidatePassedTestDTO) field.get(passedTestService);
            System.out.println(validatePassedTestDTO);

            field = passedTestService.getClass().getDeclaredField(all[3].getName());
            field.setAccessible(true);
            validateUserDTO = (ValidateUserDTO) field.get(passedTestService);
            System.out.println(validateUserDTO);

            field = passedTestService.getClass().getDeclaredField(all[4].getName());
            field.setAccessible(true);
            validatePagination = (ValidatePagination) field.get(passedTestService);
            System.out.println(validatePagination.getClass());
            testService2 = new PassedTestServiceImpl(daoFactory, validatePassedTestDTO, validateUserDTO, validatePagination);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(passedTestService.hashCode());
        System.out.println(testService2.hashCode());

        //  UserServiceImpl mockUserServuceImpl = mock(UserServiceImpl.class);

        assertTrue(passedTestService.equals(passedTestService));
        assertFalse(passedTestService.equals(null));
     //   assertFalse(passedTestService.equals(new TestServiceImpl(this.daoFactory, mockValidateTestDTO, mockValidateSubjectDTO, mockValidatePagination)));
        assertFalse(passedTestService.equals(mock(UserServiceImpl.class)));
        assertEquals(passedTestService.hashCode(), testService2.hashCode());
        assertEquals(5, all.length);
        assertEquals(passedTestService, testService2);
    }
}
