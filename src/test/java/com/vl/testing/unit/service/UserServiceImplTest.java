package com.vl.testing.unit.service;

import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.UserDTO;
import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daofactory.JDBCDaoFactory;
import com.vl.testing.persistence.daos.UserDaoImpl;

import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;
import com.vl.testing.service.enums.ExceptionMessage;
import com.vl.testing.service.exeptions.PasswordException;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateUserDTO;
import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {
    private static UserServiceImpl mockUserService;
    private static DaoFactory daoFactory;
    private static UserDaoImpl mockUserDaoImpl;
    private static UserEntity existingUserEntity;
    private static UserDTO existingUserDTO;
    private static ValidateUserDTO mockValidateUserDTO;
    private static ValidatePagination mockValidatePagination;
    private static Long id;

    @BeforeAll
    static void initGlobal() throws ValidateException {
        daoFactory = mock(JDBCDaoFactory.class);
        mockUserDaoImpl = mock(UserDaoImpl.class);
        mockValidatePagination = mock(ValidatePagination.class);
        mockValidateUserDTO = mock(ValidateUserDTO.class);
        mockUserService = new UserServiceImpl(daoFactory, mockValidateUserDTO, mockValidatePagination);
        when(daoFactory.createUserDao()).thenReturn(mockUserDaoImpl);
        when(mockValidateUserDTO.validate(any(UserDTO.class))).thenReturn(mockValidateUserDTO);
        when(mockValidateUserDTO.validateId(anyLong())).thenReturn(mockValidateUserDTO);
        when(mockValidateUserDTO.validateAdminsFields(any(UserDTO.class))).thenReturn(mockValidateUserDTO);
        when(mockValidateUserDTO.validateNameAndPassword(any(UserDTO.class))).thenReturn(mockValidateUserDTO);
    }

    @BeforeEach
    void setUp() {
        id = 2l;
        existingUserEntity = UserEntity.builder()
                .setIdUser(null)
                .setUsername("Test")
                .setPassword("$2a$12$j8DHSQIgt8sKHfuiDy22PuHJipcZmuRYcdfBwwWVu1TsfW1TIrOeG") //12345678
                .setAdmin(true)
                .setBan(false)
                //.setLocale(Localization.RU.name())
                .setLocale(Localization.getNameByLocale(Localization.RU.getLocale()))
                .build();
        existingUserDTO = UserDTO.builder()
                .setId(existingUserEntity.getIdUser())
                .setUsername(existingUserEntity.getUsername())
                .setPassword("12345678")
                .setAdmin(existingUserEntity.isAdmin())
                .setBan(existingUserEntity.isBan())
                .setLocale(Localization.getLocaleByName(existingUserEntity.getLocale()))
                .build();
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllUsersReturnListOfExistingUsers() throws RepositoryException {
        existingUserEntity.setIdUser(id);
        List<UserEntity> all = List.of(existingUserEntity, existingUserEntity);

        when(mockUserDaoImpl.getAll()).thenReturn(all);

        List<UserDTO> actual = mockUserService.getAll();
        actual.forEach(x -> System.out.println(x));

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }

    @DisplayName("Get")
    @Test
    void whenGetUsersEqualsOneOfGetUsers() throws RepositoryException, ValidateException {
        existingUserEntity.setIdUser(id);
        when(mockUserDaoImpl.get(id)).thenReturn(Optional.of(existingUserEntity));
        UserDTO actual = mockUserService.get(id).get();
        System.out.println(actual);
        existingUserDTO.setId(id);

        assertEquals(existingUserDTO, actual);
    }

    @DisplayName("Get empty User")
    @Test
    void whenGetUserGetEmptyUser() throws RepositoryException, ValidateException {
        when(mockUserDaoImpl.get(id)).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockUserService.get(id));
    }

    @DisplayName("Update")
    @Test
    void whenUpdateUserShouldReturnUpdatedUser() throws RepositoryException, ValidateException {
        existingUserEntity.setIdUser(id);
        existingUserDTO.setId(id);

        when(mockUserDaoImpl.update(any(UserEntity.class))).thenReturn(Optional.of(existingUserEntity));
        UserDTO updated = mockUserService.update(existingUserDTO).get();

        assertEquals(existingUserDTO, updated);
    }

    @DisplayName("Update empty User")
    @Test
    void whenUpdateUserGetEmptyUsers() throws RepositoryException, ValidateException {
        existingUserEntity.setIdUser(id);
        existingUserDTO.setId(id);

        when(mockUserDaoImpl.update(any(UserEntity.class))).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockUserService.update(existingUserDTO));
    }

    @DisplayName("Create")
    @Test
    void addingUserShouldReturnCreatedUser() throws RepositoryException, ValidateException {
        UserEntity userEntityWithID = UserEntity.builder()
                .setIdUser(id)
                .setUsername("Test")
                .setPassword("$2a$12$j8DHSQIgt8sKHfuiDy22PuHJipcZmuRYcdfBwwWVu1TsfW1TIrOeG") //12345678
                .setAdmin(true)
                .setBan(false)
                .setLocale(Localization.getNameByLocale(Localization.RU.getLocale()))
                .build();
        when(mockUserDaoImpl.create(any(UserEntity.class))).thenReturn(userEntityWithID);
        UserDTO created = mockUserService.create(existingUserDTO);
        UserDTO expected = existingUserDTO;
        expected.setId(id);
        expected.setPassword("");

        assertEquals(expected, created);
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedUserShouldReturnTrue() throws RepositoryException, ValidateException {
        when(mockUserDaoImpl.delete(id)).thenReturn(true);
        assertTrue(mockUserService.delete(id));
    }

    @DisplayName("GetByNameAndPassword")
    @Test
    void whenGetByNameAndPasswordUsersEqualsOneOfGetUsers() throws RepositoryException, ValidateException, PasswordException {
        existingUserEntity.setIdUser(id);
        when(mockUserDaoImpl.getByName(existingUserEntity.getUsername())).thenReturn(Optional.of(existingUserEntity));
        UserDTO actual = mockUserService.getByNameAndPassword(existingUserDTO).get();
        System.out.println(actual);
        existingUserDTO.setId(id);

        assertEquals(existingUserDTO, actual);
    }

    @DisplayName("GetByNameAndPassword empty User")
    @Test
    void whenGetByNameAndPasswordUserGetEmptyUser() throws RepositoryException, ValidateException, PasswordException {
        when(mockUserDaoImpl.getByName(existingUserEntity.getUsername())).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockUserService.getByNameAndPassword(existingUserDTO));
    }

    @DisplayName("GetByNameAndPassword return Exeption")
    @Test
    void whenGetByNameAndPasswordUsersReturnThrown() throws RepositoryException {
        existingUserEntity.setIdUser(id);
        existingUserEntity.setPassword("$2a$12$wpLOBmJnZ3T5J1lZy.hypepo4vwQHBo3PZkH.27YGRQiMJInUotTm"); //1234
        when(mockUserDaoImpl.getByName(existingUserEntity.getUsername())).thenReturn(Optional.of(existingUserEntity));
        Throwable thrown = assertThrows(PasswordException.class, () -> {
            mockUserService.getByNameAndPassword(existingUserDTO);
        });

        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains(ExceptionMessage.INVALID_PASSWORD.getMessage()));
    }

    @DisplayName("UpdateAdminsFields")
    @Test
    void whenUpdateAdminsFieldsUserShouldReturnUpdatedUser() throws RepositoryException, ValidateException {
        existingUserEntity.setIdUser(id);
        existingUserDTO.setId(id);

        when(mockUserDaoImpl.updateAdminsFields(any(UserEntity.class))).thenReturn(Optional.of(existingUserEntity));
        UserDTO updated = mockUserService.updateAdminsFields(existingUserDTO).get();

        assertEquals(existingUserDTO, updated);
    }

    @DisplayName("UpdateAdminsFields empty User")
    @Test
    void whenUpdateAdminsFieldsUserGetEmptyUsers() throws RepositoryException, ValidateException {
        existingUserEntity.setIdUser(id);
        existingUserDTO.setId(id);

        when(mockUserDaoImpl.updateAdminsFields(any(UserEntity.class))).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockUserService.updateAdminsFields(existingUserDTO));
    }

    @DisplayName("UpdateUsersFields")
    @Test
    void whenUpdateUsersFieldsUserShouldReturnUpdatedUser() throws RepositoryException, ValidateException {
        existingUserEntity.setIdUser(id);
        existingUserDTO.setId(id);

        when(mockUserDaoImpl.updateUsersFields(any(UserEntity.class))).thenReturn(Optional.of(existingUserEntity));
        UserDTO updated = mockUserService.updateUsersFields(existingUserDTO).get();

        assertEquals(existingUserDTO, updated);
    }

    @DisplayName("UpdateUsersFields empty User")
    @Test
    void whenUpdateUsersFieldsUserGetEmptyUsers() throws RepositoryException, ValidateException {
        existingUserEntity.setIdUser(id);
        existingUserDTO.setId(id);

        when(mockUserDaoImpl.updateUsersFields(any(UserEntity.class))).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockUserService.updateUsersFields(existingUserDTO));
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageUsersReturnListOfExistingUsers() throws RepositoryException, ValidateException {
        Pagination pagination = Pagination.builder()
                .setRows(23)
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn(DefaultColumn.USER.getColumn())
                .setDefaultColumn(DefaultColumn.USER.getColumn())
                .build();
        SetPage setPage = SetPage.builder()
                .setStartRows(pagination.getStartRows())
                .setRecordsPerPage(pagination.getRecordsPerPage())
                .setSort(pagination.getSort())
                .setSortColumn(pagination.getSortColumn())
                .build();
        when(mockValidatePagination.validate(pagination)).thenReturn(mockValidatePagination);
        System.out.println(pagination);
        existingUserEntity.setIdUser(id);
        List<UserEntity> all = List.of(existingUserEntity, existingUserEntity);
        System.out.println(setPage);

        when(mockUserDaoImpl.getPage(setPage)).thenReturn(all);
        List<UserDTO> actual = mockUserService.getPage(pagination);

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }

    @DisplayName("GetPageLoggedUsers")
    @Test
    void whenGetPageLoggedUsersReturnListOfExistingUsers() throws ValidateException {
        existingUserDTO.setId(id);
        LocalDateTime date = LocalDateTime.of(2024, Month.JANUARY, 16, 12, 15, 00);
        existingUserDTO.setLoginDateTime(date);
        List<UserDTO> all = new ArrayList<>();
        all.add(existingUserDTO);
        for (int i = 1; i <= 14; i++) {
            UserDTO user = UserDTO.builder()
                    .setId(existingUserDTO.getId() + i)
                    .setUsername(existingUserEntity.getUsername() + " " + (14 - i))
                    .setPassword("12345678" + i)
                    .setAdmin(existingUserEntity.isAdmin())
                    .setBan(existingUserEntity.isBan())
                    .setLocale(Localization.getLocaleByName(existingUserEntity.getLocale()))
                    .setLoginTime(date)
                    .build();
            System.out.println(user);
            all.add(user);
        }
        Pagination pagination = Pagination.builder()
                .setRows(all.size())
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn(DefaultColumn.USER.getColumn())
                .setDefaultColumn(DefaultColumn.USER.getColumn())
                .build();
        all.forEach(x -> System.out.println(x));

        List<UserDTO> actual = mockUserService.getPageLoggedUsers(all, pagination);
        actual.forEach(x -> System.out.println(x));

        List<UserDTO> expected = new ArrayList<>();
        expected = all.stream()
                .sorted(Comparator.comparing(UserDTO::getUsername))
                .skip(pagination.getStartRows())
                .limit(pagination.getRecordsPerPage())
                .collect(Collectors.toList());
        expected.forEach(x -> System.out.println(x));

        assertFalse(actual.isEmpty());
        assertEquals(5, actual.size());
        assertEquals(expected, actual);
    }


    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsUsersReturnNumberOfRowsOfExistingUsers() throws RepositoryException {
        Integer expectedRows = 23;

        when(mockUserDaoImpl.getNumberOfRows()).thenReturn(expectedRows);
        Integer rows = mockUserService.getNumberOfRows();

        assertEquals(expectedRows, rows);
    }

    @DisplayName("getNumberOfRowsLoggedUsers")
    @Test
    void whenGetNumberOfRowsLoggedUsersReturnNumberOfRowsOfExistingUsers() {
        Integer expectedRows = 2;
        List<UserDTO> all = List.of(existingUserDTO, existingUserDTO);

        Integer rows = mockUserService.getNumberOfRowsLoggedUsers(all);

        assertEquals(expectedRows, rows);
    }

    @DisplayName("Equals Class Constructors")
    @Test
    void whenConstructorUserServiceEmptyShouldReturnEqualsUserService() {
        UserServiceImpl userService = new UserServiceImpl();

        Field[] all = userService.getClass().getDeclaredFields();
        Field field;

        DaoFactory daoFactory;
        ValidateUserDTO validateUserDTO;
        ValidatePagination validatePagination;
        UserServiceImpl userService2 = new UserServiceImpl();
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        try {
            field = userService.getClass().getDeclaredField(all[1].getName());
            field.setAccessible(true);
            daoFactory = (DaoFactory) field.get(userService);
            System.out.println(daoFactory);

            field = userService.getClass().getDeclaredField(all[2].getName());
            field.setAccessible(true);
            validateUserDTO = (ValidateUserDTO) field.get(userService);
            System.out.println(validateUserDTO);

            field = userService.getClass().getDeclaredField(all[3].getName());
            field.setAccessible(true);
            validatePagination = (ValidatePagination) field.get(userService);
            System.out.println(validatePagination.getClass());
            userService2 = new UserServiceImpl(daoFactory, validateUserDTO, validatePagination);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(userService.hashCode());
        System.out.println(userService2.hashCode());

        assertTrue(userService.equals(userService));
        assertFalse(userService.equals(null));
        assertFalse(userService.equals(new UserServiceImpl(this.daoFactory, mockValidateUserDTO, mockValidatePagination)));
        assertFalse(userService.equals(mock(UserServiceImpl.class)));
        assertEquals(userService.hashCode(), userService2.hashCode());
        assertEquals(6, all.length);
        assertEquals(userService, userService2);
    }
}
