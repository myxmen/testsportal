package com.vl.testing.unit.service;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daofactory.JDBCDaoFactory;
import com.vl.testing.persistence.daos.QuestionDaoImpl;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;
import com.vl.testing.service.impl.QuestionServiceImpl;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateQuestionDTO;
import com.vl.testing.validation.impl.ValidateTestDTO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class QuestionServiceImplTest {
    private static QuestionServiceImpl mockQuestionService;
    private static DaoFactory daoFactory;
    private static QuestionDaoImpl mockQuestionDaoImpl;
    private static QuestionDTO existingQuestionDTO;
    private static QuestionEntity existingQuestionEntity;
    private static TestEntity existingTestEntity;
    private static TestDTO existingTestDTO;
    private static ValidateQuestionDTO mockValidateQuestionDTO;
    private static ValidateTestDTO mockValidateTestDTO;
    private static ValidatePagination mockValidatePagination;
    private static Long id;

    @BeforeAll
    static void initGlobal() throws ValidateException {
        daoFactory = mock(JDBCDaoFactory.class);
        mockQuestionDaoImpl = mock(QuestionDaoImpl.class);
        mockValidatePagination = mock(ValidatePagination.class);
        mockValidateQuestionDTO = mock(ValidateQuestionDTO.class);
        mockValidateTestDTO = mock(ValidateTestDTO.class);
        mockQuestionService = new QuestionServiceImpl(daoFactory, mockValidateQuestionDTO, mockValidateTestDTO, mockValidatePagination);
        when(daoFactory.createQuestionDao()).thenReturn(mockQuestionDaoImpl);
        when(mockValidateTestDTO.validate(any(TestDTO.class))).thenReturn(mockValidateTestDTO);
        when(mockValidateTestDTO.validateId(anyLong())).thenReturn(mockValidateTestDTO);
        when(mockValidateQuestionDTO.validate(any(QuestionDTO.class))).thenReturn(mockValidateQuestionDTO);
        when(mockValidateQuestionDTO.validateId(anyLong())).thenReturn(mockValidateQuestionDTO);
    }

    @BeforeEach
    void setUp() {
        id = 2l;
        existingTestEntity = TestEntity.builder()
                .setIdTest(null)
                .setSubjectId(2l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        existingQuestionEntity = QuestionEntity.builder()
                .setIdQuestion(null)
                .setTestId(id)
                .setNumberOfQuestion(3)
                .setQuestion("newTestQuestion3")
                .setAnswer1("newTestAnswer1")
                .setAnswer2("newTestAnswer2")
                .setAnswer3("newTestAnswer3")
                .setAnswer4("newTestAnswer4")
                .setAnswer5("newTestAnswer5")
                .setCorrectAnswer1(true)
                .setCorrectAnswer2(true)
                .setCorrectAnswer3(false)
                .setCorrectAnswer4(true)
                .setCorrectAnswer5(true)
                .build();
        existingQuestionDTO = QuestionDTO.builder()
                .setId(existingQuestionEntity.getIdQuestion())
                .setTestId(existingQuestionEntity.getTestId())
                .setNumberOfQuestion(existingQuestionEntity.getNumberOfQuestion())
                .setQuestion(existingQuestionEntity.getQuestion())
                .setAnswer1(existingQuestionEntity.getAnswer1())
                .setAnswer2(existingQuestionEntity.getAnswer2())
                .setAnswer3(existingQuestionEntity.getAnswer3())
                .setAnswer4(existingQuestionEntity.getAnswer4())
                .setAnswer5(existingQuestionEntity.getAnswer5())
                .setCorrectAnswer1(existingQuestionEntity.isCorrectAnswer1())
                .setCorrectAnswer2(existingQuestionEntity.isCorrectAnswer2())
                .setCorrectAnswer3(existingQuestionEntity.isCorrectAnswer3())
                .setCorrectAnswer4(existingQuestionEntity.isCorrectAnswer4())
                .setCorrectAnswer5(existingQuestionEntity.isCorrectAnswer5())
                .build();
        existingTestDTO = TestDTO.builder()
                .setId(null)
                .setSubjectId(existingTestEntity.getSubjectId())
                .setTest(existingTestEntity.getTest())
                .setDifficulty(existingTestEntity.getDifficulty())
                .setRequests(existingTestEntity.getRequests())
                .setTime(existingTestEntity.getTime())
                .build();
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllByTestQuestionsReturnListOfExistingQuestions() throws RepositoryException, ValidateException {
        existingTestEntity.setIdTest(id);
        existingTestDTO.setId(id);
        existingQuestionEntity.setIdQuestion(id);
        List<QuestionEntity> all = List.of(existingQuestionEntity, existingQuestionEntity);

        when(mockQuestionDaoImpl.getAllByTestId(existingTestEntity)).thenReturn(all);

        List<QuestionDTO> actual = mockQuestionService.getAllByTest(existingTestDTO);
        actual.forEach(x -> System.out.println(x));

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }

    @DisplayName("Get")
    @Test
    void whenGetQuestionsEqualsOneOfGetQuestions() throws RepositoryException, ValidateException {
        existingQuestionEntity.setIdQuestion(id);
        when(mockQuestionDaoImpl.get(id)).thenReturn(Optional.of(existingQuestionEntity));
        QuestionDTO actual = mockQuestionService.get(id).get();
        System.out.println(actual);
        existingQuestionDTO.setId(id);

        assertEquals(existingQuestionDTO, actual);
    }

    @DisplayName("Get empty Question")
    @Test
    void whenGetQuestionGetEmptyQuestion() throws RepositoryException, ValidateException {
        when(mockQuestionDaoImpl.get(id)).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockQuestionService.get(id));
    }

    @DisplayName("Update")
    @Test
    void whenUpdateQuestionShouldReturnUpdatedQuestion() throws RepositoryException, ValidateException {
        existingQuestionEntity.setIdQuestion(id);
        existingQuestionDTO.setId(id);

        when(mockQuestionDaoImpl.update(existingQuestionEntity)).thenReturn(Optional.of(existingQuestionEntity));
        QuestionDTO updated = mockQuestionService.update(existingQuestionDTO).get();

        assertEquals(existingQuestionDTO, updated);
    }

    @DisplayName("Update empty Question")
    @Test
    void whenUpdateQuestionGetEmptyQuestions() throws RepositoryException, ValidateException {
        existingQuestionEntity.setIdQuestion(id);
        existingQuestionDTO.setId(id);

        when(mockQuestionDaoImpl.update(existingQuestionEntity)).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockQuestionService.update(existingQuestionDTO));
    }

    @DisplayName("Create")
    @Test
    void addingQuestionShouldReturnCreatedQuestion() throws RepositoryException, ValidateException {
        QuestionEntity questionEntityWithID = QuestionEntity.builder()
                .setIdQuestion(id)
                .setTestId(existingQuestionEntity.getTestId())
                .setNumberOfQuestion(existingQuestionEntity.getNumberOfQuestion())
                .setQuestion(existingQuestionEntity.getQuestion())
                .setAnswer1(existingQuestionEntity.getAnswer1())
                .setAnswer2(existingQuestionEntity.getAnswer2())
                .setAnswer3(existingQuestionEntity.getAnswer3())
                .setAnswer4(existingQuestionEntity.getAnswer4())
                .setAnswer5(existingQuestionEntity.getAnswer5())
                .setCorrectAnswer1(existingQuestionEntity.isCorrectAnswer1())
                .setCorrectAnswer2(existingQuestionEntity.isCorrectAnswer2())
                .setCorrectAnswer3(existingQuestionEntity.isCorrectAnswer3())
                .setCorrectAnswer4(existingQuestionEntity.isCorrectAnswer4())
                .setCorrectAnswer5(existingQuestionEntity.isCorrectAnswer5())
                .build();
        when(mockQuestionDaoImpl.create(existingQuestionEntity)).thenReturn(questionEntityWithID);
        QuestionDTO created = mockQuestionService.create(existingQuestionDTO);
        QuestionDTO expected = existingQuestionDTO;
        expected.setId(id);

        assertEquals(expected, created);
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedQuestionShouldReturnTrue() throws RepositoryException, ValidateException {
        when(mockQuestionDaoImpl.delete(id)).thenReturn(true);
        assertTrue(mockQuestionService.delete(id));
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsQuestionsReturnNumberOfRowsOfExistingQuestions() throws RepositoryException, ValidateException {
        Integer expectedRows = 23;
        existingTestEntity.setIdTest(id);
        existingTestDTO.setId(id);
        existingQuestionEntity.setIdQuestion(id);
        when(mockQuestionDaoImpl.getNumberOfRows(existingTestEntity)).thenReturn(expectedRows);
        Integer rows = mockQuestionService.getNumberOfRows(existingTestDTO);

        assertEquals(expectedRows, rows);
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageQuestionsReturnListOfExistingQuestions() throws RepositoryException, ValidateException {
        Pagination pagination = Pagination.builder()
                .setRows(23)
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("test")
                .setDefaultColumn("test")
                .build();
        SetPage setPage = SetPage.builder()
                .setStartRows(pagination.getStartRows())
                .setRecordsPerPage(pagination.getRecordsPerPage())
                .setSort(pagination.getSort())
                .setSortColumn(pagination.getSortColumn())
                .build();
        when(mockValidatePagination.validate(pagination)).thenReturn(mockValidatePagination);
        System.out.println(pagination);
        existingTestEntity.setIdTest(id);
        existingTestDTO.setId(id);
        System.out.println(setPage);
        System.out.println(existingTestEntity);
        System.out.println(existingTestDTO);
        List<QuestionEntity> all = List.of(existingQuestionEntity, existingQuestionEntity);
        when(mockQuestionDaoImpl.getPage(existingTestEntity, setPage)).thenReturn(all);
        List<QuestionDTO> actual = mockQuestionService.getPage(existingTestDTO, pagination);

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }

    @DisplayName("getScore")
    @ParameterizedTest
    @MethodSource("testCases")
    void whenGetScoreReturnScore(String[] inFirst, String[] inSecond, Integer expected) {
        LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        existingQuestionDTO.setId(id);
        QuestionDTO existingQuestionDTOSecond = QuestionDTO.builder()
                .setId(id + 1)
                .setTestId(existingQuestionEntity.getTestId())
                .setNumberOfQuestion(existingQuestionEntity.getNumberOfQuestion())
                .setQuestion(existingQuestionEntity.getQuestion())
                .setAnswer1(existingQuestionEntity.getAnswer1())
                .setAnswer2(existingQuestionEntity.getAnswer2())
                .setAnswer3(existingQuestionEntity.getAnswer3())
                .setAnswer4(existingQuestionEntity.getAnswer4())
                .setAnswer5(existingQuestionEntity.getAnswer5())
                .setCorrectAnswer1(existingQuestionEntity.isCorrectAnswer1())
                .setCorrectAnswer2(existingQuestionEntity.isCorrectAnswer2())
                .setCorrectAnswer3(existingQuestionEntity.isCorrectAnswer3())
                .setCorrectAnswer4(existingQuestionEntity.isCorrectAnswer4())
                .setCorrectAnswer5(existingQuestionEntity.isCorrectAnswer5())
                .build();
        questionToAnswerMap.put(existingQuestionDTO, inFirst);
        questionToAnswerMap.put(existingQuestionDTOSecond, inSecond);
//        Arrays.stream(inFirst).forEach(x-> System.out.print(x));
//        System.out.println();
//        Arrays.stream(inSecond).forEach(x-> System.out.print(x));
//        System.out.println();
//        System.out.println(expected);
        assertEquals(expected, mockQuestionService.getScore(questionToAnswerMap, 2));
    }

    static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(new String[]{"1", "2", "4", "5"}, new String[]{"1", "2", "4", "5"}, 100),
                Arguments.of(new String[]{"1", "2", "4", "5"}, new String[]{"1"}, 50),
                Arguments.of(new String[]{"1"}, new String[]{"1", "2", "4", "5"}, 50),
                Arguments.of(new String[]{"3", "5"}, new String[]{"1"}, 0)
        );
    }

    @DisplayName("getScore with null Correct Answers")
    @ParameterizedTest
    @MethodSource("testCasesWithNull")
    void whenGetScoreWithNullCorrectAnswerReturnScore(String[] inFirst, String[] inSecond, Integer expected) {
        LinkedHashMap<QuestionDTO, String[]> questionToAnswerMap = new LinkedHashMap<>();
        existingQuestionDTO.setId(id);
        existingQuestionDTO.setCorrectAnswer5(null);
        QuestionDTO existingQuestionDTOSecond = QuestionDTO.builder()
                .setId(id + 1)
                .setTestId(existingQuestionEntity.getTestId())
                .setNumberOfQuestion(existingQuestionEntity.getNumberOfQuestion())
                .setQuestion(existingQuestionEntity.getQuestion())
                .setAnswer1(existingQuestionEntity.getAnswer1())
                .setAnswer2(existingQuestionEntity.getAnswer2())
                .setAnswer3(existingQuestionEntity.getAnswer3())
                .setAnswer4(existingQuestionEntity.getAnswer4())
                .setAnswer5(existingQuestionEntity.getAnswer5())
                .setCorrectAnswer1(existingQuestionEntity.isCorrectAnswer1())
                .setCorrectAnswer2(existingQuestionEntity.isCorrectAnswer2())
                .setCorrectAnswer3(existingQuestionEntity.isCorrectAnswer3())
                .setCorrectAnswer4(null)
                .setCorrectAnswer5(null)
                .build();
        questionToAnswerMap.put(existingQuestionDTO, inFirst);
        questionToAnswerMap.put(existingQuestionDTOSecond, inSecond);
        assertEquals(expected, mockQuestionService.getScore(questionToAnswerMap, 2));
    }

    static Stream<Arguments> testCasesWithNull() {
        return Stream.of(
                Arguments.of(new String[]{"1", "2", "4"}, new String[]{"1", "2"}, 100),
                Arguments.of(new String[]{"1", "2", "4"}, new String[]{"1"}, 50),
                Arguments.of(new String[]{"1"}, new String[]{"1", "2"}, 50),
                Arguments.of(new String[]{"3", "4"}, new String[]{"1"}, 0)
        );
    }

    @DisplayName("Equals Class Constructors")
    @Test
    void whenConstructorQuestionServiceEmptyShouldReturnEqualsQuestionService() {
        QuestionServiceImpl questionService = new QuestionServiceImpl();

        Field[] all = questionService.getClass().getDeclaredFields();
        Field field;

        DaoFactory daoFactory;
        ValidateQuestionDTO validateQuestionDTO;
        ValidateTestDTO validateTestDTO;
        ValidatePagination validatePagination;
        QuestionServiceImpl questionService2 = new QuestionServiceImpl();
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        try {
            field = questionService.getClass().getDeclaredField(all[1].getName());
            field.setAccessible(true);
            daoFactory = (DaoFactory) field.get(questionService);
            System.out.println(daoFactory);

            field = questionService.getClass().getDeclaredField(all[2].getName());
            field.setAccessible(true);
            validateQuestionDTO = (ValidateQuestionDTO) field.get(questionService);
            System.out.println(validateQuestionDTO);

            field = questionService.getClass().getDeclaredField(all[3].getName());
            field.setAccessible(true);
            validateTestDTO = (ValidateTestDTO) field.get(questionService);
            System.out.println(validateTestDTO);

            field = questionService.getClass().getDeclaredField(all[4].getName());
            field.setAccessible(true);
            validatePagination = (ValidatePagination) field.get(questionService);
            System.out.println(validatePagination.getClass());
            questionService2 = new QuestionServiceImpl(daoFactory, validateQuestionDTO, validateTestDTO, validatePagination);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(questionService.hashCode());
        System.out.println(questionService2.hashCode());

        //  UserServiceImpl mockUserServuceImpl = mock(UserServiceImpl.class);

        assertTrue(questionService.equals(questionService));
        assertFalse(questionService.equals(null));
        assertFalse(questionService.equals(new QuestionServiceImpl(this.daoFactory, mockValidateQuestionDTO, mockValidateTestDTO, mockValidatePagination)));
        assertFalse(questionService.equals(mock(UserServiceImpl.class)));
        assertEquals(questionService.hashCode(), questionService2.hashCode());
        assertEquals(6, all.length);
        assertEquals(questionService, questionService2);
    }
}
