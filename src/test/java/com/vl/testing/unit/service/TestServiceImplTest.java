package com.vl.testing.unit.service;

import com.vl.testing.controller.util.Pagination;
import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daofactory.JDBCDaoFactory;
import com.vl.testing.persistence.daos.TestDaoImpl;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;
import com.vl.testing.service.impl.TestServiceImpl;
import com.vl.testing.service.impl.UserServiceImpl;
import com.vl.testing.validation.ValidatePagination;
import com.vl.testing.validation.exeption.ValidateException;
import com.vl.testing.validation.impl.ValidateSubjectDTO;
import com.vl.testing.validation.impl.ValidateTestDTO;
import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestServiceImplTest {
    private static TestServiceImpl mockTestService;
    private static DaoFactory daoFactory;
    private static TestDaoImpl mockTestDaoImpl;
    private static TestEntity existingTestEntity;
    private static TestDTO existingTestDTO;
    private static SubjectDTO existingSubjectDTO;
    private static SubjectEntity existingSubjectEntity;
    private static ValidateTestDTO mockValidateTestDTO;
    private static ValidateSubjectDTO mockValidateSubjectDTO;
    private static ValidatePagination mockValidatePagination;
    private static Long id;

    @BeforeAll
    static void initGlobal() throws ValidateException {
        daoFactory = mock(JDBCDaoFactory.class);
        mockTestDaoImpl = mock(TestDaoImpl.class);
        mockValidatePagination = mock(ValidatePagination.class);
        mockValidateSubjectDTO = mock(ValidateSubjectDTO.class);
        mockValidateTestDTO = mock(ValidateTestDTO.class);
        mockTestService = new TestServiceImpl(daoFactory, mockValidateTestDTO, mockValidateSubjectDTO, mockValidatePagination);
        when(daoFactory.createTestDao()).thenReturn(mockTestDaoImpl);
        when(mockValidateTestDTO.validate(any(TestDTO.class))).thenReturn(mockValidateTestDTO);
        when(mockValidateTestDTO.validateId(anyLong())).thenReturn(mockValidateTestDTO);
        when(mockValidateSubjectDTO.validate(any(SubjectDTO.class))).thenReturn(mockValidateSubjectDTO);
        when(mockValidateSubjectDTO.validateId(anyLong())).thenReturn(mockValidateSubjectDTO);
    }

    @BeforeEach
    void setUp() {
        id = 2l;
        existingTestEntity = TestEntity.builder()
                .setIdTest(null)
                .setSubjectId(2l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        existingSubjectEntity = SubjectEntity.builder()
                .setIdSubject(null)
                .setSubject("Test")
                .build();
        existingSubjectDTO = SubjectDTO.builder()
                .setId(existingSubjectEntity.getIdSubject())
                .setSubject(existingSubjectEntity.getSubject())
                .setTests(null)
                .build();
        existingTestDTO = TestDTO.builder()
                .setId(null)
                .setSubjectId(existingTestEntity.getSubjectId())
                .setTest(existingTestEntity.getTest())
                .setDifficulty(existingTestEntity.getDifficulty())
                .setRequests(existingTestEntity.getRequests())
                .setTime(existingTestEntity.getTime())
                .build();
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllTestsReturnListOfExistingTests() throws RepositoryException, ValidateException {
        existingTestEntity.setIdTest(id);
        List<TestEntity> all = List.of(existingTestEntity, existingTestEntity);
        existingSubjectDTO.setId(id);
        existingSubjectEntity.setIdSubject(id);
        when(mockTestDaoImpl.getAllBySubjectId(existingSubjectEntity)).thenReturn(all);

        List<TestDTO> actual = mockTestService.getAllBySubject(existingSubjectDTO);
        actual.forEach(x -> System.out.println(x));

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }

    @DisplayName("Get")
    @Test
    void whenGetTestsEqualsOneOfGetTests() throws RepositoryException, ValidateException {
        existingTestEntity.setIdTest(id);
        when(mockTestDaoImpl.get(id)).thenReturn(Optional.of(existingTestEntity));
        TestDTO actual = mockTestService.get(id).get();
        System.out.println(actual);
        existingTestDTO.setId(id);

        assertEquals(existingTestDTO, actual);
    }

    @DisplayName("Get empty Test")
    @Test
    void whenGetTestGetEmptyTest() throws RepositoryException, ValidateException {
        when(mockTestDaoImpl.get(id)).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockTestService.get(id));
    }

    @DisplayName("Update")
    @Test
    void whenUpdateTestShouldReturnUpdatedTest() throws RepositoryException, ValidateException {
        existingTestEntity.setIdTest(id);
        existingTestDTO.setId(id);

        when(mockTestDaoImpl.update(existingTestEntity)).thenReturn(Optional.of(existingTestEntity));
        TestDTO updated = mockTestService.update(existingTestDTO).get();

        assertEquals(existingTestDTO, updated);
    }

    @DisplayName("Update empty Test")
    @Test
    void whenUpdateTestGetEmptyTests() throws RepositoryException, ValidateException {
        existingTestEntity.setIdTest(id);
        existingTestDTO.setId(id);

        when(mockTestDaoImpl.update(existingTestEntity)).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), mockTestService.update(existingTestDTO));
    }

    @DisplayName("Create")
    @Test
    void addingTestShouldReturnCreatedTest() throws RepositoryException, ValidateException {
        TestEntity testEntityWithID = TestEntity.builder()
                .setIdTest(id)
                .setSubjectId(existingTestEntity.getSubjectId())
                .setTest(existingTestEntity.getTest())
                .setDifficulty(existingTestEntity.getDifficulty())
                .setRequests(existingTestEntity.getRequests())
                .setTime(existingTestEntity.getTime())
                .build();
        when(mockTestDaoImpl.create(existingTestEntity)).thenReturn(testEntityWithID);
        TestDTO created = mockTestService.create(existingTestDTO);
        TestDTO expected = existingTestDTO;
        expected.setId(id);

        assertEquals(expected, created);
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedTestShouldReturnTrue() throws RepositoryException, ValidateException {
        when(mockTestDaoImpl.delete(id)).thenReturn(true);
        assertTrue(mockTestService.delete(id));
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageTestsReturnListOfExistingTests() throws RepositoryException, ValidateException {
        Pagination pagination = Pagination.builder()
                .setRows(23)
                .setCurrentPage("2")
                .setRecordsPerPage("5")
                .setSort(Sort.ASC.getSort())
                .setSortColumn("test")
                .setDefaultColumn("test")
                .build();
        SetPage setPage = SetPage.builder()
                .setStartRows(pagination.getStartRows())
                .setRecordsPerPage(pagination.getRecordsPerPage())
                .setSort(pagination.getSort())
                .setSortColumn(pagination.getSortColumn())
                .build();
        when(mockValidatePagination.validate(pagination)).thenReturn(mockValidatePagination);
        System.out.println(pagination);
        existingSubjectEntity.setIdSubject(id);
        existingSubjectDTO.setId(id);
        System.out.println(setPage);
        System.out.println(existingSubjectEntity);
        System.out.println(existingSubjectDTO);
        List<TestEntity> all = List.of(existingTestEntity, existingTestEntity);
        when(mockTestDaoImpl.getPage(existingSubjectEntity, setPage)).thenReturn(all);
        List<TestDTO> actual = mockTestService.getPage(existingSubjectDTO, pagination);

        assertFalse(actual.isEmpty());
        assertEquals(2, actual.size());
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsTestsReturnNumberOfRowsOfExistingTests() throws RepositoryException, ValidateException {
        Integer expectedRows = 23;
        existingSubjectEntity.setIdSubject(id);
        existingSubjectDTO.setId(id);
        existingTestEntity.setIdTest(id);
        when(mockTestDaoImpl.getNumberOfRows(existingSubjectEntity)).thenReturn(expectedRows);
        Integer rows = mockTestService.getNumberOfRows(existingSubjectDTO);

        assertEquals(expectedRows, rows);
    }

    @DisplayName("Increase request of Test")
    @Test
    void whenincreaseRequestOfTestTestShouldReturnTrue() throws RepositoryException, ValidateException {
        existingTestEntity.setIdTest(id);
        existingTestDTO.setId(id);
        when(mockTestDaoImpl.increaseRequestOfTest(existingTestEntity)).thenReturn(true);
        assertTrue(mockTestService.increaseRequestOfTest(existingTestDTO));
    }

    @DisplayName("Equals Class Constructors")
    @Test
    void whenConstructorTestServiceEmptyShouldReturnEqualsTestService() {
        TestServiceImpl testService = new TestServiceImpl();

        Field[] all = testService.getClass().getDeclaredFields();
        Field field;

        DaoFactory daoFactory;
        ValidateTestDTO validateTestDTO;
        ValidateSubjectDTO validateSubjectDTO;
        ValidatePagination validatePagination;
        TestServiceImpl testService2 = new TestServiceImpl();
        Arrays.stream(all).forEach(x -> System.out.println(x.getName()));
        try {
            field = testService.getClass().getDeclaredField(all[1].getName());
            field.setAccessible(true);
            daoFactory = (DaoFactory) field.get(testService);
            System.out.println(daoFactory);

            field = testService.getClass().getDeclaredField(all[2].getName());
            field.setAccessible(true);
            validateTestDTO = (ValidateTestDTO) field.get(testService);
            System.out.println(validateTestDTO);

            field = testService.getClass().getDeclaredField(all[3].getName());
            field.setAccessible(true);
            validateSubjectDTO = (ValidateSubjectDTO) field.get(testService);
            System.out.println(validateSubjectDTO);

            field = testService.getClass().getDeclaredField(all[4].getName());
            field.setAccessible(true);
            validatePagination = (ValidatePagination) field.get(testService);
            System.out.println(validatePagination.getClass());
            testService2 = new TestServiceImpl(daoFactory, validateTestDTO, validateSubjectDTO, validatePagination);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(testService.hashCode());
        System.out.println(testService2.hashCode());

        //  UserServiceImpl mockUserServuceImpl = mock(UserServiceImpl.class);

        assertTrue(testService.equals(testService));
        assertFalse(testService.equals(null));
        assertFalse(testService.equals(new TestServiceImpl(this.daoFactory, mockValidateTestDTO, mockValidateSubjectDTO, mockValidatePagination)));
        assertFalse(testService.equals(mock(UserServiceImpl.class)));
        assertEquals(testService.hashCode(), testService2.hashCode());
        assertEquals(5, all.length);
        assertEquals(testService, testService2);
    }
}
