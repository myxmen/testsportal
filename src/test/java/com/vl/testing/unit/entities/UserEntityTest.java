package com.vl.testing.unit.entities;

import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.entities.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;

public class UserEntityTest {
    private Long id = 1l;

    private String username = "Name";
    private String password = "Password";
    private Boolean admin = true;
    private Boolean ban = false;
    private String locale = "EN";


    private UserEntity entity = new UserEntity();

    @BeforeEach
    void setup() {
        entity.setIdUser(id);
        entity.setUsername(username);
        entity.setPassword(password);
        entity.setAdmin(admin);
        entity.setBan(ban);
        entity.setLocale(locale);
    }

    @Test
    public void userEntityBuilderReturnEqualsUserEntity() {

        UserEntity testEntity = UserEntity.builder()
                .setIdUser(id)
                .setUsername(username)
                .setPassword(password)
                .setAdmin(admin)
                .setBan(ban)
                .setLocale(locale)
                .build();

        assertAll("UserEntity fields",
                () -> assertEquals(entity.getIdUser(), testEntity.getIdUser()),
                () -> assertEquals(entity.getUsername(), testEntity.getUsername()),
                () -> assertEquals(entity.getPassword(), testEntity.getPassword()),
                () -> assertEquals(entity.isAdmin(), testEntity.isAdmin()),
                () -> assertEquals(entity.isBan(), testEntity.isBan()),
                () -> assertEquals(entity.getLocale(), testEntity.getLocale())

        );
    }

    @Test
    public void userEntityConstructorReturnEqualsUserEntity() {

        UserEntity testEntity = new UserEntity(id, username, password, admin, ban, locale);

        assertEquals(entity, testEntity);

    }

    @Test
    public void userEntityToStringReturnNotEmptyString() {

        assertFalse(entity.toString().isEmpty());

    }

    @Test
    public void userEntityHashcodeReturnEqualsUserEntityHashCode() {

        UserEntity testEntity = new UserEntity(id, username, password, admin, ban, locale);

        assertEquals(entity.hashCode(), testEntity.hashCode());

    }

    @Test
    public void userEntityEqualsInAllVariants() {

        UserEntity testEntity = new UserEntity(id + 1, username, password, admin, ban, locale);

        assertAll("UserEntity test Equals",
                () -> assertFalse(entity.equals(null)),
                () -> assertFalse(entity.equals(mock(PassedTestEntity.class))),
                () -> assertTrue(entity.equals(entity)),
                () -> assertFalse(entity.equals(testEntity))

        );

    }

}
