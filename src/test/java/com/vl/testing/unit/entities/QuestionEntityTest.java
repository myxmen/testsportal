package com.vl.testing.unit.entities;

import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.entities.TestEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;

public class QuestionEntityTest {
    private Long id = 1l;
    private Long testId = 2l;

    private Integer numberOfQuestion = 3;
    private String question = "test";
    private String answer1 = "test1";
    private String answer2 = "test2";
    private String answer3 = "test3";
    private String answer4 = "test4";
    private String answer5 = "test5";
    private Boolean correctAnswer1 = true;
    private Boolean correctAnswer2 = null;
    private Boolean correctAnswer3 = false;
    private Boolean correctAnswer4 = true;
    private Boolean correctAnswer5 = false;

    private QuestionEntity entity = new QuestionEntity();

    @BeforeEach
    void setup() {
        entity.setIdQuestion(id);
        entity.setTestId(testId);
        entity.setNumberOfQuestion(numberOfQuestion);
        entity.setQuestion(question);
        entity.setAnswer1(answer1);
        entity.setAnswer2(answer2);
        entity.setAnswer3(answer3);
        entity.setAnswer4(answer4);
        entity.setAnswer5(answer5);
        entity.setCorrectAnswer1(correctAnswer1);
        entity.setCorrectAnswer2(correctAnswer2);
        entity.setCorrectAnswer3(correctAnswer3);
        entity.setCorrectAnswer4(correctAnswer4);
        entity.setCorrectAnswer5(correctAnswer5);
    }

    @Test
    public void questionEntityBuilderReturnEqualsQuestionEntityFields() {
        QuestionEntity testEntity = QuestionEntity.builder()
                .setIdQuestion(id)
                .setTestId(testId)
                .setNumberOfQuestion(numberOfQuestion)
                .setQuestion(question)
                .setAnswer1(answer1)
                .setAnswer2(answer2)
                .setAnswer3(answer3)
                .setAnswer4(answer4)
                .setAnswer5(answer5)
                .setCorrectAnswer1(correctAnswer1)
                .setCorrectAnswer2(correctAnswer2)
                .setCorrectAnswer3(correctAnswer3)
                .setCorrectAnswer4(correctAnswer4)
                .setCorrectAnswer5(correctAnswer5)
                .build();


        assertAll("QuestionEntity fields",
                () -> assertEquals(entity.getIdQuestion(), testEntity.getIdQuestion()),
                () -> assertEquals(entity.getTestId(), testEntity.getTestId()),
                () -> assertEquals(entity.getNumberOfQuestion(), testEntity.getNumberOfQuestion()),
                () -> assertEquals(entity.getQuestion(), testEntity.getQuestion()),
                () -> assertEquals(entity.getAnswer1(), testEntity.getAnswer1()),
                () -> assertEquals(entity.getAnswer2(), testEntity.getAnswer2()),
                () -> assertEquals(entity.getAnswer3(), testEntity.getAnswer3()),
                () -> assertEquals(entity.getAnswer4(), testEntity.getAnswer4()),
                () -> assertEquals(entity.getAnswer5(), testEntity.getAnswer5()),
                () -> assertEquals(entity.isCorrectAnswer1(), testEntity.isCorrectAnswer1()),
                () -> assertEquals(entity.isCorrectAnswer2(), testEntity.isCorrectAnswer2()),
                () -> assertEquals(entity.isCorrectAnswer3(), testEntity.isCorrectAnswer3()),
                () -> assertEquals(entity.isCorrectAnswer4(), testEntity.isCorrectAnswer4()),
                () -> assertEquals(entity.isCorrectAnswer5(), testEntity.isCorrectAnswer5())
        );

    }

    @Test
    public void questionEntityConstructorReturnEqualsQuestionEntity() {

        QuestionEntity testEntity = new QuestionEntity(id, testId, numberOfQuestion, question,
                answer1, answer2, answer3, answer4, answer5, correctAnswer1, correctAnswer2, correctAnswer3, correctAnswer4, correctAnswer5);

        assertEquals(entity, testEntity);

    }

    @Test
    public void questionEntityToStringReturnNotEmptyString() {

        assertFalse(entity.toString().isEmpty());

    }

    @Test
    public void questionEntityHashCodeReturnEqualsQuestionEntityHashCode() {

        QuestionEntity testEntity = new QuestionEntity(id, testId, numberOfQuestion, question,
                answer1, answer2, answer3, answer4, answer5, correctAnswer1, correctAnswer2, correctAnswer3, correctAnswer4, correctAnswer5);

        assertEquals(entity.hashCode(), testEntity.hashCode());

    }

    @Test
    public void questionEntityEqualsInAllVariants() {

        QuestionEntity testEntity = new QuestionEntity(id + 1, testId, numberOfQuestion, question,
                answer1, answer2, answer3, answer4, answer5, correctAnswer1, correctAnswer2, correctAnswer3, correctAnswer4, correctAnswer5);


        assertAll("QuestionEntity test Equals",
                () -> assertFalse(entity.equals(null)),
                () -> assertFalse(entity.equals(mock(TestEntity.class))),
                () -> assertTrue(entity.equals(entity)),
                () -> assertFalse(entity.equals(testEntity))
        );

    }

}
