package com.vl.testing.unit.entities;

import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.entities.TestEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;

public class PassedTestEntityTest {
    private Long id = 1l;
    private Long userId = 2l;
    private Long testId = 3l;
    private Integer result = 90;

    private PassedTestEntity entity = new PassedTestEntity();

    @BeforeEach
    void setup() {
        entity.setIdPassedTest(id);
        entity.setUserId(userId);
        entity.setTestId(testId);
        entity.setResult(result);
    }

    @Test
    public void passedTestEntityBuilderReturnEqualsPassedTestEntity() {

        PassedTestEntity testEntity = PassedTestEntity.builder()
                .setIdPassedTest(id)
                .setUserId(userId)
                .setTestId(testId)
                .setResult(result)
                .build();

        assertAll("SubjectEntity fields",
                () -> assertEquals(entity.getIdPassedTest(), testEntity.getIdPassedTest()),
                () -> assertEquals(entity.getUserId(), testEntity.getUserId()),
                () -> assertEquals(entity.getTestId(), testEntity.getTestId()),
                () -> assertEquals(entity.getResult(), testEntity.getResult())
        );
    }

    @Test
    public void passedTestEntityConstructorReturnEqualsPassedTestEntity() {

        PassedTestEntity testEntity = new PassedTestEntity(id, userId, testId, result);

        assertEquals(entity, testEntity);

    }

    @Test
    public void passedTestEntityToStringReturnNotEmptyString() {

        assertFalse(entity.toString().isEmpty());

    }

    @Test
    public void passedTestEntityHashCodeReturnEqualsPassedTestEntityHashCode() {

        PassedTestEntity testEntity = new PassedTestEntity(id, userId, testId, result);

        assertEquals(entity.hashCode(), testEntity.hashCode());

    }

    @Test
    public void passedTestEntityEqualsInAllVariants() {

        PassedTestEntity testEntity = new PassedTestEntity(id + 1, userId, testId, result);

        assertAll("SubjectEntity test Equals",
                () -> assertFalse(entity.equals(null)),
                () -> assertFalse(entity.equals(mock(TestEntity.class))),
                () -> assertTrue(entity.equals(entity)),
                () -> assertFalse(entity.equals(testEntity))
        );

    }

}
