package com.vl.testing.unit.entities;

import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.entities.TestEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;

public class SubjectEntityTest {
    private Long id = 1l;
    private String subject = "Subject";

    private SubjectEntity entity = new SubjectEntity();

    @BeforeEach
    void setup() {
        entity.setIdSubject(id);
        entity.setSubject(subject);
    }

    @Test
    public void subjectEntityBuilderReturnEqualsSubjectEntity() {

        SubjectEntity testEntity = SubjectEntity.builder()
                .setIdSubject(id)
                .setSubject(subject)
                .build();

        assertAll("SubjectEntity fields",
                () -> assertEquals(entity.getIdSubject(), testEntity.getIdSubject()),
                () -> assertEquals(entity.getSubject(), testEntity.getSubject())
        );
    }

    @Test
    public void subjectEntityConstructorReturnEqualsSubjectEntity() {

        SubjectEntity testEntity = new SubjectEntity(id, subject);

        assertEquals(entity, testEntity);

    }

    @Test
    public void subjectEntityToStringReturnNotEmptyString() {

        assertFalse(entity.toString().isEmpty());

    }

    @Test
    public void subjectEntityHashCodeReturnEqualsSubjectEntityHashCode() {

        SubjectEntity testEntity = new SubjectEntity(id, subject);

        assertEquals(entity.hashCode(), testEntity.hashCode());

    }

    @Test
    public void testEntityEqualsInAllVariants() {

        SubjectEntity testEntity = new SubjectEntity(id + 1, subject);

        assertAll("SubjectEntity test Equals",
                () -> assertFalse(entity.equals(null)),
                () -> assertFalse(entity.equals(mock(TestEntity.class))),
                () -> assertTrue(entity.equals(entity)),
                () -> assertFalse(entity.equals(testEntity))
        );

    }

}
