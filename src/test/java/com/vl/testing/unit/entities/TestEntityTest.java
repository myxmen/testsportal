package com.vl.testing.unit.entities;

import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.entities.TestEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;

public class TestEntityTest {
    private Long id = 1l;
    private Long subjectId = 2l;

    private String test = "Test";
    private Integer difficulty = 50;
    private Long requests = 100l;
    private Long time = 120l;

    private TestEntity entity = new TestEntity();

    @BeforeEach
    void setup() {
        entity.setIdTest(id);
        entity.setSubjectId(subjectId);
        entity.setTest(test);
        entity.setDifficulty(difficulty);
        entity.setRequests(requests);
        entity.setTime(time);
    }

    @Test
    public void testEntityBuilderReturnEqualsTestEntity() {

        TestEntity testEntity = TestEntity.builder()
                .setIdTest(id)
                .setSubjectId(subjectId)
                .setTest(test)
                .setDifficulty(difficulty)
                .setRequests(requests)
                .setTime(time)
                .build();

        assertAll("TestEntity fields",
                () -> assertEquals(entity.getIdTest(), testEntity.getIdTest()),
                () -> assertEquals(entity.getSubjectId(), testEntity.getSubjectId()),
                () -> assertEquals(entity.getTest(), testEntity.getTest()),
                () -> assertEquals(entity.getDifficulty(), testEntity.getDifficulty()),
                () -> assertEquals(entity.getRequests(), testEntity.getRequests()),
                () -> assertEquals(entity.getTime(), testEntity.getTime())
        );
    }

    @Test
    public void testEntityConstructorReturnEqualsTestEntity() {

        TestEntity testEntity = new TestEntity(id, subjectId, test, difficulty, requests, time);

        assertEquals(entity, testEntity);

    }

    @Test
    public void testEntityToStringReturnNotEmptyString() {

        assertFalse(entity.toString().isEmpty());

    }

    @Test
    public void testEntityHashCodeReturnEqualsTestEntityHashCode() {

        TestEntity testEntity = new TestEntity(id, subjectId, test, difficulty, requests, time);

        assertEquals(entity.hashCode(), testEntity.hashCode());

    }

    @Test
    public void testEntityEqualsInAllVariants() {

        TestEntity testEntity = new TestEntity(id + 1, subjectId, test, difficulty, requests, time);

        assertAll("TestEntity test Equals",
                () -> assertFalse(entity.equals(null)),
                () -> assertFalse(entity.equals(mock(QuestionEntity.class))),
                () -> assertTrue(entity.equals(entity)),
                () -> assertFalse(entity.equals(testEntity))
        );

    }

}
