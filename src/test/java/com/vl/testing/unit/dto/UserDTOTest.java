package com.vl.testing.unit.dto;

import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.UserDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class UserDTOTest {
    private Long id = 1L;

    private String username = "Name";
    private String password = "Password";
    private Boolean admin = true;
    private Boolean ban = false;
    private Locale locale = new Locale("en", "US");
    private LocalDateTime loginTime = LocalDateTime.now();
    private List<PassedTestDTO> passedTests = List.of(mock(PassedTestDTO.class));

    private UserDTO dto = new UserDTO();

    @BeforeEach
    void setup() {
        dto.setId(id);
        dto.setUsername(username);
        dto.setPassword(password);
        dto.setAdmin(admin);
        dto.setBan(ban);
        dto.setLocale(locale);
        dto.setLoginDateTime(loginTime);
        dto.setPassedTests(passedTests);
    }

    @Test
    public void userDTOBuilderReturnEqualsUserDTO() {

        UserDTO testDto = UserDTO.builder()
                .setId(id)
                .setUsername(username)
                .setPassword(password)
                .setAdmin(admin)
                .setBan(ban)
                .setLocale(locale)
                .setLoginTime(loginTime)
                .setPassedTests(passedTests)
                .build();

        assertAll("UserDTO fields",
                () -> assertEquals(dto.getId(), testDto.getId()),
                () -> assertEquals(dto.getUsername(), testDto.getUsername()),
                () -> assertEquals(dto.getPassword(), testDto.getPassword()),
                () -> assertEquals(dto.isAdmin(), testDto.isAdmin()),
                () -> assertEquals(dto.isBan(), testDto.isBan()),
                () -> assertEquals(dto.getLocale(), testDto.getLocale()),
                () -> assertEquals(dto.getLoginTime(), testDto.getLoginTime()),
                () -> assertEquals(dto.getPassedTests(), testDto.getPassedTests())
        );
    }

    @Test
    public void userDTOConstructorReturnEqualsUserDTO() {

        UserDTO testDto = new UserDTO(id, username, password, admin, ban, locale, loginTime, passedTests);

        assertEquals(dto, testDto);

    }

    @Test
    public void userDTOToStringReturnNotEmptyString() {

        assertFalse(dto.toString().isEmpty());

    }

    @Test
    public void userDTOReturnFormatingTimeString() {

        dto.setLoginDateTime(LocalDateTime.of(2023, 10, 31, 16, 8, 11));

        assertEquals(dto.getFormatLoginTime(), "2023-10-31 16:08:11");

    }

    @Test
    public void userDTOHashcodeReturnEqualsUserDTOHashCode() {

        UserDTO testDto = new UserDTO(id, username, password, admin, ban, locale, loginTime, passedTests);

        assertEquals(dto.hashCode(), testDto.hashCode());

    }

    @Test
    public void userDTOEqualsInAllVariants() {

        UserDTO testDto = new UserDTO(id + 1, username, password, admin, ban, locale, loginTime, passedTests);

        assertAll("UserDTO test Equals",
                () -> assertFalse(dto.equals(null)),
                () -> assertFalse(dto.equals(mock(PassedTestDTO.class))),
                () -> assertTrue(dto.equals(dto)),
                () -> assertFalse(dto.equals(testDto))
        );

    }

}
