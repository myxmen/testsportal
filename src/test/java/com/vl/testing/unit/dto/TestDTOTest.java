package com.vl.testing.unit.dto;

import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.TestDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class TestDTOTest {
    private Long id = 1l;
    private Long subjectId = 2l;

    private String test = "Test";
    private Integer difficulty = 50;
    private Long requests = 100l;
    private Long time = 120l;
    private List<QuestionDTO> questions = List.of(mock(QuestionDTO.class));

    private TestDTO dto = new TestDTO();

    @BeforeEach
    void setup() {
        dto.setId(id);
        dto.setSubjectId(subjectId);
        dto.setTest(test);
        dto.setDifficulty(difficulty);
        dto.setRequests(requests);
        dto.setTime(time);
        dto.setQuestions(questions);
    }

    @Test
    public void testDTOBuilderReturnEqualsTestDTO() {

        TestDTO testDto = TestDTO.builder()
                .setId(id)
                .setSubjectId(subjectId)
                .setTest(test)
                .setDifficulty(difficulty)
                .setRequests(requests)
                .setTime(time)
                .setQuestions(questions)
                .build();

        assertAll("TestDTO fields",
                () -> assertEquals(dto.getId(), testDto.getId()),
                () -> assertEquals(dto.getSubjectId(), testDto.getSubjectId()),
                () -> assertEquals(dto.getTest(), testDto.getTest()),
                () -> assertEquals(dto.getDifficulty(), testDto.getDifficulty()),
                () -> assertEquals(dto.getRequests(), testDto.getRequests()),
                () -> assertEquals(dto.getTime(), testDto.getTime()),
                () -> assertEquals(dto.getQuestions(), testDto.getQuestions())
        );
    }

    @Test
    public void testDTOConstructorReturnEqualsTestDTO() {

        TestDTO testDto = new TestDTO(id, subjectId, test, difficulty, requests, time, questions);

        assertEquals(dto, testDto);

    }

    @Test
    public void testDTOToStringReturnNotEmptyString() {

        assertFalse(dto.toString().isEmpty());

    }

    @Test
    public void testDTOHashCodeReturnEqualsTestDTOHashCode() {

        TestDTO testDto = new TestDTO(id, subjectId, test, difficulty, requests, time, questions);

        assertEquals(dto.hashCode(), testDto.hashCode());

    }

    @Test
    public void testDTOEqualsInAllVariants() {

        TestDTO testDto = new TestDTO(id + 1, subjectId, test, difficulty, requests, time, questions);

        assertAll("TestDTO test Equals",
                () -> assertFalse(dto.equals(null)),
                () -> assertFalse(dto.equals(mock(QuestionDTO.class))),
                () -> assertTrue(dto.equals(dto)),
                () -> assertFalse(dto.equals(testDto))
        );

    }

}
