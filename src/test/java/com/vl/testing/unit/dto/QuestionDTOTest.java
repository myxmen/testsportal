package com.vl.testing.unit.dto;

import com.vl.testing.dto.QuestionDTO;
import com.vl.testing.dto.TestDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class QuestionDTOTest {
    private Long id = 1l;
    private Long testId = 2l;

    private Integer numberOfQuestion = 3;
    private String question = "test";
    private String answer1 = "test1";
    private String answer2 = "test2";
    private String answer3 = "test3";
    private String answer4 = "test4";
    private String answer5 = "test5";
    private Boolean correctAnswer1 = true;
    private Boolean correctAnswer2 = null;
    private Boolean correctAnswer3 = false;
    private Boolean correctAnswer4 = true;
    private Boolean correctAnswer5 = false;

    private QuestionDTO dto = new QuestionDTO();

    @BeforeEach
    void setup() {
        dto.setId(id);
        dto.setTestId(testId);
        dto.setNumberOfQuestion(numberOfQuestion);
        dto.setQuestion(question);
        dto.setAnswer1(answer1);
        dto.setAnswer2(answer2);
        dto.setAnswer3(answer3);
        dto.setAnswer4(answer4);
        dto.setAnswer5(answer5);
        dto.setCorrectAnswer1(correctAnswer1);
        dto.setCorrectAnswer2(correctAnswer2);
        dto.setCorrectAnswer3(correctAnswer3);
        dto.setCorrectAnswer4(correctAnswer4);
        dto.setCorrectAnswer5(correctAnswer5);
    }

    @Test
    public void questionDTOBuilderReturnEqualsQuestionDTOFields() {
        System.out.println();
        System.out.println("-- UnitTest --");
        System.out.println();

        QuestionDTO testDto = QuestionDTO.builder()
                .setId(id)
                .setTestId(testId)
                .setNumberOfQuestion(numberOfQuestion)
                .setQuestion(question)
                .setAnswer1(answer1)
                .setAnswer2(answer2)
                .setAnswer3(answer3)
                .setAnswer4(answer4)
                .setAnswer5(answer5)
                .setCorrectAnswer1(correctAnswer1)
                .setCorrectAnswer2(correctAnswer2)
                .setCorrectAnswer3(correctAnswer3)
                .setCorrectAnswer4(correctAnswer4)
                .setCorrectAnswer5(correctAnswer5)
                .build();


        assertAll("QuestionDTO fields",
                () -> assertEquals(dto.getId(), testDto.getId()),
                () -> assertEquals(dto.getTestId(), testDto.getTestId()),
                () -> assertEquals(dto.getNumberOfQuestion(), testDto.getNumberOfQuestion()),
                () -> assertEquals(dto.getQuestion(), testDto.getQuestion()),
                () -> assertEquals(dto.getAnswer1(), testDto.getAnswer1()),
                () -> assertEquals(dto.getAnswer2(), testDto.getAnswer2()),
                () -> assertEquals(dto.getAnswer3(), testDto.getAnswer3()),
                () -> assertEquals(dto.getAnswer4(), testDto.getAnswer4()),
                () -> assertEquals(dto.getAnswer5(), testDto.getAnswer5()),
                () -> assertEquals(dto.isCorrectAnswer1(), testDto.isCorrectAnswer1()),
                () -> assertEquals(dto.isCorrectAnswer2(), testDto.isCorrectAnswer2()),
                () -> assertEquals(dto.isCorrectAnswer3(), testDto.isCorrectAnswer3()),
                () -> assertEquals(dto.isCorrectAnswer4(), testDto.isCorrectAnswer4()),
                () -> assertEquals(dto.isCorrectAnswer5(), testDto.isCorrectAnswer5())
        );

    }

    @Test
    public void questionDTOConstructorReturnEqualsQuestionDTO() {

        QuestionDTO testDto = new QuestionDTO(id, testId, numberOfQuestion, question,
                answer1, answer2, answer3, answer4, answer5, correctAnswer1, correctAnswer2, correctAnswer3, correctAnswer4, correctAnswer5);

        assertEquals(dto, testDto);

    }

    @Test
    public void questionDTOToStringReturnNotEmptyString() {
        // System.out.println(dto.toString().isEmpty());
        assertFalse(dto.toString().isEmpty());

    }

    @Test
    public void questionDTOHashCodeReturnEqualsQuestionDTOHashCode() {

        QuestionDTO testDto = new QuestionDTO(id, testId, numberOfQuestion, question,
                answer1, answer2, answer3, answer4, answer5, correctAnswer1, correctAnswer2, correctAnswer3, correctAnswer4, correctAnswer5);

        assertEquals(dto.hashCode(), testDto.hashCode());

    }

    @Test
    public void questionDTOEqualsInAllVariants() {

        QuestionDTO testDto = new QuestionDTO(id + 1, testId, numberOfQuestion, question,
                answer1, answer2, answer3, answer4, answer5, correctAnswer1, correctAnswer2, correctAnswer3, correctAnswer4, correctAnswer5);


        assertAll("QuestionDTO test Equals",
                () -> assertFalse(dto.equals(null)),
                () -> assertFalse(dto.equals(mock(TestDTO.class))),
                () -> assertTrue(dto.equals(dto)),
                () -> assertFalse(dto.equals(testDto))
        );

    }
}
