package com.vl.testing.unit.dto;

import com.vl.testing.dto.SubjectDTO;
import com.vl.testing.dto.TestDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class SubjectDTOTest {
    private Long id = 1l;
    private String subject = "Subject";
    private TestDTO test = mock(TestDTO.class);

    private SubjectDTO dto = new SubjectDTO();

    @BeforeEach
    void setup() {
        dto.setId(id);
        dto.setSubject(subject);
        dto.setTests(test);
    }

    @Test
    public void subjectDTOBuilderReturnEqualsSubjectDTO() {

        SubjectDTO testDto = SubjectDTO.builder()
                .setId(id)
                .setSubject(subject)
                .setTests(test)
                .build();

        assertAll("SubjectDTO fields",
                () -> assertEquals(dto.getId(), testDto.getId()),
                () -> assertEquals(dto.getSubject(), testDto.getSubject()),
                () -> assertEquals(dto.getTest(), testDto.getTest())
        );
    }

    @Test
    public void subjectDTOConstructorReturnEqualsSubjectDTO() {

        SubjectDTO testDto = new SubjectDTO(id, subject, test);

        assertEquals(dto, testDto);

    }

    @Test
    public void subjectDTOToStringReturnNotEmptyString() {

        assertFalse(dto.toString().isEmpty());

    }

    @Test
    public void subjectDTOHashCodeReturnEqualsSubjectDTOHashCode() {

        SubjectDTO testDto = new SubjectDTO(id, subject, test);

        assertEquals(dto.hashCode(), testDto.hashCode());

    }

    @Test
    public void testDTOEqualsInAllVariants() {

        SubjectDTO testDto = new SubjectDTO(id + 1, subject, test);

        assertAll("SubjectDTO test Equals",
                () -> assertFalse(dto.equals(null)),
                () -> assertFalse(dto.equals(mock(TestDTO.class))),
                () -> assertTrue(dto.equals(dto)),
                () -> assertFalse(dto.equals(testDto))
        );

    }

}
