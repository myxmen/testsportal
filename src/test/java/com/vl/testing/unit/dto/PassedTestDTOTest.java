package com.vl.testing.unit.dto;

import com.vl.testing.dto.PassedTestDTO;
import com.vl.testing.dto.TestDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class PassedTestDTOTest {
    private Long id = 1l;
    private TestDTO test = mock(TestDTO.class);
    private Integer result = 90;

    private PassedTestDTO dto = new PassedTestDTO();

    @BeforeEach
    void setup() {
        dto.setId(id);
        dto.setTest(test);
        dto.setResult(result);
    }

    @Test
    public void passedTestDTOBuilderReturnEqualsPassedTestDTO() {

        PassedTestDTO testDto = PassedTestDTO.builder()
                .setId(id)
                .setTest(test)
                .setResult(result)
                .build();

        assertAll("SubjectDTO fields",
                () -> assertEquals(dto.getId(), testDto.getId()),
                () -> assertEquals(dto.getTest(), testDto.getTest()),
                () -> assertEquals(dto.getResult(), testDto.getResult())
        );
    }

    @Test
    public void passedTestDTOConstructorReturnEqualsPassedTestDTO() {

        PassedTestDTO testDto = new PassedTestDTO(id, test, result);

        assertEquals(dto, testDto);

    }

    @Test
    public void passedTestDTOToStringReturnNotEmptyString() {

        assertFalse(dto.toString().isEmpty());

    }

    @Test
    public void passedTestDTOHashCodeReturnEqualsPassedTestDTOHashCode() {

        PassedTestDTO testDto = new PassedTestDTO(id, test, result);

        assertEquals(dto.hashCode(), testDto.hashCode());

    }

    @Test
    public void passedTestDTOEqualsInAllVariants() {

        PassedTestDTO testDto = new PassedTestDTO(id + 1, test, result);
        // testDto.setId(testDto.getId() + 1);
        assertAll("SubjectDTO test Equals",
                () -> assertFalse(dto.equals(null)),
                () -> assertFalse(dto.equals(mock(TestDTO.class))),
                () -> assertTrue(dto.equals(dto)),
                () -> assertFalse(dto.equals(testDto))
        );

    }

}
