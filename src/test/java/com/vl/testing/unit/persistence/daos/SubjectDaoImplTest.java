package com.vl.testing.unit.persistence.daos;

import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.persistence.daos.SubjectDao;
import com.vl.testing.persistence.daos.SubjectDaoImpl;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.enums.Records;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SubjectDaoImplTest {
    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";
    private static final String DERBY_LOG_FILE = "derby.log";
    private static final String ERROR_MESSAGE = "'DROP TABLE' cannot be performed on 'SUBJECTS' because it does not exist.";
    private static final String CREATE_SUBJECT_TABLE = "CREATE TABLE subjects ("
            + "    subject_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
            + "    subject VARCHAR(255) UNIQUE"
            + ")";
    private static final String INSERT_INTO_SUBJECT_TABLE = "INSERT INTO subjects VALUES (DEFAULT, 'subject2'), "
            + "(DEFAULT, 'subject1'),"
            + "(DEFAULT, 'subject3'),"
            + "(DEFAULT, 'subject4'),"
            + "(DEFAULT, 'subject5'),"
            + "(DEFAULT, 'subject6'),"
            + "(DEFAULT, 'subject7'),"
            + "(DEFAULT, 'subject8'),"
            + "(DEFAULT, 'subject9'),"
            + "(DEFAULT, 'subject20'),"
            + "(DEFAULT, 'subject11'),"
            + "(DEFAULT, 'subject12'),"
            + "(DEFAULT, 'subject13'),"
            + "(DEFAULT, 'subject14'),"
            + "(DEFAULT, 'subject15'),"
            + "(DEFAULT, 'subject16'),"
            + "(DEFAULT, 'subject19'),"
            + "(DEFAULT, 'subject18'),"
            + "(DEFAULT, 'subject17'),"
            + "(DEFAULT, 'subject10')";

    private static final String DROP_SUBJECT_TABLE = "DROP TABLE subjects";
    private static Connection con;
    private static SubjectDao dao;
    private static SubjectEntity existingSubjectEntity;
    private static List<SubjectEntity> all;
    private static final Integer startRows = 5; // offset for Derby DB
    // private static final Integer recordsPerPage = Records.FIRST.getValue(); // limit for Derby DB
    private static final SetPage setPage = SetPage.builder()
            .setStartRows(startRows)// offset for Derby DB
            .setRecordsPerPage(Records.FIRST.getValue())// limit for Derby DB
            .setSort(Sort.ASC.getSort())
            .setSortColumn(DefaultColumn.SUBJECT.getColumn())
            .build();

    @BeforeAll
    static void globalSetUp() throws SQLException {
        con = DriverManager.getConnection(CONNECTION_URL);
        dao = new SubjectDaoImpl(con);
        DriverManager.drivers().forEach(x -> System.out.println(x));
    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
        System.out.println("Derby connection is close");

//        try {
//            DriverManager.getConnection(SHUTDOWN_URL);
//        } catch (SQLException ex) {
//            System.err.println("Derby shutdown");
//        }

        if (Files.exists(Path.of(DERBY_LOG_FILE))) {
            try {

                System.err.println("Derby can delete log File");
                Files.delete(Path.of(DERBY_LOG_FILE));
            } catch (IOException e) {
                System.err.println("Derby cannot delete log File");
            }
        }

        DriverManager.drivers().forEach(x -> System.out.println(x));
    }

    @BeforeEach
    void setUp() throws SQLException, RepositoryException {
        con.createStatement().executeUpdate(CREATE_SUBJECT_TABLE);
        System.out.println("Table is created");
        con.createStatement().executeUpdate(INSERT_INTO_SUBJECT_TABLE);
        all = dao.getAll();
        existingSubjectEntity = SubjectEntity.builder()
                .setIdSubject(null)
                .setSubject("Test")
                .build();
    }

    @AfterEach
    void tearDown() throws SQLException {
        try {
            con.createStatement().executeUpdate(DROP_SUBJECT_TABLE);
            System.out.println("Table is dropped");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            if (e.getMessage().contains(ERROR_MESSAGE)) {
                System.err.println("Table was dropped");
            } else throw e;
        }
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllSubjectsReturnListOfExistingSubjects() {
        //   List<SubjectEntity> all = dao.getAll();
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), 20);
    }

    @DisplayName("Get")
    @Test
    void whenGetSubjectsEqualsOneOfGetAllSubjects() throws RepositoryException {
        //  List<SubjectEntity> all = dao.getAll();
        SubjectEntity subjectEntity = all.get(2);
        assertNotNull(subjectEntity);
        assertNotNull(subjectEntity.getIdSubject());
        SubjectEntity selected = dao.get(subjectEntity.getIdSubject()).get();
        assertEquals(selected, subjectEntity);
    }

    @DisplayName("Get empty")
    @Test
    void whenGetSubjectsReturnEmpty() throws RepositoryException {
        assertEquals(dao.get(100l), Optional.empty());
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedSubjectShouldDecreaseList() throws RepositoryException {
        // List<SubjectEntity> all = dao.getAll();
        assertTrue(dao.delete(all.get(1).getIdSubject()));
        List<SubjectEntity> allAfter = dao.getAll();
        assertEquals(allAfter.size(), all.size() - 1);
    }

    @DisplayName("Delete false")
    @Test
    void whenDeletedSubjectShouldNotDecreaseListAndNotAffectExistingSubjectList() throws RepositoryException {
        //  List<SubjectEntity> all = dao.getAll();
        assertFalse(dao.delete(100l));
        List<SubjectEntity> allAfter = dao.getAll();
        assertEquals(allAfter.size(), all.size());
    }

    @DisplayName("Update")
    @Test
    void whenUpdatedSubjectShouldNotIncreaseDecreaseListAndAffectOnUpdatedSubject() throws RepositoryException {
        // List<SubjectEntity> all = dao.getAll();
        SubjectEntity subjectEntity = SubjectEntity.builder()
                .setIdSubject(all.get(2).getIdSubject())
                .setSubject("Test")
                .build();

        SubjectEntity updated = dao.update(subjectEntity).get();
        List<SubjectEntity> allAfter = dao.getAll();
        assertNotEquals(updated, all.get(2));
        assertEquals(updated, subjectEntity);
        assertEquals(allAfter.size(), all.size());
    }

    @DisplayName("Create")
    @Test
    void addingSubjectShouldIncreaseListAndNotAffectExistingSubject() throws RepositoryException {
        //  final List<SubjectEntity> all = dao.getAll();
//        SubjectEntity subjectEntity = SubjectEntity.builder()
//                .setId(null)
//                .setSubject("Test")
//                .build();
        SubjectEntity created = dao.create(existingSubjectEntity);
        assertNotNull(created);
        assertNotNull(created.getIdSubject());
        existingSubjectEntity.setIdSubject(created.getIdSubject());
        assertEquals(existingSubjectEntity, created);
        List<SubjectEntity> allAfter = dao.getAll();
        assertEquals(allAfter.size() - 1, all.size());
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageSubjectsReturnListOfExistingSubjects() throws RepositoryException, SQLException {
//        Integer startRows = 5; // offset for Derby DB
//        Integer recordsPerPage = Records.FIRST.getValue(); // limit for Derby DB
        Connection mockConnection = mock(Connection.class);
        SubjectDao subjectDao = new SubjectDaoImpl(mockConnection);

//        SetPage setPage = SetPage.builder()
//                .setStartRows(startRows)
//                .setRecordsPerPage(recordsPerPage)
//                .setSort(Sort.ASC.getSort())
//                .setSortColumn(SortColumn.SUBJECT.getColumn())
//                .build();
        System.out.println(setPage.getSortColumn());
        String queryDerby = "SELECT * FROM subjects ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        String query = "SELECT * FROM subjects ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " LIMIT ? OFFSET ?";
        System.out.println(queryDerby);
        when(mockConnection.prepareStatement(query))
                .thenReturn(con.prepareStatement(queryDerby));
//        when(mockConnection.prepareStatement(anyString()))
//                .thenReturn(con.prepareStatement(queryDerby));

        List<SubjectEntity> allSort = all.stream()
                .sorted(Comparator.comparing(SubjectEntity::getSubject))
                .collect(Collectors.toList());
        allSort.forEach(x -> System.out.println(x));

        List<SubjectEntity> testPage = new ArrayList<>();
        System.out.println();
        testPage.addAll(allSort.subList(setPage.getStartRows(), setPage.getStartRows() + setPage.getRecordsPerPage()));
        testPage.forEach(x -> System.out.println(x));

        List<SubjectEntity> page = subjectDao.getPage(setPage);
        page.forEach(x -> System.out.println(x));

        assertEquals(page.size(), setPage.getRecordsPerPage());
        assertEquals(testPage, page);
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsSubjectsReturnNumberOfRowsOfExistingSubjects() throws RepositoryException, SQLException {
        Integer rows = dao.getNumberOfRows();
        System.out.println(rows);
        assertEquals(all.size(), rows);
    }

    @DisplayName("getNumberOfRows empty")
    @Test
    void whenGetNumberOfRowsSubjectsReturnNumberOfRowsOfExistingSubjectsInEmptyTable() throws RepositoryException, SQLException {
        //  List<SubjectEntity> before = dao.getAll();
        //  System.out.println(before.size());
        con.createStatement().executeUpdate("TRUNCATE TABLE subjects");
        //  List<SubjectEntity> after = dao.getAll();
        //  after.forEach(x -> System.out.println(x));
        //  System.out.println(after.size());
        Integer rows = dao.getNumberOfRows();
        System.out.println(rows);
        // List<SubjectEntity> all = dao.getAll();
        assertEquals(dao.getAll().size(), rows);
    }

    @DisplayName("Create Exeption")
    @Test
    void whenCreateSubjectsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_SUBJECT_TABLE);
        //  System.out.println("Table was dropped");
//        SubjectEntity subjectEntity = SubjectEntity.builder()
//                .setId(null)
//                .setSubject("Test")
//                .build();
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.create(existingSubjectEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_CREATE_NEW_SUBJECT.getMessage()));
        //  con.createStatement().executeUpdate(CREATE_SUBJECT_TABLE);
        //  System.out.println("Table was repared");
    }

    @DisplayName("Get Exeption")
    @Test
    void whenGetSubjectsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_SUBJECT_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.get(1l);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_SUBJECT_BY_ID.getMessage()));
        // con.createStatement().executeUpdate(CREATE_SUBJECT_TABLE);
    }

    @DisplayName("Update Exeption")
    @Test
    void whenUpdateSubjectsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_SUBJECT_TABLE);
//         existingSubjectEntity.setId(100l);
//        System.out.println(dao.update(existingSubjectEntity));
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.update(existingSubjectEntity);
        });
//        System.out.println(dao.get(100l));
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_UPDATE_SUBJECT.getMessage()));
        // con.createStatement().executeUpdate(CREATE_SUBJECT_TABLE);
    }

    @DisplayName("Delete Exeption")
    @Test
    void whenDeleteSubjectsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_SUBJECT_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.delete(1l);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_DELETE_SUBJECT.getMessage()));
        // System.out.println(dao.delete(100l));
        // con.createStatement().executeUpdate(CREATE_SUBJECT_TABLE);
    }

    @DisplayName("GetAll Exeption")
    @Test
    void whenGetAllSubjectsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_SUBJECT_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getAll();
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_ALL_SUBJECT.getMessage()));
    }

    @DisplayName("GetPage Exeption")
    @Test
    void whenGetPageReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_SUBJECT_TABLE);
//        Integer startRows = 5;
//        Integer recordsPerPage = Records.FIRST.getValue();
//        SetPage setPage = SetPage.builder()
//                .setStartRows(startRows)
//                .setRecordsPerPage(recordsPerPage)
//                .setSort(Sort.ASC.getSort())
//                .setSortColumn(SortColumn.SUBJECT.getColumn())
//                .build();
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getPage(setPage);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_PAGE_OF_SUBJECT.getMessage()));
    }

    @DisplayName("getNumberOfRows Exeption")
    @Test
    void whenGetNumberOfRowsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_SUBJECT_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getNumberOfRows();
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_SUBJECT.getMessage()));
    }
}

