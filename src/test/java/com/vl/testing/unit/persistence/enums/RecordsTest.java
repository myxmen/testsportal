package com.vl.testing.unit.persistence.enums;

import com.vl.testing.persistence.enums.Records;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RecordsTest {

    @DisplayName("isRecords")
    @Test
    void whenIsRecordsReturnRecords() {
        assertEquals(Records.isRecords(10), Records.SECOND.getValue());
    }

    @DisplayName("isRecords empty records")
    @Test
    void whenIsRecordsReturnDefaultRecords() {
        assertEquals(Records.isRecords(0), Records.FIRST.getValue());
    }

    @DisplayName("getValue")
    @Test
    void whenGetValueReturnRecords() {
        assertEquals(Records.THIRD.getValue(), 15);
    }
}
