package com.vl.testing.unit.persistence.utils;

import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.persistence.enums.Records;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.utils.SetPage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SetPageTest {
    private Integer startRows = 20;
    private Integer recordsPerPage = Records.FIRST.getValue();
    private String sort = Sort.ASC.getSort();
    private String sortColumn = DefaultColumn.SUBJECT.getColumn();


    @Test
    public void setPageBuilderReturnEqualsSetPage() {

        SetPage setPage = SetPage.builder()
                .setStartRows(startRows)
                .setRecordsPerPage(recordsPerPage)
                .setSort(sort)
                .setSortColumn(sortColumn)
                .build();

        assertAll("SetPage fields",
                () -> assertEquals(setPage.getStartRows(), startRows),
                () -> assertEquals(setPage.getRecordsPerPage(), recordsPerPage),
                () -> assertEquals(setPage.getSort(), sort),
                () -> assertEquals(setPage.getSortColumn(), sortColumn)
        );
    }

    @Test
    public void setPageToStringReturnNotEmptyString() {

        SetPage setPage = SetPage.builder()
                .setStartRows(startRows)
                .setRecordsPerPage(recordsPerPage)
                .setSort(sort)
                .setSortColumn(sortColumn)
                .build();

        assertFalse(setPage.toString().isEmpty());

    }

    @Test
    public void whenSetPageReturnEqualsSetPageAndHashCode() {
        SetPage expectedSetPage = SetPage.builder()
                .setStartRows(startRows)
                .setRecordsPerPage(recordsPerPage)
                .setSort(sort)
                .setSortColumn(sortColumn)
                .build();
        SetPage actualSetPage = SetPage.builder()
                .setStartRows(startRows)
                .setRecordsPerPage(recordsPerPage)
                .setSort(sort)
                .setSortColumn(sortColumn)
                .build();
        assertEquals(expectedSetPage.hashCode(), actualSetPage.hashCode());
        assertEquals(expectedSetPage,actualSetPage);
    }
}
