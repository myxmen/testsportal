package com.vl.testing.unit.persistence.daos;

import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.entities.QuestionEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.daos.QuestionDao;
import com.vl.testing.persistence.daos.QuestionDaoImpl;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.enums.Records;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.mapper.impl.QuestionMapperEntity;
import com.vl.testing.persistence.utils.SetPage;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class QuestionDaoImlTest {
    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";
    private static final String DERBY_LOG_FILE = "derby.log";
    private static final String ERROR_MESSAGE = "'DROP TABLE' cannot be performed on 'QUESTIONS' because it does not exist.";
    private static final String CREATE_QUESTION_TABLE = "CREATE TABLE questions ("
            + "question_id      BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
            + "question_test_id BIGINT,"
            + "number_question  INT,"
            + "question         VARCHAR(255),"
            + "answer_1         VARCHAR(255),"
            + "answer_2         VARCHAR(255),"
            + "answer_3         VARCHAR(255),"
            + "answer_4         VARCHAR(255),"
            + "answer_5         VARCHAR(255),"
            + "correct_answer_1 BOOLEAN,"
            + "correct_answer_2 BOOLEAN,"
            + "correct_answer_3 BOOLEAN,"
            + "correct_answer_4 BOOLEAN,"
            + "correct_answer_5 BOOLEAN,"
            + "CONSTRAINT UC_question UNIQUE (question_test_id, number_question)"
            + ")";
    private static final String INSERT_INTO_QUESTION_TABLE = "INSERT INTO questions VALUES "
            + "(DEFAULT, 1, 1, 'question1', 'answer1', 'answer2', 'answer3', 'answer4', 'answer5', false, true, true, true, false),"
            + "(DEFAULT, 2, 2, 'question2', 'answer1', 'answer2', null, null, null, false, true, null, null, null),"
            + "(DEFAULT, 4, 1, 'question2', 'answer2', 'answer1', null, null, null, false, true, null, null, null),"
            + "(DEFAULT, 5, 1, 'question1', 'answer1', 'answer2', 'answer3', null, null, true, false, true, null, null),"
            + "(DEFAULT, 2, 1, 'question1', 'answer1', 'answer2', 'answer3', 'answer4', 'answer5', false, true, true, true, false),"
            + "(DEFAULT, 3, 1, 'question1', 'answer1', 'answer2', 'answer3', 'answer4', null, false, true, false, true, null)";

    private static final String GET_ALL_QUESTION_TABLE = "SELECT * FROM questions";
    private static final String DROP_QUESTION_TABLE = "DROP TABLE questions";
    private static Connection con;
    private static QuestionDao dao;
    private static QuestionEntity existingQuestionEntity;
    private static TestEntity existingTestEntity;
    private static List<QuestionEntity> all;
    private static final Integer startRows = 5; // offset for Derby DB
    // private static final Integer recordsPerPage = Records.FIRST.getValue(); // limit for Derby DB
    private static final SetPage setPage = SetPage.builder()
            .setStartRows(startRows)// offset for Derby DB
            .setRecordsPerPage(Records.FIRST.getValue())// limit for Derby DB
            .setSort(Sort.ASC.getSort())
            .setSortColumn(DefaultColumn.NUMQUESTION.getColumn())
            .build();

    @BeforeAll
    static void globalSetUp() throws SQLException {
        con = DriverManager.getConnection(CONNECTION_URL);
        dao = new QuestionDaoImpl(con);
        DriverManager.drivers().forEach(x -> System.out.println(x));

    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
        System.out.println("Derby connection is close");

//        try {
//            DriverManager.getConnection(SHUTDOWN_URL);
//        } catch (SQLException ex) {
//            System.err.println("Derby shutdown");
//        }

        if (Files.exists(Path.of(DERBY_LOG_FILE))) {
            try {

                System.err.println("Derby can delete log File");
                Files.delete(Path.of(DERBY_LOG_FILE));
            } catch (IOException e) {
                System.err.println("Derby cannot delete log File");
            }
        }

        DriverManager.drivers().forEach(x -> System.out.println(x));
    }

    @BeforeEach
    void setUp() throws SQLException, RepositoryException {
        con.createStatement().executeUpdate(CREATE_QUESTION_TABLE);
        System.out.println("Table is created");
        con.createStatement().executeUpdate(INSERT_INTO_QUESTION_TABLE);
        existingQuestionEntity = QuestionEntity.builder()
                .setTestId(2l)
                .setNumberOfQuestion(3)
                .setQuestion("Test question")
                .setAnswer1("newTestAnswer1")
                .setAnswer2("newTestAnswer2")
                .setAnswer3("newTestAnswer3")
                .setAnswer4("newTestAnswer4")
                .setAnswer5("newTestAnswer5")
                .setCorrectAnswer1(true)
                .setCorrectAnswer2(true)
                .setCorrectAnswer3(false)
                .setCorrectAnswer4(true)
                .setCorrectAnswer5(true)
                .build();
        existingTestEntity = TestEntity.builder()
                .setIdTest(2l)
                .setSubjectId(3l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        all = getAllQuestions();
        // System.out.println(setPage);
    }

    @AfterEach
    void tearDown() throws SQLException {
        try {
            con.createStatement().executeUpdate(DROP_QUESTION_TABLE);
            System.out.println("Table is dropped");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            if (e.getMessage().contains(ERROR_MESSAGE)) {
                System.err.println("Table was dropped");
            } else throw e;
        }
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllQuestionForTestReturnListOfExistingQuestion() throws RepositoryException {
        all.forEach(x -> System.out.println(x));
        // existingUserEntity.setId(2l);
        // assertEquals(all.size(), dao.getAllPassedTestFosUser(existingUserEntity).size());
        assertEquals(all.size(), 6);
    }

    @DisplayName("GetAllByTestId")
    @Test
    void whenGetAllQuestionsReturnListOfExistingQuestions() throws RepositoryException {
        List<QuestionEntity> filteredAll = all.stream()
                .filter(x -> x.getTestId() == existingTestEntity.getIdTest())
                .collect(Collectors.toList());
        filteredAll.forEach(x -> System.out.println(x));
        List<QuestionEntity> allByTestId = dao.getAllByTestId(existingTestEntity);
        allByTestId.forEach(x -> System.out.println(x));
        assertEquals(filteredAll.size(), allByTestId.size());
        //  assertEquals(filteredAll, allByTestId);
        for (QuestionEntity entity : allByTestId) {
            assertTrue(filteredAll.contains(entity));
        }
    }

    @DisplayName("GetAll Exeption")
    @Test
    void whenGetAllQuestionForTestReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_QUESTION_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getAllByTestId(existingTestEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_ALL_QUESTIONS_BY_TEST_ID.getMessage()));
    }

    @DisplayName("Get")
    @Test
    void whenGetQuestionEqualsOneOfGetAllQuestions() throws RepositoryException {
        QuestionEntity testEntity = all.get(1);
        assertNotNull(testEntity);
        assertNotNull(testEntity.getIdQuestion());
        QuestionEntity selected = dao.get(testEntity.getIdQuestion()).get();
        assertEquals(selected, testEntity);
    }

    @DisplayName("Get Exeption")
    @Test
    void whenGetQuestionReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_QUESTION_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.get(1l);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_QUESTION_BY_ID.getMessage()));
    }

    @DisplayName("Get empty")
    @Test
    void whenGetQuestionReturnEmpty() throws RepositoryException {
        assertEquals(dao.get(100l), Optional.empty());
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedQuestionShouldDecreaseList() throws RepositoryException, SQLException {
        assertTrue(dao.delete(all.get(0).getIdQuestion()));
        List<QuestionEntity> allAfter = getAllQuestions();
        assertEquals(allAfter.size(), all.size() - 1);
    }

    @DisplayName("Delete false")
    @Test
    void whenDeletedQuestionShouldNotDecreaseListAndNotAffectExistingTestList() throws RepositoryException {

        assertFalse(dao.delete(100l));
    }

    @DisplayName("Delete Exeption")
    @Test
    void whenDeleteQuestionReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_QUESTION_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.delete(1l);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_DELETE_QUESTION.getMessage()));
    }

    @DisplayName("Update")
    @Test
    void whenUpdatedQuestionShouldNotIncreaseDecreaseListAndAffectOnUpdatedQuestion() throws RepositoryException, SQLException {
        QuestionEntity testEntity = QuestionEntity.builder()
                .setIdQuestion(all.get(1).getIdQuestion())
                .setTestId(all.get(1).getTestId())
                .setNumberOfQuestion(all.get(1).getNumberOfQuestion())
                .setQuestion("New test question")
                .setAnswer1(all.get(1).getAnswer1())
                .setAnswer2(all.get(1).getAnswer2())
                .setAnswer3(all.get(1).getAnswer3())
                .setAnswer4(all.get(1).getAnswer4())
                .setAnswer5(all.get(1).getAnswer5())
                .setCorrectAnswer1(all.get(1).isCorrectAnswer1())
                .setCorrectAnswer2(all.get(1).isCorrectAnswer2())
                .setCorrectAnswer3(all.get(1).isCorrectAnswer3())
                .setCorrectAnswer4(all.get(1).isCorrectAnswer4())
                .setCorrectAnswer5(all.get(1).isCorrectAnswer5())
                .build();

        QuestionEntity updated = dao.update(testEntity).get();
        List<QuestionEntity> allAfter = getAllQuestions();
        assertNotEquals(updated, all.get(1));
        assertEquals(updated, testEntity);
        assertEquals(allAfter.size(), all.size());
    }

    @DisplayName("Update Exeption")
    @Test
    void whenUpdateQuestionReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_QUESTION_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.update(existingQuestionEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_UPDATE_QUESTION.getMessage()));
    }

    @DisplayName("Create")
    @Test
    void addingQuestionShouldIncreaseListAndNotAffectExistingQuestion() throws RepositoryException, SQLException {
        QuestionEntity created = dao.create(existingQuestionEntity);
        assertNotNull(created);
        assertNotNull(created.getIdQuestion());
        existingQuestionEntity.setIdQuestion(created.getIdQuestion());
        assertEquals(existingQuestionEntity, created);
        List<QuestionEntity> allAfter = getAllQuestions();
        assertEquals(allAfter.size() - 1, all.size());
    }

    @DisplayName("Create & Get Question with Null correct answers")
    @Test
    void addingQuestionWithNullAnswersShouldGetQuestionWithNullAnswers() throws RepositoryException, SQLException {
        existingQuestionEntity.setCorrectAnswer1(null);
        existingQuestionEntity.setCorrectAnswer2(null);
        existingQuestionEntity.setCorrectAnswer3(null);
        existingQuestionEntity.setCorrectAnswer4(null);
        existingQuestionEntity.setCorrectAnswer5(null);
        System.out.println(existingQuestionEntity);
        QuestionEntity created = dao.create(existingQuestionEntity);
        System.out.println(created);
        assertNotNull(created);
        assertNotNull(created.getIdQuestion());
        existingQuestionEntity.setIdQuestion(created.getIdQuestion());
        assertEquals(existingQuestionEntity, created);
        List<QuestionEntity> allAfter = getAllQuestions();
        assertEquals(allAfter.size() - 1, all.size());
        QuestionEntity entity = dao.get(created.getIdQuestion()).get();
        System.out.println(entity);
        getAllQuestions().forEach(x -> System.out.println(x));
       // assertEquals(existingQuestionEntity, entity);
        assertAll("Geted QuestionEntity fields CorrectAswer is null",
                () -> assertFalse(entity.isCorrectAnswer1()),
                () -> assertFalse(entity.isCorrectAnswer2()),
                () -> assertFalse(entity.isCorrectAnswer3()),
                () -> assertFalse(entity.isCorrectAnswer4()),
                () -> assertFalse(entity.isCorrectAnswer5())
        );
    }

    @DisplayName("Create Exeption")
    @Test
    void whenCreateQuestionReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_QUESTION_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.create(existingQuestionEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_CREATE_NEW_QUESTION.getMessage()));
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageQuestionsReturnListOfExistingQuestions() throws RepositoryException, SQLException {

        Connection mockConnection = mock(Connection.class);
        QuestionDao testDao = new QuestionDaoImpl(mockConnection);

        System.out.println(setPage.getSortColumn());
        String queryDerby = "SELECT * FROM questions WHERE question_test_id = ? ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        //SELECT * FROM questions WHERE question_test_id = ? ORDER BY " + setPage.getSortColumn() + " " + setPage.getSort() + " LIMIT ? OFFSET ?
        String query = "SELECT * FROM questions WHERE question_test_id = ? ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " LIMIT ? OFFSET ?";
        System.out.println(queryDerby);
        when(mockConnection.prepareStatement(query))
                .thenReturn(con.prepareStatement(queryDerby));
        con.createStatement().executeUpdate("TRUNCATE TABLE questions");
        for (int i = 1; i <= 20; i++) {
            QuestionEntity questionEntity = QuestionEntity.builder()
                    .setTestId(2l)
                    .setNumberOfQuestion(20 - i + 1)
                    .setQuestion("Test question " + (20 - i + 1))
                    .setAnswer1("newTestAnswer1")
                    .setAnswer2("newTestAnswer2")
                    .setAnswer3("newTestAnswer3")
                    .setAnswer4("newTestAnswer4")
                    .setAnswer5("newTestAnswer5")
                    .setCorrectAnswer1(true)
                    .setCorrectAnswer2(true)
                    .setCorrectAnswer3(false)
                    .setCorrectAnswer4(true)
                    .setCorrectAnswer5(true)
                    .build();
            dao.create(questionEntity);
        }

        getAllQuestions().forEach(x -> System.out.println(x));
        System.out.println("-sort-");

        List<QuestionEntity> allSort = getAllQuestions().stream()
                .sorted(Comparator.comparing(QuestionEntity::getNumberOfQuestion))
                .collect(Collectors.toList());
        allSort.forEach(x -> System.out.println(x));

        List<QuestionEntity> testPage = new ArrayList<>();
        System.out.println("---");
        testPage.addAll(allSort.subList(setPage.getStartRows(), setPage.getStartRows() + setPage.getRecordsPerPage()));
        testPage.forEach(x -> System.out.println(x));
        System.out.println("-get page-");
        List<QuestionEntity> page = testDao.getPage(existingTestEntity, setPage);
        page.forEach(x -> System.out.println(x));

        assertEquals(page.size(), setPage.getRecordsPerPage());
        assertEquals(testPage, page);
    }

    @DisplayName("GetPage Exeption")
    @Test
    void whenGetPageReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_QUESTION_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getPage(existingTestEntity, setPage);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_PAGE_OF_QUESTIONS.getMessage()));
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsQuestionsReturnNumberOfRowsOfExistingQuestions() throws RepositoryException {
        Integer rows = dao.getNumberOfRows(existingTestEntity);
        System.out.println(rows);
        List<QuestionEntity> filteredAll = all.stream()
                .filter(x -> x.getTestId() == existingTestEntity.getIdTest())
                .collect(Collectors.toList());
        assertEquals(filteredAll.size(), rows);
    }

    @DisplayName("getNumberOfRows empty")
    @Test
    void whenGetNumberOfRowsQuestionsReturnNumberOfRowsOfExistingQuestionsInEmptyTable() throws RepositoryException, SQLException {
        con.createStatement().executeUpdate("TRUNCATE TABLE questions");
        Integer rows = dao.getNumberOfRows(existingTestEntity);
        System.out.println(rows);
        assertEquals(getAllQuestions().size(), rows);
        assertEquals(0, rows);
    }

    @DisplayName("getNumberOfRows Exeption")
    @Test
    void whenGetNumberOfRowsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_QUESTION_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getNumberOfRows(existingTestEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_QUESTIONS.getMessage()));
    }

    private List<QuestionEntity> getAllQuestions() throws SQLException {
        final List<QuestionEntity> all = new ArrayList<>();
        try (PreparedStatement statement = con.prepareStatement(GET_ALL_QUESTION_TABLE)) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                all.add(new QuestionMapperEntity().fillEntityWithValues(resultSet));
            }
            return all;
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            throw e;
        }
    }
}
