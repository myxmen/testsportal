package com.vl.testing.unit.persistence.daos;

import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.persistence.daos.TestDao;
import com.vl.testing.persistence.daos.TestDaoImpl;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.enums.Records;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDaoImplTest {
    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";
    private static final String DERBY_LOG_FILE = "derby.log";
    private static final String ERROR_MESSAGE = "'DROP TABLE' cannot be performed on 'TESTS' because it does not exist.";
    private static final String CREATE_TEST_TABLE = "CREATE TABLE tests ("
            + "   test_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
            + "   test_subject_id BIGINT,"
            + "   test       VARCHAR(255),"
            + "   difficulty INT,"
            + "   requests   BIGINT,"
            + "   time       BIGINT,"
            + "   CONSTRAINT UC_tests UNIQUE (test_subject_id,test)"
            + ")";
    private static final String INSERT_INTO_TEST_TABLE = "INSERT INTO tests VALUES (DEFAULT, 1, 'test1', 8, 999, 10), "
            + "(DEFAULT, 2, 'test2', 6, 109, 1),"
            + "(DEFAULT, 2, 'test3', 5, 100, 2),"
            + "(DEFAULT, 5, 'test4', 3, 90, 5),"
            + "(DEFAULT, 4, 'test5', 2, 99, 4),"
            + "(DEFAULT, 6, 'test11', 10, 98, 2),"
            + "(DEFAULT, 7, 'test12', 9, 910, 15),"
            + "(DEFAULT, 8, 'test13', 5, 1, 60),"
            + "(DEFAULT, 9, 'test14', 1, 0, 90),"
            + "(DEFAULT, 13, 'test15', 8, 2, 180),"
            + "(DEFAULT, 12, 'test6', 2, 14, 5),"
            + "(DEFAULT, 11, 'test7', 7, 18, 20),"
            + "(DEFAULT, 10, 'test8', 3, 101, 30),"
            + "(DEFAULT, 15, 'test9', 5, 11, 40),"
            + "(DEFAULT, 14, 'test10', 9, 18, 15)";

    private static final String DROP_TEST_TABLE = "DROP TABLE tests";
    private static Connection con;
    private static TestDao dao;
    private static TestEntity existingTestEntity;
    private static SubjectEntity existingSubjectEntity;
    private static List<TestEntity> all;
    private static final Integer startRows = 5; // offset for Derby DB
    // private static final Integer recordsPerPage = Records.FIRST.getValue(); // limit for Derby DB
    private static final SetPage setPage = SetPage.builder()
            .setStartRows(startRows)// offset for Derby DB
            .setRecordsPerPage(Records.FIRST.getValue())// limit for Derby DB
            .setSort(Sort.ASC.getSort())
            .setSortColumn(DefaultColumn.TEST.getColumn())
            .build();

    @BeforeAll
    static void globalSetUp() throws SQLException {
        con = DriverManager.getConnection(CONNECTION_URL);
        dao = new TestDaoImpl(con);
        DriverManager.drivers().forEach(x -> System.out.println(x));

    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
        System.out.println("Derby connection is close");

//        try {
//            DriverManager.getConnection(SHUTDOWN_URL);
//        } catch (SQLException ex) {
//            System.err.println("Derby shutdown");
//        }

        if (Files.exists(Path.of(DERBY_LOG_FILE))) {
            try {

                System.err.println("Derby can delete log File");
                Files.delete(Path.of(DERBY_LOG_FILE));
            } catch (IOException e) {
                System.err.println("Derby cannot delete log File");
            }
        }

        DriverManager.drivers().forEach(x -> System.out.println(x));
    }

    @BeforeEach
    void setUp() throws SQLException, RepositoryException {
        con.createStatement().executeUpdate(CREATE_TEST_TABLE);
        System.out.println("Table is created");
        con.createStatement().executeUpdate(INSERT_INTO_TEST_TABLE);
        existingTestEntity = TestEntity.builder()
                .setIdTest(null)
                .setSubjectId(2l)
                .setTest("TestTest")
                .setDifficulty(6)
                .setRequests(99l)
                .setTime(45l)
                .build();
        existingSubjectEntity = SubjectEntity.builder()
                .setIdSubject(2l)
                .setSubject("TestSubject")
                .build();
        all = dao.getAllBySubjectId(existingSubjectEntity);
        // System.out.println(setPage);
    }

    @AfterEach
    void tearDown() throws SQLException {
        try {
            con.createStatement().executeUpdate(DROP_TEST_TABLE);
            System.out.println("Table is dropped");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            if (e.getMessage().contains(ERROR_MESSAGE)) {
                System.err.println("Table was dropped");
            } else throw e;
        }
    }

    @DisplayName("GetAllBySubjectId")
    @Test
    void whenGetAllTestsReturnListOfExistingTests() {
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), 2);
    }

    @DisplayName("Get")
    @Test
    void whenGetTestsEqualsOneOfGetAllTests() throws RepositoryException {
        TestEntity testEntity = all.get(1);
        assertNotNull(testEntity);
        assertNotNull(testEntity.getIdTest());
        TestEntity selected = dao.get(testEntity.getIdTest()).get();
        assertEquals(selected, testEntity);
    }

    @DisplayName("Get empty")
    @Test
    void whenGetTestsReturnEmpty() throws RepositoryException {
        assertEquals(dao.get(100l), Optional.empty());
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedTestShouldDecreaseList() throws RepositoryException {
        assertTrue(dao.delete(all.get(0).getIdTest()));
        List<TestEntity> allAfter = dao.getAllBySubjectId(existingSubjectEntity);
        assertEquals(allAfter.size(), all.size() - 1);
    }

    @DisplayName("Delete false")
    @Test
    void whenDeletedTestShouldNotDecreaseListAndNotAffectExistingTestList() throws RepositoryException {

        assertFalse(dao.delete(100l));
    }

    @DisplayName("Update")
    @Test
    void whenUpdatedTestShouldNotIncreaseDecreaseListAndAffectOnUpdatedTest() throws RepositoryException {
        TestEntity testEntity = TestEntity.builder()
                .setIdTest(all.get(1).getIdTest())
                .setSubjectId(all.get(1).getSubjectId())
                .setTest("TestUpdate")
                .setDifficulty(all.get(1).getDifficulty())
                .setRequests(all.get(1).getRequests())
                .setTime(all.get(1).getTime())
                .build();

        TestEntity updated = dao.update(testEntity).get();
        List<TestEntity> allAfter = dao.getAllBySubjectId(existingSubjectEntity);
        assertNotEquals(updated, all.get(1));
        assertEquals(updated, testEntity);
        assertEquals(allAfter.size(), all.size());
    }

    @DisplayName("Create")
    @Test
    void addingTestShouldIncreaseListAndNotAffectExistingTest() throws RepositoryException {
        TestEntity created = dao.create(existingTestEntity);
        assertNotNull(created);
        assertNotNull(created.getIdTest());
        existingTestEntity.setIdTest(created.getIdTest());
        assertEquals(existingTestEntity, created);
        List<TestEntity> allAfter = dao.getAllBySubjectId(existingSubjectEntity);
        assertEquals(allAfter.size() - 1, all.size());
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageTestsReturnListOfExistingTests() throws RepositoryException, SQLException {

        Connection mockConnection = mock(Connection.class);
        TestDao testDao = new TestDaoImpl(mockConnection);

        System.out.println(setPage.getSortColumn());
        String queryDerby = "SELECT * FROM tests WHERE test_subject_id = ? ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        //   SELECT * FROM tests WHERE test_subject_id = ? ORDER BY " + setPage.getSortColumn() + " " + setPage.getSort() + " LIMIT ? OFFSET ?

        String query = "SELECT * FROM tests WHERE test_subject_id = ? ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " LIMIT ? OFFSET ?";
        System.out.println(queryDerby);
        when(mockConnection.prepareStatement(query))
                .thenReturn(con.prepareStatement(queryDerby));

        for (int i = 1; i <= 15; i++) {
            TestEntity testEntity = dao.get((long) i).get();
            testEntity.setSubjectId(2l);
            dao.update(testEntity);
        }
        List<TestEntity> allSort = dao.getAllBySubjectId(existingSubjectEntity).stream()
                .sorted(Comparator.comparing(TestEntity::getTest))
                .collect(Collectors.toList());
        allSort.forEach(x -> System.out.println(x));

        List<TestEntity> testPage = new ArrayList<>();
        System.out.println("---");
        testPage.addAll(allSort.subList(setPage.getStartRows(), setPage.getStartRows() + setPage.getRecordsPerPage()));
        testPage.forEach(x -> System.out.println(x));
        System.out.println("-get page-");
        List<TestEntity> page = testDao.getPage(existingSubjectEntity, setPage);
        page.forEach(x -> System.out.println(x));

        assertEquals(page.size(), setPage.getRecordsPerPage());
        assertEquals(testPage, page);
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsTestsReturnNumberOfRowsOfExistingTests() throws RepositoryException {
        Integer rows = dao.getNumberOfRows(existingSubjectEntity);
        System.out.println(rows);
        assertEquals(all.size(), rows);
    }

    @DisplayName("getNumberOfRows empty")
    @Test
    void whenGetNumberOfRowsTestsReturnNumberOfRowsOfExistingTestsInEmptyTable() throws RepositoryException, SQLException {
        con.createStatement().executeUpdate("TRUNCATE TABLE tests");
        Integer rows = dao.getNumberOfRows(existingSubjectEntity);
        System.out.println(rows);
        assertEquals(dao.getAllBySubjectId(existingSubjectEntity).size(), rows);
        assertEquals(0, rows);
    }

    @DisplayName("Create Exeption")
    @Test
    void whenCreateTestsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.create(existingTestEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_CREATE_NEW_TEST.getMessage()));
    }

    @DisplayName("Get Exeption")
    @Test
    void whenGetTestsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.get(1l);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_TEST_BY_ID.getMessage()));
    }

    @DisplayName("Update Exeption")
    @Test
    void whenUpdateTestsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.update(existingTestEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_UPDATE_TEST.getMessage()));
    }

    @DisplayName("Delete Exeption")
    @Test
    void whenDeleteTestsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.delete(1l);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_DELETE_TEST.getMessage()));
    }

    @DisplayName("GetAll Exeption")
    @Test
    void whenGetAllTestsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getAllBySubjectId(existingSubjectEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_ALL_TEST_BY_SUBJECT_ID.getMessage()));
    }

    @DisplayName("GetPage Exeption")
    @Test
    void whenGetPageReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getPage(existingSubjectEntity, setPage);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_PAGE_OF_TEST.getMessage()));
    }

    @DisplayName("getNumberOfRows Exeption")
    @Test
    void whenGetNumberOfRowsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getNumberOfRows(existingSubjectEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_TEST.getMessage()));
    }

    @DisplayName("increaseRequestOfTest")
    @Test
    void WhenIncreaseRequestOfTestReturnAddRequest() throws RepositoryException {
        TestEntity testEntity = dao.get(2l).get();
        assertTrue(dao.increaseRequestOfTest(testEntity));
        assertFalse(dao.get(2l).get().equals(testEntity));
        assertEquals(dao.get(2l).get().getRequests(), testEntity.getRequests() + 1);
    }

    @DisplayName("increaseRequestOfTest empty")
    @Test
    void WhenIncreaseRequestOfTestEmptyTestReturnFalse() throws RepositoryException {
        TestEntity testEntity = dao.get(2l).get();
        testEntity.setIdTest(100l);
        assertFalse(dao.increaseRequestOfTest(testEntity));
    }

    @DisplayName("increaseRequestOfTest Exeption")
    @Test
    void WhenIncreaseRequestOfTestReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
        existingTestEntity.setIdTest(2l);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.increaseRequestOfTest(existingTestEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_INCREASE_REQUEST_OF_TEST.getMessage()));
    }
}
