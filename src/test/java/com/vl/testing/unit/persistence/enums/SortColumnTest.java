package com.vl.testing.unit.persistence.enums;

import com.vl.testing.persistence.enums.SortColumn;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class SortColumnTest {

    @DisplayName("getColumnByName")
    @Test
    void whenGetColumnByNameReturnColumn() {
        assertEquals(SortColumn.getColumnByName("Test"), SortColumn.TEST.getColumn());
    }

    @DisplayName("getColumnByName empty name")
    @Test
    void whenGetColumnByNameReturnDefaultColumn() {
        assertEquals(SortColumn.getColumnByName("Empty_Column"), SortColumn.NONE.getColumn());
    }

    @DisplayName("isColumn")
    @Test
    void whenIsColumnReturnTrue() {
        assertTrue(SortColumn.isColumn("Test"));
    }

    @DisplayName("isColumn empty name")
    @Test
    void whenIsColumnReturnFalse() {
        assertFalse(SortColumn.isColumn("Empty_Column"));
    }

    @DisplayName("getColumn")
    @Test
    void whenGetColumnReturnColumn() {
        assertEquals(SortColumn.TEST.getColumn(), "tests.test");
    }
}
