package com.vl.testing.unit.persistence.daos;

import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.daos.UserDao;
import com.vl.testing.persistence.daos.UserDaoImpl;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.enums.Records;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.utils.SetPage;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserDaoImplTest {
    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";
    private static final String DERBY_LOG_FILE = "derby.log";
    private static final String ERROR_MESSAGE = "'DROP TABLE' cannot be performed on 'USERS' because it does not exist.";
    private static final String CREATE_TEST_TABLE = "CREATE TABLE users (" +
            "    user_id  BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
            "    username VARCHAR(255) UNIQUE," +
            "    password VARCHAR(255)," +
            "    admin    BOOLEAN," +
            "    ban      BOOLEAN," +
            "    locale   VARCHAR(255)" +
            ")";

    private static final String INSERT_INTO_TEST_TABLE = "INSERT INTO users VALUES"
            + "(DEFAULT, 'ivanov1', '$2a$12$ef2VOvhsF.qEwjdiq93cq.RXB6tNENtoL3EQXlXaIZuv0XDMMgCtm', false, true, 'EN')," //4321
            + "(DEFAULT, 'User', '$2a$12$wIDnrcx3gw6bfjgc/fgqM.JHN.TCiiYZtIC2WUatsvURNE40Q07vO', false, false, 'RU')," //1
            + "(DEFAULT, 'Admin', '$2a$12$OC6eAUnO90xNuhDlv/iHS.bLe5LR83gLNzmbbPbGt/QNCiK.CKa9C', true, false, 'EN')," //1234
            + "(DEFAULT, 'ivanov4', '$2a$12$wpLOBmJnZ3T5J1lZy.hypepo4vwQHBo3PZkH.27YGRQiMJInUotTm', false, true, 'EN')," //4321
            + "(DEFAULT, 'ivanov5', '$2a$12$MdKnhwDjlMffuAtlwG91WeHSd2Qy2tf8e/GP9famGOenG138nOtii', false, false, 'RU')," //4321
            + "(DEFAULT, 'Petrov', '$2a$12$OC6eAUnO90xNuhDlv/iHS.bLe5LR83gLNzmbbPbGt/QNCiK.CKa9C', false, false, 'RU')"; //1234

    private static final String DROP_TEST_TABLE = "DROP TABLE users";
    private static Connection con;
    private static UserDao dao;
    private static UserEntity existingUserEntity;
    private static List<UserEntity> all;
    private static final Integer startRows = 5; // offset for Derby DB
    // private static final Integer recordsPerPage = Records.FIRST.getValue(); // limit for Derby DB
    private static final SetPage setPage = SetPage.builder()
            .setStartRows(startRows)// offset for Derby DB
            .setRecordsPerPage(Records.FIRST.getValue())// limit for Derby DB
            .setSort(Sort.ASC.getSort())
            .setSortColumn(DefaultColumn.USER.getColumn())
            .build();

    @BeforeAll
    static void globalSetUp() throws SQLException {
        con = DriverManager.getConnection(CONNECTION_URL);
        dao = new UserDaoImpl(con);
        DriverManager.drivers().forEach(x -> System.out.println(x));

    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
        System.out.println("Derby connection is close");

//        try {
//            DriverManager.getConnection(SHUTDOWN_URL);
//        } catch (SQLException ex) {
//            System.err.println("Derby shutdown");
//        }

        if (Files.exists(Path.of(DERBY_LOG_FILE))) {
            try {

                System.err.println("Derby can delete log File");
                Files.delete(Path.of(DERBY_LOG_FILE));
            } catch (IOException e) {
                System.err.println("Derby cannot delete log File");
            }
        }

        DriverManager.drivers().forEach(x -> System.out.println(x));
    }

    @BeforeEach
    void setUp() throws SQLException, RepositoryException {
        con.createStatement().executeUpdate(CREATE_TEST_TABLE);
        System.out.println("Table is created");
        con.createStatement().executeUpdate(INSERT_INTO_TEST_TABLE);
        existingUserEntity = UserEntity.builder()
                .setIdUser(null)
                .setUsername("Test")
                .setPassword("$2a$12$j8DHSQIgt8sKHfuiDy22PuHJipcZmuRYcdfBwwWVu1TsfW1TIrOeG") //12345678
                .setAdmin(true)
                .setBan(false)
                //.setLocale(Localization.RU.name())
                .setLocale(Localization.getNameByLocale(Localization.RU.getLocale()))
                .build();
        all = dao.getAll();
        // System.out.println(setPage);
    }

    @AfterEach
    void tearDown() throws SQLException {
        try {
            con.createStatement().executeUpdate(DROP_TEST_TABLE);
            System.out.println("Table is dropped");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            if (e.getMessage().contains(ERROR_MESSAGE)) {
                System.err.println("Table was dropped");
            } else throw e;
        }
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllUsersReturnListOfExistingUsers() {
        all.forEach(x -> System.out.println(x));
        assertEquals(all.size(), 6);
    }

    @DisplayName("Get")
    @Test
    void whenGetUsersEqualsOneOfGetAllUsers() throws RepositoryException {
        UserEntity userEntity = all.get(1);
        assertNotNull(userEntity);
        assertNotNull(userEntity.getIdUser());
        UserEntity selected = dao.get(userEntity.getIdUser()).get();
        assertEquals(selected, userEntity);
    }

    @DisplayName("Get empty")
    @Test
    void whenGetUsersReturnEmpty() throws RepositoryException {
        assertEquals(dao.get(100l), Optional.empty());
    }

    @DisplayName("getByName")
    @Test
    void whenGetByNameUsersEqualsOneOfGetAllUsers() throws RepositoryException {
        UserEntity userEntity = all.get(1);
        assertNotNull(userEntity);
        assertNotNull(userEntity.getUsername());
        UserEntity selected = dao.getByName(userEntity.getUsername()).get();
        assertEquals(selected, userEntity);
    }

    @DisplayName("getByName empty")
    @Test
    void whenGeByNametUsersReturnEmpty() throws RepositoryException {
        assertEquals(dao.getByName("NoName"), Optional.empty());
    }

    @DisplayName("Delete")
    @Test
    void whenDeletedUserShouldDecreaseList() throws RepositoryException {
        assertTrue(dao.delete(all.get(0).getIdUser()));
        List<UserEntity> allAfter = dao.getAll();
        assertEquals(allAfter.size(), all.size() - 1);
    }

    @DisplayName("Delete false")
    @Test
    void whenDeletedUserShouldNotDecreaseListAndNotAffectExistingUserList() throws RepositoryException {

        assertFalse(dao.delete(100l));
    }

    @DisplayName("Update")
    @Test
    void whenUpdatedUserShouldNotIncreaseDecreaseListAndAffectOnUpdatedUser() throws RepositoryException {
        UserEntity userEntity = UserEntity.builder()
                .setIdUser(all.get(1).getIdUser())
                .setUsername("TestUpdate")
                .setPassword(all.get(1).getPassword()) //12345678
                .setAdmin(all.get(1).isAdmin())
                .setBan(all.get(1).isBan())
                .setLocale(all.get(1).getLocale())
                .build();

        UserEntity updated = dao.update(userEntity).get();
        List<UserEntity> allAfter = dao.getAll();
        assertNotEquals(updated, all.get(1));
        assertEquals(updated, userEntity);
        assertEquals(allAfter.size(), all.size());
    }

    @DisplayName("updateUsersFields")
    @Test
    void whenUpdatedUserFieldsShouldNotIncreaseDecreaseListAndAffectOnUpdatedUser() throws RepositoryException {
        UserEntity userEntity = UserEntity.builder()
                .setIdUser(all.get(1).getIdUser())
                .setUsername("TestUpdate")
                .setPassword("12345678") //12345678
                .setAdmin(all.get(1).isAdmin())
                .setBan(all.get(1).isBan())
                .setLocale(Localization.getNameByLocale(Localization.EN.getLocale()))
                .build();

        UserEntity updated = dao.updateUsersFields(userEntity).get();
        List<UserEntity> allAfter = dao.getAll();
        assertNotEquals(updated, all.get(1));
        assertEquals(updated, userEntity);
        assertEquals(allAfter.size(), all.size());
    }

    @DisplayName("updateAdminFields")
    @Test
    void whenUpdatedAdminFieldsShouldNotIncreaseDecreaseListAndAffectOnUpdatedUser() throws RepositoryException {
        UserEntity userEntity = UserEntity.builder()
                .setIdUser(all.get(1).getIdUser())
                .setUsername(all.get(1).getUsername())
                .setPassword(all.get(1).getPassword()) //12345678
                .setAdmin(!all.get(1).isAdmin())
                .setBan(!all.get(1).isBan())
                .setLocale(Localization.getNameByLocale(Localization.EN.getLocale()))
                .build();

        UserEntity updated = dao.updateAdminsFields(userEntity).get();
        List<UserEntity> allAfter = dao.getAll();
        assertNotEquals(updated, all.get(1));
        assertEquals(updated, userEntity);
        assertEquals(allAfter.size(), all.size());
    }

    @DisplayName("Create")
    @Test
    void addingUserShouldIncreaseListAndNotAffectExistingUser() throws RepositoryException {
        UserEntity created = dao.create(existingUserEntity);
        assertNotNull(created);
        assertNotNull(created.getIdUser());
        existingUserEntity.setIdUser(created.getIdUser());
        assertEquals(existingUserEntity, created);
        List<UserEntity> allAfter = dao.getAll();
        assertEquals(allAfter.size() - 1, all.size());
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageSubjectsReturnListOfExistingSubjects() throws RepositoryException, SQLException {
        Connection mockConnection = mock(Connection.class);
        UserDao userDao = new UserDaoImpl(mockConnection);

        System.out.println(setPage.getSortColumn());
        String queryDerby = "SELECT * FROM users ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        //  String query = "SELECT * FROM users ORDER BY " + setPage.getSortColumn() + " " + setPage.getSort() + " LIMIT ? OFFSET ?";
        String query = "SELECT * FROM users ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " LIMIT ? OFFSET ?";
        System.out.println(queryDerby);
        when(mockConnection.prepareStatement(query))
                .thenReturn(con.prepareStatement(queryDerby));
        for (int i = 1; i <= 14; i++) {
            existingUserEntity.setUsername("TestUser" + i);
            dao.create(existingUserEntity);
        }
        dao.getAll().stream().forEach(x -> System.out.println(x));
        System.out.println("-next-");
        List<UserEntity> allSort = dao.getAll().stream()
                .sorted(Comparator.comparing(UserEntity::getUsername))
                .collect(Collectors.toList());
        allSort.forEach(x -> System.out.println(x));

        List<UserEntity> testPage = new ArrayList<>();
        System.out.println("---");
        testPage.addAll(allSort.subList(setPage.getStartRows(), setPage.getStartRows() + setPage.getRecordsPerPage()));
        testPage.forEach(x -> System.out.println(x));
        System.out.println("-get page-");
        List<UserEntity> page = userDao.getPage(setPage);
        page.forEach(x -> System.out.println(x));

        assertEquals(page.size(), setPage.getRecordsPerPage());
        assertEquals(testPage, page);
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsUsersReturnNumberOfRowsOfExistingUsers() throws RepositoryException {
        Integer rows = dao.getNumberOfRows();
        System.out.println(rows);
        assertEquals(all.size(), rows);
    }

    @DisplayName("getNumberOfRows empty")
    @Test
    void whenGetNumberOfRowsUsersReturnNumberOfRowsOfExistingUsersInEmptyTable() throws RepositoryException, SQLException {
        con.createStatement().executeUpdate("TRUNCATE TABLE users");
        Integer rows = dao.getNumberOfRows();
        System.out.println(rows);
        assertEquals(dao.getAll().size(), rows);
        assertEquals(0, rows);
    }

    @DisplayName("Create Exeption")
    @Test
    void whenCreateUsersReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.create(existingUserEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_CREATE_NEW_USER.getMessage()));
    }

    @DisplayName("Get Exeption")
    @Test
    void whenGetUsersReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.get(1l);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_USER_BY_ID.getMessage()));
    }

    @DisplayName("getByName Exeption")
    @Test
    void whenGetByNameUsersReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getByName(all.get(1).getUsername());
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_USER_BY_NAME.getMessage()));
    }

    @DisplayName("Update Exeption")
    @Test
    void whenUpdateUsersReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.update(existingUserEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_UPDATE_USER.getMessage()));
    }

    @DisplayName("updateAdminsFields Exeption")
    @Test
    void whenUpdateAdminFieldsUsersReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.updateAdminsFields(existingUserEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_UPDATE_ADMIN_FIELDS.getMessage()));
    }

    @DisplayName("updateUsersFields Exeption")
    @Test
    void whenUpdateUserFieldsUsersReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.updateUsersFields(existingUserEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_UPDATE_USER_FIELDS.getMessage()));
    }

    @DisplayName("Delete Exeption")
    @Test
    void whenDeleteUsersReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.delete(1l);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_DELETE_USER.getMessage()));
    }

    @DisplayName("GetAll Exeption")
    @Test
    void whenGetAllUsersReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getAll();
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_ALL_USER.getMessage()));
    }

    @DisplayName("GetPage Exeption")
    @Test
    void whenGetPageReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getPage(setPage);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_PAGE_OF_USER.getMessage()));
    }

    @DisplayName("getNumberOfRows Exeption")
    @Test
    void whenGetNumberOfRowsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_TEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getNumberOfRows();
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_USER.getMessage()));
    }
}
