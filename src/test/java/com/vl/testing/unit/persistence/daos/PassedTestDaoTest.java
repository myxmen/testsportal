package com.vl.testing.unit.persistence.daos;

import com.vl.testing.controller.enums.DefaultColumn;
import com.vl.testing.controller.enums.Localization;
import com.vl.testing.entities.PassedTestEntity;
import com.vl.testing.entities.SubjectEntity;
import com.vl.testing.entities.TestEntity;
import com.vl.testing.entities.UserEntity;
import com.vl.testing.persistence.daos.PassedTestDao;
import com.vl.testing.persistence.daos.PassedTestDaoImpl;
import com.vl.testing.persistence.daos.TestDaoImpl;
import com.vl.testing.persistence.enums.ExceptionMessage;
import com.vl.testing.persistence.enums.Records;
import com.vl.testing.persistence.enums.Sort;
import com.vl.testing.persistence.exeptions.RepositoryException;
import com.vl.testing.persistence.mapper.impl.PassedTestMapperEntity;
import com.vl.testing.persistence.utils.SetPage;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PassedTestDaoTest {
    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";
    private static final String DERBY_LOG_FILE = "derby.log";
    private static final String ERROR_MESSAGE = "'DROP TABLE' cannot be performed on 'PASSED_TESTS' because it does not exist.";
    private static final String CREATE_PASSEDTEST_TABLE = "CREATE TABLE passed_tests (" +
            "    passed_test_id  BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
            "    passed_test_user_id BIGINT," +
            "    passed_test_test_id BIGINT," +
            "    result  INT" +
            ")";
    private static final String CREATE_TEST_TABLE = "CREATE TABLE tests ("
            + "   test_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
            + "   test_subject_id BIGINT,"
            + "   test       VARCHAR(255),"
            + "   difficulty INT,"
            + "   requests   BIGINT,"
            + "   time       BIGINT,"
            + "   CONSTRAINT UC_tests UNIQUE (test_subject_id,test)"
            + ")";

    private static final String INSERT_INTO_PASSEDTEST_TABLE = "INSERT INTO passed_tests VALUES"
            + "(DEFAULT, 2, 2, 15),"
            + "(DEFAULT, 2, 1, 10),"
            + "(DEFAULT, 3, 2, 75),"
            + "(DEFAULT, 4, 3, 87),"
            + "(DEFAULT, 5, 5, 55),"
            + "(DEFAULT, 2, 5, 43),"
            + "(DEFAULT, 2, 6, 12),"
            + "(DEFAULT, 2, 4, 97),"
            + "(DEFAULT, 2, 3, 4)";

    private static final String GET_ALL_PASSEDTEST_TABLE = "SELECT * FROM passed_tests";
    private static final String DROP_PASSEDTEST_TABLE = "DROP TABLE passed_tests";
    private static final String DROP_TEST_TABLE = "DROP TABLE tests";
    private static Connection con;
    private static PassedTestDao dao;
    private static UserEntity existingUserEntity;
    private static PassedTestEntity existingPassedTestEntity;
    private static List<PassedTestEntity> all;
    private static final Integer startRows = 5; // offset for Derby DB
    // private static final Integer recordsPerPage = Records.FIRST.getValue(); // limit for Derby DB
    private static final SetPage setPage = SetPage.builder()
            .setStartRows(startRows)// offset for Derby DB
            .setRecordsPerPage(Records.FIRST.getValue())// limit for Derby DB
            .setSort(Sort.ASC.getSort())
            .setSortColumn(DefaultColumn.TEST.getColumn())
            .build();

    @BeforeAll
    static void globalSetUp() throws SQLException {
        con = DriverManager.getConnection(CONNECTION_URL);
        dao = new PassedTestDaoImpl(con);
        DriverManager.drivers().forEach(x -> System.out.println(x));

    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
        System.out.println("Derby connection is close");

//        try {
//            DriverManager.getConnection(SHUTDOWN_URL);
//        } catch (SQLException ex) {
//            System.err.println("Derby shutdown");
//        }

        if (Files.exists(Path.of(DERBY_LOG_FILE))) {
            try {

                System.err.println("Derby can delete log File");
                Files.delete(Path.of(DERBY_LOG_FILE));
            } catch (IOException e) {
                System.err.println("Derby cannot delete log File");
            }
        }

        DriverManager.drivers().forEach(x -> System.out.println(x));
    }

    @BeforeEach
    void setUp() throws SQLException, RepositoryException {
        con.createStatement().executeUpdate(CREATE_PASSEDTEST_TABLE);
        System.out.println("Table is created");
        con.createStatement().executeUpdate(INSERT_INTO_PASSEDTEST_TABLE);
        existingPassedTestEntity = PassedTestEntity.builder()
                .setIdPassedTest(null)
                .setUserId(2l)
                .setTestId(7l)
                .setResult(100)
                .build();
        existingUserEntity = UserEntity.builder()
                .setIdUser(2L)
                .setUsername("Test")
                .setPassword("$2a$12$j8DHSQIgt8sKHfuiDy22PuHJipcZmuRYcdfBwwWVu1TsfW1TIrOeG") //12345678
                .setAdmin(true)
                .setBan(false)
                //.setLocale(Localization.RU.name())
                .setLocale(Localization.getNameByLocale(Localization.RU.getLocale()))
                .build();
        all = getAllPassedTests();
        // System.out.println(setPage);
    }

    @AfterEach
    void tearDown() throws SQLException {
        try {
            con.createStatement().executeUpdate(DROP_PASSEDTEST_TABLE);
            System.out.println("Table is dropped");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            if (e.getMessage().contains(ERROR_MESSAGE)) {
                System.err.println("Table was dropped");
            } else throw e;
        }
    }

    @DisplayName("GetAll")
    @Test
    void whenGetAllPassedTestForUserReturnLinkedHashMapOfExistingPassedTest() throws RepositoryException {
        all.forEach(x -> System.out.println(x));
        // existingUserEntity.setId(2l);
        // assertEquals(all.size(), dao.getAllPassedTestFosUser(existingUserEntity).size());
        assertEquals(all.size(), 9);
    }

    @DisplayName("GetAll Exeption")
    @Test
    void whenGetAllPassedTestForUserReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_PASSEDTEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getAllPassedTestFosUser(existingUserEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_ALL_PASSEDTEST.getMessage()));
    }

    @DisplayName("Update")
    @Test
    void whenUpdatedPassedTestShouldNotIncreaseDecreaseListAndAffectOnUpdatedPassedTest() throws RepositoryException, SQLException {
        PassedTestEntity testEntity = PassedTestEntity.builder()
                .setIdPassedTest(all.get(1).getIdPassedTest())
                .setUserId(all.get(1).getUserId())
                .setTestId(all.get(1).getTestId())
                .setResult(100)
                .build();

        PassedTestEntity updated = dao.update(testEntity).get();
        List<PassedTestEntity> allAfter = getAllPassedTests();
        assertNotEquals(updated, all.get(1));
        assertEquals(updated, testEntity);
        assertEquals(allAfter.size(), all.size());
    }

    @DisplayName("Update Exeption")
    @Test
    void whenUpdatePassedTestReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_PASSEDTEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.update(existingPassedTestEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_UPDATE_PASSEDTEST.getMessage()));
    }

    @DisplayName("Create")
    @Test
    void addingPassedTestShouldIncreaseListAndNotAffectExistingPassedTest() throws RepositoryException, SQLException {
        PassedTestEntity created = dao.create(existingPassedTestEntity);
        assertNotNull(created);
        assertNotNull(created.getIdPassedTest());
        existingPassedTestEntity.setIdPassedTest(created.getIdPassedTest());
        assertEquals(existingPassedTestEntity, created);
        List<PassedTestEntity> allAfter = getAllPassedTests();
        assertEquals(allAfter.size() - 1, all.size());
    }

    @DisplayName("Create Exeption")
    @Test
    void whenCreatePassedTestsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_PASSEDTEST_TABLE);
        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.create(existingPassedTestEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_CREATE_NEW_PASSEDTEST.getMessage()));
    }

    @DisplayName("getAllForUser")
    @Test
    void whenGetAllPassedTestFosUserReturnLinkedHashMapOfExistingTests() throws RepositoryException, SQLException {
        con.createStatement().executeUpdate(CREATE_TEST_TABLE);

        for (int i = 1; i <= 20; i++) {
            TestEntity entity = TestEntity.builder()
                    .setIdTest(null)
                    .setSubjectId(2l)
                    .setTest("TEST " + i)
                    .setDifficulty(6 + i)
                    .setRequests(99l + i * 2)
                    .setTime(45l + i * 5)
                    .build();
            new TestDaoImpl(con).create(entity);
        }
        List<TestEntity> tests = new TestDaoImpl(con).getAllBySubjectId(SubjectEntity.builder()
                .setIdSubject(2l)
                .setSubject("TestSubject")
                .build());
        tests.stream().forEach(x -> System.out.println(x));

        System.out.println("-all page-");
        LinkedHashMap<PassedTestEntity, TestEntity> allTests = new LinkedHashMap<>();
        List<PassedTestEntity> passedTestEntities = getAllPassedTests();
        for (PassedTestEntity entity : passedTestEntities) {
            for (TestEntity test : tests) {
                if (entity.getTestId() == test.getIdTest()) {
                    allTests.put(entity, test);
                }
            }
        }
        allTests.forEach((x, y) -> System.out.println(x + " " + y));

        System.out.println("-filter page-");
        LinkedHashMap<PassedTestEntity, TestEntity> filteredPage = allTests.entrySet().stream()
                .filter((x)->x.getKey().getUserId()==existingUserEntity.getIdUser())
                .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue(), (v1, v2) -> v2, LinkedHashMap::new));
        filteredPage.forEach((x, y) -> System.out.println(x + " " + y));

        System.out.println("-get page-");
        Map<PassedTestEntity, TestEntity> page = dao.getAllPassedTestFosUser(existingUserEntity);
        page.forEach((x, y) -> System.out.println(x + " " + y));

        assertEquals(page.size(), filteredPage.size());
        assertEquals(filteredPage, page);

        con.createStatement().executeUpdate(DROP_TEST_TABLE);
    }

    @DisplayName("getPage")
    @Test
    void whenGetPageTestsReturnLinkedHashMapOfExistingTests() throws RepositoryException, SQLException {

        Connection mockConnection = mock(Connection.class);
        PassedTestDao testDao = new PassedTestDaoImpl(mockConnection);
        con.createStatement().executeUpdate(CREATE_TEST_TABLE);

        for (int i = 1; i <= 20; i++) {
            TestEntity entity = TestEntity.builder()
                    .setIdTest(null)
                    .setSubjectId(2l)
                    .setTest("TEST " + i)
                    .setDifficulty(6 + i)
                    .setRequests(99l + i * 2)
                    .setTime(45l + i * 5)
                    .build();
            new TestDaoImpl(con).create(entity);
        }
        List<TestEntity> tests = new TestDaoImpl(con).getAllBySubjectId(SubjectEntity.builder()
                .setIdSubject(2l)
                .setSubject("TestSubject")
                .build());
        tests.stream().forEach(x -> System.out.println(x));

        System.out.println(setPage.getSortColumn());

//        String queryDerby = "SELECT * FROM passed_tests WHERE passed_tests.passed_test_user_id = ? ORDER BY "
//                + "passed_test_user_id "
//                + setPage.getSort()
//                + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        String queryDerby = "SELECT * FROM passed_tests INNER JOIN tests ON passed_tests.passed_test_test_id=tests.test_id"
                + " WHERE passed_tests.passed_test_user_id = ? ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        // "SELECT * FROM passed_tests INNER JOIN tests ON passed_tests.passed_test_test_id=tests.test_id" +
        //        " WHERE passed_tests.passed_test_user_id = ? ORDER BY " + setPage.getSortColumn() + " " + setPage.getSort() + " LIMIT ? OFFSET ?";
        String query = "SELECT * FROM passed_tests INNER JOIN tests ON passed_tests.passed_test_test_id=tests.test_id"
                + " WHERE passed_tests.passed_test_user_id = ? ORDER BY "
                + setPage.getSortColumn() + " "
                + setPage.getSort()
                + " LIMIT ? OFFSET ?";
        System.out.println(queryDerby);
        when(mockConnection.prepareStatement(query))
                .thenReturn(con.prepareStatement(queryDerby));

//        for (int i = 0; i <= all.size(); i++) {
//            PassedTestEntity entity = all.get(i);
//            entity.setUserId(2l);
//            dao.update(entity);
//        }
        con.createStatement().executeUpdate("TRUNCATE TABLE passed_tests");
//        for (PassedTestEntity entity : all) {
//            //  PassedTestEntity entity = all.get(i);
//            entity.setUserId(2l);
//            entity.setTestId(5l);
//            //   System.out.println(entity);
//            dao.create(entity);
//        }
//
//        dao.create(existingPassedTestEntity);
        System.out.println("-v-");
        getAllPassedTests().stream().forEach(x -> System.out.println(x));
        System.out.println("-v-");
        for (int i = 1; i <= 20; i++) {
            PassedTestEntity passedTestEntity = PassedTestEntity.builder()
                    .setIdPassedTest(null)
                    .setUserId(2l)
                    .setTestId((long) i)
                    .setResult(100 - (i * 5))
                    .build();
            dao.create(passedTestEntity);
        }
        LinkedHashMap<PassedTestEntity, TestEntity> allTests = new LinkedHashMap<>();
        List<PassedTestEntity> passedTestEntities = getAllPassedTests();
        for (PassedTestEntity entity : passedTestEntities) {
            for (TestEntity test : tests) {
                if (entity.getTestId() == test.getIdTest()) {
                    allTests.put(entity, test);
                }
            }
        }
        System.out.println("-all page-");
        allTests.forEach((x, y) -> System.out.println(x + " " + y));
        LinkedHashMap<PassedTestEntity, TestEntity> sortPage = allTests.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.comparing(TestEntity::getTest)))
                .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue(), (v1, v2) -> v2, LinkedHashMap::new));
        System.out.println("-sort page-");
        sortPage.forEach((x, y) -> System.out.println(x + " " + y));

        LinkedHashMap<PassedTestEntity, TestEntity> testPage = new LinkedHashMap<>();
        List<PassedTestEntity> keys = new ArrayList<PassedTestEntity>(sortPage.keySet());
        List<PassedTestEntity> keysPage = new ArrayList<>();
        keysPage.addAll(keys.subList(setPage.getStartRows(), setPage.getStartRows() + setPage.getRecordsPerPage()));
        System.out.println("<-sort->");
        keysPage.forEach(x -> System.out.println(x));
        System.out.println("<--->");
        for (PassedTestEntity entity : keysPage) {
            testPage.put(entity, sortPage.get(entity));
        }
        testPage.forEach((x, y) -> System.out.println(x + " " + y));
//-------------
//        List<PassedTestEntity> allSort = getAllPassedTests().stream()
//                .sorted(Comparator.comparing(PassedTestEntity::getTestId))
//                .collect(Collectors.toList());
//        allSort.forEach(x -> System.out.println(x));
//
//        List<PassedTestEntity> testPage = new ArrayList<>();
//        System.out.println("---");
//        testPage.addAll(allSort.subList(setPage.getStartRows(), setPage.getStartRows() + setPage.getRecordsPerPage()));
//        testPage.forEach(x -> System.out.println(x));
//--------------
        System.out.println("-get table-");
        getAllPassedTests().stream().forEach(x -> System.out.println(x));
        Map<PassedTestEntity, TestEntity> page = testDao.getPage(existingUserEntity, setPage);
        System.out.println("-get page-");
        page.forEach((x, y) -> System.out.println(x + " " + y));
//
        assertEquals(page.size(), setPage.getRecordsPerPage());
        assertEquals(testPage, page);
        con.createStatement().executeUpdate(DROP_TEST_TABLE);
    }

    @DisplayName("GetPage Exeption")
    @Test
    void whenGetPageReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_PASSEDTEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getPage(existingUserEntity, setPage);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_PAGE_OF_PASSEDTEST.getMessage()));
    }

    @DisplayName("getNumberOfRows empty")
    @Test
    void whenGetNumberOfRowsPassedTestsReturnNumberOfRowsOfExistingPassedTestsInEmptyTable() throws RepositoryException, SQLException {
        con.createStatement().executeUpdate("TRUNCATE TABLE passed_tests");
        Integer rows = dao.getNumberOfRows(existingUserEntity);
        System.out.println(rows);
        assertEquals(getAllPassedTests().size(), rows);
        assertEquals(0, rows);
    }

    @DisplayName("getNumberOfRows")
    @Test
    void whenGetNumberOfRowsPassedTestsReturnNumberOfRowsOfExistingPassedTests() throws RepositoryException {
        Integer rows = dao.getNumberOfRows(existingUserEntity);
        System.out.println(rows);
        long count = all.stream()
                .filter(x -> x.getUserId() == existingUserEntity.getIdUser())
                .count();
        System.out.println(count);
        assertEquals(count, (long) rows);
    }

    @DisplayName("getNumberOfRows Exeption")
    @Test
    void whenGetNumberOfRowsReturnError() throws SQLException {
        con.createStatement().executeUpdate(DROP_PASSEDTEST_TABLE);

        RepositoryException thrown = assertThrows(RepositoryException.class, () -> {
            dao.getNumberOfRows(existingUserEntity);
        });
        assertTrue(thrown.getMessage().contains(ExceptionMessage.FAILED_TO_GET_NUMBER_OF_ROWS_ALL_PASSEDTEST.getMessage()));
    }

    private List<PassedTestEntity> getAllPassedTests() throws SQLException {
        final List<PassedTestEntity> all = new ArrayList<>();
        try (PreparedStatement statement = con.prepareStatement(GET_ALL_PASSEDTEST_TABLE)) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                all.add(new PassedTestMapperEntity().fillEntityWithValues(resultSet));
            }
            return all;
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            throw e;
        }
    }
}
