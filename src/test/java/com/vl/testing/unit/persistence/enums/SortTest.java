package com.vl.testing.unit.persistence.enums;

import com.vl.testing.persistence.enums.Sort;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SortTest {

    @DisplayName("getSortByName")
    @Test
    void whenGetSortByNameReturnSort() {
        assertEquals(Sort.getSortByName("Desc"), Sort.DESC.getSort());
    }

    @DisplayName("getSortByName empty name")
    @Test
    void whenGetSortByNameReturnDefaultSort() {
        assertEquals(Sort.getSortByName("Empty_Sort"), Sort.ASC.getSort());
    }

    @DisplayName("getSort")
    @Test
    void whenGetSortReturnSort() {
        assertEquals(Sort.ASC.getSort(), "ASC");
    }
}
