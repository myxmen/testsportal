package com.vl.testing.unit.persistence.daofactory;

import com.vl.testing.persistence.DaoFactory;
import com.vl.testing.persistence.daos.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.assertSame;

public class JDBCDaoFactoryTest {
    //  private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";
    //  private static final String APP_PROPS_FILE = "dbconfig.properties";
    private static final String APP_PROPS_FILE = "src/main/resources/dbconfig.properties";
    private static final String APP_CONTENT = "DRIVER_CLASS=\n" +
            "URL=jdbc:derby:memory:testDB;create=true\n" +
            "USERNAME=\n" +
            "PASSWORD=\n" +
            "MAXIMUM_POOL_SIZE=20\n" +
            "IDLE_TIMEOUT=300000\n" +
            "MINIMUM_IDLE=5\n" +
            "MAXIMUM_IDLE=10\n" +
            "MAX_OPEN_PREPARED_STATEMENT=100\n" +
            "CONNECTION_TIMEOUT=20000";
    private static final String DERBY_LOG_FILE = "derby.log";
    private static String userDefinedAppContent;
    private static DaoFactory daoFactory;

    @Mock
    final Connection connection = null;

    @BeforeAll
    static void globalSetUp() throws IOException {
        userDefinedAppContent = Files.readString(Path.of(APP_PROPS_FILE));
        System.out.println(userDefinedAppContent);
        Files.write(Path.of(APP_PROPS_FILE), APP_CONTENT.getBytes());
        System.out.println("\nReWrite END\n");
        System.out.println(Files.readString(Path.of(APP_PROPS_FILE)));

        daoFactory = DaoFactory.getInstance();

    }

    @AfterAll
    static void globalTearDown() throws IOException {
//        try {
//            DriverManager.drivers().forEach(x -> System.out.println(x));
//            DriverManager.getConnection(SHUTDOWN_URL);
//        } catch (SQLException ex) {
//            System.err.println("\nDerby shutdown\n");
//            DriverManager.drivers().forEach(x -> System.out.println(x));
//        }
      //  Files.delete(Path.of(DERBY_LOG_FILE));
       // System.err.println("\nDerby`s log file was deleted\n");
        try (PrintWriter out = new PrintWriter(APP_PROPS_FILE)) {
            out.print(userDefinedAppContent);
        }
        System.out.println(Files.readString(Path.of(APP_PROPS_FILE)));
    }

    @Test
    void whenDaoFactoryCreateUserDaoReturnedUserDaoImpl() throws IOException {
        UserDao dao = daoFactory.createUserDao();
        assertSame(dao.getClass(), new UserDaoImpl(connection).getClass());
        dao.close();
    }

    @Test
    void whenDaoFactoryCreatePassedTestDaoReturnedPassedTestDaoDaoImpl() {
        PassedTestDao dao = daoFactory.createPassedTestDao();
        assertSame(dao.getClass(), new PassedTestDaoImpl(connection).getClass());
        dao.close();
    }

    @Test
    void whenDaoFactoryCreateSubjectDaoReturnedSubjectDaoImpl() {
        SubjectDao dao = daoFactory.createSubjectDao();
        assertSame(dao.getClass(), new SubjectDaoImpl(connection).getClass());
        dao.close();
    }

    @Test
    void whenDaoFactoryCreateTestDaoReturnedTestDaoImpl() {
        TestDao dao = daoFactory.createTestDao();
        assertSame(dao.getClass(), new TestDaoImpl(connection).getClass());
        dao.close();
    }

    @Test
    void whenDaoFactoryCreateQuestionDaoReturnedQuestionDaoImpl() {
        QuestionDao dao = daoFactory.createQuestionDao();
        assertSame(dao.getClass(), new QuestionDaoImpl(connection).getClass());
        dao.close();
    }
}
