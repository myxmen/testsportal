--subjects
INSERT INTO subjects
VALUES (DEFAULT, 'subject1');
INSERT INTO subjects
VALUES (DEFAULT, 'subject2');
INSERT INTO subjects
VALUES (DEFAULT, 'subject3');
INSERT INTO subjects
VALUES (DEFAULT, 'subject4');
INSERT INTO subjects
VALUES (DEFAULT, 'subject5');
INSERT INTO subjects
VALUES (DEFAULT, 'subject6');
INSERT INTO subjects
VALUES (DEFAULT, 'subject14');
INSERT INTO subjects
VALUES (DEFAULT, 'subject15');
INSERT INTO subjects
VALUES (DEFAULT, 'subject16');
INSERT INTO subjects
VALUES (DEFAULT, 'subject17');
INSERT INTO subjects
VALUES (DEFAULT, 'subject18');
INSERT INTO subjects
VALUES (DEFAULT, 'subject19');
INSERT INTO subjects
VALUES (DEFAULT, 'subject20');
INSERT INTO subjects
VALUES (DEFAULT, 'subject21');
INSERT INTO subjects
VALUES (DEFAULT, 'subject7');
INSERT INTO subjects
VALUES (DEFAULT, 'subject8');
INSERT INTO subjects
VALUES (DEFAULT, 'subject9');
INSERT INTO subjects
VALUES (DEFAULT, 'subject10');
INSERT INTO subjects
VALUES (DEFAULT, 'subject11');
INSERT INTO subjects
VALUES (DEFAULT, 'subject12');
INSERT INTO subjects
VALUES (DEFAULT, 'subject13');

--users
INSERT INTO users
VALUES (DEFAULT, 'ivanov1', '$2a$12$ef2VOvhsF.qEwjdiq93cq.RXB6tNENtoL3EQXlXaIZuv0XDMMgCtm', false, true, 'EN');
--4321
INSERT INTO users
VALUES (DEFAULT, 'User', '$2a$12$wIDnrcx3gw6bfjgc/fgqM.JHN.TCiiYZtIC2WUatsvURNE40Q07vO', false, false, 'RU');
--1
INSERT INTO users
VALUES (DEFAULT, 'Admin', '$2a$12$OC6eAUnO90xNuhDlv/iHS.bLe5LR83gLNzmbbPbGt/QNCiK.CKa9C', true, false, 'EN');
--1234
INSERT INTO users
VALUES (DEFAULT, 'ivanov4', '$2a$12$wpLOBmJnZ3T5J1lZy.hypepo4vwQHBo3PZkH.27YGRQiMJInUotTm', false, true, 'EN');
--4321
INSERT INTO users
VALUES (DEFAULT, 'ivanov5', '$2a$12$MdKnhwDjlMffuAtlwG91WeHSd2Qy2tf8e/GP9famGOenG138nOtii', false, false, 'RU');
--4321
INSERT INTO users
VALUES (DEFAULT, 'Petrov', '$2a$12$OC6eAUnO90xNuhDlv/iHS.bLe5LR83gLNzmbbPbGt/QNCiK.CKa9C', false, false, 'RU');
--1234

--tests
INSERT INTO tests
VALUES (DEFAULT, 1, 'test1', 8, 999, 10);
INSERT INTO tests
VALUES (DEFAULT, 2, 'test2', 6, 109, 1);
INSERT INTO tests
VALUES (DEFAULT, 2, 'test3', 5, 100, 2);
INSERT INTO tests
VALUES (DEFAULT, 5, 'test4', 3, 90, 5);
INSERT INTO tests
VALUES (DEFAULT, 4, 'test5', 2, 99, 4);
INSERT INTO tests
VALUES (DEFAULT, 6, 'test11', 10, 98, 2);
INSERT INTO tests
VALUES (DEFAULT, 7, 'test12', 9, 910, 15);
INSERT INTO tests
VALUES (DEFAULT, 8, 'test13', 5, 1, 60);
INSERT INTO tests
VALUES (DEFAULT, 9, 'test14', 1, 0, 90);
INSERT INTO tests
VALUES (DEFAULT, 13, 'test15', 8, 2, 180);
INSERT INTO tests
VALUES (DEFAULT, 12, 'test6', 2, 14, 5);
INSERT INTO tests
VALUES (DEFAULT, 11, 'test7', 7, 18, 20);
INSERT INTO tests
VALUES (DEFAULT, 10, 'test8', 3, 101, 30);
INSERT INTO tests
VALUES (DEFAULT, 15, 'test9', 5, 11, 40);
INSERT INTO tests
VALUES (DEFAULT, 14, 'test10', 9, 18, 15);

--questions
INSERT INTO questions
VALUES (DEFAULT, 1, 1, 'question1', 'answer1', 'answer2', 'answer3', 'answer4', 'answer5', false, true, true, true,
        false);
INSERT INTO questions
VALUES (DEFAULT, 2, 2, 'question2', 'answer1', 'answer2', null, null, null, false, true, null, null, null);
INSERT INTO questions
VALUES (DEFAULT, 4, 1, 'question2', 'answer2', 'answer1', null, null, null, false, true, null, null, null);
INSERT INTO questions
VALUES (DEFAULT, 5, 1, 'question1', 'answer1', 'answer2', 'answer3', null, null, true, false, true, null, null);
INSERT INTO questions
VALUES (DEFAULT, 2, 1, 'question1', 'answer1', 'answer2', 'answer3', 'answer4', 'answer5', false, true, true, true,
        false);
INSERT INTO questions
VALUES (DEFAULT, 3, 1, 'question1', 'answer1', 'answer2', 'answer3', 'answer4', null, false, true, false, true,
        null);

--passed_tests
INSERT INTO passed_tests
VALUES (DEFAULT, 2, 2, 15);
INSERT INTO passed_tests
VALUES (DEFAULT, 2, 1, 10);
INSERT INTO passed_tests
VALUES (DEFAULT, 3, 2, 75);
INSERT INTO passed_tests
VALUES (DEFAULT, 4, 3, 87);
INSERT INTO passed_tests
VALUES (DEFAULT, 5, 5, 55);
INSERT INTO passed_tests
VALUES (DEFAULT, 2, 5, 43);
INSERT INTO passed_tests
VALUES (DEFAULT, 2, 6, 12);
INSERT INTO passed_tests
VALUES (DEFAULT, 2, 4, 97);
INSERT INTO passed_tests
VALUES (DEFAULT, 2, 3, 4);

--get 100 users sorted by increase
--SELECT * FROM users ORDER BY id ASC LIMIT 100

